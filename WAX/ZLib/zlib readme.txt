Last modified: 2009/01/29

With the development of the NF Wan Acceleration module the zippo.dll component used by LBM becomes obsolete.

The zlib include files were moved from:
${src.dir}/com/neverfail/helper/zippo/native/
to
${src.dir}/com/neverfail/nfcompress/native/WAX/ZLib
The files kept are those under 
	dll - (pre built dll's for win32 and x64 platforms) 
	include - include files for zlib
	lib - library files for win32 and x64 platforms

The version of zlib remains 1.2.3 - latest version at this time.
	
Modified: 2006/12/19

The version of zlib has been changed when the zippo project was converted to support x64 platforms.
The old version of zlib we were using, 1.2.1, has been replaced with the new 1.2.3 version.
The change to support 64bit platforms required some changes to the project and a new version of zlib
was needed that was available for both Win32 and x64 platforms.
The official site http://www.zlib.net/ only provides a Win32 version of the zlib library, named zlib1.dll.
This library uses the CDECL calling convention for all its functions and this was the library used by
Zippo before the change to support x64 platforms. Because the official site doesn't provide a 64bit
version of the library we had to look for another build of zlib that has both Win32 and x64 builds.
This site http://www.winimage.com/zLibDll/ provides both Win32 and x64 builds of zlib; this site seems
to specialize in Windows builds of zlib. The library provided by this site had a different name,
zlibwapi instead of zlib1 (this was done to prevent incompatibilities with other builds of zlib for Windows).
Because the name of the library is different we had to change the project to use the new name (zlibwapi) instead of the old one.
The binaries provided by http://www.winimage.com/zLibDll/ also use the WINAPI calling convention (as opposed to the
CDECL calling convention that the binaries from http://www.zlib.net/ use) so we had to change the calling convention used
in the zippo project - this requires that the preprocessor definition ZLIB_WINAPI be defined before including zlib.h and zconf.h. 