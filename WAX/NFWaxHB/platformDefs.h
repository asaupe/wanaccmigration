#pragma once

#include <sys/timeb.h>

// BEGIN WINDOWS
/*#ifdef _WIN32

// Include Windows headers
#include <windows.h>
#include <winbase.h>
#include <tchar.h>
#include <strsafe.h>

// Keep definition of WAX_RESULT from Windows code 
typedef ULONG WAX_RESULT;

#endif*/
// END WINDOWS

#include <cstdint>

// BEGIN LINUX
#ifdef __linux__

#include <time.h>

#endif
// END LINUX

// Define WAX_RESULT as C++ type
typedef unsigned int WAX_RESULT;

// Remove SAL attributes (Microsoft specific)
#define _In_
#define _Out_
#define _Inout_
#define __bcount(inCount)

// Define all windows specific types from C++ standard types
typedef int BOOL;
#define FALSE 0
#define TRUE  1

typedef unsigned char BYTE;
typedef unsigned char byte;
typedef wchar_t WCHAR;

//typedef unsigned short USHORT;
typedef std::uint16_t USHORT;
//typedef unsigned short WORD;
typedef std::uint16_t WORD;
//typedef int LONG;
typedef std::int32_t LONG;
//typedef unsigned int UINT;
typedef std::uint32_t UINT;
//typedef unsigned int UINT32;
typedef std::uint32_t UINT32;
//typedef unsigned long int ULONG;
typedef std::uint32_t ULONG;
//typedef unsigned long DWORD;
typedef std::uint32_t DWORD;
//typedef long long int LONGLONG;
typedef std::int64_t LONGLONG;
//typedef unsigned long long int ULONGLONG;
typedef std::uint64_t ULONGLONG;
//typedef unsigned long long int UINT64;
typedef std::uint64_t UINT64;

//typedef unsigned long long int ULONG_PTR;
typedef std::uint64_t ULONG_PTR;
//typedef unsigned long int UINT_PTR;
typedef std::uint64_t UINT_PTR;

typedef unsigned char* LPBYTE;
typedef wchar_t* LPWSTR;
typedef const wchar_t* LPCWSTR;
typedef void* LPVOID;
typedef void* HANDLE;

// Emulate Windows file time in a manner that's consistent with existing code 
// (instead of DWORD low, high struct use a 65-bit unsinged integer so we may perform direct arithmetic to obtain time differences in 100 nanoseconds)
typedef ULONGLONG FILETIME;

//typedef timeb _timeb;

//TODO check them out
typedef int SYSTEMTIME;

typedef ULONG_PTR SIZE_T, *PSIZE_T;

