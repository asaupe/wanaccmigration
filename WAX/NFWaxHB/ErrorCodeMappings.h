//********************************************************/
//  ./ErrorCodeMappings.h
//
//  Owner : Attila Vajda
// 
//	Company		: Neverfail 
//	PROJECT		: WAN Acceleration 
//  Component	: Error Handling 
//
//  $Rev: 1568 $ - Revision of last commit
//
/********************************************************
//
// Last Change - Author and Date of last change:
//  $Author: cmaris $  
//  $Date: 2009-01-22 12:07:26 +0200 (Thu, 22 Jan 2009) $
//
//********************************************************/
#pragma once

#include <WAXTypes.h>
#include <map>

//! WAX  Error code mapping - code to string.
//  it will be used in NFWaxHB in order to pass a more 
//  meaningful error event messages
//  Bug#: 7589
class ErrorCodeMappings
{
public:
	ErrorCodeMappings();
	~ErrorCodeMappings();

	//! Returns error string for a given error code
	const wchar_t* getErrorString(
		/*__in*/ const unsigned long inulErrorCode
		);

private:
	//! Link an error code to a given error string
	void linkIt( 
		/*__in*/ const WAX_RESULT_TYPES inWAXErrorCode,
		/*__in*/ const wchar_t*         inszErrorString
		);

	//! Generate the mapping between error codes and a error string.
	void generateMappings(
		);

	typedef std::map< WAX_RESULT_TYPES, const wchar_t*> MAP_ERRORSTRING;
	MAP_ERRORSTRING m_mapedErrorString; //!< map of code to string
};

