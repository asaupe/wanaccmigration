//********************************************************/
//  ./ipc.h
//
//  Owner : Ciprian Maris 
// 
//	Company		: Neverfail 
//	PROJECT		: WAN Acceleration 
//  Component	: IPC
//
//  $Rev: 2616 $ - Revision of last commit
//
/********************************************************
//
// Last Change - Author and Date of last change:
// $Author: cmaris $  
// $Date: 2009-06-10 16:16:14 +0100 (Wed, 10 Jun 2009) $
//
//********************************************************/

//********************************************************/
//	ipc.h is the include file common for all WAX components
//	that need IPC communication.
//
//	It is intended to be used only by the JNI interface and WAX Service
//	for declarations that pertain to IPC:
//		- names of memory mappings and events
//		- shared memory size and offsets
//		- types shared by components involved in IPC
//
//********************************************************/

//********************************************************/
//
//	MSDN Reference used for implementing IPC.
//
//	Shared Memory:
//		http://msdn.microsoft.com/en-us/library/aa366551(VS.85).aspx

//	Signal Object and Wait
//		http://msdn.microsoft.com/en-us/library/ms686293(VS.85).aspx

//	Create-Set Event
//		http://msdn.microsoft.com/en-us/library/ms682396.aspx
//		http://msdn.microsoft.com/en-us/library/ms686915(VS.85).aspx

//	END MSDN Reference
//********************************************************/

#pragma once

#include <windows.h>
#include "WAXTypes.h"
#include "WaxSimpleMutexSignaler.h"

#pragma warning(disable:4200)


//Definitions
#define SECOND 1000
#define MINUTE (SECOND * 60)

//	TIMEOUTS for IPC calls
#ifdef _DEBUG
//These are all set in DEBUG mode to infinite.
#define TIMEOUT_IPC_SHORT INFINITE	//used for initialize/exit out of process calls 
#define TIMEOUT_IPC_MEDIUM INFINITE	//used for encode-decode out of process calls 
#define TIMEOUT_IPC_LONG INFINITE	//used for allocate/release and stop/start out of process calls 
#else
#define TIMEOUT_IPC_SHORT	500				//used for initialize/exit out of process calls 
#define TIMEOUT_IPC_MEDIUM	(10 * SECOND)	//used for encode-decode out of process calls. BZ 8237 - extended from 5 to 10 seconds
#define TIMEOUT_IPC_LONG	MINUTE			//used for allocate/release and stop/start out of process calls 
#endif

//!Name of the shared memory mapping
#define IPC_STR_MEMORY_MAPPING_NAME L"Global\\NFIPCFileMapping"
#define IPC_STR_SIGNALER_NAME L"Global\\NFIPCSignalName"

//!Names for the call and return events
#define IPC_STR_CALL_EVENT_NAME		L"Global\\NFCallProviderMethod"
#define IPC_STR_RETURN_EVENT_NAME	L"Global\\NFProviderMethodReturn"

//! IPC operation type codes - these codes are passed from Client to Server
//	as the OperationType member of the IPCParameters structure.
enum OperationType:unsigned long
{
	OpInititialize = 0,
	OpExit,

	OpAllocate,
	OpRelease,
	
	OpStart,
	OpStop,

	OpEncode,
	OpDecode,

	OpQueryStatistics,

	OpSetDebugModeOn,
	OpSetDebugModeOff
};

//!	Common base structure for all parameters passed between processes.
//	It only has a single member, the operation(call) type.
//	The derived structures will contain the parameter sets for speciffic functions.
typedef struct _IPCParamteters
{
	OperationType operation;
}IPCParamteters;

#pragma pack(1)
typedef struct _InitializeParameters: public IPCParamteters
{
	bool   bDebugMode;

	//Character string length and value for log file. 
	unsigned long ulStringLength;
	wchar_t szString[];

}InitializeParameters;
#pragma pack()

#pragma pack(1)
typedef struct _AllocateParameters: public IPCParamteters
{
	unsigned long ulMinimumMemoryBufferSize;// Minimum memory to allocate for the compressor in MB's
	unsigned long ulMaximumMemoryBufferSize;// Maximum memory to allocate for the compressor in MB's
	unsigned long ulTransferLogSize;		// On disk transfer Log Size in MB's
	
	//Character string length and value for log file = On disk transfer Log File path (complete path)
	unsigned long ulStringLength;
	wchar_t szString[];
} AllocateParameters;

#pragma pack(1)
typedef struct _StartParameters: public IPCParamteters
{
	bool  bAsEncoder;					//!< Server role (compressor/decompressor)
	unsigned long ulSignatureBase;				//!< signature base
	unsigned long ulSignatureDistance;			//!< signature distance
	unsigned long ulMinRPRPatternsLength;		//!< minimum length of ZERO/DA pattern replaced by a RPR token
	unsigned long ulMinDeDuplicateLength;		//!< minimum length of the buffer to be considered for DD
	unsigned long ulMinDDLengthReplacedByToken;	//!< minimum length of data replaced by a DD token
	bool  bEncodeOutOfBoundaryEnabled ;	//!< advanced encode for out of boundary entries
	double dNInvariant;					//!< Factor for recent data volume and compression computation.
} StartParameters;

#pragma pack(1)
typedef struct _EncodeParameters: public IPCParamteters
{
	bool removeRepeatedPatterns;	//!< FLAG - RPR requested by caller
    bool deDuplicate;				//!< FLAG - DD requested by caller

	//Length of the output buffer.
	unsigned long ulOutBufferLen;

	//serialized ByteBuffer - input.
	unsigned long ulInBufferLen;
	BYTE  lpbInBuffer[];

} EncodeParameters;

#pragma pack(1)
typedef struct _DecodeParameters: public IPCParamteters
{
	//Length of the output buffer.
	unsigned long ulOutBufferLen;
	bool  bRPREncoded;
	bool  bDDEncoded; 

	//serialized ByteBuffer - input.
	unsigned long ulInBufferLen;
	BYTE  lpbInBuffer[];

} DecodeParameters;

//! The ReturnResults defines the data type returned by the IPC Server.
//	It will hold the WAXCompressor function call return code and the 
//	return buffer (used only in encode/decode) calls.
#pragma pack(1)
typedef struct _ReturnResults
{
	unsigned long ulReturnCode;//!< WAXCompressor function call return code 

	//serialized ByteBuffer - destination for output.
	unsigned long ulRetBufferLen;
	BYTE  lpbRetBuffer[];

}ReturnResults;
#pragma pack()

// Constants that depend on type sizes
#define SHARED_MEM_BUF_SIZE (2 * MEGA)
#define SHARED_MEM_RETURN_OFFSET (512 * KILO)
#define SHARED_MEM_STATS_OFFSET ( SHARED_MEM_RETURN_OFFSET + sizeof( ReturnResults ) )
#define SHARED_MEM_ERROR_MESSAGE_OFFSET (MEGA)


#pragma warning(default:4200)
