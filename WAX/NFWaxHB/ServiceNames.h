//********************************************************/
//  ./ServiceNames.h
//
//  Owner : Attila Vajda / Ciprian Maris
// 
//	Company		: Neverfail 
//	PROJECT		: WAN Acceleration 
//  Component	: WAXService - implementation of a Windows Service.
//
//  $Rev: 1483 $ - Revision of last commit
//
/********************************************************
//
// Last Change - Author and Date of last change:
// $Author: avajda $  
// $Date: 2009-01-14 12:20:20 +0200 (Wed, 14 Jan 2009) $
//
//********************************************************/

//********************************************************/
//	ServiceNames.h is the include file that defines the names of 
//	the WAX Service.
//
//	It is intended to be used only by the NF Wax Service and the IPc components
//	for declarations that pertain to the naming of the WAX Service:
//		- Windows Service name 
//		- Exe name of the WAX 
//
//********************************************************/

#pragma once
#include <windows.h>

#define NF_WAX_SERVICE_WINDOWS_NAME		L"NFWAXService"			//!< Internal name of the service
#define NF_WAX_SERVICE_EXE_NAME			L"NFWaxService.exe"	//!< Application (Exe) name of the service
#define NF_WAX_SERVICE_DISPLAY_NAME		L"Neverfail Wan Acceleration Service"	//!< Displayed name of the service
