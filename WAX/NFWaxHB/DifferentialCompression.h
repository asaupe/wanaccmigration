//********************************************************/
//  ./DifferentialCompression.h
//
//  Owner     : Attila Vajda
//
//  Company   : Neverfail
//  PROJECT   : WAN Acceleration
//  Component : Differential Compression 
//  Purpose   : Implementation of Differential Compression algorithm encode
//              and decode a differentialy compressed message
//
//  $Rev:$ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: $
//  $Date:$
//
//********************************************************/

#pragma once
#include <Windows.h>

//!  Differential Compression On Well Known Buffers
/*! \class DifferentialCompression
*   \brief Implements differential Compression algorithm for WAN Acceleration - Well Known Buffers.
*
*   This is an implementation of differential compression on short messages - described in 
*   document "WAN Acceleration - Performance tests - build 14550" by Wouter Senf section 3.5
*
*  Quote: 
*  "A significant part of the replication traffic consists of �cleanup and close� entries. 
*   Replication traffic can be optimized by removing such entries from the replication stream. 
*   This is an effort that can be implemented in Neverfail Heartbeat. An alternative approach is discussed here: 
*   compressing these message buffers more effectively.
*
*   We know that the message buffers consist of a number of cleanup and close sequences. 
*   More precisely, they contain a CleanupEntry, a CloseEntry and two StatusEnties. 
*   The structure of the data is static: each segment within the message buffers 
*   always starts at the same offset. When looking at the 5KB buffers in detail we find that:
*       �   On average for each segment of 136 bytes, only 12.25 bytes are different from the previous segment; 
*           however, this figure is highly skewed by two segments that have almost no matching bytes; 
*           without these segments only 6.76 bytes are different.
*       �   Almost all non-matching bytes are single bytes that can be found at fixed offsets in the segments. 
*           The 6 bytes at offsets 4, 20, 64, 88, 104 and 116 are always different from the bytes at the same 
*           offset in the previous segment. In very few cases the next byte also does not match. In about 10% 
*           of the segments cases the 3 bytes starting at offset 36 do not match
*       �   In a compressed buffer one can store the offset and value for each non-matching byte.
*           This takes two bytes for each non-matching value. Based on the 6 MB sample that was evaluated, 
*           it leads to a compression factor of 9.4.
*       �   As a further optimization one can always store the values of the bytes at offsets 4, 20, 64, 88, 104 and 116 
*           without any overhead and the offset and value for other non-matching bytes.
*           This leads to a compression factor of 15.1.
*
*  [A].- Not implemented - see section [B]
*   A similar analysis for message buffers of 816 bytes shows that only 50% of the bytes are identical to the 
*   corresponding bytes in the previous 136-byte segment. However, if a full 816-byte buffer is compared against 
*   a previous 816 byte buffer, 75% of the bytes are identical to the corresponding bytes in the previous buffer.
*   In addition, the non-matching bytes occur in groups so encoding as offset-length-value units is a good approach.
*   Based on the 6 MB sample that was evaluated, it was found that a compression factor of about 2.5 can be expected.
*
*  [B].- Implemented this version
*   A further improvement can be achieved by:
*
*       �   Comparing the first three segment of a buffer against the first three segments of the previous buffer.
*       �   Comparing the 4th segment against the 2nd segment in the same buffer.
*       �   Comparing the 5th segment against the 3rd segment in the same buffer.
*       �   Comparing the 6th segment against the 4th segment in the same buffer.
*
*   This increases the number of matching bytes to about 84%, with a corresponding improvement in the compression 
*   factor for these message buffers to about 3.5.
*
*   This leads to the following proposal:
*
*       �1� Message buffers with a size of 816 are compressed using differential compression as described above.
*           For the first three segments, the differences are encoded as �offset-length-value� units. 
*           Note that the offset cannot be stored in a byte. It is therefore suggested that the offset is stored 
*           as 10 bits and the length as 6 bits. Non-matching strings longer than 64 bytes should be extremely rare;
*           we have not seen any in a 6MB sample. Differences in the last three segments are encoded as offset-value pairs.
*
*       �2� Message buffers with a size between 2 KB and 10KB and with a length that is a multiple of 136 bytes, are
*           compressed using differential compression. Each segment of 136 bytes is compared with the previous segment. 
*           The first segment of a message buffer is compared with the last segment in the previous buffer.
*           The encoded segment consists of 6 bytes that represent the values of the bytes at offsets 
*           4, 20, 64, 88, 104 and 116 followed by offset-value pairs for other non-matching bytes."
*
*   END Quote.
*
*   NOTES ON IMPLEMENTATTION:
*   Tests and statistics results performed on implementation lead to the following changes:
*   For 816 byte buffers implemented [A] (see above description) this lead to same tokens type for all buffers.
* 
*   We have 3 different tokens:
*       � DiffMEssageHeader - contains number of blocks and CRC32 for every message 
*
*       � BlockHeader       - every 136 block in currently processed message contains a block header with 
*                                 a. nrOfDiffHeaders - nr. of DiffHeader's in the block - which is actually the 
*                                    difference between the current block and the previous block n-1 where n = current block
*                                    and for n = 0 the last block( for 816 byte messages the previous block will be n-2 
*                                    where n is the current block)
*                                 b. known[SIZE_OF_KNOWN_OFFSETS] - see gKnown_Offsets[] - the value of the known offsets from 
*                                    previous offset.
*
*       � DiffHeader        - offset - offset in the current block max offset 135
*                           - length - length of the difference - max length  255
*   
*   \author Attila Vajda
*   \version 1.0.$Rev:$
*   \date    FEB-2009
*/
class DifferentialCompression
{
protected:
	#define WAX_DIFF_IDENTICAL_BUFFER_SIZE 816	
	#define WAX_DIFF_MULTIPLIER_DIFFERENTIAL_SIZE 136
	#define WAX_DIFF_LIMIT_LEFT  ( 15*WAX_DIFF_MULTIPLIER_DIFFERENTIAL_SIZE )
	#define WAX_DIFF_LIMIT_RIGHT ( 76*WAX_DIFF_MULTIPLIER_DIFFERENTIAL_SIZE )

public:
	DifferentialCompression( void );
	~DifferentialCompression( void );
	
	//! Differential Compression Encode buffer
	bool differentialEncodeBuffer(
		/*__in*/    /*const*/ unsigned char* inlpData,                       //!< pointer - to the buffer that is processed.
		/*__in*/    const unsigned long  inulDataLength,                 //!< size of incoming buffer - inlpData
		/*__inout*/       unsigned char* inoutlpEncodedData,             //!< output buffer - Differentially Compressed message
		/*__inout*/       unsigned long& inoutulEncodedDataLength,       //!< output buffer length - inoutlpEncodedData
		/*__in*/    const bool   inbValidateCompression = false, //!< default false - validate the encode sequence 
		/*__in*/    const UINT   inunValidateCFactorValue = 1    //!< default 1 minimum CFactor for validation 
		                                                     //   to override this call validateLastDifferentialEncode()
		);

	//! Differential Compression decode buffer
	bool differentialDecodeBuffer( 
		/*__in*/    const unsigned char* inlpData,                 //!< pointer -to the buffer that is processed.
		/*__in*/    const unsigned long  inulDataLength,           //!< size of incoming buffer - inlpData
		/*__inout*/       unsigned char* inoutlpDecodedData,       //!< output buffer
		/*__inout*/       unsigned long& inoutulDecodedDataLength, //!< output buffer length
		/*__in*/    const bool   inbTestingDecodeBeforeValidate = false //!< used for testing in place only ( decode after an encode before validate ) 
		);

	//! Setting the Parameters 
	bool setParameters(
		/*__in*/ const unsigned int inuLimitLeft  = WAX_DIFF_LIMIT_LEFT, //!< minimum size of incoming buffers for encode
		/*__in*/ const unsigned int inuLimitRight = WAX_DIFF_LIMIT_RIGHT,//!< maximum size of incoming buffers for encode
		/*__in*/ const unsigned int inuIdenticalDifferentialBufferSize = WAX_DIFF_IDENTICAL_BUFFER_SIZE, //!< size of the identical buffer
		/*__in*/ const unsigned int inuMultiplierDifferentialSize      = WAX_DIFF_MULTIPLIER_DIFFERENTIAL_SIZE //!< size of the multiplier buffer( block size)
		);
	
	//! Validation of last encode if inbValidateCompression = false
	bool validateLastDifferentialEncode( void );
	
	//! Return the maximum size of the encoded buffer for current setting.
	unsigned int getMaxEncodedBufferLen( void );

	//! Return number of different bytes in last encoded message - for testing
	unsigned int getNrOfDifferentBytesFromLastEncode( void );

private:
	//! Differential Compression algorithm implementation 
	bool doDifferentialEncodeBuffer(
		/*__in*/    /*const*/ unsigned char* inlpData,                 //!< pointer -to the buffer that is processed.
		/*__in*/    const unsigned long  inulDataLength,           //!< size of incoming buffer - inlpData
		/*__inout*/       unsigned char* inoutlpEncodedData,       //!< output buffer
		/*__inout*/       unsigned long& inoutulEncodedDataLength, //!< output buffer length
		/*__in*/    /*const*/ unsigned char* inOldData,                //!< pointer - to the buffer that is processed.
		/*__in*/    const bool   inbIsAlgorithm2For816     //!< is buffer 816 and use algorithm2 for processing
		);

	//! Create a differential header and copy to out buffer - kind of a create token.
	bool createAndCopydifference( 
		/*__in*/    const unsigned char*       inlpData,             //!< in buffer where to copy the difference from
		/*__inout*/       unsigned char*       inoutlpEncodedData,   //!< out buffer where to copy the difference
		/*__inout*/       unsigned int &inoucurrentPosition, //!< currentPosition in inoutlpEncodedData
		/*__inout*/       unsigned int &inunDifferentLength, //!< size of the different data
		/*__in*/    const unsigned int inunPosition,         //!< position of the different data in current block 
		/*__in*/    const unsigned int inunBlockStart        //!< position of the block in the processed data 
		);

	//! Allocate necessary buffer for differential compression
	void allocateBuffersForDifferentialCompression( void );
	
	//! Compute the maximum size of encoded buffer - worst case when all bytes are different
	void computeMaxEncodedBufferLen( void );

	//For Unit Tests - nr of equal bytes in message
	unsigned int   m_unNrOfDifferentBytesFromLastEncode;

	unsigned short m_uIdenticalDifferentialBufferSize;
	unsigned short m_uMultiplierDifferentialSize;
	unsigned int   m_unLimitLeft;
	unsigned int   m_unLimitRight;
	unsigned int   m_unMaximumEncodedBufferLen;
	unsigned char*         m_lpOldDataIdenticalDifferentialBuffer;
	unsigned char*         m_lpOldDataMultiplierDifferentialBuffer;
	unsigned char*         m_lpLastOldData;
	unsigned char*         m_lpLastBufferToValidate;
	unsigned long          m_ulLastBufferToValidateSize;
	bool           m_bAllocated;
};