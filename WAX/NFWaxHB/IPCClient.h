//********************************************************/
//  ./IPCClient.h
//
//  Owner : Ciprian Maris 
// 
//	Company		: Neverfail 
//	PROJECT		: WAN Acceleration 
//  Component	: IPC Client
//
//  $Rev: 2616 $ - Revision of last commit
//
/********************************************************
//
// Last Change - Author and Date of last change:
// $Author: cmaris $  
// $Date: 2009-06-10 16:16:14 +0100 (Wed, 10 Jun 2009) $
//
//********************************************************/

//********************************************************/
//
//	The IPC client component is the portion of the IPC bridge that lives in HB.
//
//	Communication roles:
//		- The IPC Server - creates the shared memory and events(Call/Return).
//		- The IPC Client - opens the shared memory and events.
//
//	The invokation of WAXCompressor functions is done through the following mechanism:
//		- The IPC Client 
//			� sets the function parameters in the shared memory by using function 
//			speciffic parameter data types (defined as derivates of the basic type IPCParamteters)
//			� signal the Call event 
//		- The IPC Server
//			� retreives the OperationType requested by the client
//			� takes the input parameters and calls the appropriate WAXCompressor function
//			� signal the Return event 
//
//	NOTE:	[1] All the public methods of the IPCClient component are static and have the same signature 
//			as the ones in WaxCompressor.h. This is done so to allow compile-time swithching between 
//			the 2 operation (in-/out-of- process)modes.
//
//	NOTE:	[2] The methods that remotely invoke the compressor have only limited comments in this unit.
//			Refere to "WAXCompressor.h" for details on the functionality of each compressor related method.
//
//********************************************************/

#pragma once

#include "WaxTypes.h"
#include "Common.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
//	Define the IPC Client Class

class IPCClient
{
public:

	///////////////////////////////////////////////////////////////////////////////////////////////
	//	Methods that remotely invoke the compressor 

	
	////	Initialize - exit

	//! Method initialize() - initializes the IPC Client (logger, security, IPC components)
	//	and remotely calls the compressor initialization function.
	//See implementation for details.
	static WAX_RESULT initialize(
		/*__in*/ const wchar_t* logFileName,	//!< The name of the log file.
		/*__in*/ bool inbSetDebugMode		//!< The initial debug mode of the compressor.
		);

	//! Method exit() - cleans up all IPC components and remotely calls the compressor exit function.
	static WAX_RESULT exit(void);

	
	////	Allocate - release

	//! Method allocate() - calls compressor allocate to pre-book resouces.
	static WAX_RESULT allocate(
		/*__in*/ unsigned long inulMinimumMemoryBufferSize,		//!< Minimum memory to allocate for the compressor in MB's
		/*__in*/ unsigned long inulMaximumMemoryBufferSize,		//!< Maximum memory to allocate for the compressor in MB's
		/*__in*/ unsigned long inulTransferLogSize,				//!< On disk transfer Log Size in MB's
        /*__in*/ const StringBuffer* insbTransferLogPath//!< On disk transfer Log File path (complete path)
		);

	//! Method release() - calls compressor release to free pre-booked resouces.
	static WAX_RESULT release( void );


	////	Start - stop

	//! Method start() - starts the compressor.
	static WAX_RESULT start(
		/*__in*/ bool inAsEncoder,						//!< Server role (compressor/decompressor)
		/*__in*/ unsigned long inSignatureBase,					//!< signature base
		/*__in*/ unsigned long inSignatureDistance,				//!< signature distance
		/*__in*/ unsigned long inMinRPRPatternsLength,			//!< minimum length of ZERO/DA pattern replaced by a RPR token
		/*__in*/ unsigned long inMinDeDuplicateLength,			//!< minimum length of the buffer to be considered for DD
		/*__in*/ unsigned long inMinDDLengthReplacedByToken,	//!< minimum length of data replaced by a DD token
		/*__in*/ const bool inEncodeOutOfBoundaryEnabled = false,	//!< advanced encode for out of boundary entries
		/*__in*/ double indNInvariant = 0.05			//!< Factor for recent data volume and compression computation.
		);
	
	//! Method stop() - stops the compressor.
	static WAX_RESULT stop(void);

	
	////	Encode-decode

	//! Method encode() encodes the input buffer on source.
	static WAX_RESULT encode(
		/*__in*/ const ByteBuffer* inputBuffer,	//!< source buffer to be encoded
        /*__out*/ ByteBuffer* outputBuffer,		//!< destination buffer - pre-allocated by caller
        /*__in*/ bool removeRepeatedPatterns,	//!< FLAG - RPR requested by caller
        /*__in*/ bool deDuplicate				//!< FLAG - DD requested by caller
		);


	//! Method decode() decodes the input buffer on target.
	static WAX_RESULT decode(
        /*__in*/  const ByteBuffer* inputBuffer,	//!< source buffer to be decoded
        /*__out*/ ByteBuffer* outputBuffer,	//!< destination buffer - pre-allocated by caller
		/*__in*/ bool			bRPREncoded,
		/*__in*/ bool			bDDEncoded 
		);


	//! Method queryStatistics() - gathers stats from the WAX components.
	static WAX_RESULT queryStatistics(
		/*__inout*/ Statistics *inoutStatistics //!<	statistics structure to receive the entire statistics set
		);

	//! Method setDebugMode - set logger to enable/disable printing out of debug info.
	static WAX_RESULT setDebugMode(
		/*__in*/ bool inbOn	//!< State of debug mode to be set to.
		);

	//	END Methods that remotely invoke the compressor 
	///////////////////////////////////////////////////////////////////////////////////////////////

	//! Return a pointer to the stored Error Information.
	static wchar_t* getErrorInfo( void ) { return (wchar_t*)m_strErrorMessage.c_str(); }

	///////////////////////////////////////////////////////////////////////////////////////////////
	//	Methods that start and stop the compressor Executable

	//! Starts the compressor process. 
	static WAX_RESULT setup(
		/*__in*/ StringBuffer* inSBServicePath
		);

	//! Stops the compressor process. 
	static WAX_RESULT teardown(void);

	//	END Methods that remotely invoke the compressor 
	///////////////////////////////////////////////////////////////////////////////////////////////


private:
	IPCClient();
	~IPCClient()	{	exit();	}

	//! Signals a call event, waits for the out of process reply and processes the 
	//	return code + error message if any.
	//See implementation for details.
	static WAX_RESULT SignalEventAndProcessReturn( 
		/*__in*/ unsigned long indwTimeout,							//!< Wait timeout in milisoconds. The 
														//caller has to set it accordingly.
		/*__in*/ wchar_t* inszOperationName,					//!< Operation name to be used in error logging.
		/*__in*/ wchar_t* inszEventMessageOnsuccess = NULL	//!< Event message to be logged if the method succedes.
														//If NULL, nothing is logged.
		);

	//! Method SetError() - stores and loggs the errors from the IPC brigde/WAXCompressor.
	//See implementation for details.
	static void SetError(
		/*__in*/ wchar_t* inszErrorString,	//!< Error message to be logged and stored.
		/*__in*/ WAX_RESULT inwrErrorCode,	//!< Error code to be logged and stored.
		/*__in*/ wchar_t* inszLocation = NULL	//!< Location where the fault occured.  
										//If NULL, no location is logged.
		);

	//!	Method ResetError() - Clears the previous error messages and code.
	//See implementation for details.
	static void ResetError( void );

	//! Method ResetEvents() - Resets the events in the bridge.
	//See implementation for details.
	static void ResetEvents( void );


	//! Method initializeIPCClientLogger() - initializes the IPC Client Logger.
	//See implementation for details.
	static bool initializeIPCClientLogger(
		/*__in*/	const wchar_t* logFileName,	//!< The name of the log file passed to the compression provider.
		/*__in*/	bool inbSetDebugMode		//!< The initial debug mode of the compressor.
		);

	//! Method initializeIPCComponents() - initializes the components involved in IPC(shared memory, events, etc.) .
	//See implementation for details.
	static WAX_RESULT initializeIPCComponents( void );

	//!Method exitIPCComponents() - cleans up the components involved in IPC.
	static void exitIPCComponents( void );

	//!startWAXService() - starts the wax service in console or service mode and waits for it to initialize.
	static bool startWAXService(
		/*__in*/ const wchar_t* inszService,
		/*__in*/ bool inbIsConsole
		);

	///////////////////////////////////////////////////////////////////////////////////////////////
	////	Utility methods shared by other methods

	//!Method openSharedMemoryFileMapping() - utility method for opening the shared memory file mapping.
	static HANDLE openSharedMemoryFileMapping();

	//! Method isServiceRunning() - utility method checks if the service is running for a timeout of 
	//								indwWaitTimeout miliseconds.
	static bool isServiceRunning( 
		/*__in*/ unsigned long indwWaitTimeout = 0
		);

	////	END Utility methods shared by other methods
	///////////////////////////////////////////////////////////////////////////////////////////////

	//	Private Members
	static HANDLE m_hMapFile;				//!< Handle of the FileMapping for the shared memory buffer.

	static unsigned char* m_lpbSharedMemoryBuffer;	//!< Addres of shared memory buffer.
	static unsigned char* m_lpbReturnMemoryBuffer;	//!< Addres of shared memory buffer that holds the return structure 
											//(ReturnResults) from an IP Call
	static unsigned char* m_lpbStatisticsBuffer;	//! Addres of shared memory buffer that holds the statistics structure.

	//Error Code and Messages
	static wchar_t* m_szErrorMessageBuffer;	//!< Addres of shared memory buffer that holds the error messages.
	static wstring m_strErrorMessage;		//!< Local erro string stores faults in the bridge and the
											//ones returned from the compressor.

	static WAX_RESULT m_wrErrorCode;	//!< Error code returned by the last IP Call.
	static WAX_RESULT m_wrReturnCode;	//!< Return code of the last IP Call.

	static HANDLE m_hEventCall;			//!< Is signaled when the client makes an IP Call.
	static HANDLE m_hEventReturn;		//!< Is signaled when the server returns from an IP Call.

	static bool m_bUnitTestMode;		//!< Set to true if the service is started in console mode by setup();
};

//	END Define the IPC Client Class
///////////////////////////////////////////////////////////////////////////////////////////////////

//#endif