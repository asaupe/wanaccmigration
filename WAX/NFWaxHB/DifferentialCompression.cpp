//********************************************************/
//  ./DifferentialCompression.cpp
//
//  Owner     : Attila Vajda
//
//  Company   : Neverfail
//  PROJECT   : WAN Acceleration
//  Component : Differential Compression 
//  Purpose   : Implementation of Differential Compression algorithm encode
//              and decode a differentially compressed message
//
//  $Rev:$ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: $
//  $Date:$
//
//********************************************************/

#include "DifferentialCompression.h"


#define ZLIB_WINAPI
#include "zlib.h"

//for 816 byte message processing 
#define WAX_DIFF_IDENTICAL_BUFFER_NR_OF_PREVIOUS_SEGMENTS_TO_COMPARE 3
#define WAX_DIFF_IDENTICAL_BLOCK_OFFESTS_BEHIND 2

#define WAX_DIFF_MAX_NR_OF_BLOCKS 255


#define SIZE_OF_KNOWN_OFFSETS 6
static byte gKnown_Offsets[SIZE_OF_KNOWN_OFFSETS] = { 4, 20, 64, 88, 104, 116 };


#pragma pack(1)
//! Differential Header - the difference between this block 
//  and the previous block represented in offset and length
typedef struct _DiffHeader
{
	byte offset;
	byte length;
} DiffHeader;
#pragma pack()


#pragma pack(1)
//! Differential Block Header - this is the block header contains
// known offset array - value of every known offset
typedef struct _BlockHeader
{
	byte known[SIZE_OF_KNOWN_OFFSETS];
	byte nrOfDiffHeaders;
} BlockHeader;

#pragma pack()

#pragma pack(1)
//! Differential Message Header - every message that is differentially 
//  compressed has a message header containing the number of blocks - 
//  which is also the size of the buffer and a CRC32 - checksum of the
//  original buffer.
typedef struct _DiffMessageHeader
{
	byte nrOfBlocks;
	unsigned long ulCRC32;

} DiffMessageHeader;
#pragma pack()


//////////////////////////////////////////////////////////////////////////

DifferentialCompression::
DifferentialCompression( void )
{
	m_lpLastBufferToValidate     = NULL;
	m_ulLastBufferToValidateSize = 0;

	m_lpOldDataIdenticalDifferentialBuffer  = NULL;
	m_lpOldDataMultiplierDifferentialBuffer = NULL;
	
	m_uMultiplierDifferentialSize = 0;
	m_uMultiplierDifferentialSize = 0;
	m_unMaximumEncodedBufferLen = 0;

	m_bAllocated = false;

	setParameters(
		WAX_DIFF_LIMIT_LEFT,
		WAX_DIFF_LIMIT_RIGHT,
		WAX_DIFF_IDENTICAL_BUFFER_SIZE,
		WAX_DIFF_MULTIPLIER_DIFFERENTIAL_SIZE
	);
}

DifferentialCompression::
~DifferentialCompression( void )
{
	if ( NULL != m_lpOldDataMultiplierDifferentialBuffer )
	{
		free( m_lpOldDataMultiplierDifferentialBuffer );
		m_lpOldDataMultiplierDifferentialBuffer = NULL;
	}

	if ( NULL != m_lpOldDataIdenticalDifferentialBuffer )
	{
		free( m_lpOldDataIdenticalDifferentialBuffer );
		m_lpOldDataIdenticalDifferentialBuffer = NULL;
	}
}

//!  Differential Compression Encode buffer
/*!
*   This is the Differential compression interface for the algorithm which is responsible of
*   checking in buffer and limits and decide which algorithm to use: - for 816 byte messages 
*   a different algorithm is used in order to do differential compression.
*   
*  \sa doDifferentialEncodeBuffer()
*  \return - true  - if the buffer was Encoded 
*          - false - if the buffer was not encoded - in case we have limits error or invalid buffers
*
*/
bool DifferentialCompression::
differentialEncodeBuffer( 
	/*__in*/    /*const*/ unsigned char* inlpData,                 //!< pointer - to the buffer that is processed.
	/*__in*/    const unsigned long  inulDataLength,           //!< size of incoming buffer - inlpData
	/*__inout*/       unsigned char* inoutlpEncodedData,       //!< output buffer - Differentially Compressed message
	/*__inout*/       unsigned long& inoutulEncodedDataLength, //!< output buffer length - inoutlpEncodedData
	/*__in*/    const bool   inbValidateCompression,   //!< default false - validate the encode sequence 
	/*__in*/    const UINT   inunValidateCFactorValue  //!< default 1 minimum CFactor for validation 
	                                               //   to override this call validateLastDifferentialEncode()
	)
{
	unsigned char* lpOldBuffer;
	//For Unit Tests - nr of equal bytes in message
	m_unNrOfDifferentBytesFromLastEncode = 0;

	bool bResult = false;

	if( !( ( (inulDataLength != 0)&&
		   ( inulDataLength >= m_unLimitLeft  )&&
		   ( inulDataLength <= m_unLimitRight )&&
		   ( inoutulEncodedDataLength >= m_unMaximumEncodedBufferLen ) )||
		   ( inulDataLength == m_uIdenticalDifferentialBufferSize ) 
		   )
		)
	{
		return  false;
	}

	if( ( !m_bAllocated  )||
		( inlpData           == NULL )||
		( inoutlpEncodedData == NULL )
		)
	{
		return false;
	}

	bool bIsMultiplierDifferentialCompression = false;
	bool bNoDifferentialCompression           = false;

	if ( ( m_uIdenticalDifferentialBufferSize == inulDataLength ) )
	{
		lpOldBuffer = m_lpOldDataIdenticalDifferentialBuffer;
	}
	else 
	{
		bIsMultiplierDifferentialCompression = ( ( inulDataLength%m_uMultiplierDifferentialSize ) == 0);

		if( bIsMultiplierDifferentialCompression  )
		{
			lpOldBuffer = m_lpOldDataMultiplierDifferentialBuffer;
		}
		else 
		{
			bNoDifferentialCompression = true;
		}
	}

	if( !bNoDifferentialCompression  )
	{
		bResult = doDifferentialEncodeBuffer(
			inlpData,
			inulDataLength,
			inoutlpEncodedData,
			inoutulEncodedDataLength,
			lpOldBuffer,
			!bIsMultiplierDifferentialCompression
			);

		if( bResult )
		{
			if( bIsMultiplierDifferentialCompression )
			{
				m_lpLastOldData              = &inlpData[ inulDataLength - m_uMultiplierDifferentialSize - 1 ];
				m_lpLastBufferToValidate     = lpOldBuffer;
				m_ulLastBufferToValidateSize = m_uMultiplierDifferentialSize;
			}
			else
			{
				m_lpLastOldData              = inlpData;
				m_lpLastBufferToValidate     = lpOldBuffer;
				m_ulLastBufferToValidateSize = m_uMultiplierDifferentialSize * WAX_DIFF_IDENTICAL_BUFFER_NR_OF_PREVIOUS_SEGMENTS_TO_COMPARE;
			}

			if( inbValidateCompression )
			{
				if( ( inoutulEncodedDataLength > 0 )&&
					( ( inulDataLength / inoutulEncodedDataLength ) >= inunValidateCFactorValue ) )
				{
					bResult = validateLastDifferentialEncode();
				}
				else
				{
					bResult = false;
				}
			}
		}
	}

	return bResult;
}

//! Validation of last encode if inbValidateCompression = false
/*!
*   
*   Called after a call to differentialEncodeBuffer() if inbValidateCompression is false;
*   This enables the caller to accept the differential compression or reject it in order to not 
*   add this to sequential differentiated processing.
*   In case that differentialEncodeBuffer() was called with inbValidateCompression = true and 
*   inunValidateCompressionFactorValue this method is called from inside the differentialEncodeBuffer()
*   and the caller can skip the call of this method.
*/
bool DifferentialCompression::
validateLastDifferentialEncode( void )
{
	bool bResult = false;

	if( ( NULL != m_lpLastOldData              )&& 
		( NULL != m_lpLastBufferToValidate     )&& 
		( 0    != m_ulLastBufferToValidateSize ) )
	{
		memcpy(
			m_lpLastBufferToValidate,
			m_lpLastOldData,
			m_ulLastBufferToValidateSize
			);

		bResult = true;

		m_lpLastBufferToValidate     = NULL;
		m_ulLastBufferToValidateSize = 0;			
	}

	return bResult;
}

//! Setting the Parameters 
/*!
*  This call can be used to adapt parameters for multiplier ( which by default is 136) 
*  and identical buffer size ( which by default is 816 = 136*6)  but the max can be 255*255 = 65025 and 
*  identical buffer size is a multiple of multiplier value.
*  the default values are adapted after a research on short messages from a Neverfail dump which 
*  are internal Neverfail messages that consist of a multiple of 136 messages and messages with size if 816
*
*  \return - true  - if the parameters were set successfully 
*          - false - new parameters were not set and the default parameters are used
*/
bool DifferentialCompression::
setParameters(
	/*__in*/ const unsigned int inuLimitLeft,  //!< minimum size of incoming buffers for encode
	/*__in*/ const unsigned int inuLimitRight, //!< maximum size of incoming buffers for encode
	/*__in*/ const unsigned int inuIdenticalDifferentialBufferSize, //!< size of the identical buffer
	/*__in*/ const unsigned int inuMultiplierDifferentialSize       //!< size of the multiplier buffer( block size)
	)
{
	bool bDoReAllocate    = false;
	bool bParametersValid = false;
	
	if ( ( m_uIdenticalDifferentialBufferSize != inuIdenticalDifferentialBufferSize ) ||
		 ( m_uMultiplierDifferentialSize      != inuMultiplierDifferentialSize ) )
	{
		bDoReAllocate = true;
	}
	
	//1. Validate Multiplier and Identical buffers
	if( (   inuMultiplierDifferentialSize      != 0 )&&
		(   inuIdenticalDifferentialBufferSize != 0 )&&
		(   inuMultiplierDifferentialSize      <= WAX_DIFF_MAX_NR_OF_BLOCKS )&&
		(   inuIdenticalDifferentialBufferSize <= ( WAX_DIFF_MAX_NR_OF_BLOCKS * WAX_DIFF_MAX_NR_OF_BLOCKS) )&&
		( ( inuIdenticalDifferentialBufferSize % inuMultiplierDifferentialSize) == 0 )
		)
	{
		bParametersValid = true;
	}

	if( bParametersValid )
	{
		unsigned int unMaxRightLimit = WAX_DIFF_MAX_NR_OF_BLOCKS * inuMultiplierDifferentialSize;

		if( (inuLimitRight > unMaxRightLimit )||
			(inuLimitLeft  > inuLimitRight) )
		{
			bParametersValid = false;
		}
	}

	if( bParametersValid )
	{
		m_unLimitLeft  = inuLimitLeft;
		m_unLimitRight = inuLimitRight;

		m_uIdenticalDifferentialBufferSize = inuIdenticalDifferentialBufferSize;
		m_uMultiplierDifferentialSize      = inuMultiplierDifferentialSize;
		computeMaxEncodedBufferLen();

		if( bDoReAllocate )
		{
			computeMaxEncodedBufferLen();
			allocateBuffersForDifferentialCompression();
		}
	}

	return bParametersValid;
}

//! Allocate necessary buffer for differential compression
void DifferentialCompression::
allocateBuffersForDifferentialCompression( void	)
{
	m_bAllocated = false;

	if ( NULL != m_lpOldDataIdenticalDifferentialBuffer )
	{
		free( m_lpOldDataIdenticalDifferentialBuffer );
		m_lpOldDataIdenticalDifferentialBuffer = NULL;
	}

	m_lpOldDataIdenticalDifferentialBuffer = (unsigned char*)malloc( m_uIdenticalDifferentialBufferSize );

	if( NULL !=	m_lpOldDataIdenticalDifferentialBuffer )
	{
		memset( 
			m_lpOldDataIdenticalDifferentialBuffer,
			0 ,
			m_uIdenticalDifferentialBufferSize
			);
	}

	if ( NULL != m_lpOldDataMultiplierDifferentialBuffer )
	{
		free( m_lpOldDataMultiplierDifferentialBuffer );
		m_lpOldDataMultiplierDifferentialBuffer = NULL;
	}

	m_lpOldDataMultiplierDifferentialBuffer= (unsigned char*)malloc( m_uMultiplierDifferentialSize );

	if( NULL !=	m_lpOldDataMultiplierDifferentialBuffer )
	{
		memset( 
			m_lpOldDataMultiplierDifferentialBuffer,
			0 ,
			m_uMultiplierDifferentialSize
			);
	}

	if( (NULL != m_lpOldDataIdenticalDifferentialBuffer )&&
		(NULL != m_lpOldDataMultiplierDifferentialBuffer ) )
	{
		m_bAllocated = true;
	}
}

//! Compares inlpBuffer1 with inlpBuffer2 and returns number of equal bytes
inline unsigned int computeEqualBytes(
	/*__in*/ const                   unsigned long inCount,     //!< count of bytes to compare
	/*__in*/ const __bcount(inCount) byte* inlpBuffer1, //!< 1'st memory to be compare with 2'nd
	/*__in*/ const __bcount(inCount) byte* inlpBuffer2  //!< 2'nd memory to be compare with 1'st
				 )
{
	unsigned long i = 0;

	while ( ( i < inCount ) && ( inlpBuffer1[i] == inlpBuffer2[i] ) )
	{
		i++;
	}

	return i;
}

//! Compares inlpBuffer1 with inlpBuffer2 and returns number of different bytes
inline unsigned int computeDifferentBytes(
	/*__in*/ const                   unsigned long inCount,     //!< count of bytes to compare
	/*__in*/ const __bcount(inCount) byte* inlpBuffer1, //!< 1'st memory to be compare with 2'nd
	/*__in*/ const __bcount(inCount) byte* inlpBuffer2  //!< 2'nd memory to be compare with 1'st
	)
{
	unsigned long i = 0;

	while ( ( i < inCount ) && ( inlpBuffer1[i] != inlpBuffer2[i] ) )
	{
		i++;
	}

	return i;
}

//! Create a differential header and copy to out buffer - kind of a create token.
/*!
*    Copy header and different data in encoded buffer. Also, skips the known offsets 
*    if it starts or ends at a known offset.
* 
*    \returns true  - if a header was created and data were copied to th out encoded buffer 
*             false - if there was no difference or the difference was on a known offset.
*
*/
bool DifferentialCompression::
createAndCopydifference( 
	/*__in*/    const unsigned char*       inlpData,               //!< in buffer where to copy the difference from
	/*__inout*/       unsigned char*       inoutlpEncodedData,   //!< out buffer where to copy the difference
	/*__inout*/       unsigned int &inoucurrentPosition, //!< currentPosition in inoutlpEncodedData
	/*__inout*/       unsigned int &inunDifferentLength, //!< size of the different data
	/*__in*/    const unsigned int inunPosition,         //!< position of the different data in current block 
	/*__in*/    const unsigned int inunBlockStart        //!< position of the block in the processed data 
	)
{
	DiffHeader diffHeader;
	bool bResult = false;

	//we take 1 byte for flag of endBlock ....
	diffHeader.length = inunDifferentLength;
	diffHeader.offset = inunPosition-(inunBlockStart);

	//For Unit Tests - nr of equal bytes in message
	m_unNrOfDifferentBytesFromLastEncode += diffHeader.length;

	bool bDoNotAddToStats = false;

	for( byte kO=0; kO< SIZE_OF_KNOWN_OFFSETS; kO++ )
	{
		if( diffHeader.offset == gKnown_Offsets[kO] )
		{
			bDoNotAddToStats = true;

			diffHeader.offset++;
			diffHeader.length--;
			inunDifferentLength--;
			break;

		}
	}

	if( diffHeader.length>0 )
	{
		unsigned int sum = ( diffHeader.length + diffHeader.offset) -1;

		for(byte kO=0; kO< SIZE_OF_KNOWN_OFFSETS; kO++ )
		{
			if( sum == gKnown_Offsets[kO] )
			{
				diffHeader.length--;
				inunDifferentLength--;
				break;
			}
		}
	}

	if( diffHeader.length>0 )
	{
		memcpy(
			&inoutlpEncodedData[inoucurrentPosition],
			&diffHeader,
			sizeof( DiffHeader)
			);
	
		inoucurrentPosition += sizeof( DiffHeader);

		memcpy( 
			&inoutlpEncodedData[inoucurrentPosition],
			&inlpData[diffHeader.offset+inunBlockStart],
			diffHeader.length
			);

		inoucurrentPosition += diffHeader.length;
		bResult = true;
	}

	return bResult;
}

//! Differential Compression decode buffer
/*!
*   Used to decode a differentially encoded buffer.
*   In order to work the calls for decode have to be same order as they were encoded 
*   Returns true  - if decoded with success 
*           false - if decode failed
*/
bool DifferentialCompression::
differentialDecodeBuffer( 
	/*__in*/    const unsigned char* inlpData,                      //!< pointer -to the buffer that is processed.
	/*__in*/    const unsigned long  inulDataLength,                //!< size of incoming buffer - inlpData
	/*__inout*/       unsigned char* inoutlpDecodedData,            //!< output buffer
	/*__inout*/       unsigned long& inoutulDecodedDataLength,      //!< output buffer length
	/*__in*/    const bool   inbTestingDecodeBeforeValidate //!< used for testing in place only ( decode after an encode before validate ) 
	)
{
	unsigned char* lpOldData = NULL;

	DiffMessageHeader* differentialMessageHeader;
	differentialMessageHeader = (DiffMessageHeader*)&inlpData[0];
	inoutulDecodedDataLength  = differentialMessageHeader->nrOfBlocks*m_uMultiplierDifferentialSize;
	
	bool bIsIdenticalProccessing = false;

	if( m_uIdenticalDifferentialBufferSize == inoutulDecodedDataLength  )
	{
		bIsIdenticalProccessing = true;
	}

	if ( bIsIdenticalProccessing )
	{
		lpOldData = m_lpOldDataIdenticalDifferentialBuffer;
	}
	else
	{
		lpOldData = m_lpOldDataMultiplierDifferentialBuffer;
	}

	if( inbTestingDecodeBeforeValidate )
	{
		lpOldData = m_lpLastBufferToValidate;
	}

	unsigned int unMultiplier = (bIsIdenticalProccessing)?WAX_DIFF_IDENTICAL_BUFFER_NR_OF_PREVIOUS_SEGMENTS_TO_COMPARE:1;

	memcpy(
		inoutlpDecodedData,
		lpOldData,
		m_uMultiplierDifferentialSize*unMultiplier
		);

	DiffHeader* diffHeaderCurrent = NULL;	
	DiffHeader  diffHeader;
	diffHeaderCurrent = &diffHeader;

	BlockHeader* blockHeader;
	unsigned int unNextBlockHeaderPosition = sizeof(DiffMessageHeader);
	

	unsigned int unPosition = 0;
	unsigned int unPositionTotal = unNextBlockHeaderPosition;
	
	unsigned int lastOffset           = 0;
	unsigned int lastOffsetBlockBegin = 0; 

	lastOffsetBlockBegin -= m_uMultiplierDifferentialSize;
	unsigned int unWhereTo   = 0;
	unsigned int unWhereFrom = 0;
	
	for( unsigned int i=0; i< differentialMessageHeader->nrOfBlocks; i++ )
	{
		lastOffsetBlockBegin+= m_uMultiplierDifferentialSize;
		
		if( i>0 )
		{
			if(bIsIdenticalProccessing)
			{

				if( i>= WAX_DIFF_IDENTICAL_BUFFER_NR_OF_PREVIOUS_SEGMENTS_TO_COMPARE  )
				{
					unWhereFrom = (i-WAX_DIFF_IDENTICAL_BLOCK_OFFESTS_BEHIND) * m_uMultiplierDifferentialSize;
					unWhereTo   = unWhereFrom + (WAX_DIFF_IDENTICAL_BLOCK_OFFESTS_BEHIND*m_uMultiplierDifferentialSize);

					memcpy(
						&inoutlpDecodedData[ unWhereTo   ],  // (i*    m_uMultiplierDifferentialSize) ],
						&inoutlpDecodedData[ unWhereFrom ] , // (i-2)* m_uMultiplierDifferentialSize  ],
						m_uMultiplierDifferentialSize
						);
				}
			}
			else
			{
				unWhereFrom = (i-1) * m_uMultiplierDifferentialSize;
				unWhereTo   = unWhereFrom + m_uMultiplierDifferentialSize;

				memcpy(
					&inoutlpDecodedData[ unWhereTo   ],
					&inoutlpDecodedData[ unWhereFrom ],
					m_uMultiplierDifferentialSize
					);
			}
		}

		blockHeader = (BlockHeader*)&inlpData[unPositionTotal];
		unPositionTotal += sizeof(BlockHeader);

		unPosition = 0;
		unsigned int unMyBlockStrartPosition =i*m_uMultiplierDifferentialSize;
		
		for( byte kO=0; kO< SIZE_OF_KNOWN_OFFSETS; kO++ )
		{
			inoutlpDecodedData[ unMyBlockStrartPosition+gKnown_Offsets[kO] ] = blockHeader->known[kO];
		}

		for ( unsigned int j = 0; j < blockHeader->nrOfDiffHeaders; j++ )
		{
			memcpy( 
				&diffHeader,
				&inlpData[ unPositionTotal ],
				sizeof( DiffHeader)
			);

			unPositionTotal+= sizeof( DiffHeader);
	
			memcpy(
				&inoutlpDecodedData[  unMyBlockStrartPosition + diffHeader.offset ],
				&inlpData[unPositionTotal],
				diffHeader.length
			);

			unPositionTotal +=diffHeader.length;
		}
	}
	
	bool bResult = true;
	//////////////////////////////////////////////////////////////////////////	
	// Validation of message using CRC32 from header.
	//////////////////////////////////////////////////////////////////////////	
	
	if( inoutulDecodedDataLength >0 ) 
	{
		if( differentialMessageHeader->ulCRC32 !=  crc32( 0L, inoutlpDecodedData, inoutulDecodedDataLength ) )
		{
			bResult = false;
		}
	}

	//////////////////////////////////////////////////////////////////////////

	if( ( bResult ) &&
		( !inbTestingDecodeBeforeValidate ) )
	{
		memcpy( 
			lpOldData, 
			inoutlpDecodedData,
			m_uMultiplierDifferentialSize*unMultiplier
			);
	}

	return bResult;
}

//! Differential Compression algorithm implementation 
/*!
*   Implements algorithm as described in the class description.
*   Always returns true.
*/
bool DifferentialCompression::
doDifferentialEncodeBuffer( 
	/*__in*/    /*const*/ unsigned char* inlpData,                   //!< pointer -to the buffer that is processed.
	/*__in*/    const unsigned long  inulDataLength,             //!< size of incoming buffer - inlpData
	/*__inout*/       unsigned char* inoutlpEncodedData,       //!< output buffer
	/*__inout*/       unsigned long& inoutulEncodedDataLength, //!< output buffer length
	/*__in*/    /*const*/ unsigned char* inOldData,                //!< pointer - to the buffer that is processed.
	/*__in*/    const bool   inbIsAlgorithm2For816     //!< is buffer 816 and use algorithm2 for processing
	)
{
	unsigned int unMultiplier        = m_uMultiplierDifferentialSize;
	unsigned int unEncodedDataLength = 0;

	unsigned char* lpOldDATA = inOldData;

	unsigned int unPosition            = 0;
	unsigned int unSizeOfDataToProcess = 0;

	DiffMessageHeader* differentialMessageHeader;
	differentialMessageHeader = (DiffMessageHeader*)&inoutlpEncodedData[0];
	
	differentialMessageHeader->nrOfBlocks = 0;
	differentialMessageHeader->ulCRC32    = crc32( 0L, inlpData, inulDataLength );

	unEncodedDataLength += sizeof(DiffMessageHeader);

	BlockHeader* blockHeader;

	unsigned int sizeOfBlockHeader         = sizeof(BlockHeader);
	unsigned int unLastBlockHeaderPosition = unEncodedDataLength;
	unsigned int unLastNrOfDiffHeaders     = 0;
	unsigned int i = 0;

	for ( i=0; ( i < ( inulDataLength / unMultiplier ) ); i++)
	{
		blockHeader = (BlockHeader*)&inoutlpEncodedData[unLastBlockHeaderPosition];
		unLastBlockHeaderPosition = unEncodedDataLength;

		blockHeader->nrOfDiffHeaders = unLastNrOfDiffHeaders ;

		unLastNrOfDiffHeaders = 0;

		unEncodedDataLength  += sizeOfBlockHeader;

		unSizeOfDataToProcess = unMultiplier*(i+1);
	
		if( i>0 ) 
		{
			if( inbIsAlgorithm2For816 )
			{
				if( i< WAX_DIFF_IDENTICAL_BUFFER_NR_OF_PREVIOUS_SEGMENTS_TO_COMPARE  )
				{
					lpOldDATA = &inOldData[(i)*unMultiplier];
				}
				else
				{
					int comparedTo = i-2;
					lpOldDATA = &inlpData[(i-2)*unMultiplier];
				}
			}
			else
			{
				lpOldDATA = &inlpData[(i-1)*unMultiplier];
			}
		}

		unsigned int positionOnBuffer = i*unMultiplier;
		blockHeader = (BlockHeader*)&inoutlpEncodedData[unLastBlockHeaderPosition];
		
		for(byte kO=0; kO< SIZE_OF_KNOWN_OFFSETS; kO++ )
		{
			blockHeader->known[kO] = inlpData[ (unMultiplier*i)+gKnown_Offsets[kO]];
		}

		while( unPosition < unSizeOfDataToProcess )
		{
			unsigned int unEqualLength = 0;
			unsigned int unDifferentLength = 0;
			unsigned int maxCompare = (unSizeOfDataToProcess-unPosition);
						
			unEqualLength = computeEqualBytes( 
				maxCompare,
				&inlpData   [ unPosition ],
				&lpOldDATA [ unMultiplier - (unSizeOfDataToProcess-unPosition)]
				);

			if ( unEqualLength > 0 )
			{
				unPosition = unPosition + (unEqualLength);
			}
			
			maxCompare = unSizeOfDataToProcess-unPosition;
			
			unDifferentLength = computeDifferentBytes( 
				maxCompare,
				&inlpData   [ unPosition ],
				&lpOldDATA[ unMultiplier - ( unSizeOfDataToProcess-unPosition )]
			);

			if(unDifferentLength>0)
			{
				unPosition += unDifferentLength;

				if( createAndCopydifference( 
					inlpData, 
					inoutlpEncodedData,
					unEncodedDataLength,
					unDifferentLength,
					unPosition-unDifferentLength,
					i*unMultiplier
					) )
				{
					unLastNrOfDiffHeaders++;
				}
			}
		}
	}
	
	// save nr of blocks in message
	differentialMessageHeader->nrOfBlocks = i;
	blockHeader = (BlockHeader*)&inoutlpEncodedData[unLastBlockHeaderPosition];
	blockHeader->nrOfDiffHeaders = unLastNrOfDiffHeaders ;

	inoutulEncodedDataLength = unEncodedDataLength;

	return true;
}

//! Return number of different bytes in last encoded message
unsigned int DifferentialCompression::
getNrOfDifferentBytesFromLastEncode( void )
{
	return m_unNrOfDifferentBytesFromLastEncode;
}

//! Compute the maximum size of encoded buffer - worst case when all bytes are different
void  DifferentialCompression::
computeMaxEncodedBufferLen( void )
{
	m_unMaximumEncodedBufferLen  = 0;
	// size of the message header
	m_unMaximumEncodedBufferLen  += sizeof(DiffMessageHeader); // only 1 message header per encode

	// size of maximum nr of blocks - the number of maximum blocks that a message can have 
	m_unMaximumEncodedBufferLen  += sizeof(BlockHeader) * ( m_unLimitRight / m_uMultiplierDifferentialSize );	

	// size of maximum number of differential tokens 
	m_unMaximumEncodedBufferLen  += sizeof(DiffHeader) * m_unLimitRight; // the number of maximum headers that can be created
	// worst case scenario when all bytes in previous buffer are different
}

//! Return the maximum size of the encoded buffer for current setting.
/*!
    This value can change after a setParameter call. Caller must make sure that this
	call is made after a setParameter call.
*/
unsigned int DifferentialCompression::
getMaxEncodedBufferLen( void )
{
	return m_unMaximumEncodedBufferLen;
}