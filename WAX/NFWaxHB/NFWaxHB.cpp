//********************************************************/
//  ./NFWaxHB.cpp
//
//  Owner : Ciprian Maris
//  
//	Company		: Neverfail 
//	PROJECT		: WAN Acceleration 
//  Component	: Neverfail - WAX Interface
//
//
//  $Rev: 5451 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: KMillar $  
//  $Date: 2010-11-10 12:17:34 +0000 (Wed, 10 Nov 2010) $
//
//********************************************************/

#include "NFWaxHB.h"

#include "CompressorKnowledge.h"
#include "AllUtils.h"
#include "ErrorCodeMappings.h"

#include "Logger/Logger.h"

#define ZLIB_WINAPI
#include "zlib.h"

#ifdef WAX_COMPRESSOR_IN_HB_PROCESS
//	When WAX_COMPRESSOR_IN_HB_PROCESS IS defined, we will link to the WAXCompressor.dll
//	directly (aka run in process).
#define WAXPROVIDER
#include "WAXCompressor.h"

bool GBNeedToInitializeLogger = true;	//	BZ 8261 - in case the WAX compressor runs in process
										//	this module needs to initialize the logger itself

#else
//	When WAX_COMPRESSOR_IN_HB_PROCESS IS NOT defined, we will call the WAXCompressor.dll
//	functions through the IPC bridge (aka run out-of process). The side of the IPC bridge 
//	in this unit is the IPCClient component.
#define WAXPROVIDER IPCClient::
#include "IPCClient.h"

bool GBNeedToInitializeLogger = false;	//	BZ 8261 - in case the WAX compressor runs out of process
										//	the logger will be initialized by the IPC component

#endif


///////////////////////////////////////////////////////////////////////////////////////////////////
//////		Type definitions used in this unit only		///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//////		Encode Header and Checksum management functions

//! EncodeHeader - the header of a encoded buffer.
/*! The encode header structure holds the:
*		- compression flags			- 3xBYTE 
*		- header checksum			- 2xBYTE
*		- un-encoded buffer length	- 3xBYTE
*
*	NOTE:	[1] The checksum is represented by the ChecksumByteOne member and the high order byte of the  
*			ulOriginalBufferLength member which is always zero since a NF buffer length will not excede 
*			16 MB's. The maximum length of a NF message buffer is 256KB' (Jan 2009).
**/
typedef struct _EncodeHeader
{
	unsigned long ulOriginalBufferLength; //The high order byte is the ChecksumByteTwo of the header.
	BYTE ChecksumByteOne;
	BYTE RPR;
	BYTE DD;
	BYTE ZLIB;
	
	_EncodeHeader()
	{
		RPR		= 0;
		DD		= 0;
		ZLIB	= 0;

		ChecksumByteOne = 0;
		ulOriginalBufferLength	= 0;
	}

}EncodeHeader;

//!The offset at which the header checksum is located within che header buffer.
#define HEADER_CHECKSUM_OFFSET (3 * sizeof(BYTE))

//! Computes the header checksum and saves it in the header. 
WORD __stdcall computeHeaderChecksum( 
	/*IN*/ /*OUT*/ unsigned char* inoutEncodeHeader //!< Header for which the checksum is computed.
	);

//! Checks if the computed header checksum matches the checksum saved in the header.
bool __stdcall checkHeaderChecksum( 
	/*IN*/ unsigned char* inEncodeHeader,						//!< Header that is being checked for checksums.
	/*OUT*/ unsigned char* outEncodeHeaderWithNoChecksum = NULL //!< Return adress of the header with no checksum. 
	);

//////		END Encode Header and Checksum management functions
///////////////////////////////////////////////////////////////////////////////


//! In HB Process Statistics structure holds the set of compression provider statistics that can only be 
//	computed in hb process because the compressor does not have ZLIB compression information.
typedef struct _InHBProcessStatistics
{
	uint64_t inputSize;
	uint64_t outputSize;

	uint64_t compRateCurrent;
	uint64_t dataVolumeCurrent;

	uint64_t dataVol3AfterZlib;

	FILETIME startTime;
	FILETIME stopTime;

}InHBProcessStatistics;

//! Start parameters structure holds the set of compression provider paramaters that are relevant here.
//	These paramaters will be used in this unit only and they do not need to be passed to the WAX compressor.
typedef struct _InHBStartParamaters
{
	bool bIsEncoder;	//Server role. Compression stats are only provided for the encoder role.

	double dNInvariant;	//Factor used to compute the current compression rate.

	//!	Minimum buffer length to be de-duplicated. If the incomming buffer length is smaller than this
	//	value, a call to the WAXCompressor.dll will not be made.
	unsigned long ulMinDeDupeLength;
	unsigned long ulMinZLibLength;

}InHBStartParamaters;

///////////////////////////////////////////////////////////////////////////////////////////////////
//////		END Type definitions used in this unit only		///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////
//////		Constants and global variables used in this unit only		///////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

#define LEN_OVERHEAD_NFCMLIB sizeof(EncodeHeader)   //Overhead of the compression manager.

//! Temporary buffer used in encode/decode when both ZLIB and DD are used.
#define LEN_TMP_BUFFER 512 * KILO
ByteBuffer* GBBTmpBuffer	= NULL;

ByteBuffer* GBBOutputBuffer		= NULL;
ByteBuffer* GBBDecodeSrc		= NULL;

//! Header used in decode that has no checksum (correct original (un-encoded) length)
EncodeHeader* GEncodeHeaderNoChecksum = new EncodeHeader();

//!	Return code for functions in the WaxCompressor.dll - it is stored here for it to be retreived
//	by the compression manager. This is the only place where the retun code is stored.
//	It will be reset on each function invocation.
WAX_RESULT GWRReturnCode = WAX_RESULT_OK;

InHBProcessStatistics GInHBProcessStatistics;
InHBStartParamaters   GInHBStartParameters;

//! This flag is set to TRUE is:
//		- WAXPROVIDER initialize failed
//		- could not contact WAX Service to make the out of process invokation (IPC error).
//		- an error occured in the advanced compressor either on encode or decode.
//
//	This flag is reset prior to making calls to the advanced compressor and set if an error was 
//	encounterd in the WaxCompressor.dll code.
//	This flag is the return value of the isErrorInAdvancedCM() function.
bool GBErrorInWaxCompressor = false;

//!	The ErrorCodeMappings component transforms a WAX_RESULT error code into a meaningfull string.
//	It is used by getErrorInfoCM() to return the info string for the last call into the compressor.
//	This variable is required by the rezolution for BZ 7589.
ErrorCodeMappings GErrorCodeToInfoStringMapping;

///////////////////////////////////////////////////////////////////////////////////////////////////
//////		END Constants and global variables used in this unit only		///////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////////////////////////
////	STATE Management of the Compressor	///////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	This unit manages the flags for standard and advanced compression availability:
//		- GBStandardCompressionInitialized - true if the NF WAX HB component is initialized for standard compression
//		- GBAdvancedCompressionInitialized - true if the NF WAX HB component is initialized for advanced compression
//	The two flags are set by the initializeCM() / exitCM().
//	
//	GBAdvancedCompressionInitialized is set to FALSE in the "out of process" operation mode by
//	allocateCM(), startCM(), encodeCM() and decodeCM() - if an IPC error occurs.
//
//	If GBStandardCompressionInitialized is FALSE: the compressor is in a non initialized state and can not be used
//	for either standard or advanced encode/decode operations.
//
//	If GBAdvancedCompressionInitialized is FALSE and GBStandardCompressionInitialized is TRUE:
//		- The calls to allocateCM(), startCM() will fail 
//		- encodeCM() calls with advanced compression required will fail
//		- decodeCM() calls that require advanced de-compression will fail
//		- calls to stopCM(), releaseCM(), exitCM() will succeede
//

//! Flag that states that the NF WAX HB component is initialized for standard compression
bool GBStandardCompressionInitialized = false;

//! Flag that states that the NF WAX HB component is initialized for advanced compression
bool GBAdvancedCompressionInitialized = false;

///////////////////////////////////////////////////////////////////////////////////////////////////
////	END STATE Management of the Compressor	///////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////////////////////////
//////		Utility Functions used in this unit only		///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

//! Checks if the input buffer is not NULL and was previously encoded.
bool __stdcall checkIfValidEncodedBuffer( 
	/*IN*/ ByteBuffer* inEncodedByteBuffer,
	/*OUT*/ unsigned char* outEncodeHeaderWithNoChecksum = NULL
	);

//! Checks if the input error code is caused by the IPc component.
bool __stdcall isIPCErrorCode( 
	/*IN*/ WAX_RESULT inErrorCode
	);

//!	Sets the component state flags pertaining to advanced compression according to the input error code.
void __stdcall setStateFlagsForAdvanced(
	/*IN*/ WAX_RESULT inErrorCode
	);

//! Transforms the ZLIB error code to WAX_RESULT error code
WAX_RESULT __stdcall getZLIBToWaxResult( 
	/*IN*/ int iniZlibErrorCode
	);

///////////////////////////////////////////////////////////////////////////////////////////////////
//////		END Utility Functions used in this unit only		///////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////


//-------------------------------------------------------------------------------------------------


BOOL APIENTRY DllMain( HMODULE hModule,
                       unsigned long  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		// onAttach()
		break;
	case DLL_PROCESS_DETACH:
		// onDetach()
		break;
	}
    return TRUE;
}
//-------------------------------------------------------------------------------------------------


//! Function getDLLRevisionString() - Returns the revision string of the Compressor DLL.
wchar_t* __stdcall getDLLRevisionStringCM()
{
	return DLL_REVISION_CM;
}
//-----------------------------------------------------------------------------


//! Function InitializeByteBuffer() - initializes a byte buffer struct.
/*!
* The InitializeByteBuffer function initializes a byte buffer struct
* type with a buffer pointer, length and maximum length.
*/
inline void InitializeByteBuffer(
	/*OUT*/ ByteBuffer* outByteBuffer, 
	/*IN*/ unsigned char* buffer, 
	/*IN*/ long length, 
	/*IN*/ long maximumLength = 0
	)
{
	outByteBuffer->buffer = buffer;
	outByteBuffer->length = length;
	outByteBuffer->maximumLength = ( maximumLength != 0) ? maximumLength : length;
}
//-------------------------------------------------------------------------------------------------


//! Function initializeCM() - Initializes the compression provider.
/*
*	The function 
*		- initializez standard and advanced compression components.
*		- calls the IPC Client initialize for the out of process WAX Compressor(compression provider)
*		scenario. This in turn will result in a call to the provider initialize via IPC.
*		- calls the WAX Compressor(compression provider) initalize directly for the in process scenario.
*
*	Return
*		- true - if both standard and advanced are initialized.
*		- false if either standard and advanced initialization fails. 
*		If the advanced compression initialization fails, the GBErrorInWaxCompressor flag is set. 
*		This flag is the return value of the isErrorInAdvancedCM() function that tells the caller 
*		that advanced compression initalization has failed.
*
*	NOTE: For the out of process scenario the IPC client MUST call initSecurity(). This is not needed 
*	for the in process scenario since the WAXCompressor DLL does that.
*/
bool __stdcall initializeCM(
	/*IN*/ StringBuffer * logFileName,
	/*IN*/ long debugMode,
	/*IN*/ bool isAdvanced
	)
{
	GBErrorInWaxCompressor = false;//reset the ErrorInAdvanced flag.

	//	NOTE: The check here is done for both flags due to the fact that we allow
	//	for a second initialize() call if the first one did not initialize advanced
	//	compression.
	//	TO DO: check this statement with Lyndon.
	if( GBStandardCompressionInitialized && GBAdvancedCompressionInitialized )
	{//	Do not initialize twice.
		GWRReturnCode = WAX_RESULT_ALREADY_INITIALIZED;
		return false;
	}

	//Reset initalization flags => make sure that a "return false" leaves them in a consistent state.
	GBStandardCompressionInitialized = false;
	GBAdvancedCompressionInitialized = false;

	//Initialize the statistics.
	GInHBProcessStatistics.inputSize			= 0;
	GInHBProcessStatistics.outputSize			= 0;
	GInHBProcessStatistics.dataVol3AfterZlib	= 0;
	
	GInHBProcessStatistics.compRateCurrent		= 0;
	GInHBProcessStatistics.dataVolumeCurrent	= 0;

	//BZ 8588 - dNInvariant needs to be set to the default value for 
	//standard compression as well although it is only contained in the 
	//"start parameters" structure.

	//We just need a variable to pass in the following call.
	//This value is never checked because we are asking for the default value 
	//of NInvariant, call that can not fail.
	bool bNInvariantSet = true;
	GInHBStartParameters.dNInvariant = getConfigManagerNInvariantValue(0.0, bNInvariantSet);

	//Allocate the intermediary buffer for the compressor
	if ( !AllocateOrReleaseByteBuffer( GBBTmpBuffer, LEN_TMP_BUFFER ) )
	{//Start failed - could not allocate the intermediary buffer.

		GWRReturnCode = WAX_RESULT_BUFFER_ALLOCATION_FAILED;
		AllocateOrReleaseByteBuffer( GBBTmpBuffer, 0 );

		//If this happens we probably are in a very bad shape.
		return false;//Let the caller deal with this error.
	}

	GBBOutputBuffer	= new ByteBuffer();
	GBBDecodeSrc	= new ByteBuffer();

	//At this point the standard compressor is available => set the appropriate flag
	GBStandardCompressionInitialized = true;

	//	Do not attempt to initialize advanced compression if is not required
	if( !isAdvanced )
	{
		return GBStandardCompressionInitialized;
	}

	//	BZ 8261 - In the case the WAX compressor runs in process the logger needs to be initialized here as well.
	if( GBNeedToInitializeLogger )
	{
		wstring strLoggerInitErrorText = L"";
		if( !Logger::ComposeNameAndInitialize( logFileName->buffer, LOG_DEFAULT_NAME, LOG_HB_ADDON, 
			( debugMode != 0 ) ? true : false, 
			strLoggerInitErrorText ) )
		{
			GWRReturnCode = WAX_RESULT_LOG_INIT_FAILED;
			return false;
		}
	}

	GWRReturnCode = WAXPROVIDER initialize( logFileName->buffer, ( debugMode != 0 ) ? true : false );

	if( GWRReturnCode == WAX_RESULT_OK )
	{
		GBAdvancedCompressionInitialized = true; //set the initialized flag.
	}
	else
	{
		//	Initialization of the advanced compressor failed => set the proper flag.
		//	The failure reason could be because of IP Comunication or another reason.
		//	The caller is only intersted if the advanced compressor is initialized or not.
		GBErrorInWaxCompressor = true;
	}

	//	BZ 8261 - also log sucess/failure event
	if( GWRReturnCode == WAX_RESULT_OK )
	{
		LogEventMessage(LOG_LEVEL_INFO, 0, L"HB-CompressionProvider interface initlialized.");
		return true;
	}
	else
	{
		LogEventMessage(LOG_LEVEL_INFO, 0, L"HB-CompressionProvider failed to initialize interface. Check the compressor logs for details.");
		return false;
	}
}
//-------------------------------------------------------------------------------------------------


//! Function exitCM() - Ends the lifespan of the compressor. 
bool __stdcall exitCM( void )
{
	if( !GBStandardCompressionInitialized )
	{//	Do nothing if not initialized. Just return true.
		GWRReturnCode = WAX_RESULT_OK;
		return true;
	}

	GBStandardCompressionInitialized = false; //set the initialized flag.

	//Release the tenporary buffer
	AllocateOrReleaseByteBuffer( GBBTmpBuffer, 0 );

	if( GBBOutputBuffer )
	{
		delete GBBOutputBuffer;
		GBBOutputBuffer	= NULL;
	}

	if( GBBDecodeSrc )
	{
		delete GBBDecodeSrc;
		GBBDecodeSrc	= NULL;
	}

	if( !GBAdvancedCompressionInitialized )
	{//	Do nothing if not initialized. Just return true.
		GWRReturnCode = WAX_RESULT_OK;
		return true;
	}

	GBAdvancedCompressionInitialized = false;//set the advanced init flag.

	//	We can call advanced compressor exit here since the GBAdvancedCompressionInitialized
	//	flag was checked previously.
	GWRReturnCode = WAXPROVIDER exit( );

	return (GWRReturnCode == WAX_RESULT_OK) ;
}
//-------------------------------------------------------------------------------------------------


//! Function getErrorCodeCM() return the error code of the last failed call.
//	NOTE: this interface function does not depend on NFWaxHB.dll being initialized.
unsigned long  __stdcall getErrorCodeCM(void)
{
	return GWRReturnCode;
}
//-------------------------------------------------------------------------------------------------


//! Function getErrorInfoCM() - Retreives the error information (message) of the last failed call from the compressor.
//	NOTE: this interface function does not depend on NFWaxHB.dll being initialized.
const wchar_t* __stdcall getErrorInfoCM(void)
{
	//	BZ 7589 - replace verbose DEBUG error messages returned by "WAXPROVIDER getErrorInfo()"
	//	with a descriptive short messages obtained by interpretting the compressor error code.
	return GErrorCodeToInfoStringMapping.getErrorString( GWRReturnCode );
}
//-------------------------------------------------------------------------------------------------


//Allocate - release functions

//! Function allocateCM() - allocates the compressor pre-booked resources.
bool __stdcall allocateCM(
	/*IN*/ long minimumMemoryBufferSize,
	/*IN*/ long maximumMemoryBufferSize,
	/*IN*/ long transferLogSize,
	/*IN*/ StringBuffer * transferLogPath
	)
{
	GBErrorInWaxCompressor = false;//reset the flag.

	if( !GBAdvancedCompressionInitialized )
	{//	Do nothing if not initialized. Just return the NOT_INITIALIZED error.
		GWRReturnCode = WAX_RESULT_WAX_HB_ADVANCED_NOT_INITIALIZED;
		
		//	BZ 7587 - set the GBErrorInWaxCompressor flag to true. This will tell the caller
		//	that advanced compression is not available.
		GBErrorInWaxCompressor = true;

		return false;
	}

	GWRReturnCode = WAXPROVIDER allocate( 
									minimumMemoryBufferSize,
									maximumMemoryBufferSize,
									transferLogSize,
									transferLogPath );
	
	//Check return code and set the flags in case of error.
	if(GWRReturnCode == WAX_RESULT_OK) 
	{
		return true;
	}
	else
	{
		setStateFlagsForAdvanced( GWRReturnCode );

		return false;
	}
}
//-------------------------------------------------------------------------------------------------


//! Function releaseCM() - releases the compressor pre-booked resources.
bool __stdcall releaseCM(void)
{
	if( !GBStandardCompressionInitialized )
	{//	Do nothing if not initialized. Just return the NOT_INITIALIZED error.
		GWRReturnCode = WAX_RESULT_NOT_INITIALIZED;
		return false;
	}

	if( GBAdvancedCompressionInitialized )
	{
		GWRReturnCode = WAXPROVIDER release ();
		return (GWRReturnCode == WAX_RESULT_OK) ;
	}
	else
	{
		return true;
	}
}
//-------------------------------------------------------------------------------------------------


//Start-stop functions

//! Function startCM() - starts the compressor.
bool __stdcall startCM(
		/*IN*/ bool asEncoder,
		/*IN*/ long signatureBase,
        /*IN*/ long signatureDistance,
        /*IN*/ long minRemoveRepeatedPatternsLength,
		/*IN*/ long minDeDuplicateLength,
		/*IN*/ long minDeDuplicateLengthReplacedByToken,
        /*IN*/ long minZLibCompressLength,
		/*IN*/ bool inEncodeOutOfBoundaryEnabled,
		/*IN*/ double indNInvariant 
		)
{
	GBErrorInWaxCompressor = false; //reset the flag.

	if( !GBAdvancedCompressionInitialized )
	{//	Do nothing if not initialized. Just return the NOT_INITIALIZED error.
		GWRReturnCode = WAX_RESULT_WAX_HB_ADVANCED_NOT_INITIALIZED;
		
		//	BZ 7587 - set the GBErrorInWaxCompressor flag to true. This will tell the caller
		//	that advanced compression is not available.
		GBErrorInWaxCompressor = true;

		return false;
	}

	//	Check and Save the start parameters.
	GInHBStartParameters.bIsEncoder			= asEncoder;
	
	//	BZ 7543 - check bValidParamValue return parameter in case the input value is incorrect
	bool bValidParamValue;

	GInHBStartParameters.ulMinDeDupeLength = getConfigManagerMinDeDuplicateLength( 
												(unsigned long)minDeDuplicateLength, bValidParamValue );
	if( !bValidParamValue )
	{
		GWRReturnCode = WAX_RESULT_INVALID_START_PARAMETERS;
		LogError(0, DBG_LOCATION, L"Start parameters error: Invalid minimum DD length");
		return false;
	}

	GInHBStartParameters.ulMinZLibLength = getConfigManagerMinZLibCompressLength(
												(unsigned long)minZLibCompressLength, bValidParamValue);
	if( !bValidParamValue )
	{
		GWRReturnCode = WAX_RESULT_INVALID_START_PARAMETERS;
		LogError(0, DBG_LOCATION, L"Start parameters error: Invalid minimum ZLIB length");
		return false;
	}

	GInHBStartParameters.dNInvariant = getConfigManagerNInvariantValue( 
											indNInvariant, bValidParamValue );
	if( !bValidParamValue )
	{
		GWRReturnCode = WAX_RESULT_INVALID_START_PARAMETERS;
		LogError(0, DBG_LOCATION, L"Start parameters error: Invalid DInvariant value");
		return false;
	}

	//Allocation of intermediary buffer succes -> move on to start.
	GWRReturnCode = WAXPROVIDER start(
						asEncoder,
						signatureBase,
						signatureDistance,
						minRemoveRepeatedPatternsLength,
						minDeDuplicateLength,
						minDeDuplicateLengthReplacedByToken,
						inEncodeOutOfBoundaryEnabled,
						indNInvariant 
						);

	//Check return code and set the flags in case of error.
	if(GWRReturnCode == WAX_RESULT_OK) 
	{
		//Log the start parameters
		LogEventFormated( LOG_LEVEL_INFO, 0, L"Standard compression start parameter: MinZlibLength = %d ", GInHBStartParameters.ulMinZLibLength );
		return true;
	}
	else
	{
		setStateFlagsForAdvanced(GWRReturnCode);

		return false;
	}
}
//-------------------------------------------------------------------------------------------------


//! Function stopCM() - stops the compressor.
bool __stdcall stopCM(void)
{
	if( !GBStandardCompressionInitialized )
	{//	Do nothing if not initialized. Just return the NOT_INITIALIZED error.
		GWRReturnCode = WAX_RESULT_NOT_INITIALIZED;
		return false;
	}

	if( GBAdvancedCompressionInitialized )
	{
		GWRReturnCode = WAXPROVIDER stop();
		return (GWRReturnCode == WAX_RESULT_OK) ;
	}
	else
	{
		return true;
	}
}
//-------------------------------------------------------------------------------------------------


//! Function encodeCM() - encodes the input buffer or computes the length needed to hold the encoded buffer.
/*! The encode() function has two operation modes:
*	[1] When the output parameter length is zero, it will return the needed buffer length to hold the 
*	compressed buffer according to compression parameter settings.
*
*	[2] When the output parameter length is NON zero, it will compress the input buffer according to 
*	compression parameter settings.
*	In this case it will also set the compression flags as defined by the EncodeHeader structure
*	in this unit.
*
*	NOTE: The case when no compression is applied to the input buffer because it does not meet any of
*	the compression criteria is not considered an error. In this case the input buffer is simply copied 
*	to the output buffer.
*
*/
bool __stdcall encodeCM(
	/*IN*/ ByteBuffer * inputBuffer,
	/*OUT*/ ByteBuffer *outbbOutputBuffer,
	/*IN*/ bool removeRepeatedPatterns,
	/*IN*/ bool deDuplicate,
	/*IN*/ long zLibCompressionLevel
	)
{
	//Reset the error in wax compressor flag. See variable declaration for details.
	GBErrorInWaxCompressor = false;

	if( !GBStandardCompressionInitialized )
	{//	Do nothing if not initialized. Just return the NOT_INITIALIZED error.
		GWRReturnCode = WAX_RESULT_NOT_INITIALIZED;
		return false;
	}

	//Case when input buffer length is zero.
	//This means that the compression manager asks us for the needed buffer length;
	if( outbbOutputBuffer->maximumLength == 0 )
	{
		//NOTE: Specs from nfcmlib
		//	If the outputBufferLength is zero the method returns the maximum possible length of the buffere
		//	needed to store the encoded output of the specified input buffer length and parameters;

		//	Call CompressorKnowledge to compute the needed output for DD and RPR
		outbbOutputBuffer->length = getEncodeOutputLength( inputBuffer->length, removeRepeatedPatterns, deDuplicate );

		if( zLibCompressionLevel )
		{
			//In the case we have the ZLIB overhead
			
			//	BZ 7523 - Compute overhead for ZLIB Compression. 
			//	The ZLIB home page at http://zlib.net/zlib_tech.html - section "Maximum Expansion Factor" states :
			//		"The default settings used by deflateInit(), compress(), and compress2(), the only expansion 
			//		is an overhead of five bytes per 16 KB block (about 0.03%), plus a one-time overhead of six 
			//		bytes for the entire stream. Even if the last or only block is smaller than 16 KB, the overhead 
			//		is still five bytes."
			//
			////	Since there is no mention of other settings than the default we are going to act with caution
			//		We will add at least (inputLen/2^8) + 24 for ZLIB which is in al cases at least double than
			//		[(inputLength / 2^14) + 1] * 5 + 6
			outbbOutputBuffer->length += ( ( outbbOutputBuffer->length >> 8 ) + 24 );
		}

		//Add the Compression Manager overhead
		outbbOutputBuffer->length += LEN_OVERHEAD_NFCMLIB;

		//also set the maximum length
		outbbOutputBuffer->maximumLength = outbbOutputBuffer->length;

		//since there is nothing more to do here - return.
		return true;

	}//END compute needed buffer length.
	else
	{//Compress the buffer and set the compression flags;

		//	Initialize the return code to NO ERROR.
		//	NOTE: The case when no compression is applied to the input buffer because it does not meet any of
		//	the compression criteria is not considered an error.
		GWRReturnCode = WAX_RESULT_OK;

		//Update stats.
		GInHBProcessStatistics.inputSize += inputBuffer->length;

		EncodeHeader* encodeHeader = (EncodeHeader*)outbbOutputBuffer->buffer;
		memset( encodeHeader, 0, sizeof( EncodeHeader ) );

		//Set the original buffer length (prior to any encoding)
		encodeHeader->ulOriginalBufferLength = inputBuffer->length;

		//Use a temporary byte buffer as a output destination.
		InitializeByteBuffer( GBBOutputBuffer, 
							  outbbOutputBuffer->buffer + sizeof( EncodeHeader ),
							  outbbOutputBuffer->length - sizeof( EncodeHeader ) );

		//Assign zlib source as the input buffer in case there is no DD.
		ByteBuffer* bbZLibSource = inputBuffer;

		//Check for advanced compression criteria:
		if( ( removeRepeatedPatterns || deDuplicate ) &&					 //DD or RPR required by caller
			( inputBuffer->length >= GInHBStartParameters.ulMinDeDupeLength )//input buffer meets the length criterion
			)
		{
			if ( !GBAdvancedCompressionInitialized )
			{
				GWRReturnCode = WAX_RESULT_ADVANCED_ENCODE_NOT_AVAILABLE;
				//	BZ 7587 - set the GBErrorInWaxCompressor flag to true. This will tell the caller
				//	that advanced compression is not available.
				GBErrorInWaxCompressor = true;
				return false;
			}

			//The assumption is that we will always have ZLIB after DD so Compress into intermediary buffer.
			GBBTmpBuffer->length = LEN_TMP_BUFFER;//reset the length of the temporary buffer.

			GWRReturnCode = WAXPROVIDER encode( inputBuffer, GBBTmpBuffer, 
												removeRepeatedPatterns, deDuplicate );

			if( GWRReturnCode != WAX_RESULT_OK )
			{
				//Also sets GBErrorInWaxCompressor if this is an IPC error
				setStateFlagsForAdvanced( GWRReturnCode );

				//Set the isErrorInAdvanced flag if the criteria are met for encode/decode:
				if( (GWRReturnCode != WAX_RESULT_WRONG_ENCODE_CALL) &&	//not an wrong encode call
					(GWRReturnCode != WAX_RESULT_NOT_INITIALIZED) )		//not an not_initialized
				{
					//	This fault occured in the advanced compressor.
					GBErrorInWaxCompressor = true;
				}

				//Let the compression flags unset and return error.
				return false;
			}

			//Set the flags.
			encodeHeader->DD	= (BYTE)deDuplicate;
			encodeHeader->RPR	= (BYTE)removeRepeatedPatterns;

			//Set the ZLib Source buffer.
			bbZLibSource = GBBTmpBuffer;
		}

		if( zLibCompressionLevel && ( bbZLibSource->length >= GInHBStartParameters.ulMinZLibLength ) )
		{//	ZLIB (standard compression) required and the input buffer meets the length criteria.
			int iZLibRetCode = compress2( 
					GBBOutputBuffer->buffer, &GBBOutputBuffer->length, 
					bbZLibSource->buffer, bbZLibSource->length, (int)zLibCompressionLevel 
					) ;
			if ( iZLibRetCode != Z_OK )
			{
				GWRReturnCode = getZLIBToWaxResult( iZLibRetCode );

				//	NOTE: if this ZLIB compress fails, and we had a DD encode, the TLog is updated.
				//	This introduces a desynch between source and target TLogs since the current DD
				//	encoded buffer will never be seen by the decoder.
				GBErrorInWaxCompressor = true;
				return false;
			}
			
			encodeHeader->ZLIB = (BYTE)zLibCompressionLevel;

			//Update stats - for the case when we had ZLIB compression.
			GInHBProcessStatistics.dataVol3AfterZlib += GBBOutputBuffer->length;

		}
		else
		{//In the case we did not ZLIB compress, we must copy the ZLIB source buffer to the destination buffer.
			memcpy( GBBOutputBuffer->buffer , bbZLibSource->buffer, bbZLibSource->length );
			GBBOutputBuffer->length = bbZLibSource->length;
		}

		//Add the size of the flags.
		outbbOutputBuffer->length = GBBOutputBuffer->length + LEN_OVERHEAD_NFCMLIB;

		//Compute and add the header checksum to the header.
		computeHeaderChecksum( outbbOutputBuffer->buffer );

		//Update stats - the total output length.
		GInHBProcessStatistics.outputSize += outbbOutputBuffer->length;

	}//END 	Compress the buffer and set the compression flags;


	return (GWRReturnCode == WAX_RESULT_OK) ;
}
//-------------------------------------------------------------------------------------------------


//!Function decodeCM() - decodes the input buffer or determines the length needed to hold the decoded buffer.
/*! The decode() function has two operation modes:
*	[1] When the output parameter length is zero, it will return the needed buffer length to hold the 
*	decoded buffer.
*
*	[2] When the output parameter length is NON zero, it will de-code the input buffer according to 
*	compression parameter settings that are set in the beggingin of the encoded buffer as the EncodeHeader 
*	structure defined in this unit.
*
*	NOTE: The case when the current input buffer is not compressed in any way (DD/RPR/ZLIB) is not considered 
*	an error. In this case the input buffer is simply copied to the output buffer. This ensures symetry with
*	the encode process.
*	
*	The decode method supports the folowing combinations of compression flags:
*		- (DD && RPR)==0, ZLIB==0 - in this case the input buffer is copied to the ouput buffer.
*		- (DD && RPR)==0, ZLIB==1 - in this case ZLIB-uncompress the input buffer into the output buffer.
*		- (DD && RPR)==1, ZLIB==0 - in this case DD/RPR-decode the input buffer into the output buffer.
*		- (DD && RPR)==1, ZLIB==1 - in this case DD/RPR-decode the input buffer into the output buffer.
*
*/
bool __stdcall decodeCM(
	/*IN*/ ByteBuffer	*inputBuffer,
	/*OUT*/ ByteBuffer	*outputBuffer,
	/*IN*/ bool isAdvanced
	)
{
	//Reset the error in wax compressor flag. See variable declaration for details.
	GBErrorInWaxCompressor = false;

	if( !GBStandardCompressionInitialized )
	{//	Do nothing if not initialized. Just return the NOT_INITIALIZED error.
		GWRReturnCode = WAX_RESULT_NOT_INITIALIZED;
		return false;
	}

	//First check if the current buffer was ever encoded.
	if ( !checkIfValidEncodedBuffer( inputBuffer, (unsigned char*)GEncodeHeaderNoChecksum ) )
	{
		//!!!Do not set GWRReturnCode since it is set in checkIfValidEncodedBuffer();
		return false;
	}

	if( GEncodeHeaderNoChecksum->DD || GEncodeHeaderNoChecksum->RPR )
	{
		if ( !isAdvanced )
		{
			GWRReturnCode = WAX_RESULT_UNCOMPRESSION_NEEDS_ADVANCED;
			GBErrorInWaxCompressor = true; //Also set the isErrorInAdvanced to true.
			return false;
		}
	}

	//Case when input buffer length is zero.
	//This means that the compression manager asks us for the needed buffer length;
	if( outputBuffer->maximumLength == 0 )
	{
		//	NOTE: Specs from nfcmlib
		//	If the outputBufferLength is zero the method returns the maximum
		//	possible length of the buffer needed to store the decoded output of
		//	the specified input buffer.

		//Read the original length from the encoded buffer header (EncodeHeader)
		outputBuffer->length = GEncodeHeaderNoChecksum->ulOriginalBufferLength;
		outputBuffer->maximumLength = outputBuffer->length;
		return true; //just return now.
	}
	else
	{//Do the actual decoding.

		//	Initialize to no error - returned in case no decompression was ever perfromed
		//	because the source for compression did not meet any of the compression criteria.
		GWRReturnCode = WAX_RESULT_OK;

		//	Signals if the decode has been perfromed on the current input buffer.
		//	If not, the input buffer needs to be copied to the output buffer.
		bool bDecoded = false;

		//This initialisation is done to step over the Encode header and to have a common
		//ByteBuffer* for all decode operations.
		//IMPORTANT: step over the EncodeHeader!
		InitializeByteBuffer( GBBDecodeSrc,
							  inputBuffer->buffer + sizeof(EncodeHeader),
							  inputBuffer->length - sizeof(EncodeHeader) );


		//Get the compression flags needed for de-coding.
		GBBTmpBuffer->length = LEN_TMP_BUFFER;//reset the length of the temporary buffer.

		if( GEncodeHeaderNoChecksum->ZLIB )
		{//We need to ZLIB un-compress first.

			//	To avoid another buffer copy we check if we have a DD and/or RPR following ZLIB.
			//	If not, we will decode directly into the output buffer.
			if( GEncodeHeaderNoChecksum->DD || GEncodeHeaderNoChecksum->RPR )
			{//we will have DD/RPR decoding following the ZLIB uncompress => ZLIB-uncompress
			 //in the temporary buffer.
				if( uncompress( GBBTmpBuffer->buffer, &GBBTmpBuffer->length, GBBDecodeSrc->buffer, GBBDecodeSrc->length )
					!= Z_OK )
				{
					GWRReturnCode = WAX_RESULT_ZLIB_UNCOMPRESSION_FAILED;
					return false;
				}

				//ZLIB uncompress succes - assign the DD source as the temporary buffer.
				InitializeByteBuffer( GBBDecodeSrc, GBBTmpBuffer->buffer, GBBTmpBuffer->length );
			}
			else
			{//there is no DD/RPR decoding following the ZLIB uncompress => ZLIB-uncompress
			 //in the output buffer
				if( uncompress( outputBuffer->buffer, &outputBuffer->length, GBBDecodeSrc->buffer, GBBDecodeSrc->length )
					!= Z_OK )
				{
					GWRReturnCode = WAX_RESULT_ZLIB_UNCOMPRESSION_FAILED;
					return false;
				}
			}

			bDecoded = true;//already decoded.
		}

		//Perfrom DD and/or RPR decode.
		if( GEncodeHeaderNoChecksum->DD || GEncodeHeaderNoChecksum->RPR )
		{
			if ( !GBAdvancedCompressionInitialized )
			{
				GWRReturnCode = WAX_RESULT_ADVANCED_DECODE_NOT_AVAILABLE;
				//	BZ 7587 - set the GBErrorInWaxCompressor flag to true. This will tell the caller
				//	that advanced compression is not available.
				GBErrorInWaxCompressor = true;
				return false;
			}
			
			//DD and/or RPR decode - if we have that we will decode directly in the output buffer
			GWRReturnCode = WAXPROVIDER decode( 
								GBBDecodeSrc, outputBuffer, 
								(GEncodeHeaderNoChecksum->RPR != 0),
								(GEncodeHeaderNoChecksum->DD  != 0) );

			if( GWRReturnCode != WAX_RESULT_OK )
			{
				//Also sets GBErrorInWaxCompressor if this is an IPC error
				setStateFlagsForAdvanced( GWRReturnCode );

				//Set the isErrorInAdvanced flag if the criteria are met for encode/decode:
				if( (GWRReturnCode != WAX_RESULT_WRONG_DECODE_CALL) &&	//not a wrong call
					(GWRReturnCode != WAX_RESULT_NOT_INITIALIZED) )		//not a not initialized state
				{
					//	This fault occured in the advanced compressor.
					GBErrorInWaxCompressor = true;
				}

				//Return error.
				return false;
			}

			bDecoded = true;//already decoded.
		}//END - DD and/or RPR decode

		if (!bDecoded )
		{//NO decoding was perfromed. Copy the input buffer to the output buffer.
			outputBuffer->length = GBBDecodeSrc->length ;
			memcpy( outputBuffer->buffer, GBBDecodeSrc->buffer , GBBDecodeSrc->length  );
		}
	}

	//If we got here, the return result is true.
	return true;
}
//-------------------------------------------------------------------------------------------------


//! Function queryStatisticsCM() retreives the WAX provider statistics.
/*
*	The function
*		- Queries the stats from the WAX compressor.
*		- Performs the in hb stats computations -> compression rate, dataVol3 and 
*		dataVolCurrent/compRateCurrent.
*		- Sets the computed values in the output structure.
*
*/
bool __stdcall queryStatisticsCM(/*OUT*/ Statistics* statsWAX)
{
	if( !GBStandardCompressionInitialized )
	{//	Do nothing if not initialized. Just return the NOT_INITIALIZED error.
		GWRReturnCode = WAX_RESULT_NOT_INITIALIZED;
		return false;
	}

	if (NULL == statsWAX)
	{
		GWRReturnCode = WAX_RESULT_INBUFF_NULL;
		return false;
	}

	if( GBAdvancedCompressionInitialized )
	{
		//only request additional WAX stats in debug mode
		if( Logger::GetLogLevel() & LOG_LEVEL_DEBUG )
		{
			statsWAX->bDumpAdditionalStats = true;
		}
		else
		{
			statsWAX->bDumpAdditionalStats = false;
		}

		//Advanced compression is enabled : Get the stats from the WAX compressor.
		GWRReturnCode = WAXPROVIDER queryStatistics( statsWAX );

		// Update the state flags in case the out of process queryStats() failed.
		//	BZ 8237 - do not take any action here in case of an error!
		//	In case the WAX service is unavailable let the next encode call discover 
		//	its dissapearance and react.
		//	setStateFlagsForAdvanced( GWRReturnCode );
	}

	//	BZ 7586 - WAX queryStatisticsCM() will not return an error if the advanced compressor is not available.
	//	NOTE: the following check is NOT on the else branch of the previous "if(...)" because
	//	the state change that might occur in setStateFlagsForAdvanced() must be considered here.
	if( !GBAdvancedCompressionInitialized )
	{
		//	This means either:
		//		- there was had a state change -> the compressor could be contacted earlier, but not for the last
		//		queryStatistics() call.
		//		- the compressor was not available. 
		//There is no error return code for this case.
		GWRReturnCode = WAX_RESULT_OK;

		//	Advanced compression is NOT enabled : set all stats to 0.
		memset(statsWAX, 0, sizeof( Statistics ) );
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	////	Set the stats gathered inHB process 

	//Uptime - BZ 8257 compute uptime since replication start
	if( *((__int64*)(&GInHBProcessStatistics.stopTime)) == 0 )
	{	//The compressor is started. Compute the uptime as
		//elapsed time from start time until now.
		FILETIME ftNow;
		getCurrentFileTime( &ftNow );
		statsWAX->uptime = getElapsedFileTimeMS( GInHBProcessStatistics.startTime, ftNow );
	}
	else
	{	//The compressor is started. Compute the uptime as
		//elapsed time from start time until stop time.
		statsWAX->uptime = getElapsedFileTimeMS( GInHBProcessStatistics.startTime, GInHBProcessStatistics.stopTime );
	}

	//1.6. Volume - this is the level 0 data since WAX was initialized
	statsWAX->dataVolLZero		= GInHBProcessStatistics.inputSize;    
	//!< 2.1. Data volume since WAX was initialized - same as dataVolLZero
	statsWAX->dataVolSinceInit	= GInHBProcessStatistics.inputSize;
	//!< 2.2. Volume of uncompressed data (level 0 data)
	statsWAX->dataVolL0			= GInHBProcessStatistics.inputSize;       

	//compute the current data volume
	double dNInvariant = GInHBStartParameters.dNInvariant;
	statsWAX->dataVolCurrent = (uint64_t)(
		( 1.0 - dNInvariant ) * GInHBProcessStatistics.dataVolumeCurrent + 
		dNInvariant * statsWAX->dataVolLZero );

	GInHBProcessStatistics.dataVolumeCurrent = statsWAX->dataVolCurrent;

	//Data Volume 3 - after ZLIB
	statsWAX->dataVolL3 = GInHBProcessStatistics.dataVol3AfterZlib ;

	//Perform the in hb stats computations -> compression rate, dataVol3 and oldCompressionRate and
	//set the computed values in the output structure.
	//BZ 7524 - removed test for GInHBStartParameters.bIsEncoder when reporting compression rates. 
	if(  GInHBProcessStatistics.outputSize > 0 ) 
	{
		statsWAX->compressionRate = (uint64_t)( (double)100 * 
			( (double)GInHBProcessStatistics.inputSize / (double)GInHBProcessStatistics.outputSize) );

		//Round up to the nearest 0.1X
		if( (statsWAX->compressionRate % 10 ) > 0  )
		{
			statsWAX->compressionRate = ((statsWAX->compressionRate / 10) + 1 ) * 10;
		}

		statsWAX->compressionRateCurrent = (uint64_t)(
			( 1.0 - dNInvariant ) * (double)GInHBProcessStatistics.compRateCurrent +
			dNInvariant * (double)statsWAX->compressionRate ) ;

		GInHBProcessStatistics.compRateCurrent = statsWAX->compressionRate;

		//Round up to the nearest 0.1X
		if( (statsWAX->compressionRateCurrent  % 10 ) > 0  )
		{
			statsWAX->compressionRateCurrent  = ((statsWAX->compressionRateCurrent / 10) + 1 ) * 10;
		}
	}
	else // This else clause added for BZ 11176.
	{
		statsWAX->compressionRate = 0;
		statsWAX->compressionRateCurrent = 0;
		GInHBProcessStatistics.compRateCurrent = 0;
	}

	return (GWRReturnCode == WAX_RESULT_OK) ;
}
//-------------------------------------------------------------------------------------------------

//! Function resetStatisticsCM() resets the statistics values that are gathered by this unit.
/*!
*	The function 
*		- sets all statistics regarding data vaolumes to ZERO
*		- sets the start time that is used for computing the UpTime
*
*	NOTE: Before a call to stopStatisticsCM() the compressor UpTime is computed as
*	upTime =  getCurrentFileTime() - InHBProcessStatistics.startTime
*
*	This function is part of the resolution for BZ 8257
**/
void __stdcall resetStatisticsCM( void )
{
	//set all values to ZERO
	memset( &GInHBProcessStatistics, 0 , sizeof( InHBProcessStatistics ) );

	//set the start time as current time
	getCurrentFileTime( &GInHBProcessStatistics.startTime );
}

//! Function stopStatisticsCM() sets the statistics stop time
/*!
*	The function - sets the stop time - that is used for computing the UpTime
*
*	NOTE: After the call to this function the compressor UpTime is computed as
*	upTime = InHBProcessStatistics.stopTime - InHBProcessStatistics.startTime
*
*	This function is part of the resolution for BZ 8257
**/
void __stdcall stopStatisticsCM( void )
{
	//Avoid ovetrwriting the stop time if it was already set.
	if( *((__int64*)(&GInHBProcessStatistics.stopTime)) == 0 )
	{
		getCurrentFileTime( &GInHBProcessStatistics.stopTime );
	}
}


//! Function setDebugModeCM() sets the logger DEBUG mode.
bool __stdcall setDebugModeCM(/*IN*/ long debugMode)
{
	if( !GBStandardCompressionInitialized )
	{//	Do nothing if not initialized. Just return the NOT_INITIALIZED error.
		GWRReturnCode = WAX_RESULT_NOT_INITIALIZED;
		return false;
	}
	
	if( GBAdvancedCompressionInitialized )
	{
		GWRReturnCode = WAXPROVIDER setDebugMode( (debugMode != 0) ? true : false );
		return (GWRReturnCode == WAX_RESULT_OK) ;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------

//! Retreives the compression method applied to the current buffer.
/*!
*	The function inspects the compressed message header and return the 
*	compression method applied to it on surce.
*     
*	Returns false is the buffer length is less than the EncodeHeader declared in NFWaxHB.cpp
*	or the input buffer was not WAX encoded.
**/
bool __stdcall getCompressionMethod( 
      /*IN*/ ByteBuffer* inCompressedBuffer, 
      /*OUT*/ unsigned long &outUlCompressionMethod
      )
{
	//	Check input paramater - for NULL, length and correct header checksum
	if( !checkIfValidEncodedBuffer( inCompressedBuffer, (unsigned char*)GEncodeHeaderNoChecksum ) )
	{
		return false;
	}

	outUlCompressionMethod = COMPRESS_NONE;//initialize to no compression.

	if( GEncodeHeaderNoChecksum->ZLIB )
	{//The ZLIB flag is set, so we have Standard(ZLIB) compression.
		outUlCompressionMethod = COMPRESS_ZLIB;
	}

	if( GEncodeHeaderNoChecksum->DD || GEncodeHeaderNoChecksum->RPR )
	{//Either the DD or RPR flags are set, so we have Advanced(DD) compression.
		outUlCompressionMethod = ( COMPRESS_DD | outUlCompressionMethod );
	}

	return true;
}
//-------------------------------------------------------------------------------------------------


//! Computes the header checksum and saves it in the header. 
/*!
*	The function 
*		- computes the checksum for the input header using the IP heaer method computation
*		- inserts the checksum in the header buffer at offset HEADER_CHECKSUM_OFFSET
*
*	Returns		- the checksum value as the return value.
*
*	NOTE: This function assumes that the input byte buffer is not NULL and is at least
*	sizeof( EncodeHeader ) bytes long.
**/
WORD __stdcall computeHeaderChecksum( 
	/*IN*/ /*OUT*/ unsigned char* inoutEncodeHeader //!< Header for which the checksum is computed.
	)
{
	WORD  wWord;
	unsigned long dwChecksum = 0;
    
	//initialize the HEADER Bytes that will hold the checksum to 0
	memset( inoutEncodeHeader + HEADER_CHECKSUM_OFFSET, 0, sizeof( WORD ) );

	// make 16 bit words out of every two adjacent 8 bit words in the header and add them up
	size_t iLenOfHeader = sizeof( EncodeHeader );
	for (size_t i = 0; i < iLenOfHeader; i=i+2)
	{
		wWord =( (inoutEncodeHeader[i]<<8)&0xFF00) + (inoutEncodeHeader[i+1]&0xFF);
		dwChecksum = dwChecksum + (unsigned long)wWord;	
	}
	
	// take only 16 bits out of the 32 bit sum and add up the carries
	while( dwChecksum>>16 )
	{
		dwChecksum = (dwChecksum & 0xFFFF)+(dwChecksum >> 16);
	}

	// Compute the sum as the one's complement the result
	dwChecksum = ~dwChecksum;

	//Set the checsum at the checksum offset
	*((WORD*)(inoutEncodeHeader + HEADER_CHECKSUM_OFFSET)) = (WORD)(dwChecksum);

	return (WORD)(dwChecksum);
}
//-------------------------------------------------------------------------------------------------


//! Checks if the computed header checksum matches the checksum saved in the header.
/*!
*	The function
*		- extracts the checksum value from the input header buffer
*		- computes the checksum value for the input header buffer
*		- compares the two values
*
*	Returns 
*		- as the function return value: true if the computed header checksum matches the 
*		checksum saved in the header
*		- the header with no checksum if required (passed out parameter is non NULL.
*
*	NOTE: This function assumes that the input byte buffer is not NULL and is at least
*	sizeof( EncodeHeader ) bytes long.
**/
bool __stdcall checkHeaderChecksum( 
	/*IN*/ unsigned char* inEncodeHeader,					//!< Header that is being checked for checksums.
	/*OUT*/ unsigned char* outEncodeHeaderWithNoChecksum	//!< Return adress of the header with no checksum. 
	)
{
	//	!!!!Caution: the saved checksum has to be extracted first, since the computeHeaderChecksum()
	//	function will reset the checksum bytes.
	WORD wSavedChecksum		= *((WORD*)( inEncodeHeader + HEADER_CHECKSUM_OFFSET ));
	WORD wComputedChecksum	= computeHeaderChecksum( inEncodeHeader );

	//To avoid altering the content of the input header - put the saved checksum bytes back.
	*((WORD*)(inEncodeHeader + HEADER_CHECKSUM_OFFSET)) = (WORD)(wSavedChecksum);

	if( outEncodeHeaderWithNoChecksum != NULL )
	{
		memcpy( outEncodeHeaderWithNoChecksum, inEncodeHeader, sizeof( EncodeHeader ) );
		memset( outEncodeHeaderWithNoChecksum + HEADER_CHECKSUM_OFFSET, 0, sizeof( WORD ) );
	}

	return (wSavedChecksum == wComputedChecksum);
}
//-------------------------------------------------------------------------------------------------

//! Checks if the input buffer is not NULL and was previously encoded.
/*
*	The function:
*		- checks if the input ByteBuffer and its internal buffer are not null
*		- checks if the length of the buffer is suffcient to hold a compressed buffer
*		- calls checkHeaderChecksum() to verify the header checksum
*		- sets the proper GWRReturnCode in case of error.
*
*	Return
*		- as the return value: true if all tests pass 
*		- the Encode Header With No Checksum if all tests pass and the output paramater is not NULL.
**/
bool __stdcall checkIfValidEncodedBuffer( 
	/*IN*/ ByteBuffer* inEncodedByteBuffer,
	/*OUT*/ unsigned char* outEncodeHeaderWithNoChecksum
	) 
{
	//Check input parameters.	
	if( inEncodedByteBuffer == NULL )
	{
		GWRReturnCode = WAX_RESULT_INBUFF_NULL;
		return false;
	}

	if( inEncodedByteBuffer->buffer == NULL )
	{
		GWRReturnCode = WAX_RESULT_INBUFF_NULL;
		return false;
	}

	//Check for the minimum length
	if( inEncodedByteBuffer->length < sizeof( EncodeHeader ) )
	{
		GWRReturnCode = WAX_RESULT_UNENCODED_BUFFER;
		return false;
	}

	//Check the correctness of the header checksum.
	if( !checkHeaderChecksum( inEncodedByteBuffer->buffer, outEncodeHeaderWithNoChecksum ) )
	{
		GWRReturnCode = WAX_RESULT_UNENCODED_BUFFER;
		return false;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------

//! Returns true on success, otherwise false
bool __stdcall setupCM(
	/*IN*/ StringBuffer* inSBServicePath
	)
{
	GWRReturnCode = WAXPROVIDER setup( inSBServicePath );
	return ( GWRReturnCode == WAX_RESULT_OK );
}
//-------------------------------------------------------------------------------------------------

//! Returns true on success, otherwise false
bool __stdcall teardownCM(void)
{
	GWRReturnCode = WAXPROVIDER teardown();
	return ( GWRReturnCode == WAX_RESULT_OK );
}
//-------------------------------------------------------------------------------------------------

//! Returns true if the result is an error from advanced compression, otherwise false
bool __stdcall isErrorInAdvancedCM(void)
{
	return GBErrorInWaxCompressor;
}
//-------------------------------------------------------------------------------------------------

//! Checks if the input error code is caused by the IPc component.
/*!
*	The function return true if the input error code pertains to the 
*	IPC error codes group.
*
*	That means it's value is comprised in the interval:
*		[ WAX_RESULT_WAIT_FAILED , WAX_RESULT_SERVICE_UNAVAILABLE ]
*
**/
bool __stdcall isIPCErrorCode( 
	/*IN*/ WAX_RESULT inErrorCode
	)
{
	//	It is true if the error code is comprised in the interval
	//	[ WAX_RESULT_WAIT_FAILED , WAX_RESULT_SERVICE_UNAVAILABLE ]
	return ( (GWRReturnCode >= WAX_RESULT_WAIT_FAILED) && 
			 (GWRReturnCode <= WAX_RESULT_SERVICE_UNAVAILABLE) 
			);
}
//-------------------------------------------------------------------------------------------------

//!	Sets the component state flags pertaining to advanced compression according to the input error code.
void __stdcall setStateFlagsForAdvanced(
	/*IN*/ WAX_RESULT inErrorCode
	)
{
	if ( isIPCErrorCode( inErrorCode ) )
	{//	The return code is an IPC error => set the flags.

		//	The error occured while trying to perfrom an advanced operation.
		GBErrorInWaxCompressor = true;

		//	We set the "Advanced Compression" to "NOT INITIALIZED" only if 
		//	at this point we've lost contact with the service.
		GBAdvancedCompressionInitialized = false;
	}
}
//-------------------------------------------------------------------------------------------------

//! Transforms the ZLIB error code to WAX_RESULT error code
/*!
*	The functions analyses the ZLIB speciffic error code and returns its equivalent
*	WAX_RESULT.
*	
*	This function was implemented to:
*		- isolate ZLIB error code knowledge in one place
*		- reduce the size of the encode function by removing the switch() clase from it.
*	
**/
WAX_RESULT __stdcall getZLIBToWaxResult( 
	/*IN*/ int iniZlibErrorCode
	)
{
	switch( iniZlibErrorCode )
	{
	case Z_BUF_ERROR:
		{
			return WAX_RESULT_ZLIB_COMPRESSION_Z_BUF_ERROR;
		}
		break;
	
	case Z_MEM_ERROR:
		{
			return WAX_RESULT_ZLIB_COMPRESSION_Z_MEM_ERROR;
		}
		break;

	case Z_STREAM_ERROR:
		{
			return WAX_RESULT_ZLIB_COMPRESSION_Z_STREAM_ERROR;
		}
		break;
	
	default:
		{
			return WAX_RESULT_ZLIB_COMPRESSION_FAILED;
		}
		break;
	}
}
//-------------------------------------------------------------------------------------------------
//! Function isValidTransferLogPathCM() - checks is the input transfer log path is valid.
//	NOTE: This function is as part of the resolution for BZ 7876
bool __stdcall isValidTransferLogPathCM(
	/*IN*/ StringBuffer* transferLogPath
	)
{
	if( transferLogPath == NULL )
	{
		return false;
	}

	if( transferLogPath->buffer == NULL )
	{
		return false;
	}

	//	Pass in the inbDeleteCreatedFolders parameter as true to avoid the side effect of
	//	having unwanted directories.
	return NFCreateDirectoryStructure( transferLogPath->buffer, true );
}