//********************************************************/
//  ./CompressorKnowledge.cpp
//
//  Owner : ciprian Maris
// 
//	Company		: Neverfail ________________
//	PROJECT		: WAN Acceleration 
//  Component	: Shared Utilities  
//	
//	Purpose: Implementation of functions that have speciffic compressor knowledge.
//	This unit provides interfaces to internal WAX types needed in the HB-JNI interface.
//
//  $Rev: 2047 $ - Revision of last commit
//
//********************************************************/

//********************************************************/
//
// Last Change - Author and Date of last change:
// $Author: cmaris $  
// $Date: 2009-03-17 13:07:18 +0000 (Tue, 17 Mar 2009) $
//
//********************************************************/

#include "CompressorKnowledge.h"
#include "SharedTypes.h"
#include "ConfigMan/ConfigMan.h"

//	The following defines are the lengths that can be added by RPR, DD, and ZLib 
//	to the compressed buffer length in the worst case scenario (where the input buffer is uncompressible)

// Maximum overhead for RPR
//#define LEN_OVERHEAD_RPR	( 2 * ( sizeof(RPR_Token) + sizeof(RPR_Token_Generic) + sizeof(RPR_Token_DA) ) )
#define LEN_OVERHEAD_RPR	( 2 * ( sizeof(RPR_Token) + sizeof(TokenUncompressedFOUR) + sizeof(TokenCompressedFOUR) ) )

// Maximum overhead for UWM
#define LEN_OVERHEAD_UWM	( sizeof( UWM_Header ) + UWM_FLAG_SIZE ) 

// Maximum overhead for DD
#define LEN_OVERHEAD_DD		( 2 * (sizeof( Token ) + max( sizeof( TokenCompressed ) , sizeof( TokenUncompressed ) ) ) ) 


/**** Configuration Manager Functions ****/

//!	Function getConfigManagerNInvariantValue() retreives the value of NInvariant from the Configuration Manager.
/*!
*	The function 
*		- calls the speciffic ConfigMan:: setter to validate the input value. If the setter returns false
*	the outbValidParamValue will be set to false.
*		- calls the speciffic ConfigMan:: getter to retreive the actual parameter value for the case when
*		a default value has been set
*
*	BZ 7543 - added outbValidParamValue return parameter in case a config manager setter return an error.
*/
double getConfigManagerNInvariantValue( 
	/*__in*/ double indNInvariant,
	/*__out*/ bool &outbValidParamValue 
	)
{
	if (!ConfigMan::getInstance()->SetNInvariant( indNInvariant ) )
	{
		outbValidParamValue = false;
	}

	outbValidParamValue = true;

	return ConfigMan::getInstance()->GetNInvariant();
}
//-------------------------------------------------------------------------------------------------

//!	Function getConfigManagerMinDeDuplicateLength() retreives the value of minDeDuplicateLength from the Configuration Manager.
/*!
*	The function 
*		- calls the speciffic ConfigMan:: setter to validate the input value. If the setter returns false
*	the outbValidParamValue will be set to false.
*		- calls the speciffic ConfigMan:: getter to retreive the actual parameter value for the case when
*		a default value has been set
*
*	BZ 7543 - added outbValidParamValue return parameter in case a config manager setter return an error.
*/
unsigned long getConfigManagerMinDeDuplicateLength ( 
	/*__in*/ unsigned long inMinDeDuplicateLength,
	/*__out*/ bool &outbValidParamValue 
	)
{
	if (! ConfigMan::getInstance()->SetMinDeDuplicateLength( inMinDeDuplicateLength ) )
	{
		outbValidParamValue = false;
	}

	outbValidParamValue = true;

	return ConfigMan::getInstance()->GetMinDeDuplicateLength();
}
//-------------------------------------------------------------------------------------------------


//!	Function getConfigManagerMinZLibCompressLength() retreives the value of minZLibCompressLength from the Configuration Manager.
/*!
*	The function 
*		- calls the speciffic ConfigMan:: setter to validate the input value. If the setter returns false
*	the outbValidParamValue will be set to false.
*		- calls the speciffic ConfigMan:: getter to retreive the actual parameter value for the case when
*		a default value has been set
*
*	BZ 7543 - added outbValidParamValue return parameter in case a config manager setter return an error.
*/
unsigned long getConfigManagerMinZLibCompressLength(
	/*__in*/ unsigned long inMinZLibCompressLength,
	/*__out*/ bool &outbValidParamValue
	)
{
	if( !ConfigMan::getInstance()->SetMinZLibCompressLength( inMinZLibCompressLength ) )
	{
		outbValidParamValue = false;
	}

	outbValidParamValue = true;

	return ConfigMan::getInstance()->GetMinZLibCompressLength();
}
//-------------------------------------------------------------------------------------------------


/**** Encoder speciffic knowledge Functions ****/

//!	Function getEncodeOutputLength() computes the maximum size of an WAX encoded buffer.
/*!
*	The function computes the maximum size of an WAX encoded buffer by adding the maximum overhead 
*	from all the components involved in the encode process.
*
*/
unsigned long getEncodeOutputLength( 
	/*__in*/ unsigned long inulInputBufferLength,
	/*__in*/ bool removeRepeatedPatterns,
	/*__in*/ bool deDuplicate
	)
{
	////	NOTE:
	////	The maximum required length to hold an DD + RPR encoded buffer is:
	////	LEN_INPUT_BUFFER + UWM_HEADER + UWM_FLAGS + RPR_OVERHEAD + DD_OVERHEAD
	////
	////	NOTE for BZ 7523 - the overhead of DD and RPR can be bigger than the formula 
	////	described above for the case either DD or RPR do not actually compress the 
	////	buffer due to invalid parameter settings as follows:
	////		- DD will grow the buffer if minDataLengthReplacedByToken is smaller than Tokencompressed + TokenUncompressed
	////		- RPR will grom the buffer if minRPRLength is smaller than sizeof(WAX_TOKEN_TYPE) + sizeof(ULONG) + sizeof(TokenCompressed) + sizeof(BYTE)
	////
	////	In order to fix BZ 7523 we introduced restrictions in parameter settings to avoid these situations.

	//init the needed buffer length to the input buff len
	unsigned long ulRetLength = inulInputBufferLength ;

	if( removeRepeatedPatterns || deDuplicate )
	{
		//In the case we have the UWM overhead
		ulRetLength += LEN_OVERHEAD_UWM;
	}

	if( removeRepeatedPatterns )
	{
		//In the case RPR does not compress the buffer at all, the buffer len will increase by two
		//uncompressed token length - RPR overhead.
		ulRetLength += LEN_OVERHEAD_RPR;
	}

	if( deDuplicate )
	{
		//In the case DD does not compress the buffer at all, the buffer len will increase by two
		//uncompressed token length - DD overhead.
		ulRetLength += LEN_OVERHEAD_DD;
	}

	return ulRetLength;
}
//-------------------------------------------------------------------------------------------------

//!	Function getDecodeOutputLength() computes the initial size of an WAX encoded buffer.
/*!
*	The function computes the initial size of an WAX encoded buffer by inspecting the UWM
*	header of the buffer.
*
*	NOTE:	The caller of the function promises that the input buffer is not NULL and that it 
*			is the result of an WAXCompressor::encode(...) call.
*/
unsigned long getDecodeOutputLength(
	/*__in*/ unsigned char* inlpbEncodedBuffer 
	)
{
	return ((UWM_Header*)inlpbEncodedBuffer)->BuffSize ;
}
//-------------------------------------------------------------------------------------------------
