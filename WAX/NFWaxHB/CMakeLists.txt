cmake_minimum_required (VERSION 2.8)
project(NFWaxHB)

option( STDCALL "Build NFWaxHB with the __stdcall convention (Windows)" ON )
add_definitions( -DWIN64 -D_WIN64 -D_WINDOWS -D_USRDLL -DUNICODE -DWAX_COMPRESSOR_IN_HB_PROCESS -DNFWAXHB_EXPORTS ) 
include_directories( "${PROJECT_SOURCE_DIR}" )
include_directories( "${PROJECT_SOURCE_DIR}/../WAXCompressorDll" )
include_directories( ${ZLIB_INCLUDE_DIR} )

file( GLOB GENERAL_SRC "*.h" "*.cpp" )
set(
	GENERAL_SOURCES
	${GENERAL_SRC}
	"${PROJECT_SOURCE_DIR}/../WAXCompressorDll/Logger/Logger.cpp"
	"${PROJECT_SOURCE_DIR}/../WAXCompressorDll/Logger/Logger.h"
	"${PROJECT_SOURCE_DIR}/../WAXCompressorDll/ConfigMan/ConfigMan.cpp"
	"${PROJECT_SOURCE_DIR}/../WAXCompressorDll/ConfigMan/ConfigMan.h"
	"${PROJECT_SOURCE_DIR}/../WAXCompressorDll/AllUtils.cpp"
	"${PROJECT_SOURCE_DIR}/../WAXCompressorDll/AllUtils.h"
	#"${PROJECT_SOURCE_DIR}/../WAXCompressorDll/WAXCompressor.cpp"
	"${PROJECT_SOURCE_DIR}/../WAXCompressorDll/WAXCompressor.h"
	#"${PROJECT_SOURCE_DIR}/../WAXCompressorDll/WAXInterface.cpp"
	"${PROJECT_SOURCE_DIR}/../WAXCompressorDll/WAXInterface.h"
)

message( STATUS "Cmake files: ${GENERAL_SOURCES}" )

add_library( NFWaxHB ${GENERAL_SOURCES} )
target_link_libraries( NFWaxHB PUBLIC ${ZLIB_LIBRARIES} shlwapi.lib )
target_link_libraries( NFWaxHB PRIVATE WaxCompressor )
