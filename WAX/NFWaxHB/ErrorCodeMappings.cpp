//********************************************************/
//  ./ErrorCodeMappings.cpp
//
//  Owner : Attila Vajda
// 
//	Company		: Neverfail 
//	PROJECT		: WAN Acceleration 
//  Component	: Error Handling 
//
//  $Rev: 1568 $ - Revision of last commit
//
/********************************************************
//
// Last Change - Author and Date of last change:
//  $Author: cmaris $  
//  $Date: 2009-01-22 12:07:26 +0200 (Thu, 22 Jan 2009) $
//
//********************************************************/

#include "ErrorCodeMappings.h"

ErrorCodeMappings::ErrorCodeMappings()
{
	generateMappings();
}

ErrorCodeMappings::~ErrorCodeMappings()
{
	m_mapedErrorString.clear();
}

//! Link an error code to a given error string
void ErrorCodeMappings::linkIt( /*__in*/ const WAX_RESULT_TYPES inWAXErrorCode, /*__in*/ const wchar_t* inszErrorString )
{
	m_mapedErrorString[ inWAXErrorCode ] = inszErrorString;
	unsigned long ulerrorcode = inWAXErrorCode;
	//wprintf(L"[%u]-[%u]-[%s]\r\n", m_mapedErrorString.size(), ulerrorcode, inszErrorString );
}

//! Generate the mapping between error codes and a error string.
void ErrorCodeMappings::generateMappings()
{
	//! Common error codes - shared by modules
	linkIt( WAX_RESULT_OK              ,		L"Success" );
	linkIt( WAX_RESULT_UNKNOWN_ERROR   ,		L"Unknown Error" );
	linkIt( WAX_RESULT_INBUFF_NULL     ,		L"In buffer NULL" );
	linkIt( WAX_RESULT_OUTBUFF_NULL    ,		L"Out buffer NULL" );
	linkIt( WAX_RESULT_NOT_INITIALIZED ,		L"Not initialized" );
	linkIt( WAX_RESULT_NOT_ALLOCATED   ,		L"Not allocated" );
	linkIt( WAX_RESULT_NOT_STARTED     ,		L"Not started" );
	linkIt( WAX_RESULT_INIT_SECURITY_FAILED   , L"Initialize security failed" );
	linkIt( WAX_RESULT_LOG_INIT_FAILED        , L"Logger initialize failed" );
	linkIt( WAX_RESULT_CAN_NOT_ALLOCATE_MEMORY, L"Memory allocation failed" );
	linkIt( WAX_RESULT_INSUFFICIENT_MEMORY_FOR_CURRENT_SETTINGS, L"Insufficient memory for current settings" );
	linkIt( WAX_RESULT_ALREADY_PERFORMED,		L"Already performed" );
	linkIt( WAX_RESULT_INVALID_SETTINGS ,		L"Invalid settings" );
	linkIt( WAX_RESULT_OUTBUFF_INSUFFICIENT_SIZE, L"Insufficient out buffer size" );

	linkIt( WAX_RESULT_ZLIB_COMPRESSION_FAILED  ,		L"ZLib compression failed" );
	linkIt( WAX_RESULT_ZLIB_UNCOMPRESSION_FAILED,		L"ZLib uncompression failed" );
	linkIt( WAX_RESULT_BUFFER_ALLOCATION_FAILED ,		L"Buffer allocation failed" );
	linkIt( WAX_RESULT_UNENCODED_BUFFER         ,		L"Unencoded buffer" );
	linkIt( WAX_RESULT_UNCOMPRESSION_NEEDS_ADVANCED,	L"Decode needs advanced but advanced was not selected" );

	linkIt( WAX_RESULT_ADVANCED_ENCODE_NOT_AVAILABLE,	L"Advanced Encode can not be performed" );
	linkIt( WAX_RESULT_ADVANCED_DECODE_NOT_AVAILABLE,	L"Advanced Decode can not be performed" );
	linkIt( WAX_RESULT_WAX_HB_ADVANCED_NOT_INITIALIZED, L"Advanced Compression not initialized" );

	//!State management return codes
	linkIt( WAX_RESULT_ALREADY_INITIALIZED, L"Already initialized" );
	linkIt( WAX_RESULT_ALREADY_ALLOCATED  , L"Already allocated" );
	linkIt( WAX_RESULT_ALREADY_STARTED    , L"Already started" );

	linkIt( WAX_RESULT_ZLIB_COMPRESSION_Z_BUF_ERROR   , L"ZLib buffer error" );
	linkIt( WAX_RESULT_ZLIB_COMPRESSION_Z_MEM_ERROR   , L"ZLib memory error" );
	linkIt( WAX_RESULT_ZLIB_COMPRESSION_Z_STREAM_ERROR, L"ZLib stream error" );

	//!Error codes that pertain to IPC
	linkIt( WAX_RESULT_WAIT_FAILED               , L"Wait failed" );

	linkIt( WAX_RESULT_CREATE_FILE_MAPPING_FAILED, L"Create file mapping failed" );
	linkIt( WAX_RESULT_OPEN_FILE_MAPPING_FAILED  , L"Open file mapping failed" );
	linkIt( WAX_RESULT_MAP_VIEW_OF_FILE_FAILED   , L"Mapping view of file failed" );
	linkIt( WAX_RESULT_CREATE_EVENT_FAILED       , L"Create event failed" );
	linkIt( WAX_RESULT_OPEN_EVENT_FAILED         , L"Open event failed" );

	//!setup - teardown error codes
	linkIt( WAX_RESULT_SETUP_FAILED        , L"Setup failed" );
	linkIt( WAX_RESULT_SERVICE_START_FAILED, L"Service start failed" );
	linkIt( WAX_RESULT_TEAR_DOWN_FAILED    , L"Tear down failed" );

	linkIt( WAX_RESULT_SERVICE_UNAVAILABLE , L"Service unavailable" );

	//! WAX Interface errors
	linkIt( WAX_RESULT_WRONG_ENCODE_CALL,			L"Wrong encode call" );
	linkIt( WAX_RESULT_WRONG_DECODE_CALL,			L"Wrong decode call" );
	linkIt( WAX_RESULT_INVALID_START_PARAMETERS   , L"Invalid start parameters" );
	linkIt( WAX_RESULT_INVALID_ALLOCATE_PARAMETERS, L"Invalid allocate parameters" );

	//!  UWM errors
	linkIt( WAX_RESULT_INSTANCE_EXIST,				L"Instance exist" );
	linkIt( WAX_RESULT_PROCESSING_FLAG_REQUIRED,	L"Processing flag required" );
	linkIt( WAX_RESULT_CHECK_OUTBUFF_SIZE_INCORECT,	L"Decoded message size doesn't match header" );
	linkIt( WAX_RESULT_CHECK_CRC_INCORECT,			L"Message CRC check failed" );

	//! Store Manager specific error codes
	linkIt( WAX_RESULT_CAN_NOT_OPEN_FILE,			L"Open file failed" );
	linkIt( WAX_RESULT_CAN_NOT_SET_FILE_POINTER,	L"Set file pointer failed" );
	linkIt( WAX_RESULT_CAN_NOT_SET_FILE_EOF,		L"Set EOF failed" );
	linkIt( WAX_RESULT_CAN_NOT_SET_FILE_SIZE,		L"File size set failed" );

	linkIt( WAX_RESULT_CAN_NOT_WRITE_TO_FILE,		L"Write to file failed" );
	linkIt( WAX_RESULT_CAN_NOT_WRITE_TO_MEM,		L"Write to memory failed" );
	linkIt( WAX_RESULT_OFFSET_NOT_IN_MEM,			L"Offset is not in memory" );
	linkIt( WAX_RESULT_OFFSET_TO_BIG,				L"Offset too big" );
	linkIt( WAX_RESULT_CAN_NOT_READ_FROM_FILE,		L"Read file failed" );
	linkIt( WAX_RESULT_CAN_NOT_READ_FROM_MEM,		L"Read memory failed" );
	linkIt( WAX_RESULT_NOT_ENOUGH_MEMORY_FOR_SIG_LOG, L"Not enough memory for signatures." );

	//!  Tokenizer/RPR Tokenizer errors
	linkIt( WAX_RESULT_INBUFF_INCONSISTENT, L"Invalid input buffer" );
	linkIt( WAX_RESULT_UNKNOWN_TOKEN_TYPE , L"Unknown token type" );

	linkIt( WAX_RESULT_ENTRY_NOT_FOUND       , L"Entry not found" );
	linkIt( WAX_RESULT_GENERATE_TOKENS       , L"Generate tokens" );
	linkIt( WAX_RESULT_INVALID_BUFFER_OR_SIZE, L"Invalid buffer or size" );
	linkIt( WAX_RESULT_INVALID_ADLER         , L"Invalid checksum" );
}

//! Returns error string for a given error code
const wchar_t* ErrorCodeMappings::getErrorString(
	/*__in*/ const unsigned long inulErrorCode
	)
{
	MAP_ERRORSTRING::iterator it;

	it = m_mapedErrorString.find( (WAX_RESULT_TYPES) inulErrorCode );

	if ( it != m_mapedErrorString.end() )
	{
		return it->second;
	}

	return L"";

}