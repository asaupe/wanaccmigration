//********************************************************/
//  ./CompressorKnowledge.h
//
//  Owner : ciprian Maris
// 
//	Company		: Neverfail ________________
//	PROJECT		: WAN Acceleration 
//  Component	: Shared Utilities  
//	
//	Purpose: Implementation of functions that have speciffic compressor knowledge.
//	This unit provides interfaces to internal WAX types needed in the HB-JNI interface.
//
//  $Rev: 1889 $ - Revision of last commit
//
//********************************************************/

//********************************************************/
//
// Last Change - Author and Date of last change:
// $Author: cmaris $  
// $Date: 2009-03-04 10:17:05 +0000 (Wed, 04 Mar 2009) $
//
//********************************************************/

//********************************************************/
//
//	CompressorKnowledge is a bridge between the UWM/Tokenizer/RPR and the HB-JNI interface.
//	Its purpose is to provide interfaces to internal WAX types without affecting the encapsulation.
//
//	NOTE:	[1] The functions implemented here are all non-error prone. This unit will not use logging.
//
//	NOTE:	[2] By introducing this unit we make sure that the WAXCompressor ( speciffically UWM/RPR/tokenizer)
//			data types can change independently without affecting other parts of the code..
//
//	NOTE:	[3] The extended description of functions is present in the CompressorKnowledge.cpp gile
//
//********************************************************/

#pragma once

#include <windows.h>
#include "WAXTypes.h"      //from the Wax compressor source dir


/**** Encoder speciffic knowledge Functions ****/

//!	Function getEncodeOutputLength() computes the maximum size of an WAX encoded buffer.
unsigned long getEncodeOutputLength( 
	/*__in*/ unsigned long inulInputBufferLength,
	/*__in*/ bool removeRepeatedPatterns,
	/*__in*/ bool deDuplicate
	);

//!	Function getDecodeOutputLength() computes the initial size of an WAX encoded buffer.
unsigned long getDecodeOutputLength(
	/*__in*/ unsigned char* inlpbEncodedBuffer 
	);
	


/**** Configuration Manager Functions ****/

//!	Function getConfigManagerNInvariantValue() retreives the value of NInvariant from the Configuration Manager.
double getConfigManagerNInvariantValue( 
	/*__in*/ double indNInvariant,
	/*__out*/ bool &outbValidParamValue 
	);

//!	Function getConfigManagerMinDeDuplicateLength() retreives the value of minDeDuplicateLength from the Configuration Manager.
unsigned long getConfigManagerMinDeDuplicateLength ( 
	/*__in*/ unsigned long inMinDeDuplicateLength,
	/*__out*/ bool &outbValidParamValue
	);

//!	Function getConfigManagerMinZLibCompressLength() retreives the value of minZLibCompressLength from the Configuration Manager.
unsigned long getConfigManagerMinZLibCompressLength(
	/*__in*/ unsigned long inMinZLibCompressLength,
	/*__out*/ bool &outbValidParamValue
	);
