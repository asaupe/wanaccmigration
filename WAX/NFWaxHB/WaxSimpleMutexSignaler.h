//********************************************************/
//  ./WaxSimpleMutexSignaler.h
//
//  Owner : Attila Vajda
// 
//	Company		: Neverfail 
//	PROJECT		: WAN Acceleration 
//  Component	: Simple Mutex Signaler 
//
//  $Rev: $ - Revision of last commit
//
/********************************************************
//
// Last Change - Author and Date of last change:
// $Author: $  
// $Date:  $
//
//********************************************************/

#pragma once

#include <Windows.h>

//! WaxSimpleMutexSignaler - signal service up and running.
/*! \class WaxSimpleMutexSignaler
*   Will be used in IPCClient and IPCServer in order to signal eventual 
*   service stops and unexpected exit.
* 
*   \author Attila Vajda
*   \version 1.0.$Rev:$
*   \date    FEB-2008
*/
class WaxSimpleMutexSignaler
{
public:
	WaxSimpleMutexSignaler(void);

public:
	~WaxSimpleMutexSignaler(void);
	
	//! Creates a mutex and locks.
	bool createSignaler( 
		/*__in*/ const wchar_t* inszName
		);
	
	//! Release the mutex.
	void releaseSignaler( void );

private:
	HANDLE m_hNamedMutex;

};
