//********************************************************/
//  ./WaxSimpleMutexSignaler.cpp
//
//  Owner : Attila Vajda
// 
//  Company		: Neverfail 
//  PROJECT		: WAN Acceleration 
//  Component	: Simple Mutex Signaler 
//
//  $Rev: $ - Revision of last commit
//
/********************************************************
//
// Last Change - Author and Date of last change:
// $Author: $  
// $Date:  $
//
//********************************************************/

#include "WaxSimpleMutexSignaler.h"

WaxSimpleMutexSignaler::WaxSimpleMutexSignaler(void)
{
	m_hNamedMutex = NULL;
}

WaxSimpleMutexSignaler::~WaxSimpleMutexSignaler(void)
{
	releaseSignaler();
}

//! Creates a mutex and locks.
//  If return is true signaler was created successfully and from service point of 
//  view it is the expected result. From client point of view if result is true it 
//  means that the service no longer has the mutex and it is dead.
//
//  If return is false it means the the service got the mutex and it is up and running.
//  and the client can continue calls to WAX interfaces.

bool WaxSimpleMutexSignaler::createSignaler( 
	/*__in*/ const wchar_t* inszName
	)
{
	bool bResult = false;

	m_hNamedMutex = CreateMutexW( 
		NULL,
		FALSE,                
		inszName
		); 

	if ( NULL != m_hNamedMutex ) 
	{
		unsigned long dwWaitResult; 

		dwWaitResult = WaitForSingleObject( 
			m_hNamedMutex, // handle to mutex
			0              // check the state 
			);

		if ( (dwWaitResult == WAIT_OBJECT_0 ) || // we got ownership of the mutex ...
			 (dwWaitResult == WAIT_ABANDONED ) ) // we have an abandoned mutex ...
		{
			bResult = true;
		}
	}

	return bResult;
}

//! Release the mutex.
void WaxSimpleMutexSignaler::releaseSignaler( void )
{
	if ( NULL != m_hNamedMutex )
	{
		ReleaseMutex( m_hNamedMutex );
		CloseHandle ( m_hNamedMutex );
		m_hNamedMutex = NULL;
	}
}
