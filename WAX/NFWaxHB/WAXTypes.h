//********************************************************/
//  ./WAXTypes.h
//
//  Owner : Attila Vajda / Bogdan Hruban / Ciprian Maris
// 
//	Company		: Neverfail 
//	PROJECT		: WAN Acceleration 
//  Component	: WAX Types
//
//  $Rev: 1787 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: cmaris $  
//  $Date: 2009-02-18 12:27:24 +0000 (Wed, 18 Feb 2009) $
//
//********************************************************/

//********************************************************/
//	WAXTypes.h is the WAXCompressor include file defining types visible to the outside world.
//
//	It is intended to be used both by the components in the WAXCompressor.dll and the 
//	caller of the methods exported by the DLL.
//
//********************************************************/

#pragma once
//#include <windows.h>
//#include <platformDefs.h>

#include <cstdint>

typedef unsigned long WAX_RESULT;
typedef int                 BOOL;
#ifndef FALSE
#define FALSE               0
#endif

#ifndef TRUE
#define TRUE                1
#endif

typedef uint64_t SIZE_T, *PSIZE_T;
typedef long long SSIZE_T, *PSSIZE_T;

typedef void *HANDLE;

//!Return error codes for WAX component methods
/*!
*	The error code values specific to components are formed like this:
*	XXYYY
*	XX  - component ID (0, 1 or 2 digits). The common (shared by more than 1 component) error codes 
*		do not have a component ID, si their value is less than 999. 	
*	YYY - error code (3 digits)
*/
enum WAX_RESULT_TYPES//:ULONG
{
	//! Common error codes - shared by modules
	WAX_RESULT_OK = 0,
	WAX_RESULT_UNKNOWN_ERROR = 1,
	WAX_RESULT_INBUFF_NULL,
	WAX_RESULT_OUTBUFF_NULL,
	WAX_RESULT_NOT_INITIALIZED,
	WAX_RESULT_NOT_ALLOCATED,
	WAX_RESULT_NOT_STARTED,
	WAX_RESULT_INIT_SECURITY_FAILED,
	WAX_RESULT_LOG_INIT_FAILED,		
	WAX_RESULT_CAN_NOT_ALLOCATE_MEMORY,
	WAX_RESULT_INSUFFICIENT_MEMORY_FOR_CURRENT_SETTINGS,
	WAX_RESULT_ALREADY_PERFORMED,
	WAX_RESULT_INVALID_SETTINGS,
	WAX_RESULT_OUTBUFF_INSUFFICIENT_SIZE,	//!< The out buffer size is to small.

	WAX_RESULT_ZLIB_COMPRESSION_FAILED,
	WAX_RESULT_ZLIB_UNCOMPRESSION_FAILED,
	WAX_RESULT_BUFFER_ALLOCATION_FAILED,	//!< The allocation fo the buffer failed;

	WAX_RESULT_UNENCODED_BUFFER,
	WAX_RESULT_UNCOMPRESSION_NEEDS_ADVANCED, //!< The decode needs advanced but advanced was not selected

	WAX_RESULT_ADVANCED_ENCODE_NOT_AVAILABLE, //!< Advanced Encode can not be performed
	WAX_RESULT_ADVANCED_DECODE_NOT_AVAILABLE, //!< Advanced Decode can not be performed

	WAX_RESULT_WAX_HB_ADVANCED_NOT_INITIALIZED, //!< Advanced Encode can not be performed

	//!State management return codes
	WAX_RESULT_ALREADY_INITIALIZED = 28,
	WAX_RESULT_ALREADY_ALLOCATED,
	WAX_RESULT_ALREADY_STARTED,

	WAX_RESULT_ZLIB_COMPRESSION_Z_BUF_ERROR,
	WAX_RESULT_ZLIB_COMPRESSION_Z_MEM_ERROR,
	WAX_RESULT_ZLIB_COMPRESSION_Z_STREAM_ERROR,

	//!Error codes that pertain to IPC
	////	!!! IMPORTANT !!! - the IPC error codes must all be grouped in one place
	////		between WAX_RESULT_WAIT_FAILED and WAX_RESULT_SERVICE_UNAVAILABLE.
	////		This is done so to avoid hard coding of error codes in component source code.
	WAX_RESULT_WAIT_FAILED = 500,

	WAX_RESULT_CREATE_FILE_MAPPING_FAILED,
	WAX_RESULT_OPEN_FILE_MAPPING_FAILED,
	WAX_RESULT_MAP_VIEW_OF_FILE_FAILED,
	WAX_RESULT_CREATE_EVENT_FAILED,
	WAX_RESULT_OPEN_EVENT_FAILED,

	//!setup - teardown error codes
	WAX_RESULT_SETUP_FAILED,
	WAX_RESULT_SERVICE_START_FAILED,
	WAX_RESULT_TEAR_DOWN_FAILED,

	WAX_RESULT_SERVICE_UNAVAILABLE,//KEEP this error code as the last one in the IPC group.
	//!END Error codes that pertain to IPC

	//! WAX Interface errors
	WAX_RESULT_WRONG_ENCODE_CALL = 1001,
	WAX_RESULT_WRONG_DECODE_CALL,
	WAX_RESULT_INVALID_START_PARAMETERS,
	WAX_RESULT_INVALID_ALLOCATE_PARAMETERS,

	//!  UWM errors
	WAX_RESULT_INSTANCE_EXIST = 9001,
	WAX_RESULT_PROCESSING_FLAG_REQUIRED,		//!< UWM was called without any processing flag set
	WAX_RESULT_CHECK_OUTBUFF_SIZE_INCORECT,		//!< the final message size doesn't match the size specified in the header
	WAX_RESULT_CHECK_CRC_INCORECT,				//!< the CRC does not match

	//! Store Manager specific error codes
	WAX_RESULT_CAN_NOT_OPEN_FILE = 14001,
	WAX_RESULT_CAN_NOT_SET_FILE_POINTER,
	WAX_RESULT_CAN_NOT_SET_FILE_EOF,
	WAX_RESULT_CAN_NOT_SET_FILE_SIZE,

	WAX_RESULT_CAN_NOT_WRITE_TO_FILE,
	WAX_RESULT_CAN_NOT_WRITE_TO_MEM,
	WAX_RESULT_OFFSET_NOT_IN_MEM,
	WAX_RESULT_OFFSET_TO_BIG,
	WAX_RESULT_CAN_NOT_READ_FROM_FILE,
	WAX_RESULT_CAN_NOT_READ_FROM_MEM,
	WAX_RESULT_NOT_ENOUGH_MEMORY_FOR_SIG_LOG,


	//!  Tokenizer/RPR Tokenizer errors
	WAX_RESULT_INBUFF_INCONSISTENT = 10001,		//!< the input buffer is not in the correct 
												//!< format or some header is broken			
	WAX_RESULT_UNKNOWN_TOKEN_TYPE,				//!< the token type is unknown

	//! WAX ENCODER
	WAX_RESULT_ENTRY_NOT_FOUND        = 11001,  
	WAX_RESULT_GENERATE_TOKENS,                 
	WAX_RESULT_INVALID_BUFFER_OR_SIZE = 11007,  
	WAX_RESULT_INVALID_ADLER               
};

#define WAX_SHAREPOINT_MAGIC_NUMBER 511

//Structure to hold a character string + its length
typedef struct _StringBuffer
{
	unsigned short length;
    unsigned short maxiumLength;
	wchar_t* buffer;

	_StringBuffer()//initializer
	{
		length = 0;
		maxiumLength = 0;
		buffer = nullptr;
	}
} StringBuffer;

//Structure to hold a byte buffer + its length
typedef struct _ByteBuffer
{
	unsigned long length;
    unsigned long maximumLength;
    unsigned char* buffer;

	_ByteBuffer()//initializer
	{
		length = 0;
	    maximumLength = 0;
		buffer = nullptr;
	}

	_ByteBuffer(unsigned long inulLength, unsigned long inulMaximumLength, unsigned char* inlpbBuffer)//copy constructor
	{
		length = inulLength;
		maximumLength = inulMaximumLength;
		buffer = inlpbBuffer;
	}

} ByteBuffer;

//Attribute values - Will be used the soonest in phase 4
enum AttributeValues
{
	WAX_NO_WRITE_TO_TRANSFER_LOG = 0,
	WAX_WRITE_TO_TRANSFER_LOG
};


typedef struct _Characteristics
{
	long attribute;
	long minRegionLength;
} Characteristics;

//! The complete set of statistics and how the values are calculated is described in section 4.9 Stats.
typedef struct _Statistics
{
	/******
	The user/system can interrogate WAX specific statistics:
	***/

	uint64_t uptime;          //!< 1.1. Up time
	uint64_t memoryAllocated; //!< 1.2. Memory allocated by WAX
	uint64_t memoryUsed;      //!< 1.3. Current usage of memory 
	uint64_t diskUsed;        //!< 1.4. Current usage of disk 
	uint64_t compressionRate; //!< 1.5. Compression rate - the actual value is the float 
							   //	compression multiplied by 100.
	uint64_t dataVolLZero;    //!< 1.6. Volume - this is the level 0 data since WAX was initialized

	uint64_t dataVolCurrent;  //!< 1.7. Current data volumes and current compression rates

	uint64_t compressionRateCurrent; //!< 1.8. Current compression rate.

	/******
	The following statistics are only available through SCOPE via WMI. 
	***/

	uint64_t dataVolSinceInit;//!< 2.1. Data volume since WAX was initialized - same as dataVolLZero
	uint64_t dataVolL0;       //!< 2.2. Volume of uncompressed data (level 0 data)
	uint64_t dataVolL1;       //!< 2.3. Volume of data after compression of repeated patterns (level 1 data)
	uint64_t dataVolL2;       //!< 2.4. Volume of data after de-duplication (level 2 data)
	uint64_t dataVolL3;       //!< 2.5. Volume of data after ZLIB (level 3 data)

	unsigned long msgNumber;           //!< 2.6. Number of messages 
	unsigned long regNumber;           //!< 2.7. Number of qualifying regions since last start 
	uint64_t bytesInReg;      //!< 2.8. Bytes in qualifying region since last start

	//////	RPR statistics ///////
	uint64_t rprStringsNumber;     //!< 3.1. Number of RPR Strings 
	uint64_t rprBytesNumber;       //!< 3.2. Bytes in RPR strings 
	uint64_t genericStringsNumber; //!< 3.3. Number of zero-bytes strings
	uint64_t genericBytesNumber;   //!< 3.4. Bytes in zero-bytes strings
	uint64_t daStringsNumber;      //!< 3.5. Number of DA strings
	uint64_t daBytesNumber;        //!< 3.6. Bytes in DA strings

	//////	De-duplication statistics  ///////
	uint64_t dedupStringsNumber;       //!< 4.1. Number of De-Dup Strings 
	uint64_t dedupBytesNumber;         //!< 4.2. Bytes in De-dup strings 
	uint64_t dedupRecentStringsNumber; //!< 4.3. Number of recent De-Dup Strings
	uint64_t dedupRecentBytesNumber;   //!< 4.4. Bytes in recent De-dup strings
	uint64_t dedupOldStringsNumber;    //!< 4.5. Number of old (chunk grow) De-Dup Strings
	uint64_t dedupOldBytesNumber;      //!< 4.6. Bytes in old De-dup strings

	uint64_t cpuTime;                  //!< 5.1. CPU time of the WAX process

	//!< Added for internal use only. It this is set, the components that have supplymentary statistic
	//	values collected, will dump them to the log file using the LogEvent() function.
	bool bDumpAdditionalStats;	

} Statistics;

