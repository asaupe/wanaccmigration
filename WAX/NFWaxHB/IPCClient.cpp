//********************************************************/
//  ./IPCClient.cpp
//
//  Owner : Ciprian Maris 
// 
//	Company		: Neverfail 
//	PROJECT		: WAN Acceleration 
//  Component	: IPC Client
//
//  $Rev: 2616 $ - Revision of last commit
//
/********************************************************
//
// Last Change - Author and Date of last change:
// $Author: cmaris $  
// $Date: 2009-06-10 16:16:14 +0100 (Wed, 10 Jun 2009) $
//
//********************************************************/

//********************************************************/
//
//	The IPC client component is the portion of the IPC bridge that lives in HB.
//
//	Communication roles:
//		- The IPC Server - creates the shared memory and events(Call/Return).
//		- The IPC Client - opens the shared memory and events.
//
//	The invokation of WAXCompressor functions is done through the following mechanism:
//		- The IPC Client 
//			� sets the function parameters in the shared memory by using function 
//			speciffic parameter data types (defined as derivates of the basic type IPCParamteters)
//			� signal the Call event 
//		- The IPC Server
//			� retreives the OperationType requested by the client
//			� takes the input parameters and calls the appropriate WAXCompressor function
//			� signal the Return event 
//
//	NOTE:	All the public methods of the IPCClient component are static and have the same signature 
//			as the ones in WaxCompressor.h. This is done so to allow compile-time swithching between 
//			the 2 operation (in-/out-of- process)modes.
//
//********************************************************/

#include <Windows.h>
#include "IPCClient.h"
#include "ipc.h"
#include "Logger/Logger.h"
#include "AllUtils.h"
#include "common.h"

#include "ServiceNames.h"

#ifdef _DEBUG
#define SHOW_WINDOW true
#else
#define SHOW_WINDOW false
#endif

#pragma warning(disable:4995)
#pragma warning(disable:4996)
#include "shlwapi.h"

HANDLE IPCClient::m_hMapFile				= NULL;

unsigned char* IPCClient::m_lpbSharedMemoryBuffer	= NULL;
unsigned char* IPCClient::m_lpbReturnMemoryBuffer	= NULL;
unsigned char* IPCClient::m_lpbStatisticsBuffer		= NULL;

HANDLE IPCClient::m_hEventCall				= NULL;
HANDLE IPCClient::m_hEventReturn			= NULL;

wchar_t* IPCClient::m_szErrorMessageBuffer	= NULL;
wstring IPCClient::m_strErrorMessage		= L"";

WAX_RESULT IPCClient::m_wrErrorCode			= WAX_RESULT_OK;
WAX_RESULT IPCClient::m_wrReturnCode		= WAX_RESULT_OK;

bool IPCClient::m_bUnitTestMode = false;


//! Method SetError() - stores and loggs the errors from the IPC brigde/WAXCompressor.
/*!	
*	The method is logging the faults occuring in the bridge a bridge and to storing the ones 
*	returned from the compressor.
*	It uses m_strErrorMessage to store the error message.
*/
void IPCClient::SetError(
	/*__in*/ wchar_t* inszErrorString,	//!< Error message to be logged and stored.
	/*__in*/ WAX_RESULT inwrErrorCode,	//!< Error code to be logged and stored.
	/*__in*/ wchar_t* inszLocation		//!< Location where the fault occured. If NULL, no location 
									//is logged.
	)
{
	wchar_t* szErrorLocation = L" ";
	
	if( inszLocation != NULL )
	{
		m_strErrorMessage = wstring( inszLocation ) + wstring (L" ");
		szErrorLocation = inszLocation;
	}

	m_strErrorMessage.append( inszErrorString );
	m_wrErrorCode = inwrErrorCode;

	//Also log the error to the file.
	//	BZ 7876 - do not use formatted error messages here since it could cause a runtime error
	//	in the case the error message contains format characters that are not escaped.
    LogEventMessage(LOG_LEVEL_ERROR, WAX_ID_IPC_CLIENT,(wchar_t*)m_strErrorMessage.c_str() );
//	LogError( WAX_ID_IPC_CLIENT, szErrorLocation, (WCHAR*)m_strErrorMessage.c_str() );

}
//-------------------------------------------------------------------------------------------------


//!	Method ResetError() - Clears the previous error messages and code.
/*
*	The method clears the previous error messages and code.
*	This method must be called when entering any method in the bridge.
*/
void IPCClient::ResetError()
{
	m_strErrorMessage.clear();

	m_wrErrorCode  = WAX_RESULT_OK;
	m_wrReturnCode = WAX_RESULT_OK;
}
//-------------------------------------------------------------------------------------------------


//! Method ResetEvents() - Resets the events in the bridge.
void IPCClient::ResetEvents()
{
	ResetEvent( m_hEventCall );
	ResetEvent( m_hEventReturn );
}
//-------------------------------------------------------------------------------------------------

//! Method initialize() - initializes the IPC Client (logger, security, IPC components)
//	and remotely calls the compressor initialization function.
/*!
*	The Method
*		- calls initializeIPCClientLogger() to initialize the local logger.
*		- calls NFInitSecurity() to initialize the security need for IPC.
*		- calls initializeIPCComponents() to initialize the components involved in IPC.
*		- remotely calls the compressor initialization function.
*
*	Return: In case of success return WAX_RESULT_OK. In the case of failure it returns the failure reason.
*
*/
WAX_RESULT IPCClient::initialize(
		/*__in*/	const wchar_t* logFileName,	//!< The name of the log file.
		/*__in*/	bool inbSetDebugMode		//!< The initial debug mode of the compressor.
	)
{
	//Must be the first method call
	ResetError();

	////	Initialize the local IPC logger and debug mode.
	if ( !initializeIPCClientLogger( logFileName, inbSetDebugMode ) )
	{
		//NOTE: Since the error is already set in the initializeIPCClientLogger() method,
		//we'll let it propagate without overwriting it =>only return failure.
		return WAX_RESULT_LOG_INIT_FAILED;
	}
	

	////	Initialize security
	//	NOTE: In order to be able to log errors from init security, we need the logger to be 
	//	initialized prior to this call.
	if ( !NFInitSecurity() )
	{
		//LOG ERROR
		SetError( L"NFInitSecurity failed.", WAX_RESULT_INIT_SECURITY_FAILED, DBG_LOCATION);
		return WAX_RESULT_INIT_SECURITY_FAILED;
	}

	////	Check if the WAX service wa started in UnitTest mode or start it now.
	if( !m_bUnitTestMode )
	{
		//The WAX service was not started in console mode => Start it in service mode.
		if( !startWAXService( NF_WAX_SERVICE_WINDOWS_NAME, false ) )
		{
			//The error code is set by the startWAXService() method.
			return m_wrErrorCode;
		}
	}

	////	Initialize the components involved in IPC.
	WAX_RESULT wrInitIPCComponentsResult = initializeIPCComponents();

	if( wrInitIPCComponentsResult != WAX_RESULT_OK )
	{
		//we do not set an error in this case since the error has already been set 
		// in the initializeIPCComponents method.
		return wrInitIPCComponentsResult;
	}


	////	Call out of process initialize

	//Set the adress of the initialisation paramaters.
	InitializeParameters* initParams = (InitializeParameters*)m_lpbSharedMemoryBuffer;

	//Set init params in the shared memory buffer
	initParams->operation  = OpInititialize;
	initParams->bDebugMode = inbSetDebugMode;

	//	NOTE: If the input log file name is NULL we set the value here to emty string 
	//	leaving it to the logger to use its own	default value for the log name.
	initParams->ulStringLength = UtilSafeCopyString( initParams->szString, logFileName ) + 1;//+1 for terminating zero

	//Handle the IPC call
	return SignalEventAndProcessReturn( TIMEOUT_IPC_MEDIUM, L"Initialize", L"IPC Initialized." );
}
//-------------------------------------------------------------------------------------------------


//! Method exit() - cleans up all IPC components and remotely calls the compressor exit function.
/*!
*	The method 
*		- remotely calls the compressor exit function
*		- calls exitIPCComponents() to clean up components involved in IPC.
*		- Logs the exit event and closes the logger.
*/
WAX_RESULT IPCClient::exit(void)
{
	//ResetErrors
	ResetError();

	//Signal the Service that we exit = Call out of process exit.
	IPCParamteters* ipcParams = (IPCParamteters*)m_lpbSharedMemoryBuffer;

	//Set exit params in the shared memory buffer
	ipcParams->operation  = OpExit;

	//Handle the IPC call
	//NOTE: m_wrReturnCode is set in the Method, so we do not need to asign it here.
	SignalEventAndProcessReturn( TIMEOUT_IPC_MEDIUM, L"Exit" );

	////	Clean up the IPC components.
	exitIPCComponents();

	//Log final message and close logger.
	LogEventMessage( LOG_LEVEL_INFO, WAX_ID_IPC_CLIENT, L"IPC exited." );
	Logger::Close();

	m_bUnitTestMode = false; //signal the initialize() method that the service is stopped in unit test mode.

	return m_wrReturnCode;
}
//-------------------------------------------------------------------------------------------------


//! Method allocate() - calls compressor allocate to pre-book resouces.
WAX_RESULT IPCClient::allocate(
		/*__in*/ unsigned long inulMinimumMemoryBufferSize,		//!< Minimum memory to allocate for the compressor in MB's
		/*__in*/ unsigned long inulMaximumMemoryBufferSize,		//!< Maximum memory to allocate for the compressor in MB's
		/*__in*/ unsigned long inulTransferLogSize,				//!< On disk transfer Log Size in MB's
        /*__in*/ const StringBuffer* insbTransferLogPath//!< On disk transfer Log File path (complete path)
		)
{
	//ResetErrors
	ResetError();

	AllocateParameters* allocateParams = (AllocateParameters*)m_lpbSharedMemoryBuffer;

	//Set allocate params in the shared memory buffer
	allocateParams->operation  = OpAllocate;
	allocateParams->ulMinimumMemoryBufferSize = inulMinimumMemoryBufferSize;// Minimum memory to allocate for the compressor in MB's
	allocateParams->ulMaximumMemoryBufferSize = inulMaximumMemoryBufferSize;// Maximum memory to allocate for the compressor in MB's
	allocateParams->ulTransferLogSize		  = inulTransferLogSize;		// On disk transfer Log Size in MB's
	
	//Character string length and value for log file = On disk transfer Log File path (complete path)
	allocateParams->ulStringLength = UtilSafeCopyString( 
										allocateParams->szString, 
										insbTransferLogPath->buffer,  
										insbTransferLogPath->length)
										+ 1;//+1 for terminating zero

	//Handle the IPC call
	return SignalEventAndProcessReturn( TIMEOUT_IPC_MEDIUM, L"Allocate",  L"IPC Allocated." );

}
//-------------------------------------------------------------------------------------------------


//! Method release() - calls compressor release to free pre-booked resouces.
WAX_RESULT IPCClient::release( void )
{
	//ResetErrors
	ResetError();

	//Signal the Service that we release.
	IPCParamteters* ipcParams = (IPCParamteters*)m_lpbSharedMemoryBuffer;

	//Set release params in the shared memory buffer
	ipcParams->operation  = OpRelease;

	//Handle the IPC call
	return SignalEventAndProcessReturn( TIMEOUT_IPC_MEDIUM, L"Release", L"IPC Released." );
}
//-------------------------------------------------------------------------------------------------


//! Method start() - starts the compressor.
WAX_RESULT IPCClient::start(
		/*__in*/ bool inAsEncoder,						//!< Server role (compressor/decompressor)
		/*__in*/ unsigned long inSignatureBase,					//!< signature base
		/*__in*/ unsigned long inSignatureDistance,				//!< signature distance
		/*__in*/ unsigned long inMinRPRPatternsLength,			//!< minimum length of ZERO/DA pattern replaced by a RPR token
		/*__in*/ unsigned long inMinDeDuplicateLength,			//!< minimum length of the buffer to be considered for DD
		/*__in*/ unsigned long inMinDDLengthReplacedByToken,	//!< minimum length of data replaced by a DD token
		/*__in*/ const bool inEncodeOutOfBoundaryEnabled,	//!< advanced encode for out of boundary entries
		/*__in*/ double indNInvariant						//!< Factor for recent data volume and compression computation.
		)
{
	//ResetErrors
	ResetError();

	//Signal the Service that we release.
	StartParameters* startParams = (StartParameters*)m_lpbSharedMemoryBuffer;

	//Set start params in the shared memory buffer
	startParams->operation  = OpStart;

	startParams->bAsEncoder						= inAsEncoder;				//!< Server role (compressor/decompressor)
	startParams->ulSignatureBase				= inSignatureBase;			//!< signature base
	startParams->ulSignatureDistance			= inSignatureDistance;		//!< signature distance
	startParams->ulMinRPRPatternsLength			= inMinRPRPatternsLength; 	//!< minimum length of ZERO/DA pattern replaced by a RPR token
	startParams->ulMinDeDuplicateLength			= inMinDeDuplicateLength;	//!< minimum length of the buffer to be considered for DD
	startParams->ulMinDDLengthReplacedByToken	= inMinDDLengthReplacedByToken;	//!< minimum length of data replaced by a DD token
	startParams->bEncodeOutOfBoundaryEnabled	= inEncodeOutOfBoundaryEnabled;	//!< advanced encode for out of boundary entries
	startParams->dNInvariant					= indNInvariant;				//!< Factor for recent data volume and compression computation.

	//Handle the IPC call
	return SignalEventAndProcessReturn( TIMEOUT_IPC_LONG, L"Start", L"IPC Started." );
}
//-------------------------------------------------------------------------------------------------


//! Method stop() - stops the compressor.
WAX_RESULT IPCClient::stop(void)
{
	//ResetErrors
	ResetError();

	//Signal the Service that we release.
	IPCParamteters* ipcParams = (IPCParamteters*)m_lpbSharedMemoryBuffer;

	//Set stop params in the shared memory buffer
	ipcParams->operation  = OpStop;

	//Handle the IPC call
	return SignalEventAndProcessReturn( TIMEOUT_IPC_LONG, L"Stop", L"IPC Stopped." );
}
//-------------------------------------------------------------------------------------------------


//	Encode-decode
//! Method encode() - encodes the input buffer on source.
WAX_RESULT IPCClient::encode(
		/*__in*/ const ByteBuffer* inputBuffer,	//!< source buffer to be encoded
        /*__out*/ ByteBuffer* outputBuffer,		//!< destination buffer - pre-allocated by caller
        /*__in*/ bool removeRepeatedPatterns,	//!< FLAG - RPR requested by caller
        /*__in*/ bool deDuplicate				//!< FLAG - DD requested by caller
		)
{
	//ResetErrors
	ResetError();

	//Signal the Service that we have an encode.
	EncodeParameters* encodeParams = (EncodeParameters*)m_lpbSharedMemoryBuffer;

	//Set encode params in the shared memory buffer.
	encodeParams->operation = OpEncode;

	encodeParams->removeRepeatedPatterns	= removeRepeatedPatterns;	//!< FLAG - RPR requested by caller
    encodeParams->deDuplicate				= deDuplicate;				//!< FLAG - DD requested by caller

	//Length of the output buffer.
	encodeParams->ulOutBufferLen = outputBuffer->length;

	//serialized ByteBuffer - input.
	encodeParams->ulInBufferLen = inputBuffer->length;
	memcpy(encodeParams->lpbInBuffer, inputBuffer->buffer, inputBuffer->length );//copy input buffer bytes.

	//TO DO: deal with annotations if necessary or remove them altogether.
	//Annotations* annotations, //!< write Region info + exclusion info

	//Handle the IPC call
	if( SignalEventAndProcessReturn( TIMEOUT_IPC_MEDIUM, L"Encode"  ) == WAX_RESULT_OK )
	{//	The IPC encode call was succesfull. Copy the returned buffer to the output buffer.
		ReturnResults* returnResults = (ReturnResults*)m_lpbReturnMemoryBuffer;

		outputBuffer->length = returnResults->ulRetBufferLen;//set the length.
		memcpy( outputBuffer->buffer, returnResults->lpbRetBuffer, returnResults->ulRetBufferLen );//copy the bytes.
	}

	return m_wrReturnCode;
}
//-------------------------------------------------------------------------------------------------


//! Method decode() decodes the input buffer on target.
WAX_RESULT IPCClient::decode(
        /*__in*/  const ByteBuffer* inputBuffer,	//!< source buffer to be decoded
        /*__out*/ ByteBuffer* outputBuffer,	//!< destination buffer - pre-allocated by caller
		/*__in*/ bool	bRPREncoded,
		/*__in*/ bool	bDDEncoded 
		)
{
	//ResetErrors
	ResetError();

	//Signal the Service that we have an decode.
	DecodeParameters* decodeParams = (DecodeParameters*)m_lpbSharedMemoryBuffer;

	//Set encode params in the shared memory buffer.
	decodeParams->operation = OpDecode;

	//Length of the output buffer.
	decodeParams->ulOutBufferLen = outputBuffer->length;

	//decompression flags
	decodeParams->bRPREncoded = bRPREncoded;
	decodeParams->bDDEncoded = bDDEncoded;

	//serialized ByteBuffer - input.
	decodeParams->ulInBufferLen = inputBuffer->length;
	memcpy(decodeParams->lpbInBuffer, inputBuffer->buffer, inputBuffer->length );//copy input buffer bytes.

	//Handle the IPC call
	if( SignalEventAndProcessReturn( TIMEOUT_IPC_MEDIUM, L"Decode"  ) == WAX_RESULT_OK )
	{//	The IPC encode call was succesfull. Copy the returned buffer to the output buffer.
		ReturnResults* returnResults = (ReturnResults*)m_lpbReturnMemoryBuffer;

		outputBuffer->length = returnResults->ulRetBufferLen;//set the length.
		memcpy( outputBuffer->buffer, returnResults->lpbRetBuffer, returnResults->ulRetBufferLen );//copy the bytes.
	}

	return m_wrReturnCode;
}
//-------------------------------------------------------------------------------------------------


//! Method queryStatistics() - gathers stats from the WAX components.
WAX_RESULT IPCClient::queryStatistics(
		/*__inout*/ Statistics *inoutStatistics //!<	statistics structure to receive the entire statistics set
		)
{
	//ResetErrors
	ResetError();

	IPCParamteters* ipcParams = (IPCParamteters*)m_lpbSharedMemoryBuffer;

	//copy the input statistics structure into the shared memory budder.
	memcpy( m_lpbStatisticsBuffer, inoutStatistics, sizeof( Statistics ) ); 

	//Request query paramaters
	ipcParams->operation = OpQueryStatistics;

	if( SignalEventAndProcessReturn( TIMEOUT_IPC_MEDIUM, L"QueryStatistics"  ) == WAX_RESULT_OK )
	{
		//copy the statistics into the output structure.
		memcpy( inoutStatistics, m_lpbStatisticsBuffer, sizeof( Statistics ) ); 
	}

	return m_wrReturnCode;
}
//-------------------------------------------------------------------------------------------------


//! Method setDebugMode - set logger to enable/disable printing out of debug info.
WAX_RESULT IPCClient::setDebugMode(
		/*__in*/ bool inbOn
		)
{
	//ResetErrors
	ResetError();

	IPCParamteters* ipcParams = (IPCParamteters*)m_lpbSharedMemoryBuffer;

	//Request set debug mode on or of 
	if( inbOn )
	{
		ipcParams->operation = OpSetDebugModeOn;

		//Set the local logger debug mode to the desired value.
		Logger::SetLogLevel(Logger::GetLogLevel() | LOG_LEVEL_DEBUG);
	}
	else
	{
		ipcParams->operation = OpSetDebugModeOff;

		//Set the local logger debug mode to the desired value.
		Logger::SetLogLevel(Logger::GetLogLevel() & ~LOG_LEVEL_DEBUG);
	}

	//Signal the service and process the return result.
	SignalEventAndProcessReturn( TIMEOUT_IPC_MEDIUM, L"SetDebugMode"  ) ;

	return m_wrReturnCode;
}

//-------------------------------------------------------------------------------------------------


//! Signals a call event, waits for the out of process reply and processes the 
//	return code + error message if any.
/*!
*	The method
*		- resets the events
*		- signals the call event and waits for the return event
*		- sets the return code
*		- saves the error locally and logs it if there is one
*
**/
WAX_RESULT IPCClient::SignalEventAndProcessReturn( 
	/*__in*/ unsigned long indwTimeout,					//!< Wait timeout in milisoconds. The 
											//caller has to set it accordingly.
	/*__in*/ wchar_t* inszOperationName,			//!< Operation name to be used in error logging.
	/*__in*/ wchar_t* inszEventMessageOnsuccess	//!< Event message to be logged if the method succedes.
											//If NULL, nothing is logged.
	)
{
	ResetEvents();
	unsigned long dwWaitResult = SignalObjectAndWait( m_hEventCall, m_hEventReturn, indwTimeout, FALSE );
	ResetEvents();

	if( dwWaitResult == WAIT_OBJECT_0 )
	{
		//Get the return code from the shared memory
		LogDebug( WAX_ID_IPC_CLIENT, DBG_LOCATION, L"IPCClient - Service Return Code: %d\n", ((ReturnResults*)m_lpbReturnMemoryBuffer)->ulReturnCode );

		//store the last return code here
		m_wrReturnCode = ((ReturnResults*)m_lpbReturnMemoryBuffer)->ulReturnCode;

		if( m_wrReturnCode != WAX_RESULT_OK )
		{
			SetError( m_szErrorMessageBuffer, m_wrReturnCode ); 
		}
	}
	else
	{
		LogError( WAX_ID_IPC_CLIENT, DBG_LOCATION, L"Wait for Event failed with dwWaitResult %d while perfroming operation: '%s'", 
			dwWaitResult, inszOperationName );

		SetError( L"Wait for Event failed.", WAX_RESULT_WAIT_FAILED, DBG_LOCATION);

		//	In this case the service migh already be shut down.
		//	We only signal the caller and move to Clean-Up.
		m_wrReturnCode = WAX_RESULT_WAIT_FAILED;
	}

	// Log the on-success message.
	if( ( inszEventMessageOnsuccess != NULL ) && ( m_wrReturnCode == WAX_RESULT_OK ) )
	{
		LogEventMessage( LOG_LEVEL_INFO, WAX_ID_IPC_CLIENT, inszEventMessageOnsuccess );
	}

	return m_wrReturnCode;

}
//-------------------------------------------------------------------------------------------------



//! Initializes the IPC Client Logger.
/*!	The method
*		- Composes the IPCClient logger path name for the IPC client by appending the LOG_IPC_ADDON
*		before the extension of the logger passed to the WAX Compressor.
*		E.g.
*			log_file_name_passed_to_provider = "c:\NF\logs\WAXCpmpressor.log"
*			LOG_IPC_ADDON = -IPC-
*
*			IPCClient_log_file_name will be "c:\NF\logs\WAXCpmpressor-IPC-.log"
*
*		- Calls the logger initialize.
*	
*	This method was changed as a resolution to BZ 8261 to call Logger::ComposeNameAndInitialize().
*
*/
bool IPCClient::initializeIPCClientLogger(
		/*__in*/	const wchar_t* logFileName,	//!< The name of the log file passed to the compression provider.
		/*__in*/	bool inbSetDebugMode		//!< The initial debug mode of the compressor.
		)
{

	//initialize the return error message to 0
	wstring strErrorMessage = L"";

	if( !Logger::ComposeNameAndInitialize( logFileName, LOG_DEFAULT_NAME, LOG_IPC_ADDON, inbSetDebugMode, strErrorMessage ) )
	{
		SetError( (wchar_t*)strErrorMessage.c_str() , WAX_RESULT_LOG_INIT_FAILED, DBG_LOCATION );
		return false;//return false since we did fail to allocate the name buffer.
	}

	return true;
}
//-------------------------------------------------------------------------------------------------

//! Method initializeIPCComponents() - initializes the components involved in IPC(shared memory, events, etc.) .
/*!	The method
*		- Opens the shared memory buffer
*		- Opens the Call and Return Events
*		- Sets the ReturnMemoryBuffer, StatisticsBuffer and ErrorMessageBuffer to point to the pre-defined 
*		offsets in the shared memory buffer.
*
*	Return: In case of success return WAX_RESULT_OK. In the case of failure it returns the failure reason.
*
*	NOTE:	[1] This method uses IPC variables defined in "ipc.h". The method call comments spcify if
*			a used variable is defined in "ipc.h".
*			Refer to the "ipc.h" header comments for details on these variables.
*/
WAX_RESULT IPCClient::initializeIPCComponents( void )
{
	//Open shared Memory
	//	- open FileMapping
	//	- Map View of file

	m_hMapFile = openSharedMemoryFileMapping();

	if (m_hMapFile == NULL) 
	{ 
		//LOG ERROR
		SetError( L"OpenFileMapping failed.", WAX_RESULT_OPEN_FILE_MAPPING_FAILED, DBG_LOCATION);
		return WAX_RESULT_OPEN_FILE_MAPPING_FAILED;
	}

	m_lpbSharedMemoryBuffer = (unsigned char*) MapViewOfFile(
									m_hMapFile,			// handle to map object
									FILE_MAP_ALL_ACCESS,// read/write permission
									0,                   
									0,                   
									SHARED_MEM_BUF_SIZE// Size of the shared memory buffer declared in "ipc.h"
									);           
 
	if (m_lpbSharedMemoryBuffer == NULL) 
	{ 
		//LOG ERROR and return 
		CloseHandle( m_hMapFile );
		SetError( L"MapViewOfFile failed.", WAX_RESULT_MAP_VIEW_OF_FILE_FAILED, DBG_LOCATION);
		return WAX_RESULT_MAP_VIEW_OF_FILE_FAILED;
	}

	//Set the addrres in the shared memory of the return buffer
	m_lpbReturnMemoryBuffer =	m_lpbSharedMemoryBuffer + 
								SHARED_MEM_RETURN_OFFSET;//declared in "ipc.h"

	m_lpbStatisticsBuffer	=	m_lpbSharedMemoryBuffer + 
								SHARED_MEM_STATS_OFFSET; //declared in "ipc.h"

	//Set the addrres in the shared memory of the error message that comes form the compressor
	m_szErrorMessageBuffer = (wchar_t*)(m_lpbSharedMemoryBuffer + 
									SHARED_MEM_ERROR_MESSAGE_OFFSET);//declared in "ipc.h"

	////	Open Events (Call and Return)
	m_hEventCall = OpenEvent(
						SYNCHRONIZE | EVENT_MODIFY_STATE , 
						FALSE, 
						IPC_STR_CALL_EVENT_NAME //declared in "ipc.h"
						);
	if( m_hEventCall == NULL )
	{
		//LOG ERROR and return 
		SetError( L"OpenEvent(EventCall) failed.", WAX_RESULT_OPEN_EVENT_FAILED, DBG_LOCATION);
		return WAX_RESULT_OPEN_EVENT_FAILED;
	}

	m_hEventReturn = OpenEvent( 
						SYNCHRONIZE | EVENT_MODIFY_STATE , 
						FALSE, 
						IPC_STR_RETURN_EVENT_NAME //declared in "ipc.h"
						);
	if( m_hEventReturn == NULL )
	{
		//LOG ERROR and return 
		SetError( L"OpenEvent(EventReturn) failed.", WAX_RESULT_OPEN_EVENT_FAILED, DBG_LOCATION);
		return WAX_RESULT_OPEN_EVENT_FAILED;
	}

	//Reste the events once their handles have been opened.
	ResetEvents();

	return WAX_RESULT_OK;
}
//-------------------------------------------------------------------------------------------------

//!Method exitIPCComponents() - cleans up the components involved in IPC.
/*!
*	The method
*		- cleans up all the components involved in IPC.
*	
*	NOTE:	This method is implemented for symetry with the initializeIPCComponents() method.
*
**/
void IPCClient::exitIPCComponents( void )
{
	//Clean-Up
	if( m_hEventReturn )
	{
		CloseHandle( m_hEventReturn );
		m_hEventReturn = NULL;
	}

	if( m_hEventCall )
	{
		CloseHandle( m_hEventCall );
		m_hEventCall = NULL;
	}


	if( m_lpbSharedMemoryBuffer )
	{
		UnmapViewOfFile(m_lpbSharedMemoryBuffer);
		m_lpbSharedMemoryBuffer = NULL;
	}

	if( m_hMapFile )
	{
		CloseHandle( m_hMapFile );
		m_hMapFile = NULL;
	}

}
//-------------------------------------------------------------------------------------------------

//! Starts the compressor process in console mode. 
WAX_RESULT IPCClient::setup(
	/*__in*/ StringBuffer* inSBServicePath
	)
{
	ResetError();

	wchar_t* szCurrentDirectoryFullPath = NULL;
	wchar_t* szServicePath = L".";// By default set it to the current dir ".".

	wstring strWaxServiceFullPath;

	if( inSBServicePath != NULL )
	{
		if( inSBServicePath->buffer != NULL )
		{
			szServicePath = inSBServicePath->buffer;
		}
	}

	//	We stop the WAX Service in case it ran before. This avoids dealing with errors if the service hangs.
	//	We do not check for the error code, since the service might not be running in the first place.
	teardown();

	//Get the full path of the current process directory.
	if ( !NFGetFullPath( szServicePath, szCurrentDirectoryFullPath ) )
	{
		SetError( L"NFGetFullPath() failed.", WAX_RESULT_SETUP_FAILED, DBG_LOCATION ); 
		goto ERREXIT;
	}

	strWaxServiceFullPath = szCurrentDirectoryFullPath;

	//Append "\\" in needed
	if( strWaxServiceFullPath.c_str()[ strWaxServiceFullPath.length() - 1] != L'\\' )
	{
		strWaxServiceFullPath = strWaxServiceFullPath + L"\\";
	}

	strWaxServiceFullPath = strWaxServiceFullPath + NF_WAX_SERVICE_EXE_NAME;

	if( !startWAXService( strWaxServiceFullPath.c_str(), true ) )
	{
		//	NOTE: do not set error here since it is set in the method startWAXService().
		goto ERREXIT;
	}

	//SUCCESS EXIT
	if( szCurrentDirectoryFullPath != NULL )
	{
		delete szCurrentDirectoryFullPath;
		szCurrentDirectoryFullPath = NULL;
	}

	m_bUnitTestMode = true; //signal the initialize() method that the service is started in unit test mode.
	return WAX_RESULT_OK;

ERREXIT://eronous exit
	if( szCurrentDirectoryFullPath != NULL )
	{
		delete szCurrentDirectoryFullPath;
		szCurrentDirectoryFullPath = NULL;
	}

	//Since start failed we signal the initialize() method that the service is NOT started in unit test mode.
	m_bUnitTestMode = false;

	return WAX_RESULT_SETUP_FAILED;
}
//-------------------------------------------------------------------------------------------------

//! Stops the compressor process. 
WAX_RESULT IPCClient::teardown(void)
{
	ResetError(); 

	m_bUnitTestMode = false; //signal the initialize() method that the service is stopped in unit test mode.

	////	Stop the WAX Service if it is running.
	if( isServiceRunning() )
	{
		if (!NFStopProcess( NF_WAX_SERVICE_EXE_NAME ) )
		{
			SetError( L"NFStopProcess() failed.", WAX_RESULT_TEAR_DOWN_FAILED, DBG_LOCATION ); 
			return WAX_RESULT_TEAR_DOWN_FAILED;
		}
	}

	return WAX_RESULT_OK;

}
//-------------------------------------------------------------------------------------------------

//!startWAXService() - starts the wax service in console or service mode and waits for it to initialize.
bool IPCClient::startWAXService(
	/*__in*/ const wchar_t* inszService,
	/*__in*/ bool inbIsConsole

	)
{
	//Set the console mode "/c" command line parameter if required by caller.
	wchar_t* szCommandLineParameters = inbIsConsole ? L"/c" : L"";

	//Start the process
	if ( !NFSartProcess( inszService, szCommandLineParameters, SHOW_WINDOW, !inbIsConsole ) )
	{
		SetError( L"NFSartProcess() failed.", WAX_RESULT_SERVICE_START_FAILED, DBG_LOCATION ); 
		goto ERREXIT;
	}

	//Wait for the process to start a maximum of TIMEOUT_IPC_LONG miliseconds
	if( !isServiceRunning( TIMEOUT_IPC_LONG ) )
	{
		//	NOTE: Set the error here since the isServiceRunning() does not set it!!!
		SetError( L"timed out waiting for NFWaxService to start", WAX_RESULT_SERVICE_START_FAILED, DBG_LOCATION );
		goto ERREXIT;
	}

	return true;

ERREXIT:

	return false;
}
//-------------------------------------------------------------------------------------------------

//!Method openSharedMemoryFileMapping() - utility method for opening the shared memory file mapping.
/*!
*	The method opens the named file mapping object identified by IPC_STR_MEMORY_MAPPING_NAME.
*
*	NOTE:	[1] This method is shared by all methods that check if the WAX Service is running.
*			There will be no references to the API function OpenFileMapping() other than this one.
*
*	NOTE:	[2] This is a utility method. It does not set the IPCClient error component. The caller
*			of this method must set the appropriate error.
*
*/
HANDLE IPCClient::openSharedMemoryFileMapping( void )
{

	return OpenFileMapping(
				   FILE_MAP_ALL_ACCESS,			// read/write access
				   FALSE,						// do not inherit the name
				   IPC_STR_MEMORY_MAPPING_NAME	// name of mapping object declared in "ipc.h"
				   );               
}
//-------------------------------------------------------------------------------------------------

//! Method isServiceRunning() - utility method checks if the service is running for a timeout of 
//								indwWaitTimeout miliseconds.
/*!
*	The method attempts to open the Shared Memory File Mapping foe a period of time given by the 
*	indwWaitTimeout input parameter
*	
*	Return: True if the mapping was sucesfully opened and false otherwise.
*
*	NOTE:	This is a utility method. It does not set the IPCClient error component. The caller
*			of this method must set the appropriate error.
*
*/
bool IPCClient::isServiceRunning( 
	/*__in*/ unsigned long indwWaitTimeout 
	)
{
	unsigned long dwSleepStep = 10;
	HANDLE hMapFile = NULL;

	//Check if the process is started a maximum of indwWaitTimeout miliseconds
	for(unsigned long dwTimeElapsed = 0; dwTimeElapsed <= indwWaitTimeout ; dwTimeElapsed += dwSleepStep )
	{
		hMapFile = openSharedMemoryFileMapping();      

		if( NULL != hMapFile ) 
		{
			break;
		}
		else
		{
			Sleep( dwSleepStep );
		}
	}

	if (hMapFile == NULL) //check if we timed out
	{ 
		return false;
	}
	else
	{
		CloseHandle( hMapFile );
		hMapFile = NULL;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------

#pragma warning(default:4995)
#pragma warning(default:4996)
