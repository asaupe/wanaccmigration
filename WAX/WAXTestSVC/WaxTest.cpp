// WaxTest.cpp : Defines the entry point for the console application.
//
#pragma warning (disable:4996)
#pragma warning (disable:4995)

#include "WaxTest.h"
#include "shlwapi.h"

#include "allutils.h"

#include "IniReader.h"

#include "logger/Logger.h"

#include <io.h>
#include <stdio.h>

#include "ConfigMan/ConfigMan.h"
#include "NFWaxHB.h"
#include "ipc.h"


void timeb2wstring(_timeb Time, wstring& str);
void testLoggerInitialize(void);

//Log file names
wstring wszLogNameDLL = L"";
wstring wszLogNameEXE = L"";

AllocateParams allocateParams;
StartParamsEnc startParams;
CompressParams compressParams;
FileParams fileParams;

IniReader *iniReader = NULL;

ExecutionTimes executionTimes;
CompressionStatisitcs stats;

#define BBUFFER_READ_LEN 512 * KILO
#define BBUFFER_WRITE_LEN 1024 * KILO

ByteBuffer* bbZlib = new ByteBuffer();
bool isTimestampInDump = false;
ULONG ulOpaqueReadBuffLen = 64;
ULONG ulNrRepeatsForSameFile = 0;
ULONG ulProcessOffset = 0;

int wmain(int argc, wchar_t* argv[])
{
	int retVal = 0;

	if (argc == 1)
	{
		//the application has no input parameter. It will test the logger component and exit.
		testLoggerInitialize();
		return 0;
	}

	//0. initialize the WAX test component
	if( !NFInitSecurity() )
	{
		OUT_ERR L"Could not initialize Security for EXE!" WENDL
		return 1;
	}

	//1. get command line parameters
	if( !getCommandLineParametersAndShowLogo( argc, argv ) )
	{
		return 1;
	}

	//2. get configuration parameters
	if ( !getConfigurationParametersAndAllocateStart() )
	{
		return 1;
	}

	//3. initialize statistics
	InitStatistics();

	//4. process input file
	if( !ProcessInputFile( fileParams, compressParams ) )
	{
		retVal = 1;
		WOUT L"ProcessInputFile() failed" WENDL
	}

	//5. Trigger the write out of statistics in components.
	//	gather statistics + extended statistics and write them to disk
	GetWAXStatistics( stats.sStatsFileName, false, false );
	GetWAXStatistics( stats.sExtStatsFileName, true, true );

	//6. stop / release compressor 
	StopAndRelease();

	//7. Print out execution times
	PrintOutExecutionTimes();

	return retVal;
}

void StopAndRelease(void)
{
	stopCM();
	releaseCM();
	exitCM();
}

//initialize statistics
void InitStatistics(void)
{
	executionTimes.CPUFreq = getCPUFrequency();
	executionTimes.startTime = getTimeNowByQueryPerformanceCounter();
}

//Finalize statistics
void PrintOutExecutionTimes(void)
{
	executionTimes.stopTime = getTimeNowByQueryPerformanceCounter();
	getCPUAndRUNTimesInMS( executionTimes.msCPUTime, executionTimes.msRuntime );

	//now print them out
	WOUT L"" WENDL
	WOUT L"RUN Time (ms): " << executionTimes.msRuntime WENDL;
	WOUT L"CPU Time (me): " << executionTimes.msCPUTime WENDL;
}

// get command line parameters
bool getCommandLineParametersAndShowLogo(int argc, wchar_t* argv[])
{
	wcout << L"WAX TEST - utility for WAX compressor testing." << endl;
	wcout << L"WAX compressor version: " << getDLLRevisionStringCM() << endl << endl;

	wcout << L"Usage:   Waxtest.exe Operation RPR DD ZLibLevel isDump InputFileName OutputFileName" << endl;
	wcout << L"Example: Waxtest.exe 1 0 1 5 1 src.dat dest.dat" << endl;
	wcout << endl << endl;

	if( argc < 8 )
	{
		wcout << L"ERROR: To few Parameters!" << endl;
		return false;
	}

	compressParams.isEncoder  = (argv[1][0] == L'1') ? true : false;

	compressParams.RPR = (argv[2][0] == L'1') ? true : false;

	compressParams.DD  = (argv[3][0] == L'1') ? true : false;

	compressParams.ZLibCompLevel = _wtoi( argv[4] );
//	setZLIBCompressionLevel( (int)compressParams.ZLibCompLevel );

	fileParams.isNFdump = (argv[5][0] == L'1') ? true : false;

	fileParams.inputFileName.buffer = argv[6];
	fileParams.inputFileName.length = (USHORT)wcslen( fileParams.inputFileName.buffer );

	if( !PathFileExists( fileParams.inputFileName.buffer ) )
	{
		OUT_ERR << L"Input file does not exist." << endl; 
		return false;
	}
	if( PathIsDirectory( fileParams.inputFileName.buffer ) )
	{
		OUT_ERR << L"Input file is a directory." << endl; 
		return false;
	}

	fileParams.outputFileName.buffer = argv[7];
	fileParams.outputFileName.length = (USHORT)wcslen( fileParams.outputFileName.buffer );

	HANDLE hFileHandle = NFOpenFileMax( fileParams.outputFileName.buffer, CREATE_ALWAYS, TRUE );
	if( !NFIsValidFileHandle( hFileHandle ) )
	{
		OUT_ERR << L"Output file can not be opened." << endl; 
		return false;
	}
	CloseHandle( hFileHandle );//it will be re-opened for processing

	//get the name of the application name and the name of the ini file as AppName.ini
	char* szTmp = new char[MAX_PATH];
	memset( szTmp, 0, MAX_PATH );

	wcstombs( szTmp, argv[0], wcslen( argv[0] ) );
	string strInifileName = szTmp;
	strInifileName.append( ".ini" );

	iniReader = new IniReader( strInifileName.c_str() );
	delete(szTmp);

	//get the log name as AppName.log
	wszLogNameDLL = argv[0];
	wszLogNameEXE = argv[0];

	stats.sStatsFileName = argv[0];
	stats.sStatsFileName.append(L".STATS.txt");

	stats.sExtStatsFileName = argv[0];
	stats.sExtStatsFileName.append(L".EXT-STATS.txt");

	wszLogNameDLL.append(L".DLL.log");
	wszLogNameEXE.append(L".EXE.log");

///////////////////////////////////////////////////////////////////////////////////////////////////
//	Current run manifest
	wcout << L"Current Parameters:" << endl;

	wcout << L"\t Operation : " << ( compressParams.isEncoder ? L"Encode" : L"Decode")  << endl;
	wcout << L"\t RPR       : " << ( compressParams.RPR ? L"yes" : L"no") << endl;
	wcout << L"\t DD        : " << ( compressParams.DD  ? L"yes" : L"no") << endl;
	wcout << L"\t ZLibLevel : " << compressParams.ZLibCompLevel << endl<< endl;

	wcout << L"\t isNfDump       : " << ( fileParams.isNFdump ? L"yes" : L"no") << endl;
	wcout << L"\t InputFileName  : " << fileParams.inputFileName.buffer	<< endl;
	wcout << L"\t OutputFileName : " << fileParams.outputFileName.buffer	<< endl<< endl;

	cout  <<  "\t InifileName        : " << strInifileName.c_str() WENDL;

	wcout << L"\t StatsFileName      : " << stats.sStatsFileName.c_str() WENDL;
	wcout << L"\t Ext-StatsFileName  : " << stats.sExtStatsFileName.c_str() WENDL;

	wcout << L"\t LogFileNameDLL     : " << wszLogNameDLL.c_str() WENDL;
	wcout << L"\t LogFileNameEXE     : " << wszLogNameEXE.c_str() << endl<< endl<< endl;

	LogEventFormated( LOG_LEVEL_INFO, WAX_ID_UNKNOWN, L" Processing File: %s", fileParams.inputFileName.buffer );

///////////////////////////////////////////////////////////////////////////////////////////////////

	return true;
}

// get configuration parameters
bool getConfigurationParametersAndAllocateStart(void)
{
	/*
	ULONG MinimumMemoryBufferSize;	//Min memory to allocate for the compressor in MB's
	ULONG MaximumMemoryBufferSize;	//Max memory to allocate for the compressor in MB's
	ULONG TransferLogSize;			//On disk transfer Log Size in MB's
	StringBuffer TransferLogPath;		//On disk transfer Log File path (complete path)

	ULONG SignatureBase ;
	ULONG SignatureDistance ;
	ULONG MinRemoveRepeatedPatternsLength ;//� not used in phase 2
	ULONG MinDeDuplicateLength ;
	ULONG MinDeDuplicateLengthReplacedByToken ;
	*/

	ConfigMan* tmpConfig = ConfigMan::getInstance();

	allocateParams.MaximumMemoryBufferSize = iniReader->ReadInteger( "Allocate", "MaximumMemoryBufferSize", tmpConfig->GetMaxMemUsed() );
	allocateParams.MinimumMemoryBufferSize = iniReader->ReadInteger( "Allocate", "MinimumMemoryBufferSize", tmpConfig->GetMinMemUsed() );

	allocateParams.TransferLogSize = iniReader->ReadInteger( "Allocate", "TransferLogSize", (int)(tmpConfig->GetTLogSize()/MEGA) );
	wstring wstrTLogPath = iniReader->ReadStringW( "Allocate", "TransferLogPath", "tbin.bin");
	allocateParams.TransferLogPath.buffer = (LPWSTR)wstrTLogPath.c_str();
	allocateParams.TransferLogPath.length = (USHORT)wstrTLogPath.size();

	startParams.SignatureBase = iniReader->ReadInteger( "Start", "SignatureBase", tmpConfig->GetSignatureBase() );
	startParams.SignatureDistance = iniReader->ReadInteger( "Start", "SignatureDistance", tmpConfig->GetSignatureDistance() );
	startParams.MinRemoveRepeatedPatternsLength = iniReader->ReadInteger( "Start", "MinRemoveRepeatedPatternsLength", tmpConfig->GetMinRPRLength() );
	startParams.MinDeDuplicateLength = iniReader->ReadInteger( "Start", "MinDeDuplicateLength", tmpConfig->GetMinDeDuplicateLength() );
	startParams.MinDeDuplicateLengthReplacedByToken = iniReader->ReadInteger( "Start", "MinDeDuplicateLengthReplacedByToken", tmpConfig->GetMinDeDuplicateLengthReplacedByToken() );

	startParams.MinZLibLength = iniReader->ReadInteger( "Start", "MinZLibLength", tmpConfig->GetMinZLibCompressLength() );

	startParams.inEncodeOutOfBoundaryEnabled = ( iniReader->ReadInteger( "Start", "EncodeOutOfBoundary", 0 )==1 )?true:false;
	isTimestampInDump = iniReader->ReadInteger( "NFDump", "TimeStampPresent", 0 ) == 1 ? true : false;

	ulOpaqueReadBuffLen = (ULONG)iniReader->ReadInteger("AppParams", "OpaqueReadBufferLen", 128 ) * KILO;

	if( !fileParams.isNFdump )
	{//In case we are in OPAQUE MODE we want to know the read buffer length
		wcout << endl << L"\t Read Buufer Length : " << ulOpaqueReadBuffLen << endl<< endl ;
	}

	ulNrRepeatsForSameFile = (ULONG)iniReader->ReadInteger("Repeat", "Number", 0 );

	stats.ullExtStatsIntervalBytes = (ULONGLONG)iniReader->ReadInteger("Stats", "Interval", 0 ) * MEGA;
	stats.ulExtStatsCount = 0;
	stats.ulExtStatsCountCurrent = 0;
	
	startParams.dNInvariant = (double)iniReader->ReadInteger("Stats", "NInvariant", 5 ) / (double)100 ;

	Logger::Init(wszLogNameEXE);//init EXE (WAXTest) log
	LogEventMessage(LOG_LEVEL_INFO, WAX_ID_UNKNOWN, L"WAXTest - Logger Started");

	//	Initialize the DLL logger as well
	StringBuffer *strBuffer = new StringBuffer();
	strBuffer->length = (USHORT)wszLogNameDLL.size();
	strBuffer->buffer = new wchar_t[ strBuffer->length  +1];
	wcscpy(strBuffer->buffer,wszLogNameDLL.c_str() );


 	if (!initializeCM(strBuffer, false) )//call initialize prior to allocate/start !!!
	{
		OUT_ERR L"Could not initialize WAXCompressor DLL!" WENDL
		LogError(0, L" ", (wchar_t*)getErrorInfoCM() );
		return false;
	}
	
// 	delete []strBuffer->buffer;
// 	delete strBuffer;

	if( !allocateCM( 
		allocateParams.MinimumMemoryBufferSize, allocateParams.MaximumMemoryBufferSize, 
		allocateParams.TransferLogSize, &allocateParams.TransferLogPath ) 
		)
	{
		OUT_ERR << L"Allocation failed. Check log for details." <<endl;
		LogError(0, L" ", (wchar_t*)getErrorInfoCM() );
		return false;
	}
	
	if( !startCM( compressParams.isEncoder,
		startParams.SignatureBase,
		startParams.SignatureDistance,
		startParams.MinRemoveRepeatedPatternsLength, 
		startParams.MinDeDuplicateLength,
		startParams.MinDeDuplicateLengthReplacedByToken,
		startParams.MinZLibLength, //compressParams.ZLibCompLevel,
		startParams.inEncodeOutOfBoundaryEnabled,
		startParams.dNInvariant) 
		)
	{
		OUT_ERR << L"Start failed. Check log for details." <<endl;
		LogError(0, L" ", (wchar_t*)getErrorInfoCM() );
		return false;
	}

	return true;
}


int ReadEncodeAndWrite(HANDLE hInFile, HANDLE hOutFile, ByteBuffer *bbRead, ByteBuffer *bbWrite, CompressParams &compPar, unsigned long dwSeqNumber )
{
	bool bDDCompressed = false;
	int retVal = RET_MORE_DATA;
	__int64 lTimeStampMsg   = 0;
	BOOL paged;

	//bytes written to file
	unsigned long dwBytesWritten = 0;
	//bytes read from file
	unsigned long dwBytesRead = 0;

	//1.	Determine read buffer len - either from NF dump or Raw File
	if( fileParams.isNFdump )
	{//read as dump
		if( isTimestampInDump )
		{
			ReadFile( hInFile, &lTimeStampMsg, sizeof(__int64), &dwBytesRead, NULL );
			if( dwBytesRead != sizeof(__int64) )
			{
				goto NO_MORE_DATA;
			}
		}

		ReadFile( hInFile, &paged, sizeof(BOOL), &dwBytesRead, NULL );
		if( dwBytesRead != sizeof(BOOL) )
		{
			goto NO_MORE_DATA;
		}

		ReadFile( hInFile, &bbRead->length, sizeof(ULONG), &dwBytesRead, NULL );
		if( dwBytesRead != sizeof(ULONG) )
		{
			goto NO_MORE_DATA;
		}
	}
	else
	{//read with fixed buffer
		bbRead->length = BBUFFER_READ_LEN;
	}

	//set the parameter for reading in opaque mode
	if( !fileParams.isNFdump )
	{
		bbRead->length = ulOpaqueReadBuffLen;
	}

	//2. Read the data buffer from file
	ReadFile( hInFile, bbRead->buffer + ulProcessOffset, bbRead->length, &dwBytesRead, NULL );
	if( bbRead->length != dwBytesRead )
	{
		bbRead->length = dwBytesRead;
		retVal = RET_SUCCESS;
	}
	bbRead->length += ulProcessOffset;

	if (bbRead->length > 0)
	{//3.	We have something to compress

		//Set the inoput statistics here
		stats.bytesProcessed	+= (bbRead->length);
		stats.inputSize			+= (bbRead->length);

		
		//byte buffer that will be used as source for writing to file
		//	The buffer dance below is necessary to accommodate all compression switches (combinations):
		//	NONE, DD, ZLIB, DD + ZLIB
		ByteBuffer *bbToFile = bbRead;

		//get the encode required length
		bbWrite->length = 0;
		bbWrite->maximumLength = 0;
		encodeCM( bbRead, bbWrite, compPar.RPR, compPar.DD, compPar.ZLibCompLevel );

		//bbWrite->length = BBUFFER_WRITE_LEN;
		//bbWrite->maximumLength = BBUFFER_WRITE_LEN;

		if( !encodeCM( bbRead, bbWrite, compPar.RPR, compPar.DD, compPar.ZLibCompLevel ) )
		{
			return RET_ERROR;
		}
		else
		{
			bDDCompressed = true;
		}
		
		bbToFile = bbWrite;//use DD dest buffer to write to file

		//3.	Write the result buffer + any additional info to the result file
		if( fileParams.isNFdump )
		{
			//need to put the time stamp + paged + lenght back
			if( isTimestampInDump )
			{
				NFWriteDataToFile(hOutFile, sizeof(__int64), (LPBYTE)&lTimeStampMsg);
			}
			NFWriteDataToFile(hOutFile, sizeof(BOOL), (LPBYTE)&paged );
		}

		NFWriteDataToFile(hOutFile, sizeof(unsigned long), (LPBYTE)&bbToFile->length  );
		NFWriteDataToFile(hOutFile, sizeof(unsigned long), (LPBYTE)&dwSeqNumber );

		//write the actual data
		NFWriteDataToFile(hOutFile, bbToFile->length, bbToFile->buffer);

		//Set the stats here
		stats.outputSize+= (bbToFile->length);

		LogEventFormated( LOG_LEVEL_INFO, WAX_ID_UNKNOWN, L"Encode SQ#: %d, Input buffer size: %d, Output buffer size: %d", dwSeqNumber, bbRead->length, bbToFile->length  ); 
	}//END bbRead->length > 0

	return retVal;
NO_MORE_DATA:
	return RET_SUCCESS;
}

int ReadDecodeAndWrite(HANDLE hInFile, HANDLE hOutFile, ByteBuffer *bbRead, ByteBuffer *bbWrite, CompressParams &compPar, unsigned long &dwSeqNumber )
{
	int retVal = RET_MORE_DATA;

	__int64 lTimeStampMsg   = 0;
	BOOL paged;

	//bytes written to file
	unsigned long dwBytesWritten = 0;
	//bytes read from file
	unsigned long dwBytesRead = 0;

	//1.	Determine read buffer len - either from NF dump or Raw File
	if( fileParams.isNFdump )
	{//read as dump
		if( isTimestampInDump )
		{
			ReadFile( hInFile, &lTimeStampMsg, sizeof(__int64), &dwBytesRead, NULL );
			if( dwBytesRead != sizeof(__int64) )
			{
				goto NO_MORE_DATA;
			}
		}

		ReadFile( hInFile, &paged, sizeof(BOOL), &dwBytesRead, NULL );
		if( dwBytesRead != sizeof(BOOL) )
		{
			goto NO_MORE_DATA;
		}
	}

	//read length and seq number
	//current size
	ReadFile( hInFile, &bbRead->length, sizeof( unsigned long ), &dwBytesRead, NULL);
	if( dwBytesRead != sizeof( unsigned long ) )
	{
		goto NO_MORE_DATA;
	}

	ReadFile( hInFile, &dwSeqNumber, sizeof( unsigned long ), &dwBytesRead, NULL);
	if( dwBytesRead != sizeof( unsigned long ) )
	{
		goto NO_MORE_DATA;
	}

	//read the data buffer from file
	ReadFile( hInFile, bbRead->buffer, bbRead->length, &dwBytesRead, NULL );
	if( bbRead->length != dwBytesRead )
	{
		bbRead->length = dwBytesRead;
		retVal = RET_SUCCESS;
	}

	if (bbRead->length > 0)
	{
		//Set the inoput statistics here
		stats.bytesProcessed	+= (bbRead->length);
		stats.inputSize			+= (bbRead->length);

		//byte buffer that will be used as source for writing to file
		//	The buffer dance below is necessary to accomodate all compression switches (combinations):
		//	NONE, DD, ZLIB, DD + ZLIB
		ByteBuffer *bbToFile = bbRead;
		bbWrite->length = BBUFFER_WRITE_LEN;
		bbWrite->maximumLength = BBUFFER_WRITE_LEN;
		if( !decodeCM( bbToFile, bbWrite, true ) )
		{
			retVal = RET_ERROR;	
		}
		
		if (retVal == RET_ERROR)
		{
			goto NO_MORE_DATA;
		}

		bbToFile = bbWrite;
		//3.	Write the result buffer + any additional info to the result file
		if( fileParams.isNFdump )
		{
			//need to put the time stamp + paged + lenght back
			if( isTimestampInDump )
			{
				NFWriteDataToFile(hOutFile, sizeof(__int64), (LPBYTE)&lTimeStampMsg);
			}
			NFWriteDataToFile(hOutFile, sizeof(BOOL), (LPBYTE)&paged );
			NFWriteDataToFile(hOutFile, sizeof(unsigned long), (LPBYTE)&bbToFile->length  );
		}

		//write the actual data
		NFWriteDataToFile(hOutFile, bbToFile->length, bbToFile->buffer);
		//Set the stats here

		stats.outputSize+= (bbToFile->length);

		LogEventFormated( LOG_LEVEL_INFO, WAX_ID_UNKNOWN, L"Decode SQ#: %i, Input buffer size: %d, Output buffer size: %d", dwSeqNumber, bbRead->length, bbToFile->length  ); 
	}

	return retVal;
NO_MORE_DATA:
	return RET_SUCCESS;
}

bool ProcessInputFile(const FileParams &filePar, CompressParams &compPar )
{
	bool retVal = true;
	WAX_RESULT waxResult = WAX_RESULT_OK;
	unsigned long dwSeqNumber = 0;

	ULONGLONG ullNextIntermediaryStatsSizeBytes = stats.ullExtStatsIntervalBytes;

	LARGE_INTEGER liFileSize;

	//open input and output file handles
	HANDLE hInFile = NFOpenFileMax( filePar.inputFileName.buffer, OPEN_EXISTING, FALSE );
	if( !NFIsValidFileHandle( hInFile ) )
	{
		LogError(0, DBG_LOCATION, L"Can not open input file: '%s'",filePar.inputFileName.buffer ); 			
		goto ERREXIT;
	}
	else
	{
		//get the input file length for stats
		GetFileSizeEx( hInFile, &liFileSize );
		stats.inFileSize = liFileSize.QuadPart;
	}

	HANDLE hOutFile = NFOpenFileMax( filePar.outputFileName.buffer, CREATE_ALWAYS, TRUE );
	if( !NFIsValidFileHandle( hOutFile ) )
	{
		LogError(0, DBG_LOCATION, L"Can not open output file: '%s'",filePar.outputFileName.buffer ); 			
		goto ERREXIT;
	}

	//Buffers for writting and reading
	ByteBuffer* bbRead  = NULL;
	ByteBuffer* bbWrite = NULL;

	AllocateOrReleaseByteBuffer( bbRead,  BBUFFER_READ_LEN * 2 );
	AllocateOrReleaseByteBuffer( bbWrite, BBUFFER_WRITE_LEN );
	if( compressParams.ZLibCompLevel > 0 )
	{//we also have ZLIB compression
		AllocateOrReleaseByteBuffer( bbZlib, BBUFFER_WRITE_LEN );
	}

	int iencDecReturnValue = RET_MORE_DATA;

	ulNrRepeatsForSameFile++; //If it is 0, go at least on time

	for( ULONG i=0; i < ulNrRepeatsForSameFile; i++ )
	{

		while(iencDecReturnValue == RET_MORE_DATA)
		{
			if( compPar.isEncoder )
			{//Encode path
				iencDecReturnValue = ReadEncodeAndWrite( hInFile, hOutFile, bbRead, bbWrite, compressParams, dwSeqNumber );
			}
			else
			{//Decode Path
				iencDecReturnValue = ReadDecodeAndWrite(hInFile, hOutFile, bbRead, bbWrite, compressParams, dwSeqNumber );
			}

			if( iencDecReturnValue == RET_ERROR )
			{
				break;//exit procedure on error
			}

			if( stats.ullExtStatsIntervalBytes > 0 )
			{
				if( stats.bytesProcessed > ullNextIntermediaryStatsSizeBytes )
				{
					//Dump the statistics
					GetWAXStatistics(stats.sExtStatsFileName, true, true);
					ullNextIntermediaryStatsSizeBytes += stats.ullExtStatsIntervalBytes;
				}
			}

			LogEventFormated(LOG_LEVEL_INFO, WAX_ID_UNKNOWN, L"Seq# : %d, InputSize: %I64u, OutputSize: %I64u", dwSeqNumber, stats.inputSize, stats.outputSize ); 			

			if( compPar.isEncoder )
			{
				dwSeqNumber++;//increment on source, read on target
			}
		}

		if( iencDecReturnValue == RET_ERROR )
		{
			LogError(0, DBG_LOCATION, L"Seq#: %d, InputSize: %I64u, OutputSize: %I64u", dwSeqNumber, stats.inputSize, stats.outputSize ); 			
			goto ERREXIT;
		}

		if( compPar.isEncoder )
		{//prepare for next round
			ulProcessOffset++;//TO DO: see if this needs to be more.
			NFSetFilePointer( hInFile, 0, FILE_BEGIN );
			iencDecReturnValue = RET_MORE_DATA;

			if( (ulNrRepeatsForSameFile > 1) && (i < (ulNrRepeatsForSameFile - 1 ) ) )
			{
				GetWAXStatistics(stats.sStatsFileName, true,true);
			}
		}
		else
		{
			break;
		}

	}

	//get the output file size
	GetFileSizeEx( hOutFile, &liFileSize );
	stats.outFileSize = liFileSize.QuadPart;

	//clean up and return success
	CloseHandle( hInFile );
	CloseHandle( hOutFile );

	AllocateOrReleaseByteBuffer( bbRead, 0 );
	AllocateOrReleaseByteBuffer( bbWrite, 0 );
	if( compressParams.ZLibCompLevel > 0 )
	{//we also have ZLIB compression
		AllocateOrReleaseByteBuffer( bbZlib, 0 );
	}

	return true;

ERREXIT://clean up and return failure
	//clean up and return success
	CloseHandle( hInFile );
	CloseHandle( hOutFile );

	AllocateOrReleaseByteBuffer( bbRead, 0);
	AllocateOrReleaseByteBuffer( bbWrite, 0 );
	if( compressParams.ZLibCompLevel > 0 )
	{//we also have ZLIB compression
		AllocateOrReleaseByteBuffer( bbZlib, 0 );
	}
	return false;
}
//-------------------------------------------------------------------------------------------------
void timeb2wstring(_timeb Time, wstring& str)
{
	wchar_t _Time[MAX_PATH];

	time_t time = Time.time;
	wcsftime(_Time, MAX_PATH, L"[%Y-%m-%d] %H:%M:%S", localtime(&time));
	
	str = _Time;
	str += L",";
	_itow(Time.millitm, _Time, 10);
	str += _Time;
}

//	Get the statistics from the WAX component
void GetWAXStatistics(wstring& instrFileName, bool inbIsIntermediary, bool inbUpdateCompressionRate)
{
	//	This function prints out statistics for both ext stats and 
	//	WAX stats.

	//get the current WAX stats in the statsWAX structure
	Statistics* statsWAX = new Statistics();
	statsWAX->bDumpAdditionalStats = true;
	queryStatisticsCM( statsWAX );

	executionTimes.stopTime = getTimeNowByQueryPerformanceCounter();
	getCPUAndRUNTimesInMS( executionTimes.msCPUTime, executionTimes.msRuntime );


///////////////////////////////////////////////////////////////////////////////////////////////////
////////		Print out statistics	///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
	//FILE* fStats = _wfopen(stats.sExtStatsFileName.c_str(), L"a" );
	FILE* fStats = _wfopen(instrFileName.c_str(), L"a" );

	if( !fStats )
	{
		LogError( 0, DBG_LOCATION, L"Can not open ext-stats file: %s.", instrFileName.c_str() );
		OUT_ERR L"Can not open ext-stats file." WENDL
		return;
	}

#define STATSOUT fwprintf(fStats, 

//�	The date and time of the test run. - time that the test finished
	_timeb now;
	_ftime(&now);
	wstring wsTimeNow;
	timeb2wstring( now,  wsTimeNow );

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////		WAX- Test Statistics			///////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

	STATSOUT L"Date-Time = %s ; ", wsTimeNow.c_str() );

//�	The version of the WA software (specifically the build number).
	STATSOUT L"DLL_VERSION = %s; ", getDLLRevisionStringCM() );
	if( inbIsIntermediary )
	{
		//�	The count of the intermediary STATS output
		STATSOUT L"Stats Count = %d; ", stats.ulExtStatsCountCurrent++);
	}

//�	All parameters that are supplied to the WA software. Parameters are recorded as a name-value pair, so parameters can be added or removed without changing the format of the data. All parameters are recorded including parameters with default values: as default values may change over time, it would otherwise be hard to track such values especially for older tests.
	STATSOUT L"Encode = %d ; ", compressParams.isEncoder );
	STATSOUT L"RPR = %d ; ",compressParams.RPR );
	STATSOUT L"DD = %d ; ",compressParams.DD   );
	STATSOUT L"ZLibLevel = %d ; ",compressParams.ZLibCompLevel  );
	STATSOUT L"isNFDump = %d ; ",fileParams.isNFdump );

	STATSOUT L"MaximumMemoryBufferSize = %u ; ", allocateParams.MaximumMemoryBufferSize );
	STATSOUT L"MinimumMemoryBufferSize = %u ; ", allocateParams.MinimumMemoryBufferSize );	//Min memory to allocate for the compressor in MB's
	STATSOUT L"TransferLogSize = %u ; ", allocateParams.TransferLogSize );			//On disk transfer Log Size in MB's

	STATSOUT L"SignatureBase = %u ; ", startParams.SignatureBase );
	STATSOUT L"SignatureDistance = %u ; ", startParams.SignatureDistance );
	STATSOUT L"MinRemoveRepeatedPatternsLength = %u ; ", startParams.MinRemoveRepeatedPatternsLength  );//� not used in phase 2
	STATSOUT L"MinDeDuplicateLength = %u ; ", startParams.MinDeDuplicateLength  );
	STATSOUT L"MinDeDuplicateLengthReplacedByToken = %u ; ", startParams.MinDeDuplicateLengthReplacedByToken  );
	STATSOUT L"MinZLibLength = %u ; ", startParams.MinZLibLength  );

	STATSOUT L"EncodeOutOfBoundaryEnabled = %d ; ", startParams.inEncodeOutOfBoundaryEnabled  );
	STATSOUT L"NInvariant = %f ; ", startParams.dNInvariant  );

//�	The name of the input file, note that the name of the input files should be a logical description of the file content.
	STATSOUT L"Input File Name = %s ; ", fileParams.inputFileName.buffer );
	STATSOUT L"Output File Name = %s ; ", fileParams.outputFileName.buffer );

//�	The size of the input file and output file in bytes.
	STATSOUT L"Input File Size = %I64u ; ", stats.inFileSize );
	STATSOUT L"Output File Size = %I64u ; ", stats.outFileSize );

//�	The elapsed time and the CPU cycles used by WAXtest during the test.
	STATSOUT L"Elapsed Time (ms) = %I64u ; ", executionTimes.msRuntime) ;
	STATSOUT L"CPU  Time (ms) = %I64u ; ", executionTimes.msCPUTime );

//�	The name of the perfmon data file for the test run. It is not obvious how this name is obtained; one possibility is that WAXtest starts and stops perfmon (and pauses some time after starting and before stopping). 
	STATSOUT L"Perfmon data file = unknown ; ");

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////		WAX Statistics			///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

	STATSOUT L"uptime = %I64u ; ", statsWAX->uptime );						//!< 1.1. Up time
	STATSOUT L"memoryAllocated = %I64u ; ", statsWAX->memoryAllocated  );	//!< 1.2. Memory allocated by WAX
	STATSOUT L"memoryUsed = %I64u ; ", statsWAX->memoryUsed  );				//!< 1.3. Current usage of memory 
	STATSOUT L"diskUsed = %I64u ; ", statsWAX->diskUsed  );					//!< 1.4. Current usage of disk 
	STATSOUT L"compressionRate = %I64u ; ", statsWAX->compressionRate  );	//!< 1.5. Compression rate - the actual value is the float 
																			//	compression multiplied by 100.
	STATSOUT L"dataVolLZero = %I64u ; ", statsWAX->dataVolLZero  );			//!< 1.6. Volume � this is the level 0 data since WAX was initialized
	STATSOUT L"dataVolCurrent = %I64u ; ", statsWAX->dataVolCurrent  );		//!< 1.7. Current data volumes and current compression rates
	STATSOUT L"compressionRateCurrent = %I64u ; ", statsWAX->compressionRateCurrent  );  //!< 1.8. Current compression rates

	/******
	The following statistics are only available through SCOPE via WMI. 
	***/
	STATSOUT L"dataVolSinceInit = %I64u ; ", statsWAX->dataVolSinceInit  );//!< 2.1. Data volume since WAX was initialized - same as

	// dataVolLZero
	STATSOUT L"dataVolL0 = %I64u ; ", statsWAX->dataVolL0  );	//!< 2.2. Volume of uncompressed data (level 0 data)
	STATSOUT L"dataVolL1 = %I64u ; ", statsWAX->dataVolL1  );   //!< 2.3. Volume of data after compression of repeated
	// patterns (level 1 data)
	STATSOUT L"dataVolL2 = %I64u ; ", statsWAX->dataVolL2  );   //!< 2.4. Volume of data after de-duplication (level 2 data)
	STATSOUT L"dataVolL3 = %I64u ; ", statsWAX->dataVolL3  );   //!< 2.5. Volume of data after ZLIB (level 3 data)

	STATSOUT L"msgNumber = %u ; ", statsWAX->msgNumber  );      //!< 2.6. Number of messages 
	STATSOUT L"regNumber = %u ; ", statsWAX->regNumber );       //!< 2.7. Number of qualifying regions since last start 
	STATSOUT L"bytesInReg = %I64u ; ", statsWAX->bytesInReg );  //!< 2.8. Bytes in qualifying region since last start

	//////	RPR statistics ///////
	STATSOUT L"rprNumberOfStrings = %I64u ; ", statsWAX->rprStringsNumber );//!< 3.1. Number of RPR Strings 
	STATSOUT L"rprBytesNumber = %I64u ; ", statsWAX->rprBytesNumber );		//!< 3.2. Bytes in RPR strings 

	STATSOUT L"genericNumberOfStrings = %I64u ; ", statsWAX->genericStringsNumber );	//!< 3.3. Number of zero-bytes strings
	STATSOUT L"genericBytesNumber = %I64u ; ", statsWAX->genericBytesNumber );			//!< 3.4. Bytes in zero-bytes strings
	
	STATSOUT L"daNumberOfStrings = %I64u ; ", statsWAX->daStringsNumber );	//!< 3.5. Number of DA strings
	STATSOUT L"daBytesNumber = %I64u ; ", statsWAX->daBytesNumber );		//!< 3.6. Bytes in DA strings

	//////	De-duplication statistics  ///////
	STATSOUT L"dedupNumberOfStrings = %I64u ; ", statsWAX->dedupStringsNumber );	//!< 4.1. Number of De-Dup Strings 
	STATSOUT L"dedupBytesNumber = %I64u ; ", statsWAX->dedupBytesNumber );			//!< 4.2. Bytes in De-dup strings 

	STATSOUT L"dedupNumberOfRecentStrings = %I64u ; ", statsWAX->dedupRecentStringsNumber );//!< 4.3. Number of recent De-Dup Strings
	STATSOUT L"dedupRecentBytesNumber = %I64u ; ", statsWAX->dedupRecentBytesNumber );		//!< 4.4. Bytes in recent De-dup strings

	STATSOUT L"dedupNumberOfOldStrings = %I64u ; ", statsWAX->dedupOldStringsNumber );	//!< 4.5. Number of old (chunk grow) De-Dup Strings
	STATSOUT L"dedupOldBytesNumber = %I64u ; ", statsWAX->dedupOldBytesNumber );		//!< 4.6. Bytes in old De-dup strings

	STATSOUT L"cpuTime = %I64u ; ", statsWAX->cpuTime  );	//!< 5.1. CPU time of the WAX process

	//IMPORTANT!!! DO NOT FORGET TO PUT THE line terminator
	STATSOUT L"\n");

	fclose( fStats );

	delete statsWAX;
}

//!
void testLoggerInitialize(void)
{
	wcout << L"WARNING: To few Parameters!" << endl;
	wcout << L"The application will test the logger and exit!" << endl;

	Logger::Init(L"log-test.txt");
	Logger::Close();

	Logger::Init(L"");
	Logger::Close();

	Logger::Init(L"*");
	Logger::Close();

	Logger::Init(L".ext");
	Logger::Close();

	Logger::Init(L" ");
	Logger::Close();
}


// code bck
/*
string _narrowString(wstring str)
{
	locale locl("");
	size_t iResLen = wcslen(str.c_str()) + 1;
	char *result = new char[ iResLen ];
	wchar_t *tmp = (wchar_t *)str.c_str();
	size_t tmpLen = wcslen(tmp);

	use_facet<ctype<wchar_t> >(locl)._Narrow_s(tmp, tmp + tmpLen, ' ', result, iResLen);
	result[tmpLen] = '\0';
	string ret = string(result);

	delete result;
	return ret;
}
//-----------------------------------------------------------------------------


void testWriteSpeeds()
{
	FILE* m_filePointer = _wfopen(L"afile.txt", L"a+");
	DWORD dwStart;
	wstring strW = L"basca de basca\n";

	if( m_filePointer )
	{
		dwStart = GetTickCount();
		for( int i=0; i<1000000;i++)
		{
			fwprintf(m_filePointer, strW.c_str() );

			if( !(i%1000) )
			{
				fflush( m_filePointer );
			}
		}
		wcout << L"It took for FILE (ms): " << (GetTickCount() - dwStart) << endl;

	}

	fclose( m_filePointer );
	m_filePointer = _wfopen(L"bfile.txt", L"a+");

	if( m_filePointer )
	{
		int m_logFileHandle = m_filePointer->_file;


		string str ;
		char* strChar = "basca de basca\n";
		unsigned int iLen = (unsigned int)strW.size();

		dwStart = GetTickCount();
		for( int i=0; i<1000000;i++)
		{
			//str = _narrowString( strW );

			//_write(m_logFileHandle, str.c_str(), iLen);
			_write(m_logFileHandle, strChar, iLen);

			if( !(i%1000) )
			{
				_commit(m_logFileHandle);
			}
		}
		wcout << L"It took for FDES (ms): " << (GetTickCount() - dwStart) << endl;
	}

}
*/












