#pragma once

#include "WAXTypes.h"      //from the Wax compressor source dir
#include "WAXCompressor.h" //DLL Exported declarations

#define OUT_ERR wcout << L"ERROR: "
#define WENDL << endl;
#define WOUT wcout <<

typedef enum ENC_DEC_RET_VAL
{
	RET_ERROR = 0,
	RET_MORE_DATA ,
	RET_SUCCESS
};

/*
Allocation and start parameters are specified via configuration files.

The following allocation parameters are set via configuration file:

	MinimumMemoryBufferSize//Min memory to allocate for the compressor in MB's
	MaximumMemoryBufferSize//Max memory to allocate for the compressor in MB's
	TransferLogSize//On disk transfer Log Size in MB's
	TransferLogPath//On disk transfer Log File path (complete path)

	The following start parameters are set via configuration file:
	SignatureBase 
	SignatureDistance 
	MinRemoveRepeatedPatternsLength � not used in phase 2
	MinDeDuplicateLength 
	MinDeDuplicateLengthReplacedByToken 
*/
typedef struct _AllocateParams
{
	ULONG MinimumMemoryBufferSize;	//Min memory to allocate for the compressor in MB's
	ULONG MaximumMemoryBufferSize;	//Max memory to allocate for the compressor in MB's
	ULONG TransferLogSize;			//On disk transfer Log Size in MB's
	StringBuffer TransferLogPath;	//On disk transfer Log File path (complete path)
} AllocateParams;

typedef struct _StartParamEnc
{
	ULONG SignatureBase;
	ULONG SignatureDistance;
	ULONG MinRemoveRepeatedPatternsLength ;//� not used in phase 2
	ULONG MinDeDuplicateLength ;
	ULONG MinDeDuplicateLengthReplacedByToken ;
	ULONG MinZLibLength;
	bool  inEncodeOutOfBoundaryEnabled;
	double dNInvariant;
} StartParamsEnc;

typedef struct _CompressParams
{
	bool isEncoder;// Operation type � 1 = Encode, 0 = Decode
	bool RPR ;//� not used in phase 2
	bool DD;
	ULONG ZLibCompLevel;			 
} CompressParams;

typedef struct _FileParams
{
	bool isNFdump;
	StringBuffer inputFileName;
	StringBuffer outputFileName;
} FileParams;

typedef struct _ExecutionTimes
{
	ULONGLONG CPUFreq;	
	ULONGLONG startTime;
	ULONGLONG stopTime;

	ULONGLONG msRuntime;
	ULONGLONG msCPUTime;

	_ExecutionTimes()
	{
		 CPUFreq = 0;	
		 startTime = 0;
		 stopTime = 0;

		 msRuntime = 0;
		 msCPUTime = 0;
	}

} ExecutionTimes;

typedef struct _CompressionStatisitcs
{
	wstring sStatsFileName;
	wstring sExtStatsFileName;

	ULONG ulExtStatsCount;
	ULONG ulExtStatsCountCurrent;

	ULONGLONG ullExtStatsIntervalBytes;

	ULONGLONG inFileSize;
	ULONGLONG outFileSize;
	ULONGLONG bytesProcessed;

	ULONGLONG inputSize;
	ULONGLONG outputSize;

	bool isCompression;

	_CompressionStatisitcs()
	{
		isCompression = 0;

		ulExtStatsCount = 0;
		ulExtStatsCountCurrent = 0;
		ullExtStatsIntervalBytes = 0;

		bytesProcessed	= 0;
		inputSize		= 0;
		outputSize		= 0;
	}

} CompressionStatisitcs;



// get command line parameters
bool getCommandLineParametersAndShowLogo(int argc, wchar_t* argv[]);

// get configuration parameters
bool getConfigurationParametersAndAllocateStart(void);

//initialize statistics
void InitStatistics(void);

//
void PrintOutExecutionTimes(void);

//Stop compressor and Release resources
void StopAndRelease(void);

//
bool ProcessInputFile(const FileParams &filePar, CompressParams &compPar );

void GetWAXStatistics(wstring& instrFileName,bool inbIsIntermediary, bool inbUpdateCompressionRate = true);
