// IniReader.h : Defines the interface for the IniReader class
//		it is an implementation for reading Windows .ini files
//

#include "IniReader.h"
#include <iostream>

#pragma warning (disable:4996)
#pragma warning (disable:4995)

IniReader::IniReader(const char* szFileName)
{
	memset(m_szFileName, 0x00, MAX_PATH);
	GetFullPathNameA(szFileName, MAX_PATH, m_szFileName, NULL );
}

int IniReader::ReadInteger(char* szSection, char* szKey, int iDefaultValue)
{
	int iResult = GetPrivateProfileIntA(szSection,  szKey, iDefaultValue, m_szFileName); 
	return iResult;
}
float IniReader::ReadFloat(char* szSection, char* szKey, float fltDefaultValue)
{
	char szResult[255];
	char szDefault[255];
	float fltResult;
	sprintf(szDefault, "%f",fltDefaultValue);
	GetPrivateProfileStringA(szSection,  szKey, szDefault, szResult, 255, m_szFileName); 
	fltResult =  (float)atof(szResult);
	return fltResult;
}
bool IniReader::ReadBoolean(char* szSection, char* szKey, bool bolDefaultValue)
{
	char szResult[255];
	char szDefault[255];
	bool bolResult;
	sprintf(szDefault, "%s", bolDefaultValue? "True" : "False");
	GetPrivateProfileStringA(szSection, szKey, szDefault, szResult, 255, m_szFileName); 
	bolResult =  (strcmp(szResult, "True") == 0 || strcmp(szResult, "true") == 0) ? true : false;
	return bolResult;
}
char* IniReader::ReadString(char* szSection, char* szKey, const char* szDefaultValue)
{
	char* szResult = new char[255];
	memset(szResult, 0x00, 255);
	GetPrivateProfileStringA(szSection,  szKey, szDefaultValue, szResult, 255, m_szFileName); 
	return szResult;
}

wchar_t* IniReader::ReadStringW(char* szSection, char* szKey, const char* szDefaultValue)
{
	char* szResult = new char[255];
	memset(szResult, 0x00, 255);
	GetPrivateProfileStringA(szSection,  szKey, szDefaultValue, szResult, 255, m_szFileName); 

	wchar_t *szResultW = new wchar_t[255];
	memset(szResultW, 0, 255 * sizeof(wchar_t));

	mbstowcs( szResultW, szResult, strlen( szResult ) );

	return szResultW;
}


void IniReader::ReleaseString( char* szSection )
{
	if (NULL!= szSection)
	{
		// allocated in ReadString
		delete []szSection;
	}
}

/*
IniReader::IniReader(WCHAR* szFileName)
{
	memset(m_szFileName, 0, MAX_INI_PATH_SIZE );
	m_szFileName[0] = NULL;
	memcpy(m_szFileName, szFileName, wcslen (szFileName) * sizeof(WCHAR));
}
int IniReader::ReadInteger(WCHAR* szSection, WCHAR* szKey, int iDefaultValue)
{
	int iResult = GetPrivateProfileInt(szSection,  szKey, iDefaultValue, m_szFileName); 
	return iResult;
}
double IniReader::ReadDouble(WCHAR* szSection, WCHAR* szKey, double fltDefaultValue)
{
	WCHAR szResult[255];
	WCHAR szDefault[255];
	double fltResult;
	wsprintf(szDefault, L"%f",fltDefaultValue);
	GetPrivateProfileString(szSection,  szKey, szDefault, szResult, 255, m_szFileName); 
	fltResult =  _wtof(szResult);
	return fltResult;
}

bool IniReader::ReadBoolean(WCHAR* szSection, WCHAR* szKey, bool bolDefaultValue)
{
	WCHAR szResult[255];
	WCHAR szDefault[255];
	bool bolResult;
	wsprintf(szDefault, L"%s", bolDefaultValue? L"True" : L"False");
	GetPrivateProfileString(szSection, szKey, szDefault, szResult, 255, m_szFileName); 
	bolResult =  (wcscmp(szResult, L"True") == 0 || wcscmp(szResult, L"true") == 0) ? true : false;
	return bolResult;
}

WCHAR* IniReader::ReadString(WCHAR* szSection, WCHAR* szKey, const WCHAR* szDefaultValue)
{
	WCHAR* szResult = new WCHAR[MAX_INI_STRING_SIZE];
	memset(szResult, 0, MAX_INI_STRING_SIZE * sizeof(WCHAR) );
	GetPrivateProfileString(szSection,  szKey, szDefaultValue, szResult, MAX_INI_STRING_SIZE, m_szFileName); 
	return szResult;
}
*/