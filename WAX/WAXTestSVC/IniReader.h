// IniReader.h : Defines the interface for the IniReader class
//		it is an implementation for reading Windows .ini files
//	the .ini file FORMAT must be ANSI
#pragma once

#include <Windows.h>

//#define MAX_INI_PATH_SIZE	30000 //arbitrary length (could be bigger)
//#define MAX_INI_STRING_SIZE   1024

class IniReader
{
public:
	IniReader(const char* szFileName); 
	int ReadInteger(char* szSection, char* szKey, int iDefaultValue);
	float ReadFloat(char* szSection, char* szKey, float fltDefaultValue);
	bool ReadBoolean(char* szSection, char* szKey, bool bolDefaultValue);
	char* ReadString(char* szSection, char* szKey, const char* szDefaultValue);
	wchar_t* IniReader::ReadStringW(char* szSection, char* szKey, const char* szDefaultValue);
	void ReleaseString( char* szSection );
private:
	char m_szFileName[MAX_PATH];
};

/*
class IniReader
{
public:
	IniReader(WCHAR* szFileName); 
	int		ReadInteger	(WCHAR* szSection, WCHAR* szKey, int iDefaultValue);
	double	ReadDouble	(WCHAR* szSection, WCHAR* szKey, double fltDefaultValue);
	bool	ReadBoolean	(WCHAR* szSection, WCHAR* szKey, bool bolDefaultValue);
	WCHAR*	ReadString	(WCHAR* szSection, WCHAR* szKey, const WCHAR* szDefaultValue);

private:
	WCHAR m_szFileName[MAX_INI_PATH_SIZE];
};
*/