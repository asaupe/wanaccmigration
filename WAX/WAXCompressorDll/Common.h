//********************************************************/
//  ./Common.h
//
//  Owner : Ciprian Maris / Attila Vajda / Bogdan Hruban
// 
//	Company		: Neverfail 
//	PROJECT		: WAN Acceleration 
//  Component	: Common 
//
//  $Rev: 2489 $ - Revision of last commit
//
/********************************************************
//
// Last Change - Author and Date of last change:
// $Author: cmaris $  
// $Date: 2009-05-26 11:01:41 +0100 (Tue, 26 May 2009) $
//
//********************************************************/

//********************************************************/
//	Common.h is the include file common for all WAX components
//
//	It is intended to be used only by the Compression provider components: 
//	WAXCompressor.dll, NFWaxHB.dll, WaxService.exe for items that are shared
//	by these components:
//		- headers inluded in all the modules
//		- types shared by more than on component
//
//********************************************************/

#pragma once

// General includes
//#include <windows.h>
//#include <winbase.h>
#include <iostream>
#include <tchar.h>
#include <strsafe.h>

// STL includes
#include <vector>
#include <algorithm>
#include <string>
#include <fstream>
#include <cstdint>

using namespace std;   

#define KILO (1024)
#define MEGA ((KILO)*(KILO))

//	BZ 8261 - included these declarations here as they are shared by IPC and NFWaxHB components.
#define LOG_DEFAULT_NAME L"CompressionProvider.log"
#define LOG_IPC_ADDON L"-IPC-"
#define LOG_HB_ADDON L"-HB-"


// Component ID definitions
typedef unsigned long WAX_ID;

/*!
	The following enumeration holds the UNIQUE component ID's for all wax components.
	These ID's are used for logging purposes, to identify the component perfroming the logging.
*/
enum WAXComponentIDs
{
	WAX_ID_UNKNOWN      = 0,
	WAX_ID_WAXINTERFACE = 1, //!< 
	WAX_ID_WAXXER       = 2,
	WAX_ID_STATS        = 7,
	WAX_ID_RPR          = 8,
	WAX_ID_UWM          = 9,
	WAX_ID_TOKENIZER    = 10,
	WAX_ID_ENCODER      = 11,
	WAX_ID_MEMORY_POOL  = 12,
	WAX_ID_CONFIGMAN    = 13,
	WAX_ID_STOREMAN     = 14,
	WAX_ID_SIGNATURELOG = 15,
	WAX_ID_DICTIONARY   = 16,
	WAX_ID_INDEX        = 17,

	WAX_ID_IPC_CLIENT   = 30,
	WAX_ID_IPC_SERVER   = 40,
	WAX_ID_SERVICE		= 50
};

//! Basic structure used for representing a block of data within a data buffer.
typedef struct _Basic_Region
{
	unsigned long offset;	//<! The offset of the data
	unsigned long length;	//<! The length of the data

	_Basic_Region()
	{
		offset = 0;
		length = 0;
	}

	//! Create a new basic region from the input params.
	_Basic_Region(unsigned long pOffset, unsigned long pLength)
	{
		offset = pOffset;
		length = pLength;
	}

	/*! We have overwritten the LESS operator because
	in our terms a region is less than another if it 
	has the same offset but it has a greater length.

	This type of ordering is required by the UWM for double-checking
	if the regions of data received from Encoder, RPR and DirtyData do
	not contain duplicate data.
	*/
	bool operator < (const _Basic_Region& other) const
	{
		bool res = false;

		if (offset < other.offset)
			res = true;
		else
			if (offset > other.offset)
			{
				res = false;
			}
			else
				res = length > other.length;		// change the order of the elements with the same offset
		// put the longest one in front

		return res;
	}

	bool operator == (const _Basic_Region& other) const
	{
		return ((offset == other.offset) && (length == other.length));
	}
} Basic_Region, *PBasic_Region;
typedef vector<Basic_Region> BasicRegionVector;

//! The structure used by WAX Encoder to fully identify a replaced data string in the current buffer.
/*!
	This structure extends the Basic RGN structure and it uses the same rule for comparing the elements. 
	The offset in transfer log is not taken into account by the comparison method.
*/
typedef struct _CompressedToken : public Basic_Region
{
	uint64_t	tLogOffset;  //!< the offset in TransferLog

	//! Default constructor
	_CompressedToken():Basic_Region()
	{
		tLogOffset = 0;
	}

	//! Compare two elements
	/*!
	Use the same rule of comparison as Basic_Region does.
	*/
	bool operator < (const _CompressedToken& other) const
	{
		return Basic_Region::operator <(other);
	}
}
CompressedToken;           //!< CompressedToken structure

//! Typedef for Token vector - wax encoder will push tokens to this buffer
typedef vector<CompressedToken> CompressedTokenVector; //! CompressedTokens returned to UWM by WAX Encoder

// Types for Signatures
typedef unsigned long     SignatureType;       //!< Type for Signatures ( for signature base )
typedef uint64_t StrongSignatureType; //!< Type for Signatures ( for (signature base)-(signature distance) )
