//********************************************************/
//  ./SharedTypes.h
//
//  Owner : ciprian Maris
// 
//	Company		: Neverfail ________________
//	PROJECT		: WAN Acceleration 
//  Component	: Shared Utilities  
//	
//	Purpose: Declaration of the types shared between 
//	WAXCompressor.dll and HFWaxHB.dll. 
//	This header declares all the types involved in 
//	WAX data encoding and serialisation after encoding.
//
//  $Rev: 2047 $ - Revision of last commit
//
//********************************************************/

//********************************************************/
//
// Last Change - Author and Date of last change:
// $Author: cmaris $  
// $Date: 2009-03-17 13:07:18 +0000 (Tue, 17 Mar 2009) $
//
//********************************************************/

//********************************************************/
//
//	SharedTypes holds the declaration of the types shared between WAXCompressor.dll and HFWaxHB.dll. 
//	This header declares all the types involved in WAX data encoding and serialisation after encoding.
//
//********************************************************/

#pragma once

#include <windows.h>

//This id done to avoid the warning C4200: nonstandard extension used : zero-sized array in struct/union
#pragma warning(disable:4200)	

///////////////////////////////////////////////////////////////////////////////////////////////////
//////		UWM Shared Types

//! UWM header information
/*!
*	The header is meant to keep informations about the original buffer, to
*	be able to check if the reconstructed one is the same.
*	For this we keep the original buffer size and a CRC of it.
*/
#pragma pack(1)
typedef struct _UWM_Header
{
	unsigned long BuffSize;	// the size of the buffer (message) before encode
	uint64_t CRC;	// CRC of the original unprocessed message 
} UWM_Header, *PUWM_Hearder;
#pragma pack()

//Size of the flags set by UWM for encoding type identification.
#define UWM_FLAG_SIZE (4 * sizeof(BYTE))

//The maximum number of regions that can exist in a NF message.
//TO DO: decide if we use it after RPR is implemented.
#define UWM_MAX_REGION_COUNT 256	

//////		END UWM Shared Types
///////////////////////////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////////////////////////
//////		RPR Shared Types

//	BZ 7534 NOTES:
//	The RPR token types were restructured as part of the resolution for BZ 7534. 

//! The types of RPR tokens
enum RPRTokenTypes:unsigned char
{
	//new super compressed types
	RPR_TOKEN_CMP_ONE_SINGLE, //compressed, one byte used for length, 1-byte pattern
	RPR_TOKEN_CMP_ONE_DOUBLE,
	RPR_TOKEN_CMP_TWO_SINGLE,
	RPR_TOKEN_CMP_TWO_DOUBLE,
	RPR_TOKEN_CMP_FOUR_SINGLE,
	RPR_TOKEN_CMP_FOUR_DOUBLE,
	RPR_TOKEN_UCP_ONE, //uncompressed, one byte used for length
	RPR_TOKEN_UCP_TWO,
	RPR_TOKEN_UCP_FOUR,
	RPR_TOKEN_DA_ONE,
	RPR_TOKEN_DA_TWO,
	RPR_TOKEN_DA_FOUR
};

//!	RPR Compressed token types - type name contains _CMP_ or _DA_
/*!
	The compressed tokens are used to encode:
		DA patterns - type name contains _DA_
		Generic repetitive patterns of 1 or 2 bytes - type name contains _CMP_

	The structure of the compressed tokens is the following:
	- Length : data length replaced by token. The type of the length field is given by the Token Type:
		BYTE	- for all token types which have _ONE_ in thir names
		WORD	- for all token types which have _TWO_ in thir names
		ULONG	- for all token types which have _FOUR_ in thir names
	
	- tokenData : data for the current token which has the following characteristics given by the Token Type:
		_DA_ tokens: the tokenData field is 1 BYTE long with the following meaning:
		The BYTE is split in two parts
			- first 4 bits used for storing the position of the first different byte (does not exced 0x0F)
			- last 4 bits used for storing the value of the first different byte (does not exced 0x0F)

		_CMP_ tokens: the tokenData is 1 BYTE long for SINGLE and 2 BYTES for DOUBLE
		The value of tokenData is the pattern replaced by the current token.

**/
#pragma pack(1)
typedef struct _TokenCompressedONE
{
	unsigned char bLength;
	unsigned char tokenData[1];

} TokenCompressedONE;
#pragma pack()

#pragma pack(1)
typedef struct _TokenCompressedTWO
{
	WORD wLength;
	unsigned char tokenData[1];

} TokenCompressedTWO;
#pragma pack()

#pragma pack(1)
typedef struct _TokenCompressedFOUR
{
	unsigned long ulLength;
	unsigned char tokenData[1];

} TokenCompressedFOUR;
#pragma pack()

//!	RPR Uncompressed token types - type name contains _UCP_ 
/*!
	The uncompressed tokens are used to encode the length of the uncompressed data portion
	that can occur between two compressed tokens.

	The structure of the uncompressed tokens is the following:
	- Length : data length of uncompressed data. The type of the length field is given by the Token Type:
		BYTE	- for all token types which have _ONE_ in thir names
		WORD	- for all token types which have _TWO_ in thir names
		ULONG	- for all token types which have _FOUR_ in thir names
**/
#pragma pack(1)
typedef struct _TokenUncompressedONE
{
	unsigned char bLength;

} TokenUncompressedONE;
#pragma pack()

#pragma pack(1)
typedef struct _TokenUncompressedTWO
{
	WORD wLength;

} TokenUncompressedTWO;
#pragma pack()

#pragma pack(1)
typedef struct _TokenUncompressedFOUR
{
	unsigned long ulLength;

} TokenUncompressedFOUR;
#pragma pack()


//! RPR Token union
#pragma pack(1)
typedef union _RPR_TokenContent
{
	TokenCompressedONE tCompressedOne;
	TokenCompressedTWO tCompressedTwo;
	TokenCompressedFOUR tCompressedFour;

	TokenUncompressedONE tUncompressedOne;
	TokenUncompressedTWO tUncompressedTwo;
	TokenUncompressedFOUR tUncompressedFour;

}RPR_TokenContent;
#pragma pack()

//! RPR Token structure
#pragma pack(1)
typedef struct _RPR_Token
{
	RPRTokenTypes tokenType;
	RPR_TokenContent content;

}RPR_Token, *PRPR_Token;
#pragma pack()

//////		END RPR Shared Types
///////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////
//////		Tokenizer Shared Types

//! Uncompressed token structure
/*!
	This structure represents the particular data required by the encoding
	of data in Uncompressed format.
	
	This structure keeps a pointer to the data that is not deduplicated.
*/
#pragma pack(1)
typedef struct
{
	unsigned char	uncompmressedData[];		//!< uncompressed data
} TokenUncompressed, *PTokenUncompressed;
#pragma pack()

//! Compressed token structure
/*!
	The structure keeps the offset in TransferLog for the deduplicated data.
*/
#pragma pack(1)
typedef struct
{
	uint64_t       compressedDataTLogOffset;	//!< offset of the deduplicated data in TLog
} TokenCompressed, *PTokenCompressed;
#pragma pack()

//! 
typedef union 
{
	TokenUncompressed	tUncompressed;
	TokenCompressed     tCompressed;
} TOKEN_DATA;
#pragma pack()

//! Tokens types present in the WAX compressor
enum WAX_TOKEN_TYPE:unsigned char
{
	TOKEN_TYPE_UNCOMPRESSED = 0,
	TOKEN_TYPE_COMPRESSED 
};

//! The token structure
/*!
*	The structure holds all the information required to fully identify a data, either
*	deduplicated or not.
*
*	It contains the token type, the length of the data replaced by the token, and the 
*	token data, which can be compressed or uncompressed.
*
*	\sa TokenComressed
*	\sa TokenUncompressed
*/
#pragma pack(1)
typedef struct
{
	WAX_TOKEN_TYPE	tokenType;			//!< the type of the token
	unsigned long			tokenDataLength;	//!< the length of the data represented by the token
	TOKEN_DATA		tokenData;			//!< the token data
} Token, *PToken;
#pragma pack()

//////		END Tokenizer Shared Types
///////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////
/////	Encoder minimum parameter values for:
/////		- MinRemoveRepeatedPatternsLength 
/////		- MinDeDuplicateLengthReplacedByToken

////	BZ 7523 the minimum values for RPR length and DD length (min data replaced by token)
////	are extended here so that the encode process does not extend the buffer.
////	As a side effect if the user sets invalid values for these parameters, the parameter 
////	values will be set by the coimpressor to the minimum value computed below.

//!min values for MinRemoveRepeatedPatternsLength and MinDeDuplicateLengthReplacedByToken
#define MIN_RPR_LENGTH (2 * (sizeof(RPRTokenTypes) + sizeof(RPR_TOKEN_LENGTH) + 2 * sizeof(BYTE) ) ) 
#define MIN_DD_LENGTH  (2 * (sizeof(WAX_TOKEN_TYPE) + sizeof(ULONG) + sizeof(TokenCompressed) + sizeof(BYTE) ) )
/////	END Encoder minimum parameter values 
///////////////////////////////////////////////////////////////////////////////////////////////////


#pragma warning(default:4200)	


