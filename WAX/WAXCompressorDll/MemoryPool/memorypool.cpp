//********************************************************/
//  ./MemoryPool/memorypool.cpp
//
//  Owner     : Attila Vajda
//
//  Company   : Neverfail
//  PROJECT   : WAN Acceleration
//  Component : Memory Pool 
//
//  $Rev: 1483 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: avajda $
//  $Date: 2009-01-14 10:20:20 +0000 (Wed, 14 Jan 2009) $
//
//********************************************************/

#include "memorypool.h"
#include "..\ConfigMan\ConfigMan.h"
#include "..\StoreManager\SignatureLog.h"
#include "..\StoreManager\HashMap.h"
#include "MemoryAllocators.h"

#include "..\AllUtils.h"

#include <Psapi.h>

WAX_ID MemoryPool::m_ComponentID = (WAX_ID)WAX_ID_MEMORY_POOL;

MemoryPool::MemoryPool()
{
	initialize();

	m_sizeOfTotalMemory     = 0;
	m_bufferAllocatedMemory = NULL;
}

MemoryPool::~MemoryPool()
{
	// in case that release is not called
	if ( NULL != m_bufferAllocatedMemory )
	{
		m_memoryAllocators.freeMemory( m_bufferAllocatedMemory );
		m_bufferAllocatedMemory = NULL;
	}
}

UINT_PTR MemoryPool::
getComponentMemorySize(
	/*__in*/ WAX_ID innComponentID )
{
	UINT_PTR ulReturn = 0;

	if( innComponentID != 0 )
	{
		switch ( innComponentID )
		{
		case WAX_ID_STOREMAN:
			{
				ulReturn = m_sizeOfTransferLOG;
			}
			break;

		case WAX_ID_SIGNATURELOG:
			{
				ulReturn = m_sizeOfSignatureLOG;
			} 
			break;

		case WAX_ID_INDEX:
			{
				ulReturn = m_sizeOfIndex;
			}
			break;

		default:
			//ulReturn = 0;
			break;
		}
	}

	return ulReturn;
}

unsigned char* MemoryPool::
gimme(
	/*__in*/   UINT inunSize,
	/*__in*/ WAX_ID innComponentID )
{
	unsigned char* lpBuffer = NULL;

	if( innComponentID != 0 )
	{ 
		switch ( innComponentID )
		{
		case WAX_ID_STOREMAN:
			{
				lpBuffer = m_bufferAllocatedMemory + m_offsetTransferLog;
			}
			break;

		case WAX_ID_SIGNATURELOG:
			{
				lpBuffer = m_bufferAllocatedMemory + m_offsetSignatureLog;
			}
			break;

		case WAX_ID_INDEX:
			{
				lpBuffer = m_bufferAllocatedMemory + m_offsetIndex;
			}
			break;

		default:
			//lpBuffer = NULL;
			break;
		}
	}

	return lpBuffer;
}

WAX_RESULT MemoryPool::
computeHashMemorySettings(
	/*__in*/ uint64_t inTransferLogSize,
	/*__in*/     unsigned long inSignatureDistance )
{
	if ( 0 == inSignatureDistance )
	{
		return WAX_RESULT_INVALID_SETTINGS;
	}

	m_ullMaxEntriesInIndex = ( inTransferLogSize / (uint64_t)inSignatureDistance ) + 1; //!< max nr. of entries that will be in the hash map

	//!< maxMemoryUsedByBuckets+maxMemoryUsedByEntriesInIndex
	m_sizeOfIndex = HashMap::getMaxMemory( (size_t)m_ullMaxEntriesInIndex );

	return WAX_RESULT_OK;
}

WAX_RESULT MemoryPool::
computeComponentsMemorySize()
{
	//all values are ULONGLONG so we do not have a calculus overflow
	uint64_t memCalc = 0;

	unsigned long ulSignatureDistance = ConfigMan::getInstance()->GetSignatureDistance();

	uint64_t minimumMemorySizeNeeded = 
		(( m_ullMaxEntriesInIndex  + 1 ) * sizeof( SignatureLogEntry ) ) +
		m_sizeOfIndex +
		(2 * ulSignatureDistance); // add requirements form minimum 2 signature distance for in-memory transferLog

	if ( m_sizeOfTotalMemory < minimumMemorySizeNeeded )
	{
		return WAX_RESULT_INSUFFICIENT_MEMORY_FOR_CURRENT_SETTINGS;
	}

	m_offsetSignatureLog = 0; // set signatureLog at beginning of pre-allocated memory

	m_sizeOfSignatureLOG = (UINT_PTR)( ( m_ullMaxEntriesInIndex + 1 ) * sizeof( SignatureLogEntry ) );

	m_offsetIndex = m_offsetSignatureLog + m_sizeOfSignatureLOG;

	m_offsetTransferLog = m_offsetIndex + m_sizeOfIndex + ulSignatureDistance;//we add a padding of ulSignatureDistance for safety

	m_sizeOfTransferLOG = m_sizeOfTotalMemory - m_offsetTransferLog;

	//ALIGN in memory transferLog
	m_sizeOfTransferLOG = m_sizeOfTransferLOG/ulSignatureDistance;
	m_sizeOfTransferLOG = m_sizeOfTransferLOG*ulSignatureDistance;

	return WAX_RESULT_OK;
}


WAX_RESULT MemoryPool::
allocate(
	/*__in*/ unsigned long minimumMemoryBufferSize,	//!< Minimum memory to allocate for the compressor in MB's
	/*__in*/ unsigned long maximumMemoryBufferSize )//!< Maximum memory to allocate for the compressor in MB's
{
	WAX_RESULT waxResult = WAX_RESULT_ALREADY_ALLOCATED;

	if ( m_isAllocated )
	{
		return waxResult;
	}

	MEMORYSTATUSEX statex;
	statex.dwLength = sizeof( statex );
	GlobalMemoryStatusEx ( &statex );

	//query actual physical space on the system and start from there
	unsigned long newMaximumMemoryBufferSize = maximumMemoryBufferSize;
	uint64_t sizeMaxInBytes = (uint64_t)
		( (uint64_t)maximumMemoryBufferSize * (uint64_t)MEGA );

	if ( sizeMaxInBytes>statex.ullTotalPhys )
	{
		newMaximumMemoryBufferSize = (unsigned long)
			( ( statex.ullTotalPhys/(uint64_t)MEGA ) ) -
			( ConfigMan::getInstance()->GetAllocatorStep() );
	}

	if ( minimumMemoryBufferSize > newMaximumMemoryBufferSize )
	{
		return WAX_RESULT_INVALID_SETTINGS;
	}

	waxResult = allocateMemory( minimumMemoryBufferSize, newMaximumMemoryBufferSize );

	if( WAX_RESULT_OK == waxResult )
	{
		WAXControlableBase::allocate();
	}

	return waxResult;
}


WAX_RESULT MemoryPool::
allocateMemory(
	/*__in*/ const unsigned long minimumMemoryBufferSize, //!< Minimum memory to allocate for the compressor in MB's
	/*__in*/ const unsigned long maximumMemoryBufferSize  //!< Maximum memory to allocate for the compressor in MB's
)
{
	WAX_RESULT waxResult = WAX_RESULT_ALREADY_ALLOCATED;

	// allocate memory in jumps of 128 (ConfigMan.GetAllocatorStep()),
	// not less than minimumMemoryBufferSize with first attempt at maximumMemoryBufferSize;

	UINT_PTR curentAttemptInBytes = 0;
	UINT_PTR ulAllocatedMemory    = 0;

	m_bufferAllocatedMemory = NULL;

	unsigned long ulAllocatorStep = ( ConfigMan::getInstance()->GetAllocatorStep() ) * MEGA;

	for ( UINT k=0;( ( k < NR_OF_ALLOCATORS ) && ( NULL == m_bufferAllocatedMemory ) ); k++ )
	{
		UINT_PTR largePageMinimum = NFGetLargePageMinimum();

		//align memory to largePageMinimum
		curentAttemptInBytes = (UINT_PTR)( (UINT_PTR)maximumMemoryBufferSize * (UINT_PTR)MEGA ) / largePageMinimum;
		curentAttemptInBytes = curentAttemptInBytes*largePageMinimum;

		UINT_PTR minimumMemoryBufferSizeInBytes = (UINT_PTR)minimumMemoryBufferSize*(UINT_PTR)MEGA;

		while( ( NULL == m_bufferAllocatedMemory ) &&
			( curentAttemptInBytes>= minimumMemoryBufferSizeInBytes ) )
		{
			m_bufferAllocatedMemory = (byte*)
				m_memoryAllocators.allocateMemory(
				curentAttemptInBytes,
				ulAllocatedMemory,
				(ALLOCATORS_ENUM)k
				);

			if( NULL == m_bufferAllocatedMemory )
			{
				if ( curentAttemptInBytes > ulAllocatorStep )
				{
					curentAttemptInBytes  -= ulAllocatorStep;
				}
				else
				{
					curentAttemptInBytes = minimumMemoryBufferSizeInBytes-1;
				}
			}
			else
			{
				if ( ulAllocatedMemory < minimumMemoryBufferSizeInBytes )
				{
					LogEventMessage(
						LOG_LEVEL_INFO,
						WAX_ID_MEMORY_POOL,
						L"Memory allocation failed allocated less than minimum."
						);

					m_memoryAllocators.freeMemory(
						m_bufferAllocatedMemory
						);

					m_bufferAllocatedMemory = NULL;

					curentAttemptInBytes = ulAllocatedMemory;
					//leave it to fall to next allocator
				}
			}
		}
	}

	if (NULL == m_bufferAllocatedMemory )
	{
		waxResult = WAX_RESULT_CAN_NOT_ALLOCATE_MEMORY;
		LogError(
			m_ComponentID,
			DBG_LOCATION,
			L"Memory Allocation failed - Not enough memory."
			);
	}
	else
	{
		m_sizeOfTotalMemory = ulAllocatedMemory;

		//	Set the actual allocated memory size value in ConfigMan.
		//	There are other components that will read this value.
		ConfigMan::getInstance()->SetMemorySize( m_sizeOfTotalMemory );

		waxResult = WAX_RESULT_OK;
	}

	return waxResult;
}

WAX_RESULT MemoryPool::
release()
{
	if ( m_isAllocated )
	{
		if ( NULL != m_bufferAllocatedMemory )
		{
			m_memoryAllocators.freeMemory( m_bufferAllocatedMemory );
			m_bufferAllocatedMemory = NULL;
		}
	}

	WAXControlableBase::release();

	return WAX_RESULT_OK;
}

WAX_RESULT MemoryPool::
initialize()
{
	if ( !m_isInitialized )
	{
		/////////////////////////////////////////////////////////////
		// By default we will use following priority for allocators :
		// 1. AWE Allocator 
		// 2. Standard malloc 
		/////////////////////////////////////////////////////////////

		m_memoryAllocators.registerAllocator<MemoryPoolAllocatorAWE>();
		//m_memoryAllocators.registerAllocator<MemoryPoolAllocatorHeapAlloc>();
		m_memoryAllocators.registerAllocator<MemoryPoolAllocatorMALLOC>();

		WAXControlableBase::initialize();
	}

	return WAX_RESULT_OK;
}

//! Get the statistics from MemoryPool
/*! 
*
*   Function is responsible with populating inoutStat with
*   statistical data from MemoryPool.
*   
*/
WAX_RESULT MemoryPool::
GetStatistics(
	/*__inout*/ Statistics *inoutStats ) //!< output statistic structure
{
	inoutStats->memoryAllocated = m_sizeOfTotalMemory;
	inoutStats->memoryUsed = 0;

	PROCESS_MEMORY_COUNTERS pmc;
	if ( ::GetProcessMemoryInfo( ::GetCurrentProcess(), &pmc, sizeof(pmc)) )
	{
		inoutStats->memoryUsed = pmc.WorkingSetSize;
	}

	return WAX_RESULT_OK;
}


WAX_RESULT MemoryPool::
start(
	/*__in*/ bool inbAsEncoder //!< is encoder
	)
{
	WAX_RESULT waxResult = WAX_RESULT_OK;

	if ( inbAsEncoder )
	{
		unsigned long ulSignatureDistance = ConfigMan::getInstance()->GetSignatureDistance();
		uint64_t ullTransferLogSize = ConfigMan::getInstance()->GetTLogSize();

		computeHashMemorySettings( ullTransferLogSize, ulSignatureDistance );

		waxResult = computeComponentsMemorySize();

		if( WAX_RESULT_INSUFFICIENT_MEMORY_FOR_CURRENT_SETTINGS == waxResult )
		{
			LogError(
				m_ComponentID,
				DBG_LOCATION, 
				L"Insufficient memory for current settings %d",
				waxResult
				);
		}
	}
	else
	{
		m_offsetTransferLog = 0;
		m_sizeOfTransferLOG = m_sizeOfTotalMemory;
	}

	if ( WAX_RESULT_OK == waxResult )
	{
		WAXControlableBase::start();
	}

	return waxResult;
}

WAX_RESULT MemoryPool::
stop()
{
	WAXControlableBase::stop();

	return WAX_RESULT_OK;
}