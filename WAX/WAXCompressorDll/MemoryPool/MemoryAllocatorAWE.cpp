//********************************************************/
//  ./MemoryPool/MemoryAllocatorAWE.cpp
//
//  Owner     : Attila Vajda
//
//  Company   : Neverfail
//  PROJECT   : WAN Acceleration
//  Component : Memory Pool - AWE Allocator
//
//  $Rev: 1483 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change: 
//  $Author: avajda $
//  $Date: 2009-01-14 10:20:20 +0000 (Wed, 14 Jan 2009) $
//
//********************************************************/

#pragma once

#include "MemoryAllocatorAWE.h"

MemoryPoolAllocatorAWE::MemoryPoolAllocatorAWE()
{

}

MemoryPoolAllocatorAWE::~MemoryPoolAllocatorAWE()
{

}

//! Free memory adapted to be used internally in allocateMemory and freeMemory
BOOL MemoryPoolAllocatorAWE::
freeThis(
		/*__in*/ const bool inFreeArrayOnly,
		/*__in*/ const bool inUnMap,
		/*__in*/ void* inlpUnMapMemory
		)
{
	BOOL bResult = TRUE;

	if ( ( TRUE == bResult ) &&
		( true == inUnMap ) &&
		( NULL != inlpUnMapMemory ) )
	{
		BOOL bResult = MapUserPhysicalPages(
			inlpUnMapMemory,
			m_NumberOfPages,
			NULL 
			);
	}

	if ( ( TRUE == bResult) &&
		( false == inFreeArrayOnly ) )
	{
		// Free the physical pages.
		bResult = FreeUserPhysicalPages(
			GetCurrentProcess(),
			&m_NumberOfPages,
			m_PageArray 
			);

		// Free virtual memory.
		if ( ( TRUE == bResult) &&
			( NULL != inlpUnMapMemory ) )
		{
			bResult = VirtualFree(
				inlpUnMapMemory,
				0,
				MEM_RELEASE
				);
		}
	}

	if ( ( TRUE == bResult ) )
	{
		bResult = HeapFree(
			GetProcessHeap(),
			0, 
			m_PageArray
			);
	}

	return bResult;
}

//! Allocate memory
void* MemoryPoolAllocatorAWE::allocateMemory(
		/*__in*/  UINT_PTR  inSizeOfMemoryToAllocate, //!< Size of memory to allocate for the compressor in bytes
		/*__out*/ UINT_PTR& outSizeOfAllocatedMemory  //!< Size of memory allocated in bytes
		)
{
	void* lpReturnedBuffer = NULL;
	UINT_PTR ulSizeOfAllocatedMemory = inSizeOfMemoryToAllocate;

	bool bContinue = true;
	BOOL bResult   = TRUE;

	__try
	{
		SYSTEM_INFO sSysInfo;
		GetSystemInfo(&sSysInfo);

		ulSizeOfAllocatedMemory = ulSizeOfAllocatedMemory/(UINT_PTR)NFGetLargePageMinimum();
		ulSizeOfAllocatedMemory = ulSizeOfAllocatedMemory*(UINT_PTR)NFGetLargePageMinimum();

		m_NumberOfPages = ulSizeOfAllocatedMemory/sSysInfo.dwPageSize;

		m_PageArray = (ULONG_PTR*) HeapAlloc(
			GetProcessHeap(),
			0,
			m_NumberOfPages * sizeof (ULONG_PTR)
			);

		if ( NULL == m_PageArray )
		{
			bContinue = false;
		}

		if ( bContinue)
		{
			ULONG_PTR ulNumberOfPagesSaved = m_NumberOfPages;

			bResult = AllocateUserPhysicalPages(
				GetCurrentProcess(),
				&m_NumberOfPages,
				m_PageArray 
				);

			if( TRUE != bResult )
			{
				freeThis( true );
				bContinue = false;
			}
			else
			{
				if( ulNumberOfPagesSaved != m_NumberOfPages )
				{
					freeThis( false );
					bContinue = false;
				}
			}
		}

		if ( bContinue )
		{
			ulSizeOfAllocatedMemory = m_NumberOfPages * sSysInfo.dwPageSize;
			PVOID    lpMemReserved;

			// Reserve the virtual memory
			lpMemReserved = VirtualAlloc(
				NULL,
				ulSizeOfAllocatedMemory,
				MEM_RESERVE | MEM_PHYSICAL,
				PAGE_READWRITE 
				);

			if( NULL == lpMemReserved )
			{
				LogEventMessage(
					LOG_LEVEL_INFO, 
					WAX_ID_MEMORY_POOL,
					L"AWE - Failed to reserve virtual memory."
					); 

				freeThis( false );
				bContinue = false;
			}

			if ( bContinue )
			{
				// Map the physical memory into the window.
				bResult = MapUserPhysicalPages(
					lpMemReserved,
					m_NumberOfPages,
					m_PageArray
					);

				if( TRUE == bResult ) 
				{
					lpReturnedBuffer = lpMemReserved;

					LogEventFormated(
						LOG_LEVEL_INFO,
						WAX_ID_MEMORY_POOL,
						L"AWE - Allocated %u.",
						ulSizeOfAllocatedMemory 
						);
				}
				else
				{
					LogEventMessage(
						LOG_LEVEL_INFO,
						WAX_ID_MEMORY_POOL,
						L"AWE Failed to map physical memory."
						);

					freeThis( false, false, lpMemReserved );
				}
			}
		}
	}
	__except( STATUS_NO_MEMORY == _exception_code() ? EXCEPTION_EXECUTE_HANDLER:EXCEPTION_CONTINUE_SEARCH) 
	{
		if ( STATUS_NO_MEMORY == GetExceptionCode() ) 
		{
			LogError(
				WAX_ID_MEMORY_POOL,
				DBG_LOCATION,
				L"AWE - STATUS_NO_MEMORY"
				);
		}
		else
		{
			LogError(
				WAX_ID_MEMORY_POOL,
				DBG_LOCATION,
				L"AWE - Failed to allocate memory."
				);
		}
	}

	if ( NULL != lpReturnedBuffer )
	{
		outSizeOfAllocatedMemory = ulSizeOfAllocatedMemory;
	}

	return lpReturnedBuffer;
}

//! Free memory 
void MemoryPoolAllocatorAWE::
freeMemory(
		/*__in*/ void* lpMemoryToFree //!< memory buffer to free
		)
{
	if ( FALSE == freeThis( false, true, lpMemoryToFree ) )
	{
		LogEventMessage(
			LOG_LEVEL_INFO,
			WAX_ID_MEMORY_POOL,
			L"AWE - Free returned FALSE"
			);
	}
}
