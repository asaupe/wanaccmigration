//********************************************************/
//  ./MemoryPool/MemoryAllocatorAWE.h
//
//  Owner     : Attila Vajda
//
//  Company   : Neverfail
//  PROJECT   : WAN Acceleration
//  Component : Memory Pool - AWE Allocator
//
//  $Rev: 1483 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: avajda $
//  $Date: 2009-01-14 10:20:20 +0000 (Wed, 14 Jan 2009) $
//
//********************************************************/

#pragma once

#include "MemoryAllocatorBase.h"
#include "..\AllUtils.h"

//! MemoryPoolAllocatorAWE - memory management and preallocation
/*!
*  Used by MemoryPool in order to allocate memory.
*  Implementing AWE -  Address Windowing Extensions.
*  From MSDN : 
*  The VirtualAlloc function can be used to reserve an Address Windowing Extensions 
*  (AWE) region of memory within the virtual address space of a specified process. 
*  This region of memory can then be used to map physical pages into and out of 
*  virtual memory as required by the application. 
*  The MEM_PHYSICAL and MEM_RESERVE values must be set in the AllocationType parameter. 
*  The MEM_COMMIT value must not be set.
*  The page protection must be set to PAGE_READWRITE.
*
*  For more details : http://msdn.microsoft.com/en-us/library/aa366527(VS.85).aspx
*
*   \class MemoryPoolAllocatorAWE
*
*   \author Attila Vajda
*   \version 1.0.$Rev: 1483 $
*   \date    OCT-2008
*
*/
class MemoryPoolAllocatorAWE: public MemoryAllocatorBase<MemoryPoolAllocatorAWE, UID_ALLOCATOR_AWE>
{
public:
	MemoryPoolAllocatorAWE();
	~MemoryPoolAllocatorAWE();
	
	//! Allocate memory
	void* allocateMemory(
		/*__in*/  UINT_PTR  inSizeOfMemoryToAllocate, //!< Size of memory to allocate for the compressor in bytes
		/*__out*/ UINT_PTR& outSizeOfAllocatedMemory  //!< Size of memory allocated in bytes
		);

	//! Free memory 
	void freeMemory( 
		/*__in*/ void* lpMemoryToFree //!< memory buffer to free
		);

private:

	//! Free memory adapted to be used internally in allocateMemory and freeMemory
	BOOL freeThis(
		/*__in*/ const  bool inFreeArrayOnly = true,
		/*__in*/ const  bool inUnMap         = false,
		/*__in*/ void* inlpUnMapMemory      = NULL 
		);

	ULONG_PTR* m_PageArray    ; //!< page info
	ULONG_PTR  m_NumberOfPages; //!< number of pages allocated
};
