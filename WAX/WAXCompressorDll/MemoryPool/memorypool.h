//********************************************************/
//  ./MemoryPool/memorypool.cpp
//
//  Owner     : Attila Vajda
//
//  Company   : Neverfail
//  PROJECT   : WAN Acceleration
//  Component : Memory Pool 
//
//  $Rev: 1483 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: avajda $
//  $Date: 2009-01-14 10:20:20 +0000 (Wed, 14 Jan 2009) $
//
//********************************************************/

#pragma once

#include "WAXTypes.h"
#include "../Common.h"
#include "../WaxSingletonTemplate.h"
#include "../WAXControlableBase.h"
#include "MemoryAllocatorBase.h"
#include "../Stats/IStats.h"

//! MemoryPool - memory management and preallocation
/*!
*  Memory pool uses HeapAlloc and malloc in order to allocate memory.
*  First allocator is HeapAlloc and if this one fails to allocate the requested memory will use malloc.
*  allocate() is called with 2 parameters minimumMemoryBufferSize and maximumMemoryBufferSize - allocate memory
*  will try to allocate maximumMemoryBufferSize and will try to allocate in 128 MB step ( ConfigMan->GetAllocatorStep() )
*  until reaches minimumMemoryBufferSize. It will do this with both allocators in case that the first cannot allocate the minimum.
*  With preallocated memory size computes the required size for signatureLOG and hashMap( signature index ) and it assigns
*  buffer for transferLOG
*   \class MemoryPool
*
*   \author Attila Vajda
*   \version 1.0.$Rev: 1483 $
*   \date    JUN-2008
*
*/
class MemoryPool: public WaxSingletonTemplate<MemoryPool>, WAXControlableBase, public IStats
{
	friend class WaxSingletonTemplate<MemoryPool>;
public:
	
	// Methods declared in WaxControlerBase that are also implemented in the encoder.
	//! Implementation of initialize - for details see WaxControlerBase::initialize.
	WAX_RESULT initialize(
		);

	//! Allocate memory 
	WAX_RESULT allocate(
		/*__in*/ unsigned long minimumMemoryBufferSize, //!< Minimum memory to allocate for the compressor in MB's
		/*__in*/ unsigned long maximumMemoryBufferSize  //!< Maximum memory to allocate for the compressor in MB's
		);

	//! Release memory allocated with allocate()
	WAX_RESULT release(
		);

	//! Get the statistics from MemoryPool
	WAX_RESULT GetStatistics(
		/*__inout*/ Statistics *inoutStats //!< output statistic structure
		);

	//! Implementation of start - for details see WaxControlerBase::start().
	WAX_RESULT start(
	/*__in*/ bool inbAsEncoder //!< is encoder
	);

	//! Implementation of stop - for details see WaxControlerBase::stop().
	WAX_RESULT stop(
	);

	//! Returns memory sizes for different components
	UINT_PTR getComponentMemorySize(
		/*__in*/ WAX_ID innComponentID = 0 //!< in component ID for which we will return the size
		);

	//! Returns the buffer for different components
	unsigned char* gimme  ( 
		/*__in*/ UINT inunSize,            //!< size of the buffer returned from getComponentMemorySize;
		/*__in*/ WAX_ID innComponentID = 0 //!< in component ID for which we will return the buffer 
		);

private:
	MemoryPool();
	~MemoryPool();

	//! Compute Memory Size needed for hash_map
	WAX_RESULT computeHashMemorySettings( 
		/*__in*/ uint64_t inTransferLogSize,   //!< transferLOG size
		/*__in*/ unsigned long     inSignatureDistance  //!< signature distance size
		);

	//! Compute Memory Size needed for different components
	WAX_RESULT computeComponentsMemorySize(
		);

	//! Allocate the memory - allocate computes sizes and allocateMemory calls allocators for memory
	WAX_RESULT allocateMemory(
		/*__in*/ const unsigned long minimumMemoryBufferSize, //!< Minimum memory to allocate for the compressor in MB's
		/*__in*/ const unsigned long maximumMemoryBufferSize  //!< Maximum memory to allocate for the compressor in MB's
		);

	//////////////////////////////////////////////////////////////////////////
	// Variables                                                            //
	//////////////////////////////////////////////////////////////////////////

	///////////////
	// STATIC    //
	///////////////
	static WAX_ID m_ComponentID;

	///////////////
	// LOCAL     //
	///////////////
	// Memory Buffer offsets
	UINT_PTR  m_sizeOfTotalMemory      ; //!< total pre-allocated memory in bytes
	UINT_PTR  m_sizeOfSignatureLOG     ; //!< size of signatureLog
	UINT_PTR  m_sizeOfTransferLOG      ; //!< size of transferLOG
	UINT_PTR  m_sizeOfIndex            ; //!< maxMemoryUsedByBuckets+maxMemoryUsedByEntriesInIndex
	uint64_t m_ullMaxEntriesInIndex   ; //!< max nr. of entries that will be in the hash map
	UINT_PTR  m_offsetIndex            ; //!< offset of index memory in pre-allocated buffer - signature log entries
	UINT_PTR  m_offsetTransferLog      ; //!< offset of transferLog  memory in pre-allocated buffer
	UINT_PTR  m_offsetSignatureLog     ; //!< offset of SignatureLog memory in pre-allocated buffer
	UINT_PTR  m_offsetTemporaryBuffers ; //!< offset of tempBuffer   memory in pre-allocated buffer
	byte*     m_bufferAllocatedMemory  ; //!< memory location of pre-allocated buffers 
	
	MemoryAllocators m_memoryAllocators; //!< contains allocators
};
