//********************************************************/
//  ./MemoryPool/MemoryAllocators.h
//
//  Owner     : Attila Vajda
//
//  Company   : Neverfail
//  PROJECT   : WAN Acceleration
//  Component : Memory Pool - all allocators include 
//
//  $Rev: 1483 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: avajda $
//  $Date: 2009-01-14 10:20:20 +0000 (Wed, 14 Jan 2009) $
//
//********************************************************/

#pragma once
#include "MemoryAllocatorAWE.h"
//#include "MemoryAllocatorHeapAlloc.h"
#include "MemoryAllocatorMALLOC.h"
