//********************************************************/
//  ./MemoryPool/memoryallocatorbase.h
//
//  Owner     : Attila Vajda
//
//  Company   : Neverfail
//  PROJECT   : WAN Acceleration
//  Component : Memory Pool - MemoryAllocatorBase
//
//  $Rev: 1483 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: avajda $
//  $Date: 2009-01-14 10:20:20 +0000 (Wed, 14 Jan 2009) $
//
//********************************************************/

#pragma once
#include "../Logger/Logger.h"
#include "../ConfigMan/ConfigMan.h"

enum ALLOCATORS_ENUM
{
	UID_ALLOCATOR_AWE = 0,
//	UID_ALLOCATOR_HEAP,
	UID_ALLOCATOR_MALLOC,
	NR_OF_ALLOCATORS,
	// disabled - not used - we will keep this for testing
};

class MemoryAllocatorInterface
{
public:
	//! Allocate memory
	virtual void* allocateMemory(
		/*__in*/  UINT_PTR  inSizeOfMemoryToAllocate, //!< Size of memory to allocate for the compressor in bytes
		/*__out*/ UINT_PTR& outSizeOfAllocatedMemory  //!< Size of memory allocated in bytes
	)=0;

	//! Free memory 
	virtual void freeMemory( 
		/*__in*/ void* lpMemoryToFree //!< memory buffer to free
	) = 0;
	
	//! Get Class UID
	virtual int getUID()
	{
		return m_myUID;
	};

protected:
	int m_myUID;
};


class MemoryAllocators
{
public:
	MemoryAllocators()
	{
		m_usedAllocator = NULL;
	}

	template<class What> void registerAllocator()
	{
		m_AllocatorsArray[What::myID] = What::createInstanceOf();
	};


	template<class What> void Create()
	{
		What* existingInstance = get<What>( What::myID ); pula
	};

	 void* allocateMemory(
		/*__in*/  UINT_PTR  inSizeOfMemoryToAllocate, //!< Size of memory to allocate for the compressor in bytes
		/*__out*/ UINT_PTR& outSizeOfAllocatedMemory, //!< Size of memory allocated in bytes
		/*__in*/ ALLOCATORS_ENUM inAllocatorType = UID_ALLOCATOR_AWE //!< default allocator AWE
		)
	{
		void* lpResult = NULL;

		if ( inSizeOfMemoryToAllocate >0 )
		{
			m_usedAllocator = m_AllocatorsArray[inAllocatorType];

			if ( NULL != m_usedAllocator )
			{
				lpResult = m_usedAllocator->allocateMemory( inSizeOfMemoryToAllocate, outSizeOfAllocatedMemory );
			}
		}

		return lpResult;
	}

	virtual void freeMemory( 
		/*__in*/ void* lpMemoryToFree //!< memory buffer to free
		)
	{
		if ( NULL != m_usedAllocator )
		{
			m_usedAllocator->freeMemory( lpMemoryToFree );
		}
	}

protected:
	MemoryAllocatorInterface* m_AllocatorsArray[NR_OF_ALLOCATORS];
	MemoryAllocatorInterface* m_usedAllocator;
};


//////////////////////////////////////////////////////////////////////////
// MemoryAllocators
//////////////////////////////////////////////////////////////////////////
template< class ClassName, int UID = 0>
class MemoryAllocatorBase : public MemoryAllocatorInterface
{
public:
	enum{
		myID = UID 
	};

	static ClassName* createInstanceOf()
	{
		static ClassName* aWholeNewInstance = new ClassName;
		return aWholeNewInstance;
	};

protected:
	MemoryAllocatorBase()
	{
		m_myUID = myID;
	}
};
