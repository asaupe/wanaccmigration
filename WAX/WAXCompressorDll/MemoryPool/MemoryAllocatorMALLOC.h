//********************************************************/
//  ./MemoryPool/MemoryAllocatorMALLOC.h
//
//  Owner     : Attila Vajda
//
//  Company   : Neverfail
//  PROJECT   : WAN Acceleration
//  Component : Memory Pool - standard malloc()
//
//  $Rev: 1483 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: avajda $
//  $Date: 2009-01-14 10:20:20 +0000 (Wed, 14 Jan 2009) $
//
//********************************************************/

#pragma once

#include "MemoryAllocatorBase.h"

//! MemoryPoolAllocatorMALLOC - memory management and preallocation - implementing standard malloc
/*!
*  Used by MemoryPool in order to allocate memory.
*   \class MemoryPoolAllocatorMALLOC
*
*   \author Attila Vajda
*   \version 1.0.$Rev: 1483 $
*   \date    OCT-2008
*
*/
class MemoryPoolAllocatorMALLOC: public MemoryAllocatorBase<MemoryPoolAllocatorMALLOC, UID_ALLOCATOR_MALLOC>
{
public:
	MemoryPoolAllocatorMALLOC();
	~MemoryPoolAllocatorMALLOC();

	//! Allocate memory
	void* allocateMemory(
		/*__in*/  UINT_PTR  inSizeOfMemoryToAllocate, //!< Size of memory to allocate for the compressor in bytes
		/*__out*/ UINT_PTR& outSizeOfAllocatedMemory  //!< Size of memory allocated in bytes
		);

	//! Free memory 
	void freeMemory( 
		/*__in*/ void* lpMemoryToFree //!< memory buffer to free
		);
};
