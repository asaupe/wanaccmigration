//********************************************************/
//  ./MemoryPool/MemoryAllocatorMALLOC.cpp
//
//  Owner     : Attila Vajda
//
//  Company   : Neverfail
//  PROJECT   : WAN Acceleration
//  Component : Memory Pool - standard malloc()
//
//  $Rev: 1483 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: avajda $
//  $Date: 2009-01-14 10:20:20 +0000 (Wed, 14 Jan 2009) $
//
//********************************************************/

#include "MemoryAllocatorMALLOC.h"


MemoryPoolAllocatorMALLOC::MemoryPoolAllocatorMALLOC()
{
}

MemoryPoolAllocatorMALLOC::~MemoryPoolAllocatorMALLOC()
{
}

//! Allocate memory
void* MemoryPoolAllocatorMALLOC::
allocateMemory( 
	/*__in*/  UINT_PTR  inSizeOfMemoryToAllocate, //!< Size of memory to allocate for the compressor in bytes
	/*__out*/ UINT_PTR& outSizeOfAllocatedMemory  //!< Size of memory allocated in bytes
	)
{
	void* lpReturn = NULL;

	__try 
	{
		lpReturn = malloc( inSizeOfMemoryToAllocate );
	}
	__except ( STATUS_NO_MEMORY == _exception_code() ? EXCEPTION_EXECUTE_HANDLER:EXCEPTION_CONTINUE_SEARCH ) 
	{
		if (STATUS_NO_MEMORY == GetExceptionCode() ) 
		{
			LogError( WAX_ID_MEMORY_POOL, DBG_LOCATION, L"STATUS_NO_MEMORY" );
		}
		else
		{
			LogError( WAX_ID_MEMORY_POOL, DBG_LOCATION, L"Failed to allocate memory." );
		}
	}

	if ( NULL != lpReturn )
	{
		outSizeOfAllocatedMemory = inSizeOfMemoryToAllocate;
	}

	return lpReturn;
}

//! Free memory
void MemoryPoolAllocatorMALLOC::
freeMemory( 
	/*__in*/ void* lpMemoryToFree //!< memory buffer to free
	)
{
	if ( NULL != lpMemoryToFree )
	{
		free( lpMemoryToFree );
	}
}