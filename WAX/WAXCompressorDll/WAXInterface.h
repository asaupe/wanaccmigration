//********************************************************/
//  ./WaxInterface.h
//
//  Owner : Ciprian Maris
//  
//	Company		: Neverfail 
//	PROJECT		: WAN Acceleration 
//  Component	: WAX Interface
//
//  $Rev: 1568 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: cmaris $  
//  $Date: 2009-01-22 10:07:26 +0000 (Thu, 22 Jan 2009) $
//
//********************************************************/

#pragma once

#include "Common.h"
#include "WaxTypes.h"

#include "WaxSingletonTemplate.h"
#include "WAXControlableBase.h"

#include "UWM/UWM.h"
#include "Stats/Stats.h"

//! WAXInterface - interface for the WAX native compressor components
/*! \class WAXInterface
*   \brief Implements the interface for the WAX native compressor components 
*
*	This component implements 
*		- WAXXER (section 4.1	WAXXER - in design document)
*		- Entry Point Interface (Section 3.2.6	Entry Point Interface - in design document)
*	Location of the design document for the entire project is:
*	http://ng-eng-share/SiteDirectory/dev/Docs/Projects/WAN%20Acceleration/Design/TD-Wan%20Acceleration%20-%20Detailed%20Design.doc
*
*	Creates all components by calling their getInstance() method.
*   Is called by WAXCompressorDLL for the exported DLL functions for
*		- init/exit, allocate/relase and start/stop
*		- encode/decode 
*		- statistics
*
*	NOTE:	This WAXInterface component will not be involved in WAXCompressor state management.
*			This means that no state flags are kept in WaxInterface.
*			WaxCompressor.cpp unit is responsible for maintaining state.
*
*   \author CM
*   \version 1.0.$Rev: 1568 $
*   \date    JUN-2008
*/
class WAXInterface: public WaxSingletonTemplate<WAXInterface>
{
	friend class WaxSingletonTemplate<WAXInterface>;

public:

	///////////////////////////////////////////////////////////////////////////
	//////	Control and Management methods		///////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	//! Method initialize() - Initializes the components of the compressor. 
	WAX_RESULT initialize(void);

	//! Method exit() - ends the lifespan of the compressor. 
	WAX_RESULT exit(void);

	//! Method queryStatistics() - gathers stats from the WAX components.
	WAX_RESULT queryStatistics(
		/*__inout*/ Statistics *inoutStatistics //!<	statistics structure to receive the entire statistics set
		);

	//! Method allocate() - Allocates resources for the compressor. 
	WAX_RESULT allocate(
		/*__in*/ unsigned long inMinimumMemoryBufferSize,	//!< Minimum memory to allocate for the compressor in MB's
		/*__in*/ unsigned long inMaximumMemoryBufferSize,	//!< Maximum memory to allocate for the compressor in MB's
		/*__in*/ unsigned long inTransferLogSize,			//!< On disk transfer Log Size in MB's
        /*__in*/ const StringBuffer * inTransferLogPath	//!< On disk transfer Log File path (complete path)
               );

	//! Method release() - releases pre-allocated disk space and memory.
	WAX_RESULT release(void);

	//! Method start() - Sets the start parameters and starts the components that need to aquire pre-allocated resources.
	WAX_RESULT start(
		/*__in*/ bool inAsEncoder,					//!< Server role (compressor/decompressor)
		/*__in*/ unsigned long inSignatureBase,				//!< signature base
		/*__in*/ unsigned long inSignatureDistance,			//!< signature distance
		/*__in*/ unsigned long inMinRPRPatternsLength,		//!< minimum len of ZERO/DA pattern replaced by a RPR token
		/*__in*/ unsigned long inMinDeDuplicateLength,		//!< min len of the buffer to be considered for DD
		/*__in*/ unsigned long inMinDDLengthReplacedByToken,	//!< minimum length of data replaced by a DD token
		/*__in*/ const bool inEncodeOutOfBoundaryEnabled = false, //!< advanced encode for out of boundary entries
		/*__in*/ double indNInvariant = 0.05			//!< Factor for recent data volume and compression computation.
				);

	//! Method stop() - Stops the Compressor.
	WAX_RESULT stop(void);

	//! Method encode() encodes the input buffer on source.
	WAX_RESULT encode(
		/*__in*/	const ByteBuffer*  inputBuffer,	//!< source buffer to be encoded
        /*__out*/ ByteBuffer*  outputBuffer,		//!< destination buffer - pre-allocated by caller
        /*__in*/	bool removeRepeatedPatterns,	//!< FLAG - RPR requested by caller
        /*__in*/	bool deDuplicate				//!< FLAG - DD requested by caller
		);

	//! Method decode() decodes the input buffer on target.
	WAX_RESULT decode(
        /*__in*/	const ByteBuffer*  inputBuffer,	//!< source buffer to be decoded
        /*__out*/ ByteBuffer*  outputBuffer,		//!< destination buffer - pre-allocated by caller
		/*__in*/ bool bRPREncoded,
		/*__in*/ bool bDDEncoded
		);

private:
	static WAX_ID m_ComponentID;

	///////////////////////////////////////////////////////////////////////////
	//////	Singleton specific private members   /////////////////////////////
	///////////////////////////////////////////////////////////////////////////
	WAXInterface();
	~WAXInterface();

	//!UWM local component - will be used for all subsequent encode/decode calls.
	//	NOTE:	The reason why only UWM is a local member is that all other components are invoked
	//			during allocate/start, stop/release, so we can use their getInstance() method without
	//			performance penalty.
	UWM* m_UWM;

	WAXStatistics* m_Stats;
};

