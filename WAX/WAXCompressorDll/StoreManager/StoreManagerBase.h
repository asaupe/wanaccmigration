//********************************************************/
//  ./StoreManager/StoreManagerBase.h
//
//  Owner : Bogdan Hruban
//  
//  Neverfail             ________________
//           PROJECT   :  WAN Acceleration
//           Component :  Store Manager
//
//  $Rev: 1483 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: avajda $  
//  $Date: 2009-01-14 10:20:20 +0000 (Wed, 14 Jan 2009) $
//
//********************************************************/

//******************************************************************/
//
// This component was implemented to be able to use different Store
// Manager components in UnitTests. 
// 
// The StoreManagerBase interface publishes methods for adding data
// and removing data from store manager. 
// The actual store manager is replaced in UnitTests\TokenizerTest.
//
//******************************************************************/

#pragma once

#include "WAXTypes.h"

//! StoreManager interface
/*!
	StoreManagerBase Interface is used for allowing the use of
	different store managers. This is the case of UnitTests.

	This approach has been made in order not to change the code
	for Tokenizer in the UnitTests.

	\sa StoreManager
	\sa DummyStoreManager
*/
class StoreManagerBase
{
public:
	//! Get data from StoreManager from a given offset
	virtual WAX_RESULT getBufferByTLogOffset (
		/*__in*/	long long inllTLOffset,			//!< Offset in transfer log (relative to file)
		/*__in*/	const unsigned long inulDataLength,		//!< Length of required buffer
		/*__inout*/	unsigned char* inoutDestinationBuffer	//!< Destination of data to be written to preallocated by the caller
		) = 0;
#ifdef _DUMMY_STORE_MANAGER
	//! Add data to StoreManager (Used only in case of DummyStoreManager)
	virtual unsigned long addDataToTLog(const unsigned char* inlpData,const unsigned long inulDataLength) = 0;
#endif
};