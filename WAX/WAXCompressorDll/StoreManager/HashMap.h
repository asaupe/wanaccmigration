//********************************************************/
//  ./StoreManager/HashMap.h
//
//	Company		: Neverfail 
//	PROJECT		: WAN Acceleration 
//	Component	: Store Manager / Hash Map
//  Owner		: Ciprian Maris
//
//  $Rev: 1483 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: avajda $  
//  $Date: 2009-01-14 10:20:20 +0000 (Wed, 14 Jan 2009) $
//
//********************************************************/

#pragma once

#include <windows.h>
#include <list>

using namespace std;


//! Hash Map - WAX speciffic implementation of hash map.
/*! \class HashMap
*   \brief WAX speciffic implementation of hash map with focus on speed and memory usage.
*
*	This HashMap implementation is speciffic to the WAX project and was designed and implemented 
*	to comply with the following requirements:
*		- Search speed bigger than the one in stdext::hash_map. This is done by:
*			- Pre-computation of the number of buckets based on the maximum number of unique keys.
*			- Re-hashing is not needed - the number of buckets remains fixed from contruction 
*			to destruction.
*		- Needed memory resources are predictable, can be established from the component 
*		construction, and does not exceede the ones of stdext::hash_map.
*
*	The class is implemented as follows:
*		- A fixed size (size is given in constructor) array of buckets.
*		- Every bucket is a BST, with the head being referenced from the array. This implementation was
*		chosen to maximize search speed.
*
*		- Bucket index computation is based on the int 32 hashing function implemented in the method 
*		getBucketIndex(). This method will return the bucket index for a speciffic input parameter key.
*		Any key can then be searched in the bucket (BST) by the key value.
*
*		- The maximum memory size used by the class is deterministic. It is computed by the static 
*		method getMaxMemory().
*
*	The class can operate from the memory allocation perspective in two ways:
*		- [1] The memory buffer is allocated by the caller. This is used in the WAX implementation.
*		- [2] The memory buffer is allocated in the class. This is used in UnitTesting.
*
*   Is called by 
*		- SignatureLog::start() / stop()  - for creation / destruction.
*
*		- SignatureLog::writeSignatures() - for updating and removing index entries.
*
*		- SignatureLog::searchEntryBySignature () - for retreiving a signature from the hash map.
*
*	NOTE:	[1] The current tests (Oct 2008) show it to be about 3 times faster in the WAX context as the 
*			STL stdext::hash_map<> implementation.
*
*			[2] The search time can improved by balancing the BST's used. Test implementations with AVL did
*			not show improvements, so the unbalanced BST implementation was prefered at this stage.
*
*			[3] The current hashing function is the best one we found so far (Oct 2008). The distribution 
*			of the keys in buckets can be improved if a better hashing function is found.
*
*			[4] Due to perfromance constraints, there are a set of assumptions that the methods make, and 
*			do not check for. These conditions need to be satisfied by the caller. In production, the caller 
*			must make sure it provides the hash map with sufficient memory resources:
*				- No checking for NULL memory buffers.
*				- There is enough memory to hold all possible key values.
*	
*			[5] The methods that are implemented in the .CPP file have their detailed description in the .CPP file.
*
*   \author Ciprian Maris
*   \version 1.0.$Rev: 1483 $
*   \date    OCT-2008
*/
class HashMap
{
public:
	HashMap();
	
	//! Main Constructor - computes the number of buckets and allocates memory buffers if necessary.
	HashMap( 
		/*__in*/ size_t inMaxKeys,			//!< Maximum number of unique keys that the hash map will ever see. 
		/*__in*/ unsigned char* inlpbMemoryBuffer,	//!< Pre-allocated memory buffer to be used by the hash. If NULL, 
										// the hash map will attempt to allocate memory locally.
		/*__in*/ unsigned long inulBufferLength		//!< Length of the input memory buffer.
		);

	~HashMap( );

	//! Method that must be called after an constructor call with the input memory buffer set to NULL.
	//	If this method returns false the current HashMap object can not be used.
	//	This method will only be called from unit tests.
	bool getConstructorSuccess(void) { return (m_lpbMemoryBuffer != NULL); }

	///////////////////////////////////////////////////////////////////////////////////////////////
	////	HashMap operation methods.

	//! Inserts or updates a <key, data> pair in the hash map. Returns the old data 
	//	value if the key is already present in the hash map.
	bool insert( 
		/*__in*/	size_t inKey,		//!< Value of the key to be inserted/updated.
		/*__in*/	void* invpValue,	//!< Value of the new data member.
		/*__out*/	void* &outvpOldValue//!< Value of the old data member if the key 
									//	is already present in the map.
				);

	//! Searches for a key in the hash map.
	bool find( 
		/*__in*/ size_t inKey,		//!< Value of the key to be searched for.
		/*__out*/ void* &outvpValue //!< Value of the data member if the key is found in the map.
	);

	//!	Removes a key from the hash map.
	bool remove(
		/*__in*/ size_t inKey //<! Value of the key to be removed.
		);
	
	//! Removes all <key, value> pairs from the map.
	void clear(void);

	////	END HashMap operation methods.
	///////////////////////////////////////////////////////////////////////////////////////////////

	//! Computes the memory needed to hold a maximum number "inMaxKeys" of unique elements.
	static unsigned long getMaxMemory(
		/*__in*/ size_t inMaxKeys 
		);

	///////////////////////////////////////////////////////////////////////////////////////////////
	////	Getters.

	//! Returns the number of unique keys in the hash map.
	size_t size(void) { return m_size; }

	//! Return the maximum number of levels in the buckets.
	unsigned long getMaxBucketDepth(void)	{ return m_MaxBucketDepth ; }

	//! Returns the number of buckets that ever reached the maximum number of levels.
	unsigned long getNrBucketsAtMaxDepth(void)	{ return m_NrBucketsAtMaxDepth ; }

	//! Returns the maximum number of keys in the map since its creation.
	//	NOTE: the current number of keys returned by size() can be less than getMaxKeysInMap().
	unsigned long getMaxKeysInMap(void)		{ return m_MaxKeysInMap ; }

	//! Returns the number of buckets that have at least one element in them.
	unsigned long getUsedBucketsNow(void);
	
	////	END Getters.
	///////////////////////////////////////////////////////////////////////////////////////////////

private:
	///////////////////////////////////////////////////////////////////////////////////////////////
	////	The HashMap Element structure - defined as a Binary Search Tree node.
	typedef struct _hashmap_element
	{
		size_t key;				//!< Key value
		void* data;				//!< Data Pointer. IMPORTANT: If NULL, the current NODE is not in use.
		_hashmap_element* left;
		_hashmap_element* right;

	} hashmap_element;
	////	
	///////////////////////////////////////////////////////////////////////////////////////////////

	//! Hash function on 32bit integers used by the hash map.
	inline size_t getBucketIndex( 
		/*__in*/ size_t inKey //!< Key for which to compute the hash value (bucket index).
		)
	{
		//	Robert Jenkins' 32 bit integer hash function
		//	Source: http://www.concentric.net/~Ttwang/tech/inthash.htm
		inKey = ( inKey+0x7ed55d16 ) + ( inKey<<12 );
		inKey = ( inKey^0xc761c23c ) ^ ( inKey>>19 );
		inKey = ( inKey+0x165667b1 ) + ( inKey<<5 );
		inKey = ( inKey+0xd3a2646c ) ^ ( inKey<<9 );
		inKey = ( inKey+0xfd7046c5 ) + ( inKey<<3 );
		inKey = ( inKey^0xb55a4f09 ) ^ ( inKey>>16 );
		return inKey % m_nrBuckets;
	}

	//! Locates a key in the hash map. Can also insert one if it does not exist. 
	bool locateKey( 
		/*__in*/	size_t inKey,						//!< Value of the key to be located.
		/*__out*/	hashmap_element* &outhmelValue,		//!< Output data value if the key is found.
		/*__out*/	hashmap_element** &outhmelPrevRef,  //!< Putput parent link if the node has one.
		/*__in*/	bool inbInsertIfNotFound = false	//!< Insert if not exists flag.
		);

	//! Deletes the maximum value of the left subtree for an tree identified by the head element.
	void removeMaxValueInLeftSubtree( 
		/*__in*/ hashmap_element* inhmElHead //!< Root of the tree for which the delete is performed.
		);

	//!	Returns an empty meomory location for one node(hashmap_element).
	void* getMemoryForOneElement(void);

	//!	Adds the address of the node (hashmap_element) to the list of free adresses.
	void releaseMemoryForOneElement( void* inAdressToRelease );
	
	////
	///////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////
	////	Private Members
	unsigned char*  m_lpbMemoryBuffer;	//!< Memory buffer for the index. Is allocated by either the caller 
								// or the index constructor. Will hold all the nodes of the index.
	bool    m_bMemAllocatedLocally;	//! If true, the memory buffer was allocated by the
									// index constructor and will be freed in the destructor.
	
	unsigned long	m_ulMemoryBufferSize;	//!< Size of the in memory buffer.
	
	//	Members for free positions in memory buffer management 
	list<void*> m_listFreeAddresses;	//!< List containing adresses of the last nodes removed from the map.
	size_t m_nrCurrentPositionInBuffer;	//!< Position of the current free location in the mem buffer.

	hashmap_element** m_bucketArray;	//!< Array containing pointers to bucket BST's of the hash map.

	size_t m_size;		//!< Number of unique keys in hash map.
	size_t m_nrBuckets;	//!< Number of buckets in the hash map.
	////
	///////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////
	///////		Statistic components	///////////////////////////////////////////////////////////
	unsigned long m_MaxBucketDepth;		//!< Maximum number of levels in the buckets.

	unsigned long m_NrBucketsAtMaxDepth;//!< Number of buckets that ever reached the maximum number of levels.

	unsigned long m_MaxKeysInMap;	//!< Maximum number of keys in the map since its creation.
							// NOTE: the current number of keys (m_size) can be less than m_MaxKeysInMap.
	////
	///////////////////////////////////////////////////////////////////////////////////////////////
} ;
