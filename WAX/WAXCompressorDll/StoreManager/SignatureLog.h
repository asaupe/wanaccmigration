//********************************************************/
//  ./StoreManager/SignatureLog.h
//
//	Company		: Neverfail 
//	PROJECT		: WAN Acceleration 
//	Component	: Store Manager / Signature Log
//  Owner		: Ciprian Maris
//
//  $Rev: 1483 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: avajda $  
//  $Date: 2009-01-14 10:20:20 +0000 (Wed, 14 Jan 2009) $
//
//********************************************************/

#pragma once

#include "../Common.h"
#include "WAXTypes.h"

#include "../WAXControlableBase.h"
#include "../WaxSingletonTemplate.h"
#include "../memorypool/memorypool.h"
#include "HashMap.h"

//!Signature Log Entry definition 
//	NOTE: It is defined here and visible to other compilation units 
//	because it is also used by other components.
#pragma pack(1)
typedef struct _SignatureLogEntry
{
	SignatureType		signature;
	StrongSignatureType strongSignature;
	bool				isSigUpdated;
} SignatureLogEntry;
#pragma pack()

//!Chunk Characteristic definition 
//	NOTE: It is defined here and visible to other compilation units 
//	because it is also used by other components.
typedef struct _ChunkCharacteristic // aka _SignatureChecksumFlag in Design Document
{
	SignatureType		signature;
	StrongSignatureType strongSignature;
	SignatureLogEntry*	sigLogEntry;
} ChunkCharacteristic;

//! SignatureLog - storage and management for signatures.
/*! \class SignatureLog
*   \brief Implements the storage class for Chunk signatures and the index(dictionary) for them.
*
*	The class has two major components:
*		- The signature array is the actual storage for signature log entries. There is a signature log 
*		entry in this array for each full SD data buffer in the Transfer Log. Resulting from this, the
*		offset of a buffer in the transfer log is computed as:
*			TLogOffset = PositionInSigLog * SignatureDistance
*
*		When the Transfer Log rolls over, the signature log also rolls over.
*
*		- The signature index(dictionary) is a collection of unique signatures from the signature array
*		with fast search/update capabilities.
*
*   Is called by 
*		- StoreManager::writeBuffer() - for writting signatures to the signature log and updating signature 
*										index entries.
*		- WAXEncoder::findTokensInBuffer() - for finding entries in the signature index.
*
*	NOTE:	[1] The methods that are implemented in the .CPP file have their detailed description in the .CPP file.
*
*   \author CM
*   \version 1.0.$Rev: 1483 $
*   \date    JUN-2008
*/
class SignatureLog: public WaxSingletonTemplate<SignatureLog>, WAXControlableBase
{
	friend class WaxSingletonTemplate<SignatureLog>;

public:

	//! Method searchEntryBySignature() - searches the input param signature in the signature index(dictionary).
	WAX_RESULT searchEntryBySignature (
		/*__in*/	SignatureType inSignature,			//!< signature to be searched for
		/*__out*/	SignatureLogEntry* &outSigLogEntry	//!< Signature Log Entry if the signature was found in the index
		);

	///////////////////////////////////////////////////////////////////////////
	//////	Control and Management methods		///////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	//! Method start()
	WAX_RESULT start(
		/*__in*/ bool inAsEncoder,			//!< Role of current server.
		/*__in*/ unsigned long inulMaxChunksInTrLog	//!< Number of signature log entries.
		); 

	//! Method stop() 
	WAX_RESULT stop(void);

	///////////////////////////////////////////////////////////////////////////
	//////	Operations methods		///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	//! Method writeSignatures() - writes the signatures presented as ChunkCharacteristic to the signature log
	void writeSignatures ( 
		/*__in*/ vector <ChunkCharacteristic> &inChunkCharacteristics	//!< collection of characteristics for the 
																	//new signatures to be written
		);

	///////////////////////////////////////////////////////////////////////////
	//////	Getters		///////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	//!Returns the maximum number of signatures in the signature log
	unsigned long getSigLogMaxSize(void)	{ return m_ulSigLogMaxCount; }

	//!Returns the current position in the signature log
	unsigned long getSigLogCurPosition(void){ return m_ulSigLogCurrent; }

	//!This geter is used for testing purposes only.
	SignatureLogEntry* getSignatureLogArray(void){ return m_sigLog; }

	//!Returns the number of entries in the signature log index(dictionary)
	unsigned long getIndexSize (void)		{ return (unsigned long)m_Index->size(); }

	//!Returns the offset of the input SignatureLog Entry in the Signature Log Array
	UINT_PTR getSigLogEntryPosition( 
		/*__in*/  SignatureLogEntry* inSigLogEntry //!< The signature log entry for which the position is computed.
		) 
	{ 
		return ((UINT_PTR)inSigLogEntry - (UINT_PTR)m_sigLog)/sizeof( SignatureLogEntry );
	}

	//!	Queries the WAX component for statistic data.
	WAX_RESULT GetStatistics(
		/*__inout*/	Statistics *inoutStats	//!< Output statistic structure.
		);


private:
	static WAX_ID m_ComponentID;

	SignatureLog(void);
	~SignatureLog(void);

	///////////////////////////////////////////////////////////////////////////
	//////	Internal private members              /////////////////////////////
	///////////////////////////////////////////////////////////////////////////
	
	//!	Members for signatures storage, position in collection and collection size
	SignatureLogEntry* m_sigLog;	//Storage for signatures
	unsigned long m_ulSigLogCurrent;		//current position in signature collection 
	unsigned long m_ulSigLogMaxCount;		//maximum number of signatures
	unsigned long m_ulSigLogSizeBytes;		//number of bytes allocated in signature log

	MemoryPool* m_MemoryPool; //!< memory pool is used to request memory for the SignatureLog array

	//! Signature Log Index(dictionary) - is a collection of <signature, SignatureLogEntrey> pairs 
	//	All signatures in this collection are unique.
	HashMap* m_Index;

	//! Index memory buffer and buffer length.
	//	The memory buffer must be big enough to hold all index entries + internal bookkeeping 
	//	structures. The computation of the needed length and the memory allocation is performed 
	//	by the MemoryPool component.
	unsigned char* m_lpbMemoryBufferForIndex;	//! Memory buffer for the index. 
	unsigned long m_ulIndexSizeBytes;			//! Number of bytes allocated for the index.

	//! Method RemoveFromIndex() - removes the requested <Signature, SignatureLogEntry> pair from
	//	the index(dictionary)
	void RemoveFromIndex( 
		/*__in*/ SignatureLogEntry* inSigLogEntry //! Signature log entry to be removed from the index.
		);

	//! Method UpdateIndexEntry() - updates the SignatureLogEntry for a Signature.
	//	If the signature is not found in the index a <Signature, SignatureLogEntry> pair will be 
	//	added to the index(dictionary).
	void UpdateIndexEntry( 
		/*__in*/ SignatureLogEntry* inSigLogEntry //! Signature log entry to be updated in the index.
		);

};
