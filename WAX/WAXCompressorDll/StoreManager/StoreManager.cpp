//********************************************************/
//  ./StoreManager/StoreManager.cpp
//
//	Company		: Neverfail 
//	PROJECT		: WAN Acceleration 
//	Component	: Store Manager
//  Owner		: Ciprian Maris
//
//  $Rev: 4862 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: KMillar $  
//  $Date: 2010-09-01 14:26:15 +0100 (Wed, 01 Sep 2010) $
//
//********************************************************/

#include "windows.h"

#include "Shlwapi.h"

#include "StoreManager.h"
#include "../AllUtils.h"
#include "../ConfigMan/ConfigMan.h"
#include "../Logger/Logger.h"

#define WAX_TRANSFER_LOG_FILE_NAME_DEF L"NFWAXTranferLog.bin"


WAX_ID StoreManager::m_ComponentID = (WAX_ID)WAX_ID_STOREMAN;

StoreManager::StoreManager(void)
{
	//Handle for the Transfer log file. Only used on target
	m_hFileHandle = NULL;
	
	//in memory transfer log cache on target
	//in memory part of the transfer log on source
	m_lpbInMem = NULL;

	//signature log member
	m_signatureLog = SignatureLog::getInstance();

	//memory pool member
	m_MemoryPool   = MemoryPool::getInstance();

	//config man member
	m_ConfigMan = ConfigMan::getInstance();

	//Transfer log file name.
	m_strTLogFileName = L"";
}
//-----------------------------------------------------------------------------

StoreManager::~StoreManager(void)
{
	release();//clean up
}
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
//////	Control and Management methods		///////////////////////////////////
///////////////////////////////////////////////////////////////////////////////


//! Method openTransferLogFile() - Creates the directory structure if needed and pens the transfer log file.
/*!
*	The method attempts to open the file for the transfer log. 
*	there are two operation modes implemented for opening the file:
*		- Method 1 - the input path is a the complete file name path to the transfer log. In this case the 
*		file identified by inszTransferLogPath will be opened and its handle returned.
*
*		- Method 2 - the input path is a directory path to the transfer log. In this case the 
*		file identified by "inszTransferLogPath + WAX_DEFAULT_TRANSFER_LOG_FILE_NAME" will be opened 
*		and its handle returned.
*
*	In both cases, it the directory structure does not exists, the method attempts to create it.
*
*	Return: In case of success return the handle to the transfer log file. In the case of failure it returns 
*			an invalid file handle.
*
*	Caller: StoreManager::allocate().
*/
HANDLE StoreManager::openTransferLogFile( 
	/*__in*/ const wchar_t * inszTransferLogPath //!< complete path of the transfer log (path + file name or path)
	) 
{
	//Input parameter check.
	if( inszTransferLogPath == NULL )
	{
		LogError( m_ComponentID, DBG_LOCATION, L"Input transfer log path is NULL." );
		return NULL;
	}

	HANDLE hTrLogFileHandle = NULL;

	//Initialize the destination file name with the input path.
	//If this is a directory structure, the file name will be appended later.
	wstring strFilePath = inszTransferLogPath;

	//	Extract the directory structure from the path string.

	//	Directory structure (Path) to the transfer log file.
	wchar_t* szDirectoryStructure = NULL;
	bool bAllocatedLocally = false;

	wchar_t* szExtension = (wchar_t*)PathFindExtension( inszTransferLogPath );//get the extension, if any.
	if( (wchar_t)(szExtension[0]) == (wchar_t)(L'.') )
	{//The current path is a file name => we need to extract the dir structure from it.
		szDirectoryStructure = (wchar_t*)malloc( ( strFilePath.length() + 1 ) * sizeof(wchar_t) );
		bAllocatedLocally = true;
		
		if( szDirectoryStructure == NULL )
		{
			LogError( m_ComponentID, DBG_LOCATION, L"Can not allocate buffer for directory structure." );
			return NULL;
		}

		UtilSafeCopyString( szDirectoryStructure, strFilePath.c_str() ); 
		PathRemoveFileSpec( szDirectoryStructure );
	}
	else
	{//We already have a directory structure (no file extension).
		szDirectoryStructure = (wchar_t*)inszTransferLogPath;
	}

	//  Create the on disk directory structure.
	if( !NFCreateDirectoryStructure( szDirectoryStructure ) )
	{
		LogError( m_ComponentID, DBG_LOCATION, L"Can not create transfer log path directory structure." );
		return NULL;
	}

	//If we reach this point, the directory structure to the transfer log path is created.
	//We need to check if the input parameter does not contains the file spec. In this case we append the 
	//default file spec to it.

	if( PathIsDirectory( inszTransferLogPath ) )
	{//we create the tensfer log file name as DirectoryStructure + WAX_TRANSFER_LOG_FILE_NAME_DEF 
		strFilePath = inszTransferLogPath; //szDirectoryStructure;

		//Avoid puting two back-slashes
		if( strFilePath.c_str()[ strFilePath.length() - 1 ] != L'\\' )
		{
			strFilePath.append( L"\\" ); 
		}

		strFilePath.append( WAX_TRANSFER_LOG_FILE_NAME_DEF );
	}

	//	Open and check transfer log file
	// BZ 7866 - open the transfer log file with DELETE_ON_CLOSE flag
	hTrLogFileHandle = NFOpenFileMax( strFilePath.c_str(), CREATE_ALWAYS, TRUE, 0, TRUE );

	//	Set the transfer log file name if the handle is valid
	if( NFIsValidFileHandle( hTrLogFileHandle ) )
	{
		m_strTLogFileName = strFilePath;
	}

	if( bAllocatedLocally )
	{
		free( szDirectoryStructure );
		szDirectoryStructure = NULL;
	}

	return hTrLogFileHandle;
}


//! Method allocate() - Pre-allocates disk space
/*!
*	The method attempts to allocate disk for the Transfer Log.
*
*	Return: In case of success return WAX_RESULT_OK. In the case of failure it returns the 
*			failure reason.
*
*	Caller: WAXInterface::allocate().
*/
WAX_RESULT StoreManager::allocate(
	/*__in*/ unsigned long inTransferLogSize,				//!< transfer log size in MB's
	/*__in*/ const StringBuffer *inTransferLogPath	//!< complete path of the transfer log (path + file name or path)
	)
{
	WAX_RESULT retVal = WAX_RESULT_OK;
	
	//	Let openTransferLogFile() to create the directory structure and open transfer log file.
	m_hFileHandle = openTransferLogFile( inTransferLogPath->buffer );

	if( NFIsValidFileHandle( m_hFileHandle ) == FALSE )
	{
		// LOG Error/Set Status
		LogError( m_ComponentID, DBG_LOCATION, L"NFOpenFileMax() returned: %u", m_hFileHandle  );
		retVal = WAX_RESULT_CAN_NOT_OPEN_FILE;
		goto ERREXIT;
	}

	//	Pre-allocate disk space
	//	NOTE: we only set the file size here (setFilePointer + setEOF)
	//	the start method will move the file pointer to the beginning of the file
	//  This value will be updated by start() to be a multiple of SD
	m_llTLogSize = (long long)((long long)inTransferLogSize * (long long)MEGA);

	//	Set the file size 
	//  This operation will be done by start() to match the new size as a multiple of SD
	if( !NFResizeFile( m_hFileHandle, m_llTLogSize, FALSE ) )
	{
		// LOG Error/Set Status
		retVal = WAX_RESULT_CAN_NOT_SET_FILE_SIZE;
		LogError( m_ComponentID, DBG_LOCATION, L"NFResizeFile() returned: %u", retVal  );
		goto ERREXIT;
	}

	//set disk size to the Configuration Manager
	m_ConfigMan->SetTLogSize(m_llTLogSize);

	//update flags
	WAXControlableBase::allocate();

	return WAX_RESULT_OK;

ERREXIT://Error exit
	if( NFIsValidFileHandle( m_hFileHandle ) ) CloseHandle( m_hFileHandle );
	return retVal;
}
//-----------------------------------------------------------------------------

//! Method release() - releases pre-allocated disk space.
/*!
*	Caller: WAXInterface::release().
*/
WAX_RESULT StoreManager::release(void)
{
	WAX_RESULT retVal = WAX_RESULT_UNKNOWN_ERROR;

	if( !m_isAllocated )//check done so we do not return an error
	{
		return WAX_RESULT_OK;
	}

	//	Close transfer log file
	if( NFIsValidFileHandle( m_hFileHandle ) )
	{
		CloseHandle( m_hFileHandle );
		m_hFileHandle = NULL;
		m_strTLogFileName = L"";
	}

	//	NOTE:	Since the memory was requested from the memory pool in the start()
	//			method, it will be released in the stop() method.
	stop(); //we call stop to make sure we do not leak.

	//set the allocated flag
	WAXControlableBase::release();

	return WAX_RESULT_OK;
}
//-----------------------------------------------------------------------------

//! Method start() - Starts the StoreManager with the role given by inAsEncoder.
/*!
*	The method
*		Starts the StoreManager with the role given by inAsEncoder.
*		Retrieves the memory pre-allocated for the in memory store manager from the memory pool.
*		Initializes all internal parameters (offset management, file and in memory pointer positions).
*		Calls the start method of the Signature Log: m_signatureLog->start( inAsEncoder, (ULONG)m_llMaxChunksInTrLog );
*
*	Return: In case of success return WAX_RESULT_OK. In the case of failure it returns the failure reason.
*
*	Caller: WAXInterface::start().
*/
WAX_RESULT StoreManager::start(
	/*__in*/ bool inAsEncoder 
	)
{
	WAX_RESULT retVal = WAX_RESULT_UNKNOWN_ERROR;
	
	// Enhancement Bug 7885 -  Optimal usage of resources.
	// BZ 8119 - Make the transfer log size a multiple of SD => If the currenlty allocated Tlog size
	// is biger than MaxMemUsed by at least 2 SD's the transfer log size will be the shrinked to 
	// [(MaxMemUsed / SD) + 0 ] * SD
	long long llMaxMemUsed = (long long)( m_ConfigMan->GetMaxMemUsed() )*(MEGA);
	long long llSD = (long long)m_ConfigMan->GetSignatureDistance();
	long long llNewTlogSize = m_llTLogSize;

	if( ( false == m_ConfigMan->GetIsAdvancedDeduplicationEnabled() ) &&
		( NFIsValidFileHandle( m_hFileHandle ) == TRUE ) )
	{
		if( m_llTLogSize > (llMaxMemUsed + 2 * llSD ) )
		{
			llNewTlogSize = ( (llMaxMemUsed / llSD) + 0 ) * llSD;

			if( !NFResizeFile( m_hFileHandle, llNewTlogSize, TRUE, TRUE ) )
			{
				LogError(
					m_ComponentID, 
					DBG_LOCATION, 
					L"Resize Transfer Log failed. NFResizeFile() returned: %u",
					WAX_RESULT_CAN_NOT_SET_FILE_SIZE
					);

				// carry on ... we cannot resize 
			}
			else
			{
				m_llTLogSize = llNewTlogSize;
				m_ConfigMan->SetTLogSize( m_llTLogSize );

				LogEventFormated(
					LOG_LEVEL_INFO,
					m_ComponentID,
					L"Resized Transfer Log to %I64d.",
					m_llTLogSize 
					);
			}
		}
	}
		
	//Get The memory size
	//forceRecompute
	m_uipInMemsize = m_MemoryPool->getComponentMemorySize( WAX_ID_STOREMAN );

	if( m_uipInMemsize == 0 )
	{
		// LOG Error/Set Status
		LogError( m_ComponentID, DBG_LOCATION, L"Memory size for StoreManager is Zero. m_MemoryPool->getComponentMemorySize() returned 0.");
		retVal = WAX_RESULT_CAN_NOT_ALLOCATE_MEMORY;
		goto ERREXIT;
	}
	
	//Make the in memory size be a multiple of signature distance
	m_llMaxChunksInMem = (m_uipInMemsize / m_ConfigMan->GetSignatureDistance());	//number of chunks in memory
	m_uipInMemsize = (UINT_PTR)m_llMaxChunksInMem * m_ConfigMan->GetSignatureDistance();

	//Retrieve the actual memory buffer
	m_lpbInMem = m_MemoryPool->gimme( (UINT)m_uipInMemsize, WAX_ID_STOREMAN );
	if( NULL == m_lpbInMem )
	{
		// LOG Error/Set Status
		LogError( m_ComponentID, DBG_LOCATION, L"Memory allocation for StoreManager failed. m_MemoryPool->gimme() returned NULL.");
		retVal = WAX_RESULT_CAN_NOT_ALLOCATE_MEMORY;
		goto ERREXIT;
	}

	//	Make the in transfer log size be a multiple of signature distance (SD = chunkSize)
	//  This takes the value allocated in allocate() and makes it a multiple of SD
	m_llMaxChunksInTrLog = (m_llTLogSize / m_ConfigMan->GetSignatureDistance());//number of chunks in the entire transfer log
	m_llTLogSize =  m_llMaxChunksInTrLog * m_ConfigMan->GetSignatureDistance();

	//	Set the new TLog Size - as a multiple of SD
	if( !NFResizeFile( m_hFileHandle, m_llTLogSize, TRUE, TRUE ) )
	{
		// LOG Error/Set Status
		LogError( m_ComponentID, DBG_LOCATION, L"NFResizeFile() failed.");
		retVal = WAX_RESULT_CAN_NOT_SET_FILE_SIZE;
		goto ERREXIT;
	}

	//	Mapping of in memory transfer log to File Transfer Log - updated on each write
	m_llFileOffsetOfInMemStart = 0;					// start offset 
	m_llFileOffsetOfInMemEnd   = m_uipInMemsize;	// end offset
	m_bUpdateFileToMemoryMapping = false;			// set to false for the first m_uipInMemsize writes

	m_llFilePosition = 0;	//file position relative to file start

	m_uipMemPosition = 0;	//in memory position relative to start

	//Offset management initialization
	m_ulRollTLog	= 0;//nr of roll overs in transfer log
	
	//diff = X % x
	m_ulRollDiff  = (unsigned long) (m_llTLogSize % m_uipInMemsize) ;

	//start the Signature Log
	retVal = m_signatureLog->start( inAsEncoder, (unsigned long)m_llMaxChunksInTrLog );
	if( retVal != WAX_RESULT_OK )
	{
		// LOG Error/Set Status
		LogError( m_ComponentID, DBG_LOCATION, L"Failed to start SignatureLog. m_signatureLog->start() returned: %u ", retVal );
		goto ERREXIT;
	}

	//Success exit - set flags and return success
	WAXControlableBase::start( inAsEncoder );

	return WAX_RESULT_OK;

ERREXIT:
	return retVal;
}
//-----------------------------------------------------------------------------

//! Method stop() - Stops the StoreManager.
/*!
*	The method Stops the StoreManager.
*
*	Return: WAX_RESULT_OK
*
*	Caller: WAXInterface::stop().
*/
WAX_RESULT StoreManager::stop(void)
{
	if( !m_isStarted )
	{
		return WAX_RESULT_OK;
	}

	//	NOTE:	For symetry, since the SignatureLog was started by StoreManager,
	//			it will also be stopped by the store manager.
	m_signatureLog->stop();

	//	NOTE:	The memory pool can now delete the buffers allocated for the store manager 
	//			and signature log at any time without notice.

	//Set flags as stopped and return success
	WAXControlableBase::stop( );
	return WAX_RESULT_OK;
}
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
//////	END Control and Management methods		///////////////////////////////
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////
//////	Operations methods		///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

//! Write Buffer - writes a data buffer to the transfer log and calls SignatureLog to update the signatures for the buffer.
/*! 
*	The Method
*	o	Writes the buffer to the transfer log (memory and disk).
*	o	Writes each signature-checksum pair to the Signature Log. 
*
*	The Encoder and Decoder call the it for writing a buffer to Transfer Log.
*	
*	On target the parameter inChunkCharacteristics is ignored.
*
*	Caller : Encoder
*   Uses : SignatureLog
*/	
WAX_RESULT StoreManager::writeBuffer (
	/*__in*/ const ByteBuffer *inbBuffer,		//!< byte buffer to be added to the transfer log
	/*__in*/ vector <ChunkCharacteristic> &inChunkCharacteristics	//!< Collection of characteristics 
																// for the new signature to be written.
	)
{
	WAX_RESULT retVal = WAX_RESULT_UNKNOWN_ERROR;

	//***	Parameter validation
	if( isValidByteBuffer( inbBuffer ) == FALSE )
	{
		// LOG Error/Set Status
		LogError( m_ComponentID, DBG_LOCATION, L"Invalid byte Buffer.");
		retVal = WAX_RESULT_INBUFF_NULL;
		goto ERREXIT;
	}

	//Write the buffer to disk and memory
	retVal = writeBufferToDisk( inbBuffer );
	if( retVal != WAX_RESULT_OK )
	{
		// LOG Error/Status already set
		LogError( m_ComponentID, DBG_LOCATION, L"Failed to write to disk. writeBufferToDisk() returned: %u", retVal);
		goto ERREXIT;
	}

	retVal = writeBufferToMem ( inbBuffer );
	if( retVal != WAX_RESULT_OK )
	{
		// LOG Error/Status already set
		LogError( m_ComponentID, DBG_LOCATION, L"Failed to write to memory. writeBufferToMem() returned: %u", retVal);
		goto ERREXIT;
	}

	//*** Only the Encoder has to update the signature log...write characteristics
	//NOTE: the decoder ignores the chunk characteristic parameter of this function
	if( m_isEncoder )
	{
		m_signatureLog->writeSignatures( inChunkCharacteristics );
	}

	return WAX_RESULT_OK;

ERREXIT://Error exit
	return retVal;

}
//-----------------------------------------------------------------------------

//!Method writeBufferToDisk() writes a buffer to the disk.
/*!	
*	The method 
*		Computes the position of the file pointer:
*			on source - the position is only virtual (there is no actual writing to disk.
*						only mappings and positions are updated).
*			on target - the position is also the actual position of the file pointer,
*
*		Writes the buffer to the on disk transfer log on target.
*		Updates the mapping of the in memory transfer log to the one on disk.
*
*	NOTE:	The buffer can hold any number of bytes (including a number less than SignatureDistance.
*	
*	Return: WAX_RESULT_OK if the method succedes. If the method fails it will return WAX_RESULT_CAN_NOT_WRITE_TO_FILE.
*
*	Caller: writeBuffer()
*
*	\sa writeBuffer()
*/
WAX_RESULT StoreManager::writeBufferToDisk( 
	/*__in*/ const ByteBuffer *inbBuffer //!< Input buffer to be written to disk.
	)
{
	WAX_RESULT retVal = WAX_RESULT_OK;

	//***	Do the actual writing to disk
	//Compute if the current buffer can be written to the file in one shot
	long long	llFromCurrent = 0,	//how many bytes can we write from the current position
				llFromStart = 0;	//how many bytes must we write from start of the log

	getWriteLimits( m_llFilePosition, m_llTLogSize, inbBuffer->length, llFromCurrent, llFromStart );

	//***	Write to current position and start if necessary
	if( llFromCurrent > 0 )
	{//Write to current position
		// If we only write from the current position, the file pointer will be updated accordingly
		if( m_isEncoder == FALSE )//do the actual write only on target
		{
			// Only do the write for advanced deduplication
			if(m_ConfigMan->GetIsAdvancedDeduplicationEnabled() == TRUE)
			{
				//Set file pointer here if it is not in the right position.
				//	This was moved here from the method getBufferByTLogOffset() for perfromance resons on 
				//	the target server.
				if( !NFSetFilePointer( m_hFileHandle, m_llFilePosition, FILE_BEGIN ) )
				{
					LogError( m_ComponentID, DBG_LOCATION, L"NFSetFilePointer() failed. ");
					retVal = WAX_RESULT_CAN_NOT_SET_FILE_POINTER;
					goto ERREXIT;
				}

				if( ! NFWriteDataToFile( m_hFileHandle, (unsigned long)llFromCurrent,  inbBuffer->buffer ) )
				{
					// LOG Error/Set Status
					LogError( m_ComponentID, DBG_LOCATION, L"Failed to write to file. NFWriteDataToFile() returned: FALSE.");
					retVal = WAX_RESULT_CAN_NOT_WRITE_TO_FILE;
					goto ERREXIT;
				}
			}
		}

		m_llFilePosition = m_llFilePosition + llFromCurrent;//update position in file

		if( m_llFilePosition > m_llFileOffsetOfInMemEnd )//only move if current write excedes inital mem size
		{
			//	This flag is set the first time when m_llFilePosition reaches the in memory TLog size
			//  From now on, the m_llFileOffsetOfInMemEnd will be updated to m_llFilePosition
			//	and m_llFileOffsetOfInMemStart will stay at m_uipInMemsize distance from m_llFileOffsetOfInMemEnd
			m_bUpdateFileToMemoryMapping = true;
		}

		if( m_bUpdateFileToMemoryMapping )
		{
			//***	Update on each write - Mapping of in memory transfer log to File Transfer Log  
			//	in this case we have a normal write: from the current position onwards
			//	The in mem position of end can only grow
			m_llFileOffsetOfInMemEnd = m_llFilePosition;

			//  The start position follows the end position at m_uipInMemsize even for a roll over
			long long llMemEndPositionMinusMemSize = m_llFileOffsetOfInMemEnd - (long long)m_uipInMemsize;//optimisation: do subtract only once
			if( llMemEndPositionMinusMemSize >= 0 )
			{
				//	CASE when after the update:
				//	[........MemStart..............MemEnd...........]
				//  0            |<--- InMemSize --->|           TLogSize
				m_llFileOffsetOfInMemStart = llMemEndPositionMinusMemSize; 
			}
			else
			{
				//	CASE when after the update:
				//	[.............MemEnd..............MemStart............]
				//  |<- InMemSize1 ->|                    | <- MemSize2 ->|
				//  InMemSize = InMemSize1 + InMemSize2
				// is expressed as: TLogsize - Memsize + EemEnd:
				m_llFileOffsetOfInMemStart = m_llTLogSize + llMemEndPositionMinusMemSize;

			}
		}
	}

	if( llFromStart > 0 )
	{//Write to start position

		// If we also write from the start of file the file pointer will be updated accordingly
		if( FALSE == m_isEncoder )//do the actual write only on target
		{
			// Only do the write for advanced deduplication
			if(m_ConfigMan->GetIsAdvancedDeduplicationEnabled() == TRUE)
			{
				NFSetFilePointer( m_hFileHandle, 0, FILE_BEGIN );

				if( ! NFWriteDataToFile( m_hFileHandle, (unsigned long)llFromStart,  (unsigned char*)(inbBuffer->buffer + (size_t)llFromCurrent) ) )
				{
					// LOG Error/Set Status
					LogError( m_ComponentID, DBG_LOCATION, L"Failed to write to file. NFWriteDataToFile() returned: FALSE.");
					retVal = WAX_RESULT_CAN_NOT_WRITE_TO_FILE;
					goto ERREXIT;
				}
			}
		}

		m_llFilePosition = llFromStart;//update position in file

		//***	Update on each write - Mapping of in memory transfer log to File Transfer Log  
		//	in this case for each write we update the position of the in memory mapping
		m_llFileOffsetOfInMemEnd	= m_llFilePosition;

		//CPI - BUG: m_llFileOffsetOfInMemStart computation
		//m_llFileOffsetOfInMemStart = m_llFileOffsetOfInMemStart + llFromStart;
		m_llFileOffsetOfInMemStart	= m_llTLogSize - (long long)m_uipInMemsize + m_llFileOffsetOfInMemEnd;

		//roll over - if we ever write from the start of the TLog - we have a roll over
		m_ulRollTLog++;
	}

	return WAX_RESULT_OK;

ERREXIT://Error exit
	return retVal;
}
//-----------------------------------------------------------------------------

//!Method writeBufferToMem() writes a buffer to memory.
/*! 
*	The method
*		Writes the input buffer to the in memory transfer log.
*		Updates the in memory position m_uipMemPosition for the next buffer to be written
*
*	Return: WAX_RESULT_OK if the method succedes. If the method fails it will return WAX_RESULT_CAN_NOT_WRITE_TO_MEM.
*
*	Caller: writeBuffer().
*
*	NOTE: The processing on source and target server is the same.
*
*	\sa writeBuffer()
*/
WAX_RESULT StoreManager::writeBufferToMem ( 
	/*__in*/ const ByteBuffer *inbBuffer //!< Input buffer to be written to memory.
	)
{
	WAX_RESULT retVal = WAX_RESULT_OK;

	//***	Do the actual writing to mem
	//Compute if the current buffer can be written in one shot
	long long llFromCurrent = 0, llFromStart = 0;
	getWriteLimits( m_uipMemPosition, m_uipInMemsize, inbBuffer->length, llFromCurrent, llFromStart );

	//***	Write to current position and start if necessary
	if( llFromCurrent > 0 )
	{//Write to current position
		// If we only write from the current position, the memory position will be updated accordingly
		if( ! NFWriteDataToMem( (unsigned char*)( m_lpbInMem + m_uipMemPosition ), (size_t)llFromCurrent, inbBuffer->buffer ) )
		{
			// LOG Error/Set Status
			LogError( m_ComponentID, DBG_LOCATION, L"Failed to write to memory. NFWriteDataToMem() returned: FALSE.");
			retVal = WAX_RESULT_CAN_NOT_WRITE_TO_MEM;
			goto ERREXIT;
		}

		m_uipMemPosition = m_uipMemPosition + (UINT_PTR)llFromCurrent;//update position in mem
	}

	if( llFromStart > 0 )
	{//Write to start of in mem TLog position

		if( !NFWriteDataToMem( m_lpbInMem, (size_t)llFromStart, (unsigned char*)(inbBuffer->buffer + llFromCurrent) ) )
		{
			// LOG Error/Set Status
			LogError( m_ComponentID, DBG_LOCATION, L"Failed to write to memory. NFWriteDataToMem() returned: FALSE.");
			retVal = WAX_RESULT_CAN_NOT_WRITE_TO_MEM;
			goto ERREXIT;
		}

		m_uipMemPosition = (UINT_PTR)llFromStart;//update position in mem
	}

	return WAX_RESULT_OK;

ERREXIT://Error exit
	return retVal;
}
//-----------------------------------------------------------------------------

//! Method getWriteLimits() 
/*!	The method computes the maximum length that can be written to the destination (memory or disk).
*		The characteristics of the destination are:
*			- inllCurrentPosition - current position/offset
*			- inllMaxPosition - size(or maximum position/offset)  
*
*	Output parameters (Return values): 
*		outllLenFromCurrentPosition - number of bytes that can be written from the current position.
*		outllFromStart - number of bytes that will be written from the start.
*
*	Caller: writeBufferToDisk(), writeBufferToMem(), getBufferDecoder()
*/
void StoreManager::getWriteLimits( 
	/*__in*/	long long inllCurrentPosition,			//!< current position/offset in destination
	/*__in*/	long long inllMaxPosition,				//!< maximum position/offset in destination
	/*__in*/	long long inllLength,					//!< length of the requested write
	/*__out*/	long long &outllLenFromCurrentPosition,	//!< number of bytes that can be written from the current position
	/*__out*/	long long &outllFromStart				//!< number of bytes that will be written from the start
	)
{
	if( ( inllCurrentPosition + inllLength ) < inllMaxPosition )
	{
		outllLenFromCurrentPosition = inllLength;
		outllFromStart = 0;
		return;
	}
	else
	{
		outllLenFromCurrentPosition = inllMaxPosition - inllCurrentPosition;
		outllFromStart = inllLength - outllLenFromCurrentPosition;
	}
}
//-----------------------------------------------------------------------------

//!Method getMemoryOffsetOfOnDiskOffset -	Computes the in memory offset of an offset in the Transfer Log
/*!
*	The method
*		Computes the in memory offset of an offset in the Transfer Log.
*		Uses the Offset management members for the operation.
*		For the following notations:
*			p - in memory offset
*			P - on disk offset
*			N - number of times the on disk transfer log rolled over: m_ulRollTLog; //!< Counter of Transfer log roll overs
*			x - size of the in memory transfer log
*			X - size of the on disk transfer log
*			C - correction factor needed after a roll over for the in memory offsets that are located as follows
*					|.....MemStop......MemStart....Offset.....|
*
*			diff - m_ulRollDiff; //!< TLogSize denoted as X, in memory TLog size denoted as x => m_ulRollDiff = X % x
*
*		The formula for computing the in memory offset is:
*			p = [ P + (N - C) * diff ] % x;
*
*		Return: The in memory offset of the buffer found at offset inllFileOffset in the file.
*
*		NOTE:	
*		[1]	The method assumes the offset is in memory. The caller has to call isTLogOffsetInMemory()
*			to make sure the parameter offset passed to this method is in memory.
*
*		[2]
*			The input parameter inllFileOffset will always be a valid one. 
*			We will never ask for offsets that were not written.
*
*		Caller: getBufferEncoder(), getBufferDecoder()
*
*		\sa getBufferEncoder(), getBufferDecoder(), isOnDiskOffsetInMemory()
*/

long long StoreManager::getMemoryOffsetOfOnDiskOffset(
	/*__in*/ long long inllFileOffset,		//!< on disk offset for which we want to know if it is in meory
	/*__in*/ LONG inlRollCorrectionFactor	//!< correction factor having values 0 or 1. 
										//\sa See also isOnDiskOffsetInMemory() NOTE for details on this param.
	)
{
	long long retValOffset;

	// p = [ P + (N - C) * diff ] % x;
	retValOffset = (inllFileOffset + 
					( (long long)m_ulRollTLog - (long long)inlRollCorrectionFactor) * (long long)m_ulRollDiff) 
					% (long long)m_uipInMemsize;

	return retValOffset;
}
//-----------------------------------------------------------------------------

//!Method isOnDiskOffsetInMemory - checks if an offset in the transfer log is also currently located in memory.
/*!
*	The method
*		Check is the inllFileOffset parameter offset relative to the on disk transfer log is currently located 
*		within the boundatries memory.
*
*	Return: WAX_INVALID_MEM_OFFSET if the offset is not in memory, WAX_RESULT_OK if the offset is in memory.
*
*	NOTE: on outlRollCorrectionFactor
*	For the correct computation of the in memory offset we need to address the following case:
*		 |.....MemStop......MemStart....Offset.....|
*		 For this particular, if we are currently wrapping the in memory portion of the tlog over the on disk 
*		 portion, the inMemory offsets of the transfer log that were written before the wrap need to be computed 
*		 with the formula valid at the time of their writing.
*		 So whenever the on disk offset relative to the mapping of the memory to file is located as in the drawing, 
*		 the method will set the outlRollCorrectionFactor parameter to 1.
*
*	Caller: getBufferBySigLogEntry(), getBufferByTLogOffset()
*
*/
BOOL StoreManager::isOnDiskOffsetInMemory( 
	/*__in*/	long long inllFileOffset,		//!< on disk offset for which we want to know if it is in meory
	/*__out*/	LONG &outlRollCorrectionFactor  //!< correction factor having values 0 or 1. see NOTE above for details
	)
{
	BOOL isInMemory = FALSE;

	//Case 1: End > Start
	if( m_llFileOffsetOfInMemEnd > m_llFileOffsetOfInMemStart )
	{
		if( ( inllFileOffset >= m_llFileOffsetOfInMemStart ) && ( inllFileOffset < m_llFileOffsetOfInMemEnd ))
		{
			//compute in memory offset
			//CASE: |.....MemStart...Offset...MemStop.........|
			//      0                                      TlogSize
			isInMemory = TRUE;
		}
	}
	else
	{
		if( ( inllFileOffset >= m_llFileOffsetOfInMemEnd ) || ( inllFileOffset <= m_llFileOffsetOfInMemStart ))
		{
			//compute in memory offset
			if( inllFileOffset >= m_llFileOffsetOfInMemStart )
			{
			//CASE: |.....MemStop......MemStart....Offset.....|
				isInMemory = TRUE;

				if ( m_ulRollTLog > 0 )
				{
					outlRollCorrectionFactor = 1;
				}
			}

			if( inllFileOffset < m_llFileOffsetOfInMemEnd )//fix error of returning true for this if branch
			{
			//CASE: |...Offset.....MemStop......MemStart......|
				isInMemory = TRUE;
			}
		}
	}

	return isInMemory;

}
//-----------------------------------------------------------------------------

//!	Method getBufferBySigLogEntry() - retreives a buffer from the in memory StoreManager on source.
/**!
*	The Encoder calls it for retrieving a buffer from Transfer Log.
*
*	Return: WAX_RESULT_OFFSET_NOT_IN_MEM if the buffer refered by inSigLogEntry is not in memory.
*			WAX_RESULT_OK othwerwise.
*
*	Caller: Encoder
*
*	\sa Encoder class
*/
WAX_RESULT StoreManager::getBufferBySigLogEntry(  
	/*__in*/	SignatureLogEntry* inSigLogEntry,	//!< Signature Log Entry for which we want the buffer.
	/*__inout*/ unsigned long& inoutMaxLeftLength,			//!< Maximum distance to the left that can be used for byte by byte comparison
	/*__inout*/ unsigned long& inoutMaxRightLength,			//!< Maximum distance to the right that can be used for byte by byte comparison
	/*__out*/	long long& outTrLogOffset,			//!< Offset in transfer log (relative to on disk file)
	/*__out*/	unsigned char* &outBufferPointer			//!< Pointer to the data buffer if the data corresponding to 
													// inSigLogEntry is in memory. 
	)
{
	//1. Compute the offset of the requested buffer
	//	CAUTION: this assignment has to me done prior to return!
	outTrLogOffset = (long long)m_signatureLog->getSigLogEntryPosition(inSigLogEntry) * (long long)m_ConfigMan->GetSignatureDistance();

	if( outTrLogOffset == m_llTLogSize )//we need to roll over
	{
		outTrLogOffset = 0;
	}

	//2. See if it is in memory
	LONG lRollCorrectionFactor = 0;
	if( !isOnDiskOffsetInMemory( outTrLogOffset, lRollCorrectionFactor ) )
	{
		//	CAUTION: make sure outTrLogOffset is assigned prior to this return!
		return WAX_RESULT_OFFSET_NOT_IN_MEM;
	}
	
	//3. If in memory -> compute the n memory offset value and the limits 
	
	//	3.1 compute the in memory offset value
	long long inMemOffset = 0;//of type LONGLONG so we do not overshoot
	inMemOffset = getMemoryOffsetOfOnDiskOffset( outTrLogOffset, lRollCorrectionFactor );	

	//	3.2 compute the limits
	//	We compute the limits for the current buffer relative to in memory Transfer Logs
	//	The Buffer adressed by [outBuffer - inoutMaxLeftLength, outBuffer + inoutmaxRightLength]
	//	can not excede the limits of in memory log and does not roll over
	//	This is done so, because a memory roll over would require a new token to be sent over

	//3.1 Compare against in memory limits
	//	Set maximum left
	inoutMaxLeftLength = (unsigned long)( (inMemOffset - (long long)inoutMaxLeftLength) > 0 )? 
								inoutMaxLeftLength : 0 ;

	//	Set maximum right
	inoutMaxRightLength = (unsigned long)( ((uint64_t)inMemOffset + inoutMaxRightLength) < m_uipInMemsize )? 
								inoutMaxRightLength : (unsigned long)(m_uipInMemsize - inMemOffset);

	// Make sure the min function with subtraction in it does not return a bigger value than the initial one
	unsigned long ulMaxRightLenReq = inoutMaxRightLength;

	if( (long long)m_uipMemPosition > inMemOffset )
	{
		inoutMaxRightLength = (unsigned long)min(inoutMaxRightLength, m_uipMemPosition - inMemOffset);
	}
	else
	{
		if( !m_bUpdateFileToMemoryMapping )//all memory filled once
		{
			inoutMaxRightLength = 0;
		}
	}
	inoutMaxRightLength = min( inoutMaxRightLength, ulMaxRightLenReq ); 

    //	3.2  Need to also check the max left and right against on disk mapping.
    //  In this perspective, the memory mapping to disk can not be exceeded for either
	//	start and end limits.
    if( (long long)outTrLogOffset >= m_llFileOffsetOfInMemStart )
    {//Check the lower limit m_llFileOffsetOfInMemStart
        if( ( (long long)outTrLogOffset - (long long)inoutMaxLeftLength ) < m_llFileOffsetOfInMemStart )
        {
            unsigned long ulMaxLL = (unsigned long)( (long long)outTrLogOffset - m_llFileOffsetOfInMemStart );
			inoutMaxLeftLength = min( inoutMaxLeftLength, ulMaxLL );//Also make sure this operation does not increase the requested length;
        }
    }

    if( m_llFileOffsetOfInMemEnd >= (long long)outTrLogOffset )
    {//Check the upper limit: m_llFileOffsetOfInMemEnd
        if( ( (long long)outTrLogOffset + (long long)inoutMaxRightLength ) > m_llFileOffsetOfInMemEnd )
        {
            unsigned long ulMaxRL = (unsigned long)( m_llFileOffsetOfInMemEnd - (long long)outTrLogOffset );
			inoutMaxRightLength = min( inoutMaxRightLength, ulMaxRL );
        }
    }

	//	3.3 Check limits and transfer log offset against on disk TLog limits (0 and TLog_size)

	//	(TLog_Offset - Max_Left) must be bigger thgan 0
	if( ((long long)outTrLogOffset - (long long)inoutMaxLeftLength) < 0 )
	{
		inoutMaxLeftLength = (unsigned long)outTrLogOffset;
	}

	//	(TLog_Offset + Max_Left) must NOT exceede TLogSize
	if( ( outTrLogOffset + (long long)inoutMaxRightLength ) > m_llTLogSize )
	{
		inoutMaxRightLength = (unsigned long)( m_llTLogSize - outTrLogOffset ) ;
	}

	//4. Assign the return pointer
	outBufferPointer = (unsigned char*)(m_lpbInMem + (UINT_PTR)inMemOffset);

	return WAX_RESULT_OK;
}
//-----------------------------------------------------------------------------


//!Method getBufferByTLogOffset() - retreives a buffer from the Transfer Log on target
/**!
*	The Decoder calls it for retrieving a buffer from Transfer Log.
*	The caller will pass in an allocated buffer as data destination.
*	The method will copy inulDataLength bytes located in the transfer log at offset inullTLOffset
*	to the outDestinationBuffer.
*
*	Return: In case of success return WAX_RESULT_OK. In the case of failure it returns the 
*			failure reason.
*
*	Caller: Tokenizer::DeTokenize()
*
*	\sa Tokenizer::DeTokenize()
*/
WAX_RESULT StoreManager::getBufferByTLogOffset (
	/*__in*/	long long inllTLOffset,			//!< Offset in transfer log (relative to file).
	/*__in*/	const unsigned long inulDataLength,		//!< Length of required buffer.
	/*__inout*/	unsigned char* inoutDestinationBuffer	//!< Destination of data to be written too.It is preallocated by the caller.
	)
{
	/**
		NOTE: The first WAX implementation holds the last m_uipInMemsize bytes of the TL in memory.
		It does not use a caching mechanism for data that is not in memory (is on disk).
		This results in a Disk IO operation for each request that is not in memory.
	*/

	WAX_RESULT retVal; 
	long long llFromCurrent = 0, llFromStart = 0;
	long long llInMemStart, llInMemStop;

	if( inllTLOffset > m_llTLogSize )
	{
		LogError( m_ComponentID, DBG_LOCATION, L"Input Transfer log Offset too big.");
		return WAX_RESULT_OFFSET_TO_BIG;
	}

	//	Roll over - for the case the decoder requests a TLog Offset equal to the TLog size
	//	this would mean that the actual requested offset is 0
	if( (long long)inllTLOffset == m_llTLogSize )
	{
		inllTLOffset = 0;//roll over
	}

	long long llReqBufferEnd = inllTLOffset + (long long)inulDataLength;
	LONG lRollCorrectionFactorStart = 0;
	LONG lRollCorrectionFactorEnd = 0;

	//***	The requested buffer is in memory only if both start and end are in memory
	if( isOnDiskOffsetInMemory( inllTLOffset, lRollCorrectionFactorStart ) && 
		isOnDiskOffsetInMemory( llReqBufferEnd, lRollCorrectionFactorEnd ) )
	{//fetch it from memory

		//***	Compute in memory start and stop offsets
		llInMemStart = getMemoryOffsetOfOnDiskOffset( inllTLOffset, lRollCorrectionFactorStart );
		llInMemStop  = getMemoryOffsetOfOnDiskOffset( llReqBufferEnd, lRollCorrectionFactorEnd  );
		
		//***	Read data from memory
		//check if it is contiguous, or we need two reads
		getWriteLimits( llInMemStart, m_uipInMemsize, inulDataLength, llFromCurrent, llFromStart );

		if( llFromCurrent > 0 )
		{//Read from input offset(position)
			if( ! NFWriteDataToMem( inoutDestinationBuffer, (size_t)llFromCurrent, m_lpbInMem + llInMemStart ) )
			{
				// LOG Error/Set Status
				LogError( m_ComponentID, DBG_LOCATION, L"Failed to write to memory. NFWriteDataToMem() returned: FALSE.");
				retVal = WAX_RESULT_CAN_NOT_READ_FROM_MEM;
				goto ERREXIT;
			}
		}

		if( llFromStart > 0 )
		{//Read from start of the MEMORY position

			if( ! NFWriteDataToMem( (unsigned char*)(inoutDestinationBuffer + llFromCurrent), (size_t)llFromStart, m_lpbInMem ) )
			{
				// LOG Error/Set Status
				LogError( m_ComponentID, DBG_LOCATION, L"Failed to write to memory. NFWriteDataToMem() returned: FALSE.");
				retVal = WAX_RESULT_CAN_NOT_READ_FROM_MEM;
				goto ERREXIT;
			}
		}
	}//END fetch it from memory
	else
	{//get the data from disk
		
		//***	Read data from file
		//check if it is contiguous, or we need two reads
		getWriteLimits( inllTLOffset, m_llTLogSize, inulDataLength, llFromCurrent, llFromStart );

		if( llFromCurrent > 0 )
		{//Read from inullTLOffset position
			if( ! NFReadDataFromFile( m_hFileHandle, inllTLOffset, (unsigned long)llFromCurrent, inoutDestinationBuffer ) )
			{
				// LOG Error/Set Status
				LogError( m_ComponentID, DBG_LOCATION, L"Failed to read from file. NFReadDataFromFile() returned: FALSE.");
				retVal = WAX_RESULT_CAN_NOT_READ_FROM_FILE;
				goto ERREXIT;
			}
		}

		if( llFromStart > 0 )
		{//Read from start position

			if( ! NFReadDataFromFile( m_hFileHandle, 0, (unsigned long)llFromStart, (unsigned char*)inoutDestinationBuffer + llFromCurrent ) )
			{
				// LOG Error/Set Status
				LogError( m_ComponentID, DBG_LOCATION, L"Failed to read from file. NFReadDataFromFile() returned: FALSE.");
				retVal = WAX_RESULT_CAN_NOT_READ_FROM_FILE;
				goto ERREXIT;
			}
		}

		//Remove set file pointer. Will be set in write buffer to disk method.
		/*
		//Set the file pointer back to its original position.
		if( !NFSetFilePointer( m_hFileHandle, m_llFilePosition, FILE_BEGIN ) )
		{
			LogError( m_ComponentID, DBG_LOCATION, L"NFSetFilePointer() failed. ");
			retVal = WAX_RESULT_CAN_NOT_SET_FILE_POINTER;
			goto ERREXIT;
		}
		*/

	}//END get the data from disk

	return WAX_RESULT_OK;

ERREXIT:
	return retVal;
}
//-----------------------------------------------------------------------------


///////////////////////////////////////////////////////////////////////////
//////	END Operations methods		///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

//!	Queries the WAX component for statistic data.
WAX_RESULT StoreManager::GetStatistics(
	/*__inout*/ Statistics *inoutStats		//!< output statistic structure
						 )
{
	if (m_ulRollTLog > 0)
	{
		inoutStats->diskUsed = m_llTLogSize;
	}
	else
	{
		inoutStats->diskUsed = m_llFilePosition;
	}

	return WAX_RESULT_OK;
}
//-----------------------------------------------------------------------------


//! Validates the input ByteBuffer for NULL and ByteBuffer->buffer for NULL.
BOOL StoreManager::isValidByteBuffer(
	/*__in*/ const ByteBuffer* inbBuffer
	)
{
	if( inbBuffer == NULL )
	{
		return FALSE;
	}
	if( inbBuffer->buffer == NULL )
	{
		return FALSE;
	}

	return TRUE;
}
//-----------------------------------------------------------------------------
