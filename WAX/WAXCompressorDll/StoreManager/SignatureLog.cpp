//********************************************************/
//  ./StoreManager/SignatureLog.cpp
//
//	Company		: Neverfail 
//	PROJECT		: WAN Acceleration 
//	Component	: Store Manager / Signature Log
//  Owner		: Ciprian Maris
//
//  $Rev: 1694 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: cmaris $  
//  $Date: 2009-02-09 08:57:56 +0000 (Mon, 09 Feb 2009) $
//
//********************************************************/

#include "SignatureLog.h"
#include "../Logger/Logger.h"

WAX_ID SignatureLog::m_ComponentID = (WAX_ID)WAX_ID_SIGNATURELOG;

SignatureLog::SignatureLog(void)
{
	m_sigLog			= NULL;	//Storage for signatures
	m_ulSigLogSizeBytes	= 0;	//number of bytes allocated in signature log
	m_ulSigLogMaxCount	= 0;	//maximum number of signatures
	m_ulSigLogCurrent	= 0;	//current position in signature collection 

	m_Index = NULL;
	m_lpbMemoryBufferForIndex = NULL;
	m_ulIndexSizeBytes = 0;			//! Number of bytes allocated for the index.

}

SignatureLog::~SignatureLog(void)
{
	if( NULL != m_Index )
	{
		delete m_Index;
		m_Index = NULL;
	}

	//Nothing to do here
}
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
//////	Control and Management methods		///////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

//! Method start()
/*!
	The method
		Starts the SignatureLog only when the current server role is encoder.
		Retreives the memory pre-allocated for the log from the memory pool.
		Initialises all internal paramaters.

	Return: In case of success return WAX_RESULT_OK. In the case of failure it returns the 
			failure reason.

	Caller: StoreManager::start().
*/
WAX_RESULT SignatureLog::start(
	/*__in*/ bool inAsEncoder,			//!< Role of current server
	/*__in*/ unsigned long inulMaxChunksInTrLog	//!< Number of signature log entries
	) 
{
	//	Remove all entries from the index(dictionary) to release memory (if any allocated)
	if( m_Index != NULL )
	{
		delete m_Index;
		m_Index = NULL;
	}

	//***	Only exists on encoder side - so it will only be started if the current role is encoder
	if( inAsEncoder )
	{
		//get an instance of the memory pool
		m_MemoryPool = MemoryPool::getInstance();

		//get the size in bytes
		m_ulSigLogSizeBytes = (unsigned long)m_MemoryPool->getComponentMemorySize( WAX_ID_SIGNATURELOG );
		//make the size be a multiple of SignatureLogentry size
		m_ulSigLogSizeBytes = ( m_ulSigLogSizeBytes / sizeof( SignatureLogEntry ) ) * sizeof( SignatureLogEntry );

		//get the adress from mem pool
		m_sigLog = (SignatureLogEntry*)m_MemoryPool->gimme( m_ulSigLogSizeBytes, m_ComponentID ) ;	//Storage for signatures
		memset( m_sigLog, 0, m_ulSigLogSizeBytes );//Set to 0 just to be sure

		//compute the maximum number of entries alowed by the allocated memory
		m_ulSigLogMaxCount = (m_ulSigLogSizeBytes / sizeof( SignatureLogEntry ) ) - 1 ;	//maximum number of signatures
		
		if( m_ulSigLogMaxCount < inulMaxChunksInTrLog )
		{
			LogError( m_ComponentID, DBG_LOCATION, L"Not enough memory for Signature Log." );
			return WAX_RESULT_NOT_ENOUGH_MEMORY_FOR_SIG_LOG;
		}
		else
		{
			//assign the number of signatures needed to reference the entire TLog
			m_ulSigLogMaxCount = inulMaxChunksInTrLog - 1;
		}

		//set current position to 0
		m_ulSigLogCurrent	= 0;	//current position in signature collection

		//	Now get the memory for the INDEX
		m_ulIndexSizeBytes = (unsigned long)m_MemoryPool->getComponentMemorySize( WAX_ID_INDEX );

		m_lpbMemoryBufferForIndex = m_MemoryPool->gimme( m_ulIndexSizeBytes, WAX_ID_INDEX );

		//Create the index passing it the maximum number of entries and the pointer to the allocated buffer.
		m_Index = new HashMap( m_ulSigLogMaxCount + 1, m_lpbMemoryBufferForIndex, m_ulIndexSizeBytes );
	}

	WAXControlableBase::start(inAsEncoder);

	return WAX_RESULT_OK;
}
//-----------------------------------------------------------------------------

//! Method stop()
/*!
	The method stops the SignatureLog.

	Return: WAX_RESULT_OK

	Caller: StoreManager::stop().
*/
WAX_RESULT SignatureLog::stop(void)
{
	//	Since the index was created in the start method, it will be deleted here.
	if( m_Index != NULL )
	{
		delete m_Index;
		m_Index = NULL;
	}

	m_ulSigLogMaxCount = 0;

	//	NOTE: The memory buffers do not need to be given back to the MemoryPool nor do they need to
	//	be invalidated. The MemoryPool component will deal with the allocated buffers.
	//	We only need to make sure on the next start to initialize the buffers properly.

	WAXControlableBase::stop();

	return WAX_RESULT_OK;
}
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////
//////	Operations methods		///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

//! Method writeSignatures() - writes the signatures presented as ChunkCharacteristic to the signature log.
/*!
*	The method
*		Writes the signatures presented as ChunkCharacteristic to the signature log.
*		Updates the internal counters.
*		Deletes the overwritten index entries.
*		Updates the index entries that matched during DD to point to the new location in the signature log array.
*
*	NOTE:	The current implementation does not allow this method to fail and return an error code.
*
*	Caller: StoreManager::writeBuffer()
*
*	\sa StoreManager::writeBuffer()
**/
void SignatureLog::writeSignatures ( 
	/*__in*/ vector <ChunkCharacteristic> &inChunkCharacteristics	//!< collection of characteristics for the 
																	// new signature to be written
		)
{
	//Check if we have characteristics: process them
	size_t iNrCharacteristics = inChunkCharacteristics.size(); 
	if( iNrCharacteristics > 0 )
	{
		ChunkCharacteristic* currentChunkCharacteristicPtr;
		//	Parse through all chunk characteristics
		for( size_t i=0; i < iNrCharacteristics; i++)
		{
			//PERFORMANCE: store the Chunkcharacteristic only once and acess it several times
			currentChunkCharacteristicPtr = &inChunkCharacteristics[i];
			SignatureLogEntry*	addedSigLogEntry;		//signature log entry to be added to the log
			SignatureLogEntry*	deletedSigLogEntry;	//signature log entry to be updated/deleted on next step

			//Write the current signature to Signature Log
			addedSigLogEntry = &m_sigLog[ m_ulSigLogCurrent ];

			addedSigLogEntry->signature			= currentChunkCharacteristicPtr->signature; 
			addedSigLogEntry->strongSignature	= currentChunkCharacteristicPtr->strongSignature;
			addedSigLogEntry->isSigUpdated		= false;

			//	Update the current position in signature log array
			//	NOTE: m_ulSigLogMaxCount value is  NrOfPositionsInSigLog-1
			//		  m_ulSigLogCurrent  range is [0...m_ulSigLogMaxCount] = [0...NrOfPositionsInSigLog-1] 
			m_ulSigLogCurrent = ( m_ulSigLogCurrent <  m_ulSigLogMaxCount ) ? (m_ulSigLogCurrent + 1) : 0;

			//If after the current position update we overwrite a signature, we need to remove the old one from the dictionary
			deletedSigLogEntry = &m_sigLog[ m_ulSigLogCurrent ];

			//	Delete old signatures from index as they are removed from the signature log
			//	All position of the signature log are initializwed to 0
			//	If (m_deletedSigLogEntry->signature != 0) means that we already rolled the sig log over at least once
			if( deletedSigLogEntry->signature != 0 )
			{
				//	Only remove the signature if it points to the current index - i.e. it was not updated
				//	to point to a different (newer) entry in sig log.
				//	The isSigUpdated flag is set when the index entry for the current signature is updated to
				//	point to a different SigLogEntry than the current one.
				if( !deletedSigLogEntry->isSigUpdated )
				{
					RemoveFromIndex( deletedSigLogEntry );
				}
				//	Set the signature to 0 just to make sure since this SignatureLogEntry 
				//	will be overwritten on the next insert into the SignatureLog.
				deletedSigLogEntry->signature = 0;
			}

			//	Update the index entry to point to the curently inserted signature log entry
			UpdateIndexEntry( addedSigLogEntry );

		}//END	Parse through all chunk characteristics
	}//END	Check if we have characteristics: process them
}
//-----------------------------------------------------------------------------

//! Method searchEntryBySignature() - searches the input param signature in the signature index(dictionary).
/*!
*	The method
*		Searches for the input signature in the signature index.
*		Return the found SignatureLogEntry.
*
*	Return: 
*		In case of success return WAX_RESULT_OK. 
*		In the case the signature is not found it returns WAX_RESULT_ENTRY_NOT_FOUND.
*
*	Caller: WAXEncoder::findTokensInBuffer().
*/
WAX_RESULT SignatureLog::searchEntryBySignature (
	/*__in*/	SignatureType inSignature,			//!< signature to be searched for
	/*__out*/	SignatureLogEntry* &outSigLogEntry	//!< Signature Log Entry if the signature was found in the index
		)
{
	void* vpRetVal;

	if ( m_Index->find(inSignature, vpRetVal) )
	{
		outSigLogEntry = (SignatureLogEntry*) vpRetVal;

		return WAX_RESULT_OK;
	}

	return WAX_RESULT_ENTRY_NOT_FOUND;
}
//-------------------------------------------------------------------------------------------------

//! Method RemoveFromIndex() - removes the requested <Signature, SignatureLogEntry> pair from the index(dictionary)
/*!
*	The method
*		Removes the requested <Signature, SignatureLogEntry> pair from the index(dictionary).
*		If the signature does NOT exists in the index - NOOP.
*
*	NOTE: The method does not return a SUCCESS/FAILURE code because there is no need to.
*	
*	Caller: writeSignatures().
*/
void SignatureLog::RemoveFromIndex( 
	/*__in*/ SignatureLogEntry* inSigLogEntry //! Signature log entry to be removed from the index.
	)
{
	m_Index->remove( inSigLogEntry->signature );
}
//-------------------------------------------------------------------------------------------------

//! Method UpdateIndexEntry() - updates the SignatureLogEntry for a Signature
/*!
*	The method adds or updates a <Signature, SignatureLogEntry> in the index(dictionary).
*		- If the signature does not exist in the index it will be inserted. NOTE: this is desired and intended behavior.
*		- If the exists in the index, its Signature Log pointer will be updated. NOTE: this is desired and intended behavior.
*		In this case the SignatureLogEntry->isSigUpdated flag will be set to true. This signals the fact that the same 
*		signature now has a different "most recent" signature log entry pointer in the index from the one it was inserted
*		with.
*		If on signature log roll over, the curent signature to be deleted has this flag set, it will not be removed from the 
*		index.
*
*	NOTE: The method does not return a SUCCESS/FAILURE code because there is no need to.
*
*	Caller: writeSignatures().
*/
void SignatureLog::UpdateIndexEntry(
	/*__in*/ SignatureLogEntry* inSigLogEntry //! Signature log entry to be updated in the index.
	)
{
	void* sigLogEntryOld;

	if( m_Index->insert( inSigLogEntry->signature, (void*)inSigLogEntry, sigLogEntryOld ) )
	{
		((SignatureLogEntry*)sigLogEntryOld)->isSigUpdated = true;
	}

}
//-------------------------------------------------------------------------------------------------

//!	Queries the WAX component for statistic data.
WAX_RESULT SignatureLog::GetStatistics(
	/*__inout*/ Statistics *inoutStats		//!< output statistic structure
						 )
{
	////	BZ 7530 - 
	//	The SignatureLog stats will only be gathered if these three conditions are met:
	//	m_isEncoder == true AND m_isStarted == true AND inoutStats->bDumpAdditionalStats == true
	//	m_isEncoder is only true if m_isStarted is true, so we only do one check.
	
	if( m_isEncoder && inoutStats->bDumpAdditionalStats )
	{
		LogEventFormated( LOG_LEVEL_INFO, m_ComponentID, L"IndexSize: %d,\t MaxIndexSize: %d, \t MaxBucketDepth: %d,\t NrBucketsAtMaxDepth: %d,\t m_UsedBuckets: %d", 
				m_Index->size(), m_Index->getMaxKeysInMap(),  m_Index->getMaxBucketDepth(), m_Index->getNrBucketsAtMaxDepth(),
				m_Index->getUsedBucketsNow() );
	}

	return WAX_RESULT_OK;
}
//-------------------------------------------------------------------------------------------------
