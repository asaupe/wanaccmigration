//********************************************************/
//  ./StoreManager/StoreManager.h
//
//	Company		: Neverfail 
//	PROJECT		: WAN Acceleration 
//	Component	: Store Manager
//  Owner		: Ciprian Maris
//
//  $Rev: 1483 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: avajda $  
//  $Date: 2009-01-14 10:20:20 +0000 (Wed, 14 Jan 2009) $
//
//********************************************************/

#pragma once

#include "../Common.h"
#include "WAXTypes.h"

#include "../MemoryPool/MemoryPool.h"
#include "../ConfigMan/ConfigMan.h"
#include "../Stats/IStats.h"
#include "SignatureLog.h"
#include "StoreManagerBase.h"

//! StoreManager - Storage for a portion of the most current data traffic seen by the WAX Compressor.
/*! \class StoreManager
*  \brief Implements the storage class for traffic data buffers.
*
*	The class has two major components:
*		- The on disk Transfer Log. This is the file that stores the most recent data traffic.
*		NOTE:	Actual writing to disk is only perfromed on the target system. On the source system
*				we only maintain notions of size, position and mapping of the in memory transfer log to
*				the on disk transfer log without ever going to disk.
*
*		- The in memory portion of the transfer log. These is an in memory buffer maintaing a SubSet 
*		of the traffic seen by the WAX Compressor. This portion is implemented and works the same both
*		on source and on target but they are used in different ways:
*			- On source: it is used for byte by byte grow compare.
*			- On target: it is used as a cache only. 
*
*  Is called by 
*		- WAXEncoder to store and retreive data buffers.
*		- Tokenizer to retreive data buffers from the Transfer log.
*
*	Manages the SignatureLog by calling the SignatureLog::writeSignatures() method.
*
*	The order in which initialize/allocate/start and stop/release/exit are called is 
*	managed and checked by the WAX Compressor unit.
*
*	NOTE:	[1] The methods that are implemented in the .CPP file have their detailed description in the .CPP file.
*
*   \author CM
*   \version 1.0.$Rev: 1483 $
*   \date    JUN-2008
*/
class StoreManager: public WaxSingletonTemplate<StoreManager>, WAXControlableBase, StoreManagerBase, public IStats
{
	friend class WaxSingletonTemplate<StoreManager>;

public:
	~StoreManager(void);

	///////////////////////////////////////////////////////////////////////////
	//////	Control and Management methods		///////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	//! Method allocate() - Pre-allocates disk space.
	WAX_RESULT allocate(
		/*__in*/ unsigned long inTransferLogSize,				//!< transfer log size in MB's
		/*__in*/ const StringBuffer *inTransferLogPath	//!< complete path of the transfer log (path + file name or path)
		);

	//! Method release() - releases pre-allocated disk space.
	WAX_RESULT release(void);

	//! Method start() - Starts the StoreManager with the role given by inAsEncoder.
	WAX_RESULT start( 
		/*__in*/ bool inAsEncoder //!< Current role of the server.
		); 

	//! Method stop() - Stops the StoreManager.
	WAX_RESULT stop(void);

	///////////////////////////////////////////////////////////////////////////
	//////	Operations methods		///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	//! Write Buffer - writes a data buffer to the transfer log and calls SignatureLog to update the
	//	signatures for the buffer.
	WAX_RESULT writeBuffer ( 
		/*__in*/ const ByteBuffer *inbBuffer,		//!< byte buffer to be added to the transfer log
		/*__in*/ vector <ChunkCharacteristic> &inChunkCharacteristics	//!< Collection of characteristics 
																	// for the new signature to be written.
		);

	//!	Method getBufferBySigLogEntry() - retrieves a buffer from the in memory Transfer Log on source.
	WAX_RESULT getBufferBySigLogEntry(
		/*__in*/	SignatureLogEntry* inSigLogEntry,	//!< Signature Log Entry for which we want the buffer.
		/*__inout*/ unsigned long& inoutMaxLeftLength,			//!< Maximum distance to the left that can be used for byte by byte comparison
		/*__inout*/ unsigned long& inoutMaxRightLength,			//!< Maximum distance to the right that can be used for byte by byte comparison
		/*__out*/	long long& outTrLogOffset,			//!< Offset in transfer log (relative to on disk file)
		/*__out*/	unsigned char* &outBufferPointer			//!< Pointer to the data buffer if the data corresponding to 
													// inSigLogEntry is in memory. 
		);

	//!Method getBufferByTLogOffset() - retrieves a buffer from the Transfer Log on target.
	WAX_RESULT getBufferByTLogOffset(
		/*__in*/	long long inllTLOffset,			//!< Offset in transfer log (relative to file).
		/*__in*/	const unsigned long inulDataLength,		//!< Length of required buffer.
		/*__inout*/	unsigned char* inoutDestinationBuffer	//!< Destination of data to be written too.It is preallocated by the caller.
		);

	//!Method isOnDiskOffsetInMemory - checks if an offset in the transfer log is also currently located in memory.
	BOOL isOnDiskOffsetInMemory( 
		/*__in*/	long long inllFileOffset,		//!< on disk offset for which we want to know if it is in meory.
		/*__out*/	LONG &outlRollCorrectionFactor  //!< Correction factor having values 0 or 1. See NOTE in .cpp file for details.
		);

	///////////////////////////////////////////////////////////////////////////
	//////	Getters		///////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	//!Method getMemoryOffsetOfOnDiskOffset - Computes the in memory offset of an offset in the Transfer Log.
	long long getMemoryOffsetOfOnDiskOffset( 
		/*__in*/	long long inllFileOffset,		//!< on disk offset for which we want to know if it is in meory
		/*__in*/	LONG inlRollCorrectionFactor	//!< correction factor having values 0 or 1. \sa isOnDiskOffsetInMemory()
												//!< NOTE for details on this param.
		);

	//!	getTransferLogPosition() - retrieve the current position (offset) in the on disk Transfer Log.
	//!	This is the FileOffset where the next buffer will be written.
	long long getTransferLogPosition(void)	{ return m_llFilePosition; }

	//! Returns the size of the on disk transfer log.
	long long getTransferLogSize (void)		{ return m_llTLogSize; }

	//! Return the file name of the transfer log
	const wchar_t* getTransferLogFileName( void )	{ return m_strTLogFileName.c_str(); } 

	//!	getMemoryPosition() - retrieve the current position (offset) in the in memory Transfer Log.
	//!	This is the MemoryOffset where the next buffer will be written.
	UINT_PTR getMemoryPosition(void)		{ return m_uipMemPosition; }

	//! Return the size of the in memory portion of the transfer log.
	UINT_PTR getMemorySize(void)			{ return m_uipInMemsize; }

	//!	Queries the WAX component for statistic data.
	WAX_RESULT GetStatistics(
		/*__inout*/	Statistics *inoutStats	//!< output statistic structure
		);

private:
	static WAX_ID m_ComponentID;

	SignatureLog*	m_signatureLog;	//signature log object - stores the signature array
	MemoryPool*		m_MemoryPool;	//memory pool object used for acquiring the in memory Transfer Log buffer
	ConfigMan*		m_ConfigMan;	//config man object 

	HANDLE m_hFileHandle;//Handle for the Transfer log file. Only used on target

	//!	Memory cache/buffer for transfer log
	//		in memory part of the transfer log - on source
	//		in memory transfer log cache - on target
	unsigned char* m_lpbInMem;

	long long m_llTLogSize;		//total size of the transfer log
	UINT_PTR m_uipInMemsize;	//Size of in Memory transfer log

	wstring m_strTLogFileName;	//File name of the tranfer log as provided in allocate().

	//!	File position relative to file start
	long long m_llFilePosition;//!< On Source, this is a "virtual" position of current file pointer in file.
							  //	It is only used for offset management, since we have no file on source.

	UINT_PTR  m_uipMemPosition;		//in memory position (offset) relative to start

	long long m_llMaxChunksInTrLog;  //number of chunks in the entire transfer log
	long long m_llMaxChunksInMem;	//number of chunks in memory

	//!	Mapping of in memory transfer log to File Transfer Log - updated on each write
	long long m_llFileOffsetOfInMemStart;	//!< start offset of the in  memory portion of the TLog
	long long m_llFileOffsetOfInMemEnd;		//!< end offset of the in  memory portion of the TLog
	bool	 m_bUpdateFileToMemoryMapping;  //!< states that the memory mapping needs to be updated 
											//!< is first set to true when when m_llFilePosition reaches the in memory TLog size

	//!	Offset Management members
	unsigned long m_ulRollTLog; //!< Counter of Transfer log roll overs
	unsigned long m_ulRollDiff; //!< TLogSize denoted as X, in memory TLog size denoted as x => m_ulRollDiff = X % x

	StoreManager(void);

	///////////////////////////////////////////////////////////////////////////
	//////	Operations methods		///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	//!Method writeBufferToDisk() writes a buffer to the disk.
	WAX_RESULT writeBufferToDisk( 
		/*__in*/ const ByteBuffer *inbBuffer //!< Input buffer to be written to disk.
		);

	//!Method writeBufferToMem() writes a buffer to memory.
	WAX_RESULT writeBufferToMem ( 
		/*__in*/ const ByteBuffer *inbBuffer //!< Input buffer to be written to memory.
		);

	///////////////////////////////////////////////////////////////////////////
	//////	Helper - Util functions ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	//! Method getWriteLimits() - computes the maximum length that can be written to the destination (memory or disk).
	void getWriteLimits( 
		/*__in*/	long long inllCurrentPosition,			//!< current position/offset in destination
		/*__in*/	long long inllMaxPosition,				//!< maximum position/offset in destination
		/*__in*/	long long inllLength,					//!< length of the requested write
		/*__out*/	long long &outllLenFromCurrentPosition,	//!< number of bytes that can be written from the current position
		/*__out*/	long long &outllFromStart				//!< number of bytes that will be written from the start
		);
	
	//! Validates the input ByteBuffer for NULL and ByteBuffer->buffer for NULL.
	BOOL isValidByteBuffer(
		/*__in*/ const ByteBuffer* inBBuffer
		);

	//! Method openTransferLogFile() - Creates the directory structure if needed and pens the transfer log file.
	HANDLE openTransferLogFile( 
		/*__in*/ const wchar_t * inszTransferLogPath //!< complete path of the transfer log (path + file name or path)
		); 

};

