//********************************************************/
//  ./StoreManager/HashMap.cpp
//
//	Company		: Neverfail 
//	PROJECT		: WAN Acceleration 
//	Component	: Store Manager / Hash Map
//  Owner		: Ciprian Maris
//
//  $Rev: 1483 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: avajda $  
//  $Date: 2009-01-14 10:20:20 +0000 (Wed, 14 Jan 2009) $
//
//********************************************************/

#include <stdlib.h>
#include <stdio.h>
#include "hashmap.h"

#define INITIAL_SIZE 1024

//	Multiplication factor for bucket number. nrBuckets = maxKeys * BUCKET_MULTIPLICATION_FACTOR.
#define BUCKET_MULTIPLICATION_FACTOR 2

//! Computes the memory needed to hold a maximum number "inMaxKeys" of unique elements.
/*!
*	The method computes the needed memory amount as:
*		maxMemory =	inMaxKeys *( Size_of_Element + Size_of_Pointer * BUCKET_MULTIPLICATION_FACTOR).
*
*	This way we ensure that irrespective of the max number of unique keys and their distribution 
*	in the buckets, there will always be enough spare space allocated to hold all <key, value> pairs.
*
**/
unsigned long HashMap::getMaxMemory(
	/*__in*/ size_t inMaxKeys 
	)
{
	size_t iSizePointer = sizeof( hashmap_element* );
	size_t iSizeElement = sizeof( hashmap_element );

	inMaxKeys++;//just in case

	return (unsigned long) (inMaxKeys *( iSizeElement + iSizePointer * BUCKET_MULTIPLICATION_FACTOR));
}
//-----------------------------------------------------------------------------


HashMap::HashMap()
{
	HashMap( INITIAL_SIZE, NULL, 0 );
}
//-----------------------------------------------------------------------------

//! Main Constructor - computes the number of buckets and allocates memory buffers if necessary.
/*!
*	The method 
*		- computes the number of buckets in the hash map
*		- allocates the memory buffer for the nodes if the input buffer is NULL - this operation 
*		mode is used for testing only.
*		- sets all the bytes of the memory buffer to 0 (initialisation)
*		- sets the statistic counters to 0.
*
*	NOTE:	[1] For unit testing the input parameter inlpbMemoryBuffer can be NULL. In this case the 
*			memory allocation attempt is done in the contructor. 
*			If the hash map can not allocate memory for buffers the current HashMap object can not be used.
*			It is left up to the caller to invoke getConstructorSuccess() to check that the allocation was
*			successful.
**/
HashMap::HashMap( 
	/*__in*/ size_t inMaxKeys,			//!< Maximum number of unique keys that the hash map will ever see. 
	/*__in*/ unsigned char* inlpbMemoryBuffer,	//!< Pre-allocated memory buffer to be used by the hash. If NULL, 
									// the hash map will attempt to allocate memory locally.
	/*__in*/ unsigned long inulBufferLength		//!< Length of the input memory buffer.
			)
{
	//	Set the number of buckets in accordance to the maximum nr of unique keys.
	m_nrBuckets = (inMaxKeys * BUCKET_MULTIPLICATION_FACTOR);

	if( inlpbMemoryBuffer == NULL )
	{//	If the caller does not pass an allocated buffer for the index, allocate it here.
		m_ulMemoryBufferSize  = getMaxMemory(inMaxKeys);
		m_lpbMemoryBuffer = (unsigned char*)malloc( (size_t)m_ulMemoryBufferSize );
		if( m_lpbMemoryBuffer == NULL )
		{
			//	NOTE: If the hash map can not allocate memory for buffers the current HashMap object can not be used.
			//	It is left up to the caller to invoke getConstructorSuccess() to check that the allocation was
			//	successful.
			m_bMemAllocatedLocally = false;
			return;
		}
		m_bMemAllocatedLocally = true; //Flag that the allocation was done locally. The destructor will delete it.
	}
	else
	{
		m_ulMemoryBufferSize = inulBufferLength;
		m_lpbMemoryBuffer = inlpbMemoryBuffer;
		m_bMemAllocatedLocally = false;
	}

	//	Init the memory buffer: set all values to 0.
	memset( m_lpbMemoryBuffer, 0, m_ulMemoryBufferSize );

	//	Pre-allocate the array of bucket heads
	m_bucketArray = (hashmap_element**)m_lpbMemoryBuffer;
	m_size = 0;

	//	Set the in memory pointer correctly
	size_t nrBucketArraySizeInBytes = m_nrBuckets * sizeof ( hashmap_element* ) + 1;
	m_nrCurrentPositionInBuffer = nrBucketArraySizeInBytes;

	//	NOTE: We do not need to initialize the first set of values to 0 since the entire memory
	//	buffer where these values will exist is set to 0.

	//	Init stats members
	m_MaxBucketDepth		= 0;
	m_NrBucketsAtMaxDepth	= 0;
	m_MaxKeysInMap			= 0;
}
//-----------------------------------------------------------------------------


HashMap::~HashMap()
{
	if( m_lpbMemoryBuffer != NULL )
	{
		if( m_bMemAllocatedLocally )
		{//	The memory buffer was allocated in constructor, so we need to free it here.
			free ( m_lpbMemoryBuffer );
			m_lpbMemoryBuffer = NULL;
			m_ulMemoryBufferSize = 0;
			m_bucketArray = NULL;
		}
	}
}
//-----------------------------------------------------------------------------


//! Returns the number of buckets that have at least one element in them.
/*!
	Parses the bucket array and counts the number of buckets in use (head not NULL).
**/
unsigned long HashMap::getUsedBucketsNow()
{
	unsigned long nrUsedBuckets = 0;
	hashmap_element* hmElBucketHead;

	if( NULL == m_bucketArray )
	{
		return 0;
	}

	for( size_t i=0; i < m_nrBuckets; i++ )
	{
		hmElBucketHead = m_bucketArray[ i ];

		if( NULL != hmElBucketHead )
		{
			nrUsedBuckets++;
		}
	}

	return nrUsedBuckets;
}
//-----------------------------------------------------------------------------


//! Locates a key in the hash map. If required inserts one if it does not exist. 
/*!
*	The method searches for the input key in the hash map.
*		- If it is found, it returns the data member as the outhmelValue parameter.
*		- If it is not found and the inbInsertIfNotFound flag is set, it will insert one node
*		in the tree.
*		- In both cases above, the outhmelPrevRef parameter will be the parent link pointing to 
*		the found/inserted node and NULL if the current node has no parent .
*
*	NOTE: [1] The hash map operation methods insert and remove call locate to find a key.
*			  The speed perfromance of the hash map insert / remove is based on this method.
*
*	Return: Returns true if the key was found in the hash map and false if not.
*
**/
bool HashMap::locateKey( 
	/*__in*/	size_t inKey,						//!< Value of the key to be located.
	/*__out*/	hashmap_element* &outhmelValue,		//!< Output data value if the key is found.
	/*__out*/	hashmap_element** &outhmelPrevRef,  //!< Putput parent link if the node has one.
	/*__in*/	bool inbInsertIfNotFound			//!< Insert if not exists flag.
	)
{
	bool bFound = false;

	unsigned long nBucketDepth = 0;

	outhmelPrevRef = &m_bucketArray[ getBucketIndex( inKey ) ];
	hashmap_element* hmElCurent = *outhmelPrevRef;

	while( ( !bFound ) && ( hmElCurent != NULL ) )
	{//search for key in the entire bucket (BST) - until found or NULL reached
		if( inKey == (hmElCurent)->key )
		{//found!
			bFound = true;
		}
		else
		{//not found - go to left or right in bucket
			if( inKey < hmElCurent->key )
			{
				outhmelPrevRef = &hmElCurent->left;
				hmElCurent =  *outhmelPrevRef;
			}
			else
			{ 
				outhmelPrevRef = &hmElCurent->right;
				hmElCurent = *outhmelPrevRef;
			}

			nBucketDepth++;//increase the current bucket depth
		}
	}

	if( ( !bFound ) && ( inbInsertIfNotFound ) )
	{//insert a new item
		//we need to allocate an element as insert place
		hmElCurent = (hashmap_element*)getMemoryForOneElement( );
		hmElCurent->left  = NULL;
		hmElCurent->right = NULL;

		//make the link from parent to currently inserted node
		*outhmelPrevRef = hmElCurent;

		if (nBucketDepth > m_MaxBucketDepth )
		{
			m_MaxBucketDepth = nBucketDepth;
		}

		if (nBucketDepth == m_MaxBucketDepth )
		{
			m_NrBucketsAtMaxDepth++;
		}

		m_size++;

		//	Stats: set the maximum number of keys in the map.
		if( m_size > m_MaxKeysInMap )
		{
			m_MaxKeysInMap = (unsigned long)m_size;
		}
	}

	outhmelValue = hmElCurent;//set the return value

	return bFound;	

}
//-----------------------------------------------------------------------------


//! Inserts or updates a <key, data> pair in the hash map. Returns the old data 
//	value if the key is already present in the hash map.
/*!
*	The method 
*		- searches for the input key in the map
*			- if the key is not found, it is inserted
*			- if the key is found, it replaces the data memeber of the hashmap element
*			and return the overwritten data element as the outvpOldValue parameter.
*
*	Return: Returns true if the key was found in the hash map and false if not.
*
**/
bool HashMap::insert( 
	/*__in*/	size_t inKey,		//!< Value of the key to be inserted/updated.
	/*__in*/	void* invpValue,	//!< Value of the new data member.
	/*__out*/	void* &outvpOldValue//!< Value of the old data member if the key 
								//	is already present in the map.
				 )
{
	hashmap_element *hmElforInsert;
	hashmap_element **hmElRef;

	//	Seach the key in the map. Do insert an node if nof found.
	bool bFound = locateKey( inKey, hmElforInsert, hmElRef, true );

	//	Set the old data value to be rerurned to the caller.
	//	CAUTION! : This assignment has to come prior to the m_hmElforInsert->data = invpValue; assignment.
	if( bFound )
	{
		outvpOldValue = hmElforInsert->data;		
	}

	//	Assign the key and data memebers for the updated/newly inserted element
	hmElforInsert->key  = inKey;
	hmElforInsert->data = invpValue;

	return bFound;
}
//-----------------------------------------------------------------------------


//! Searches for a key in the hash map.
/*!
*	The method 
*		- searches for the input key in the map
*			- if the key is not found the method returns false
*			- if the key is found the method returns true and sets the outvpValue parameter
*			as the data member of the found element.
*
*	Return: Returns true if the key was found in the hash map and false if not.
*
*	NOTE: [1] The find method uses a stripped down search routine to maximize speed.
*			  It could employ the search routine in locateKey() but it does not due to 
*			  perfromance reasons.
*
**/
bool HashMap::find( 
	/*__in*/ size_t inKey,		//!< Value of the key to be searched for.
	/*__out*/ void* &outvpValue //!< Value of the data member if the key is found in the map.
				)
{
	hashmap_element* hmElCurent = m_bucketArray[ getBucketIndex( inKey ) ];

	while( hmElCurent != NULL )
	{//search for key in the entire bucket (BST) - until found or NULL reached

		if( inKey == hmElCurent->key  )
		{//found!
			outvpValue = hmElCurent->data;
			return true;
		}
		else
		{//not found - go to left or right in bucket
			if( inKey < hmElCurent->key )
			{
				hmElCurent =  hmElCurent->left;
			}
			else
			{ 
				hmElCurent = hmElCurent->right;
			}
		}
	}

	return false;

}
//-----------------------------------------------------------------------------


//!	Removes a key from the hash map.
/*!
*	The method searches for the key in the hash map using the locate() private method.
*
*	If the key is found, it perfroms a NON-RECOURSIVE deletion of a BST node with the following sub-cases:
*		- Left link is NULL
*		- Right link is NULL
*		- None of the above are NULL - delete node that holds the max value in the left subtree.
*		- When the element to be deleted is a bucket head we also have the check for both children being NULL.
*		In this case the node will never be deleted, only its data member will be set to NULL to mark that the 
*		node is empty.
*
*	NOTE:
*		[1] The non recoursive deletion was chosen due to perfromance constraints.
*
*	Return: Returns true if the key was found in the hash map and false if not.
*
*/
bool HashMap::remove( 
	/*__in*/ size_t inKey //<! Value of the key to be removed.
				)
{
	hashmap_element**hmElRef ;
	hashmap_element* hmElCurent ;

	//	Seach the key in the map. Do not insert if nof found.
	bool bFound = locateKey( inKey, hmElCurent, hmElRef, false );

	if( bFound )
	{
		m_size--;//

			//CASE 1: left link of node to delete is NULL
		if (hmElCurent->left == NULL) 
		{
			//This assignment covers the case ( hmElforLocate->right == NULL )
			*hmElRef = hmElCurent->right;

			releaseMemoryForOneElement( hmElCurent );
				
			return true;
		}

		//CASE 2: right link of node to delete is NULL
		if (hmElCurent->right == NULL) 
		{
			*hmElRef = hmElCurent->left;

			releaseMemoryForOneElement( hmElCurent );
				
			return true;
		}

		//CASE 3: left and right links are not null
		// hashmap_element has two children - get max of left subtree
		removeMaxValueInLeftSubtree( hmElCurent );

		return true;
		
	}

	return false;
}
//-----------------------------------------------------------------------------


//! Deletes the maximum value of the left subtree for an tree identified by the head element.
/*!
*	The method finds the max key value in the left subtree, puts that value in head and 
*	deletes the old location of the maximum.
*
* 	The remove() method will call it for the case where the hashmap_element has two children. 
*
**/
void HashMap::removeMaxValueInLeftSubtree( 
	/*__in*/ hashmap_element* inhmElHead //!< Root of the tree for which the delete is performed.
	)
{
	hashmap_element *hmElPrev;
	hashmap_element *hmElToDelete;

	// get left subtree maximum value
	hmElToDelete = inhmElHead->left; 
	hmElPrev     = inhmElHead;
 
	// find the rightmost child of the subtree of the inhmElHead->left
	while (hmElToDelete->right != NULL) 
	{
		hmElPrev = hmElToDelete;
		hmElToDelete = hmElToDelete->right;
	}
 
	// copy the value from the in-order predecessor to the original inhmElHead
	inhmElHead->key  = hmElToDelete->key;
	inhmElHead->data = hmElToDelete->data;

	/**
	CASE:
	         . = Key_To_Del
		    /
		   x = max key on left subtree 
		  /
		 E = max key node has a left sub-node
	*/
	if( (hmElPrev->left != NULL) && (hmElPrev->left == hmElToDelete) )
	{
		hmElPrev->left = hmElToDelete->left;
	}
	else
	{
		// We found the element to be replaced. Assign the left link
		// of the element to be deleted as the right link of the prev
		// if  if( hmElToDelete->left != NULL ) - we do not lose the link
		//			else - its NULL already
		//
		hmElPrev->right = hmElToDelete->left;
	}

	releaseMemoryForOneElement( hmElToDelete );
	hmElToDelete = NULL;

}
//-----------------------------------------------------------------------------

 
//!	Returns an empty meomory location for one node(hashmap_element).
/*!
*	The free memory location is taken either 
*		- from the list of empty adresses represented by the m_listFreeAddresses member
*		OR
*		- from the current position of the memory buffer (m_lpbMemoryBuffer + m_nrCurrentPositionInBuffer)
*
*	NOTE:	[1] The hash map knows for sure that there are enough free memory positions to service this request.
*			The free locations are either in the list of free locations (m_listFreeAddresses) or 
*			in the unused part of the buffer ( m_lpbMemoryBuffer + m_nrCurrentPositionInBuffer ).
*
*	Returns an adress location to be used for the node
*
*/
void* HashMap::getMemoryForOneElement( )
{
	void* vpRetAdress;

	if( m_listFreeAddresses.size() > 0 )
	{//	We assign the memory of the first empty element in list
		vpRetAdress = *m_listFreeAddresses.begin();

		m_listFreeAddresses.pop_front();
	}
	else
	{
		vpRetAdress = ( m_lpbMemoryBuffer + m_nrCurrentPositionInBuffer );
		m_nrCurrentPositionInBuffer += sizeof( hashmap_element );
	}

	return vpRetAdress;
}
//-----------------------------------------------------------------------------


//!	Adds the address of the node (hashmap_element) to the list of free adresses.
/*!
*	Adds the address of the node (hashmap_element) being released to the list of free adresses. 
*	This method belongs to the memory buffer management routines of the hash map.
*	
*/
void HashMap::releaseMemoryForOneElement( void* inAdressToRelease )
{
	m_listFreeAddresses.push_front( inAdressToRelease );
}
//-----------------------------------------------------------------------------


//! Removes all <key, value> pairs from the map.
/*!
*	The method parses through all the buckets in the map .For each bucket it keeps on deleting
*	the head key until the bucket is emty.
*/
void HashMap::clear(void)
{
	hashmap_element *hmElBucketHead;

	//	Parse through all buckets
	for( size_t i = 0; i < m_nrBuckets; i++ )
	{
		hmElBucketHead = m_bucketArray[ i ];

		while ( hmElBucketHead != NULL )//means we still have keys in this bucket
		{
			remove( hmElBucketHead->key );
			hmElBucketHead = m_bucketArray[ i ];
		}
	}
}
