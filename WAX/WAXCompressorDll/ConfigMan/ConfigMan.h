//********************************************************/
//  ./ConfigMan/ConfigMan.h
//
//  Owner : Bogdan Hruban
// 
//	Company		: Neverfail 
//	PROJECT		: WAN Acceleration 
//	Component	: ConfigMan 
//
//  $Rev: 1889 $ - Revision of last commit
//
/********************************************************
//
// Last Change - Author and Date of last change:
// $Author: cmaris $  
// $Date: 2009-03-04 10:17:05 +0000 (Wed, 04 Mar 2009) $
//
//********************************************************/

//********************************************************/
//
//	The ConfigMan class holds all configuration parameters for the 
//	WAX compressor DLL.
//
//  At the first call the ConfigMan initializes all data with the default
//  values. All the parameters can be set later on to different values.
//
//	The ConfigMan is implemented as a Singleton and is called by almost 
//	every module of WAX.
//
//********************************************************/

#pragma once
#include "../Common.h"
#include "WAXTypes.h"
#include "../waxsingletontemplate.h"
#include "../SharedTypes.h"

//! Get/Set application parameters
/*!
	The ConfigMan holds the running parameters for all modules
	running in WAX.
*/
class ConfigMan: public WaxSingletonTemplate<ConfigMan>
{
public:
	//
	//Getters
	//

	//! Get the allocator step size in MB
	unsigned long GetAllocatorStep(void);
	
	//! Is advanced deduplication enabled
	bool GetIsAdvancedDeduplicationEnabled(void);

	//! Get the TransferLog size
	uint64_t GetTLogSize(void);

	//! Get the memory size
	UINT_PTR GetMemorySize(void);

	//! Is the current process started as an encoder or decoder
	bool GetIsEncoder(void);

	//! Get the signature base
	unsigned long GetSignatureBase(void);

	//! Get the signature distance
	unsigned long GetSignatureDistance(void);

	//! Get the minimum RPR pattern length which we replace with a generic token
	unsigned long GetMinRPRLength(void);

	//! Get the minimum buffer size processed by WAX Encoder
	unsigned long GetMinDeDuplicateLength(void);

	//! Get the minimum buffer size to be replaced by a token
	unsigned long GetMinDeDuplicateLengthReplacedByToken(void);

	//! Get the maximum memory used
	unsigned long GetMaxMemUsed(void);

	//! Get the minimum memory used
	unsigned long GetMinMemUsed(void);

	//! Get the encoder temporary buffer size
	unsigned long GetEncoderTmpBufferSize(void);

	//! Get the N invariant
	double GetNInvariant(void);

	//! Get the maximum pattern size replaced by RPR
	unsigned char GetMaxRPRPatternSize(void);

	//!< Get the minimum buffer length to be ZLIB compressed
	unsigned long GetMinZLibCompressLength(void);

	//
	//Setters
	//

	//! Set the allocator step size in MB
	bool SetAllocatorStep(
		/*__in*/ unsigned long inullAllocatorStep
		);

	//! Set advanced de-duplication 
	bool SetIsAdvancedDeduplicationEnabled(
		/*__in*/ bool inbIsAdvancedDeduplication 
		);
	
	//! Set the TransferLog size
	bool SetTLogSize(
		/*__in*/ uint64_t inullTLogSize			//!< TransferLog size
		);

	//! Set the memory size
	bool SetMemorySize(
		/*__in*/ UINT_PTR inuipMemorySize		//!< Memory size
		);

	//! Set the state of the current process (Encoder/Decoder)
	bool SetIsEncoder(
		/*__in*/ bool inIsEncoder				//!< Encoder flag
		);

	//! Set the signature base
	bool SetSignatureBase(
		/*__in*/ unsigned long inulSignatureBase			//!< Signature base
		);

	//! Set the signature distance
	bool SetSignatureDistance(
		/*__in*/ unsigned long inulSignatureDistance		//!< Signature distance
		);

	//! Set the minimum buffer size processed by RPR
	bool SetMinRPRLength(
		/*__in*/ unsigned long inulMinRPRLength			//!< Minimum buffer length required for RPR processing
		);

	//! Set the minimum buffer size processed by WAX Encoder
	bool SetMinDeDuplicateLength(
		/*__in*/ unsigned long inulMinDeDuplicateLength	//!< Minimum buffer length required for DeDuplication processing
		);

	//! Set the minimum buffer size to be replaced by a token 
	bool SetMinDeDuplicateLengthReplacedByToken(
		/*__in*/ unsigned long inulMinDeDuplicateLengthReplacedByToken	//!< Minimum buffer length to be replaced by token
		);

	//! Set the maximum memory used
	bool SetMaxMemUsed(
		/*__in*/ unsigned long inulMaxMemUsed_Mb			//!< Maximum memory used (specified in MB)
		);

	//! Get the minimum memory used
	bool SetMinMemUsed(
		/*__in*/ unsigned long inulMinMemUsed_Mb			//!< Minimum memory used (specified in MB)
		);

	//! Set the encoder temporary buffer size
	bool SetEncoderTmpBufferSize(
		/*__in*/ unsigned long inulEncoderTmpBufferSize	//!< Encoder temporary buffer size
		);

	//! Set the N invariant
	bool SetNInvariant(
		/*__in*/ double indNInvariant			//!< Current volume computation invariant
		);

	//! Set the maximum pattern size replaced by RPR
	bool SetMaxRPRPatternSize(
		/*__in*/ unsigned char inshMaxRPRPatternSize		//!< Maximum RPR pattern size
		);

	//! Set the minimum buffer length to be ZLIB compressed
	bool SetMinZLibCompressLength(
		/*__in*/ unsigned long inulMinZLibCompressLength //!< Minimum buffer length to be ZLIB compressed
		);

private:
	ConfigMan();

	static WAX_ID m_ComponentID;					//!<

	uint64_t m_ullTLogSize;						//!< value in Bytes
	UINT_PTR  m_uipMemorySize;						//!< value in Bytes
	bool m_bIsEncoder;								//!< 
	bool m_bIsAdvancedDeduplicationEnabled;         //!<
	unsigned long m_ulSignatureBase;						//!< 
	unsigned long m_ulSignatureDistance;					//!< 
	unsigned long m_ulMinRPRLength;							//!< 
	unsigned long m_ulMinDeDuplicateLength;					//!< 
	unsigned long m_ulMinDeDuplicateLengthReplacedByToken;	//!< 
	unsigned long m_ulMaxMemUsed_MB;						//!<
	unsigned long m_ulMinMemUsed_MB;						//!<
	unsigned long m_ulEncoderTmpBufferSize;					//!<
	unsigned long m_ulMinZLibCompressLength;				//!< 
	unsigned long m_ulAllocatorStep;						//!< inMB's
	unsigned char m_shMaxRPRPatternSize;						//!<

	double m_dNAverageInvariant;

	friend class WaxSingletonTemplate<ConfigMan>;
};
