//********************************************************/
//  ./ConfigMan/ConfigMan.cpp
//
//  Owner : Bogdan Hruban
// 
//	Company		: Neverfail 
//	PROJECT		: WAN Acceleration 
//	Component	: ConfigMan 
//
//  $Rev: 3184 $ - Revision of last commit
//
/********************************************************
//
// Last Change - Author and Date of last change:
// $Author: cmaris $  
// $Date: 2009-10-02 09:48:01 +0100 (Fri, 02 Oct 2009) $
//
//********************************************************/

//********************************************************/
//
//	The ConfigMan class holds all configuration parameters for the 
//	WAX compressor DLL.
//
//********************************************************/

//********************************************************/
//		CHANGE LOG
//	
//	2009-02-26: 
//		- Removed obsolete m_sbFileExclusion member
//
//		BZ 7543
//		- Changed default values to comply with the values given by the SYSTEM parameter document:
//		http://ng-eng-share/SiteDirectory/dev/Docs/Projects/WAN%20Acceleration/Integration/WA%20-%20System%20Parameters.doc
//
//		- Changed setters to return a boolean. If the return value is false, the input value for the setter was out of range.
//		This needs to be handeled by the caller.
//		There are also a set of Setters that do not have a range defined. These will just return true.
//
//********************************************************/


#include "ConfigMan.h"

/*! Default configuration parameter values. */  
enum DefaultWAXValues
{
	WAX_DEF_SB					= 128,			//!< Default Signature Base
	WAX_DEF_SD					= 384,			//!< Default Signature Distance
	WAX_DEF_MAX_IN_BUFFER_SIZE	= (256*KILO),	//!< Maximum input buffer size for the encoder
	WAX_DEF_MAX_MEMORY_USED		= 512,			//!< Maximum memory to be allocated by the compressor
	WAX_DEF_MIN_MEMORY_USED		= 512,			//!< Minimum memory to be allocated by the compressor
	WAX_DEF_ALLOCATOR_STEP		= 128,			//!< Size of steps (in Mb) to decrease memory while allocating

	WAX_DEF_IS_ENCODER			= true,			//!< current server role

	WAX_DEF_MIN_RPR_LEN        = 12,			//!< minimum len of a RPR buffer to be replaced by a compressed token
	WAX_DEF_MIN_DEDUPE_LEN     = KILO,			//!< min len of the buffer to be considered for DD 
	WAX_DEF_MIN_REPLACED_BY_TOKEN=WAX_DEF_SB,	//!< minimum length of data replaced by a DD token

	WAX_DEF_MIN_ZLIB_LEN	= 512,				//!minimum buffer length to be ZLIB compressed

	WAX_DEF_RPR_MAX_PATTERN_SIZE = 1,			//!< The maximum pattern size to be searched for by RPR
	
	//Valid ranges for parameters
	WAX_DEF_SB_MIN					= 100,			//!< Default Signature Base
	WAX_DEF_SB_MAX					= 512,			//!< Default Signature Base

	WAX_DEF_SD_MIN					= 256,			//!< Default Signature Distance
	WAX_DEF_SD_MAX					= 4 * KILO,		//!< Default Signature Distance

	WAX_DEF_MAX_MEMORY_USED_MIN		= 256,			//!< Maximum memory to be allocated by the compressor
	WAX_DEF_MAX_MEMORY_USED_MAX		= 1536,			//!< Maximum memory to be allocated by the compressor

	WAX_DEF_MIN_MEMORY_USED_MIN		= 256,			//!< Minimum memory to be allocated by the compressor
	WAX_DEF_MIN_MEMORY_USED_MAX		= 1536,			//!< Minimum memory to be allocated by the compressor

	WAX_DEF_MIN_RPR_LEN_MIN        = 12,			//!< minimum len of a RPR buffer to be replaced by a compressed token
	WAX_DEF_MIN_RPR_LEN_MAX        = 1024,			//!< minimum len of a RPR buffer to be replaced by a compressed token

	WAX_DEF_MIN_DEDUPE_LEN_MIN     = 512,			//!< min len of the buffer to be considered for DD 
	WAX_DEF_MIN_DEDUPE_LEN_MAX	   = 4 * KILO,		//!< min len of the buffer to be considered for DD 

	WAX_DEF_MIN_REPLACED_BY_TOKEN_MIN = 100,		//!< minimum length of data replaced by a DD token
	WAX_DEF_MIN_REPLACED_BY_TOKEN_MAX = 4 * KILO,	//!< minimum length of data replaced by a DD token

	WAX_DEF_MIN_ZLIB_LEN_MIN	= 256,				//!minimum buffer length to be ZLIB compressed
	WAX_DEF_MIN_ZLIB_LEN_MAX	= 4 * KILO,			//!minimum buffer length to be ZLIB compressed

};

//! Default config value for N invariant statistical computational value
const double WAX_DEF_N_INVARIANT		= (double)0.05;	//!< invariant computational value for statistical current volume
const double WAX_DEF_N_INVARIANT_MIN	= (double)0.01;	//!< invariant computational value for statistical current volume
const double WAX_DEF_N_INVARIANT_MAX	= (double)1.0;	//!< invariant computational value for statistical current volume

//! Defaults for Transfer log size
const uint64_t	WAX_DEF_TLOG_SIZE       =(2 * KILO * (uint64_t)MEGA);	//!< Value in BYTES
const uint64_t	WAX_DEF_TLOG_SIZE_MIN	= 256 * MEGA;					//!< Value in BYTES
const uint64_t	WAX_DEF_TLOG_SIZE_MAX   = 4 * KILO * (uint64_t)MEGA;	//!< Value in BYTES


WAX_ID		ConfigMan::m_ComponentID	= (WAX_ID)WAX_ID_CONFIGMAN;

ConfigMan::ConfigMan():
			m_ullTLogSize(WAX_DEF_TLOG_SIZE),
			m_uipMemorySize(WAX_DEF_MAX_MEMORY_USED*MEGA),
			m_bIsEncoder(WAX_DEF_IS_ENCODER),
			m_ulSignatureBase(WAX_DEF_SB),
			m_ulSignatureDistance(WAX_DEF_SD),
			m_ulMinRPRLength(WAX_DEF_MIN_RPR_LEN),
			m_ulMinDeDuplicateLength(WAX_DEF_MIN_DEDUPE_LEN),
			m_ulMinDeDuplicateLengthReplacedByToken(WAX_DEF_MIN_REPLACED_BY_TOKEN),
			m_ulMinZLibCompressLength( WAX_DEF_MIN_ZLIB_LEN),
			m_ulMaxMemUsed_MB(WAX_DEF_MAX_MEMORY_USED),
			m_ulMinMemUsed_MB(WAX_DEF_MIN_MEMORY_USED),
			m_ulEncoderTmpBufferSize(/*TEMP Remove this*/ 1),
			m_dNAverageInvariant(WAX_DEF_N_INVARIANT),
			m_shMaxRPRPatternSize(WAX_DEF_RPR_MAX_PATTERN_SIZE),
			m_ulAllocatorStep(WAX_DEF_ALLOCATOR_STEP),
			m_bIsAdvancedDeduplicationEnabled(false)
{}

//
//Getters
//
//! Get the allocator step size in MB
unsigned long ConfigMan::GetAllocatorStep(void)
{
	return m_ulAllocatorStep;
}

//! Is advanced deduplication enabled
bool ConfigMan::GetIsAdvancedDeduplicationEnabled(void)
{
	return m_bIsAdvancedDeduplicationEnabled;
}


//! Get the TransferLog size
uint64_t ConfigMan::GetTLogSize(void)
{
	return m_ullTLogSize;
}

//! Get the memory size
UINT_PTR  ConfigMan::GetMemorySize(void)
{
	return m_uipMemorySize;
}

//! Is the current process started as an encoder or decoder
bool ConfigMan::GetIsEncoder(void)
{
	return m_bIsEncoder;
}

//! Get the signature base
unsigned long ConfigMan::GetSignatureBase(void)
{
	return m_ulSignatureBase;
}

//! Get the signature distance
unsigned long ConfigMan::GetSignatureDistance(void)
{
	return m_ulSignatureDistance;
}

//!  Get the minimum RPR pattern length which we replace with a generic token
/*!
	If the RPR pattern length is lower than this value we won't replace it by a 
	RPR generic token, it will be part fromt he previous uncompressed token
*/
unsigned long ConfigMan::GetMinRPRLength(void)
{
	return m_ulMinRPRLength;
}

//! Get the minimum buffer size processed by WAX Encoder
/*!
	If the buffer length is not greater than this parameters' value
	WAX Encoder will not be called for the current buffer region.
*/
unsigned long ConfigMan::GetMinDeDuplicateLength(void)
{
	return m_ulMinDeDuplicateLength;
}

//! Get the minimum buffer size to be replaced by a token
/*!
	In case of DeDuplication processing a token will be created for
	a matching pattern only if it's size will be greater (or equal) 
	to this parameters' value.
*/
unsigned long ConfigMan::GetMinDeDuplicateLengthReplacedByToken(void)
{
	return m_ulMinDeDuplicateLengthReplacedByToken;
}

//! Get the maximum memory used
unsigned long ConfigMan::GetMaxMemUsed(void)
{
	return m_ulMaxMemUsed_MB;
}

//! Get the minimum memory used
unsigned long ConfigMan::GetMinMemUsed(void)
{
	return m_ulMinMemUsed_MB;
}

//! Get the encoder temporary buffer size
unsigned long ConfigMan::GetEncoderTmpBufferSize(void)
{
	return m_ulEncoderTmpBufferSize;
}

//! Get the N invariant
double ConfigMan::GetNInvariant(void)
{
	return m_dNAverageInvariant;
}

//! Get the maximum pattern size replaced by RPR
unsigned char ConfigMan::GetMaxRPRPatternSize(void)
{
	return m_shMaxRPRPatternSize;
}

//!< Get the minimum buffer length to be ZLIB compressed
unsigned long ConfigMan::GetMinZLibCompressLength(void)
{
	return m_ulMinZLibCompressLength;
}


//
//Setters
//
bool ConfigMan::SetAllocatorStep(
	/*__in*/ unsigned long inulAllocatorStep
	)
{
	m_ulAllocatorStep = inulAllocatorStep;
	return true;
}

//! Set advanced de-duplication 
bool ConfigMan::SetIsAdvancedDeduplicationEnabled( 
	/*__in*/ bool inbIsAdvancedDeduplication 
	)
{
	m_bIsAdvancedDeduplicationEnabled = inbIsAdvancedDeduplication;
	return true;
}

//! Set the memory size
bool ConfigMan::SetMemorySize(
	/*__in*/ UINT_PTR inuipMemorySize
	)
{
	m_uipMemorySize = inuipMemorySize;
	return true;
}

//! Set the state of the current process (Encoder/Decoder)
bool ConfigMan::SetIsEncoder(
	/*__in*/ bool inIsEncoder
	)
{
	m_bIsEncoder = inIsEncoder;
	return true;
}

//! Set the signature base
bool ConfigMan::SetSignatureBase(
	/*__in*/ unsigned long inulSignatureBase
	)
{
	if( inulSignatureBase == 0 )
	{
		m_ulSignatureBase = WAX_DEF_SB;
		return true;
	}

	if( (inulSignatureBase >= WAX_DEF_SB_MIN) && (inulSignatureBase <= WAX_DEF_SB_MAX) )
	{
		m_ulSignatureBase = inulSignatureBase;
		return true;
	}

	return false;
}

//! Set the signature distance
bool ConfigMan::SetSignatureDistance(
	/*__in*/ unsigned long inulSignatureDistance
	)
{
	if( inulSignatureDistance == 0 )
	{
		m_ulSignatureDistance = WAX_DEF_SD;
		return true;
	}
	
	if( (inulSignatureDistance >= WAX_DEF_SD_MIN) && (inulSignatureDistance <= WAX_DEF_SD_MAX) )
	{
		m_ulSignatureDistance = inulSignatureDistance;
		return true;
	}

	return false;
}

//! Set the minimum buffer size processed by RPR
/*!
	If the buffer length is not greater than this parameters' value
	RPR processing will not be called.
*/
bool ConfigMan::SetMinRPRLength(
	/*__in*/ unsigned long inulMinRPRLength
	)
{
	if( inulMinRPRLength == 0 )
	{
		m_ulMinRPRLength = WAX_DEF_MIN_RPR_LEN;
		return true;
	}

	//	The following change for BZ 7543 also complies with the fix for BZ 7523 described below:
	//	BZ 7523 the minimum values for RPR length so that we do not extend the input buffer.
	if( (inulMinRPRLength >= WAX_DEF_MIN_RPR_LEN_MIN) && (inulMinRPRLength <= WAX_DEF_MIN_RPR_LEN_MAX) )
	{
		m_ulMinRPRLength = inulMinRPRLength;
		return true;
	}

	return false;
}

//! Set the minimum buffer size processed by WAX Encoder
/*!
	If the buffer length is not greater than this parameters' value
	WAX Encoder will not be called for the current buffer region.
*/
bool ConfigMan::SetMinDeDuplicateLength(
	/*__in*/ unsigned long inulMinDeDuplicateLength
	)
{
	if( inulMinDeDuplicateLength == 0 )
	{
		m_ulMinDeDuplicateLength = WAX_DEF_MIN_DEDUPE_LEN;
		return true;
	}

	if( (inulMinDeDuplicateLength >= WAX_DEF_MIN_DEDUPE_LEN_MIN) && (inulMinDeDuplicateLength <= WAX_DEF_MIN_DEDUPE_LEN_MAX) )
	{
		m_ulMinDeDuplicateLength = inulMinDeDuplicateLength;
		return true;
	}

	return false;

}

//! Set the minimum buffer size to be replaced by a token 
/*!
	In case of DeDuplication processing a token will be created for
	a matching pattern only if it's size will be greater (or equal) 
	to this parameters' value.
*/
bool ConfigMan::SetMinDeDuplicateLengthReplacedByToken(
	/*__in*/ unsigned long inulMinDeDuplicateLengthReplacedByToken
	)
{
	if( inulMinDeDuplicateLengthReplacedByToken == 0 )
	{
		m_ulMinDeDuplicateLengthReplacedByToken = WAX_DEF_MIN_REPLACED_BY_TOKEN;
		return true;
	}

	//	The following change for BZ 7543 also complies with the fix for BZ 7523 described below:
	//	BZ 7523 the minimum values for data replaced by DD token length so that we do not extend the input buffer.
	if( (inulMinDeDuplicateLengthReplacedByToken >= WAX_DEF_MIN_REPLACED_BY_TOKEN_MIN) && 
		(inulMinDeDuplicateLengthReplacedByToken <= WAX_DEF_MIN_REPLACED_BY_TOKEN_MAX))
	{
		m_ulMinDeDuplicateLengthReplacedByToken = inulMinDeDuplicateLengthReplacedByToken;
		return true;
	}

	return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
//////	Setters for Allocation Parameters
//////	NOTE: The setters for allocation parameters do not set the default value for the config 
//////	parameter if the input value is ZERO and will return false in stead.

//! Set the TransferLog size
//!	NOTE - this is an allocate parameter. It will not default if input value is 0.
//! IMPORTANT:	The transfer log size is expressed in BYTES!!! (as oposed to the other two 
//				allocation parameters min/max memory which are in MEGA BYTES).
bool ConfigMan::SetTLogSize(
	/*__in*/ uint64_t inullTLogSize
	)
{
	if( (inullTLogSize >= WAX_DEF_TLOG_SIZE_MIN) && ( inullTLogSize <= WAX_DEF_TLOG_SIZE_MAX ) ) 
	{
		m_ullTLogSize = inullTLogSize;
		return true;
	}

	return false;
}

//! Set the maximum memory used
//!	NOTE - this is an allocate parameter. It will not default if input value is 0.
bool ConfigMan::SetMaxMemUsed(
	/*__in*/ unsigned long inulMaxMemUsed_Mb
	)
{
	if( (inulMaxMemUsed_Mb >= WAX_DEF_MAX_MEMORY_USED_MIN) && (inulMaxMemUsed_Mb <= WAX_DEF_MAX_MEMORY_USED_MAX) )
	{
		m_ulMaxMemUsed_MB = inulMaxMemUsed_Mb;
		return true;
	}

	return false;
}

//! Set the minimum memory used
//!	NOTE - this is an allocate parameter. It will not default if input value is 0.
bool ConfigMan::SetMinMemUsed(
	/*__in*/ unsigned long inulMinMemUsed_Mb
	)
{
	if( (inulMinMemUsed_Mb >= WAX_DEF_MIN_MEMORY_USED_MIN) && (inulMinMemUsed_Mb <= WAX_DEF_MIN_MEMORY_USED_MAX))
	{
		m_ulMinMemUsed_MB = inulMinMemUsed_Mb;
		return true;
	}

	return false;
}

//////	END Setters for Allocation Parameters
///////////////////////////////////////////////////////////////////////////////////////////////////


//! Set the encoder temporary buffer size
bool ConfigMan::SetEncoderTmpBufferSize(
	/*__in*/ unsigned long inulEncoderTmpBufferSize
	)
{
	m_ulEncoderTmpBufferSize = inulEncoderTmpBufferSize;
	return true;
}

//! Set the N invariant
bool ConfigMan::SetNInvariant(
	/*__in*/ double indNInvariant
	)
{
	if( indNInvariant == 0.0 )
	{
		m_dNAverageInvariant = WAX_DEF_N_INVARIANT;
		
		//BZ 8588 - in case the default value was selected, return true as well
		return true;
	}

	if( (indNInvariant >= WAX_DEF_N_INVARIANT_MIN) && (indNInvariant <= WAX_DEF_N_INVARIANT_MAX) )
	{
		m_dNAverageInvariant = indNInvariant;
		return true;
	}

	return false;
}

//! Set the maximum pattern size replaced by RPR
bool ConfigMan::SetMaxRPRPatternSize(
	/*__in*/ unsigned char inshMaxRPRPatternSize
	)
{
	m_shMaxRPRPatternSize = inshMaxRPRPatternSize;
	return true;
}

//! Set the minimum buffer length to be ZLIB compressed
bool ConfigMan::SetMinZLibCompressLength(
	/*__in*/ unsigned long inulMinZLibCompressLength //!< Minimum buffer length to be ZLIB compressed
	)
{
	if( inulMinZLibCompressLength == 0 )
	{
		m_ulMinZLibCompressLength = WAX_DEF_MIN_ZLIB_LEN;
		return true;
	}

	if( (inulMinZLibCompressLength >= WAX_DEF_MIN_ZLIB_LEN_MIN) && (inulMinZLibCompressLength <= WAX_DEF_MIN_ZLIB_LEN_MAX) )
	{
		m_ulMinZLibCompressLength = inulMinZLibCompressLength;
		return true;
	}
	
	return false;
}
