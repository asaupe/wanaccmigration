//********************************************************/
//  ./AllUtils.cpp
//
//  Owner : CM / AV / BH
// 
//	Company		: Neverfail ________________
//	PROJECT		: WAN Acceleration 
//  Component	: Utilities 
//	
//	Purpose: Implementation of the utilites (File,etc.) used by the WAX compressor
//
//  $Rev: 2400 $ - Revision of last commit
//
//********************************************************/

//********************************************************/
//
// Last Change - Author and Date of last change:
// $Author: cmaris $  
// $Date: 2009-05-12 07:56:18 +0100 (Tue, 12 May 2009) $
//
//********************************************************/

//********************************************************/
//
//	AllUtils module will be used for implementing general purpose utilities throughout 
//	the WAX Compression Rpovider project.
//
//********************************************************/

//#include <windows.h>

#include <AllUtils.h>

#include <Logger/Logger.h>

#include <Shlwapi.h>
#include <cstdio>

BOOL SECURITY_ENABLED = false;

//! Utility Wrapper over CreateFile(). 
HANDLE NFOpenFileMax(
	/*__in*/ const wchar_t * inszFileName, 
	/*__in*/ unsigned long indwCreationDisposition, 
	/*__in*/ BOOL  inbOpen4Write,
	/*__in*/ unsigned long indwShareDisposition,
	/*__in*/ BOOL  inbDeleteOnClose 
	)
{
	HANDLE fileHandle;
	unsigned long dwAccess, dwFlagsAndAttributes;
	
	if( inbOpen4Write )
	{//file is to be opened for write
		dwAccess = ACCESS_FILE_WRITE;
	}
	else
	{
		dwAccess = ACCESS_FILE_READ;
	}

	dwFlagsAndAttributes = ( FILE_ATTRIBUTE_NORMAL );
	if( inbDeleteOnClose )
	{
		dwFlagsAndAttributes = ( dwFlagsAndAttributes | FILE_FLAG_DELETE_ON_CLOSE );
	}

	if ((fileHandle = CreateFile(inszFileName,
							dwAccess,
							indwShareDisposition,
							NULL,
							indwCreationDisposition,
							dwFlagsAndAttributes,
							NULL)) == INVALID_HANDLE_VALUE)
	{
		fileHandle = NULL;
		LogError(0 , DBG_LOCATION, L"CreateFile('%s') failed.", inszFileName);
	}

	return fileHandle;
}
//-----------------------------------------------------------------------------

//! Checks the input file handle for NULL and INVALID_HANDLE_VALUE
BOOL NFIsValidFileHandle( 
	/*__in*/ HANDLE inhFileHandle 
	)
{
	if( (inhFileHandle != NULL) && ( inhFileHandle != INVALID_HANDLE_VALUE )) 
		return TRUE;

	return FALSE;
}
//-----------------------------------------------------------------------------

//! Sets the file pointer to the desired value.
BOOL NFSetFilePointer(
	/*__in*/ HANDLE		inhFile,			//!<
	/*__in*/ long long	inllDistanceToMove,	//!<
	/*__in*/ unsigned long		indwMoveMethod		//!<
	)
{
	LARGE_INTEGER liTLogSize;
	liTLogSize.QuadPart = inllDistanceToMove;

	if (INVALID_SET_FILE_POINTER == SetFilePointer( inhFile, liTLogSize.LowPart,&liTLogSize.HighPart, indwMoveMethod ) )
	{
		LogError(0 , DBG_LOCATION, L"SetFilePointer() failed.");
		return FALSE;
	}
	return TRUE;
}
//-----------------------------------------------------------------------------

//! Writes a data buffer to the file identified by inhFile.
BOOL NFWriteDataToFile( 
	/*__in*/ HANDLE inhFile,
	/*__in*/ unsigned long	indwDataLen,
	/*__in*/ unsigned char* inlpbData 
	)
{
	unsigned long dwBytesWritten = 0;

	//	Write Data
	WriteFile( inhFile, inlpbData, indwDataLen, &dwBytesWritten, NULL);
	if( indwDataLen != dwBytesWritten ) 
	{	
		LogError(0 , DBG_LOCATION, L"WriteFile() failed.");
		return FALSE;	
	}

	return TRUE;
}
//-----------------------------------------------------------------------------

//! Writes a data buffer to the memory location identified by inDestination.
BOOL NFWriteDataToMem (
	/*__in*/ void* inDestination, 
	/*__in*/ size_t innDataLen, 
	/*__in*/ void* inSource 
					   )
{
	void* memCpyResult = memcpy( inDestination, inSource, innDataLen ); 

	if( memCpyResult != inDestination ) 
	{	
		LogError(0 , DBG_LOCATION, L"Memory Copy failed.");
		return FALSE;	
	}

	return TRUE;

}
//-----------------------------------------------------------------------------

//! Reads a data buffer from the file identified by inhFile
BOOL NFReadDataFromFile(
	/*__in*/ HANDLE inhFile,
	/*__in*/ long long inFileOffset,
	/*__in*/ unsigned long indwDataLen,
	/*__out*/ unsigned char* outbData 
	)
{
	//set file pointer
	if( !NFSetFilePointer(inhFile, inFileOffset, FILE_BEGIN ) )
	{
		LogError(0 , DBG_LOCATION, L"NFSetFilePointer() failed.");
		return FALSE;
	}

	//read the data
	unsigned long dwBytesRead = 0;
	if( !ReadFile( inhFile, outbData , indwDataLen, &dwBytesRead, NULL) )
	{
		LogError(0 , DBG_LOCATION, L"ReadFile() failed.");
		return FALSE;
	}
	
	return TRUE;
}
//-----------------------------------------------------------------------------

//! Initialises security for current process
BOOL NFInitSecurity()
{
	if( SECURITY_ENABLED )
	{//we allready have security enabled
		return true;
	}

	//initialise security
	HANDLE hToken = NULL;
	unsigned long tkpSz = 6 * sizeof(LUID_AND_ATTRIBUTES) + sizeof(TOKEN_PRIVILEGES);
	TOKEN_PRIVILEGES *tkp = (TOKEN_PRIVILEGES *)malloc(tkpSz);

	if ( !tkp )
	{
		SECURITY_ENABLED = false;
		LogError(0, DBG_LOCATION, L"Failed to allocate token privileges");
		goto ERREXIT;
	}

	if( ! OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken ))
	{
		SECURITY_ENABLED = false;
		LogError(0, DBG_LOCATION, L"OpenProcessToken() failed.");
		hToken = NULL;
		goto ERREXIT;
	}

	if ( !LookupPrivilegeValue( NULL, SE_SECURITY_NAME, &tkp->Privileges[0].Luid ) )
	{
		SECURITY_ENABLED = false;
		LogError(0, DBG_LOCATION, L"LookupPrivilegeValue() failed.");
		goto ERREXIT;
	}

	if ( !LookupPrivilegeValue( NULL, SE_TCB_NAME, &tkp->Privileges[1].Luid ) )
	{
		SECURITY_ENABLED = false;
		LogError(0, DBG_LOCATION, L"LookupPrivilegeValue() failed.");
		goto ERREXIT;
	}

	if ( !LookupPrivilegeValue( NULL, SE_RESTORE_NAME, &tkp->Privileges[2].Luid ) )
	{
		SECURITY_ENABLED = false;
		LogError(0, DBG_LOCATION, L"LookupPrivilegeValue() failed.");
		goto ERREXIT;
	}
		
	if ( !LookupPrivilegeValue( NULL, SE_BACKUP_NAME, &tkp->Privileges[3].Luid ) )
	{
		SECURITY_ENABLED = false;
		LogError(0, DBG_LOCATION, L"LookupPrivilegeValue() failed.");
		goto ERREXIT;
	}

	if ( !LookupPrivilegeValue( NULL, SE_TAKE_OWNERSHIP_NAME, &tkp->Privileges[4].Luid ) )
	{
		SECURITY_ENABLED = false;
		LogError(0, DBG_LOCATION, L"LookupPrivilegeValue() failed.");
		goto ERREXIT;
	}

	if ( !LookupPrivilegeValue( NULL, SE_LOCK_MEMORY_NAME, &tkp->Privileges[5].Luid ) )
	{
		SECURITY_ENABLED = false;
		LogError(0, DBG_LOCATION, L"LookupPrivilegeValue() failed.");
		goto ERREXIT;
	}

	tkp->PrivilegeCount = 6;
	tkp->Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	tkp->Privileges[1].Attributes = SE_PRIVILEGE_ENABLED;
	tkp->Privileges[2].Attributes = SE_PRIVILEGE_ENABLED;
	tkp->Privileges[3].Attributes = SE_PRIVILEGE_ENABLED;
	tkp->Privileges[4].Attributes = SE_PRIVILEGE_ENABLED;
	tkp->Privileges[5].Attributes = SE_PRIVILEGE_ENABLED;
		
	if( AdjustTokenPrivileges( hToken, FALSE, tkp, tkpSz, NULL, NULL) )
    {
        unsigned long dwErr = GetLastError();

	    if( (ERROR_SUCCESS != dwErr) && (ERROR_NOT_ALL_ASSIGNED == dwErr) )
	    {
			LogEventMessage(LOG_LEVEL_INFO, WAX_ID_UNKNOWN, L"WARNING: AdjustTokenPrivileges() returned NOT_ALL_ASSIGNED");
	    }
	    SECURITY_ENABLED = true;
    }
	else
	{
		SECURITY_ENABLED = false;
		LogError(0, DBG_LOCATION, L"AdjustTokenPrivileges() failed.");
		goto ERREXIT;
	}

	//Clean-UP and return SECURITY_ENABLED
	if( NULL != hToken ) 
	{
		CloseHandle( hToken );
		hToken = NULL;
	}

	if( NULL != tkp ) 
	{
		free( tkp );
		tkp = NULL;
	}

	return SECURITY_ENABLED;

	
ERREXIT://clean up snd return error 
	if( NULL != hToken ) 
	{
		CloseHandle( hToken );
		hToken = NULL;
	}
	if( NULL != tkp ) 
	{
		free( tkp );
		tkp = NULL;
	}

	return SECURITY_ENABLED;
}
//-----------------------------------------------------------------------------

//!Sets the size of an opened file. Resets its contents if requested.
BOOL NFResizeFile(
	/*__in*/ HANDLE   inhFile,			//!<
	/*__in*/ long long inllNewFileSize,	//!<
	/*__in*/ BOOL	inbResetContent,	//!<
	/*__in*/ BOOL	inbGoToFileBegin	//!<
	)
{
	BOOL retVal = TRUE;


	if( inbResetContent )
	{//Reset the file content by calling the same function with new file size 0
		if( !NFResizeFile( inhFile, 0, FALSE ) )
		{
			LogError(0 , DBG_LOCATION, L"NFResizeFile() failed.");
			retVal = FALSE;
		}
	}

	if( !NFSetFilePointer( inhFile, inllNewFileSize , FILE_BEGIN ) )
	{
		LogError(0 , DBG_LOCATION, L"NFSetFilePointer() failed.");
		retVal = FALSE;
	}

	if( !SetEndOfFile( inhFile ) )
	{
		LogError(0 , DBG_LOCATION, L"SetEndOfFile() failed.");
		retVal = FALSE;
	}

	if( inbGoToFileBegin )
	{
		if( !NFSetFilePointer( inhFile, 0 , FILE_BEGIN ) )
		{
			LogError(0 , DBG_LOCATION, L"NFSetFilePointer() failed.");
			retVal = FALSE;
		}
	}

	return retVal;
}
//-----------------------------------------------------------------------------

//! Convertes the CPU Frequency returned by QueryPerformanceFrequency() to ULONGLONG
uint64_t getCPUFrequency()
{
	LARGE_INTEGER li;
	QueryPerformanceFrequency(&li);//get processor frequency

	return li.QuadPart;
}
//-----------------------------------------------------------------------------

//!	Converts the return of QueryPerformanceCounter() to ULONGLONG
uint64_t getTimeNowByQueryPerformanceCounter()
{
	LARGE_INTEGER li;
	::QueryPerformanceCounter( &li );

	return li.QuadPart;
}
//-----------------------------------------------------------------------------

//! Converts the hour, minute, second and milisonond portion of a SYSTEMTIME structure to Miliseconds.
uint64_t SystemTimeToMiliSeconds(
	/*__in*/ const SYSTEMTIME& systemTime
	)
{
	return 
		(uint64_t)systemTime.wMilliseconds +
		(uint64_t)systemTime.wSecond * (uint64_t)1000 +
		(uint64_t)systemTime.wMinute * (uint64_t)1000 * (uint64_t)60 +
		(uint64_t)systemTime.wHour   * (uint64_t)1000 * (uint64_t)60 * (uint64_t)60;
}
//-----------------------------------------------------------------------------

//! Writes the buffer length and buffer data to the file named inszFileName.
BOOL UtilWriteBufferToFile(
	/*__in*/ const wchar_t* inszFileName,
	/*__in*/ unsigned char* inlpbBuffer, 
	/*__in*/ unsigned long indwBufferLen, 
	/*__in*/ bool inbAppend
	)
{
	BOOL retVal = TRUE;
	unsigned long dwCreationDisposition = CREATE_ALWAYS;
	if( inbAppend )
	{
		dwCreationDisposition = OPEN_ALWAYS;
	}
	
	HANDLE hFile = NFOpenFileMax(inszFileName, dwCreationDisposition, TRUE );
	
	if( !NFIsValidFileHandle( hFile ) )
	{
		return FALSE;
	}

	if( inbAppend )
	{
		NFSetFilePointer( hFile, 0, FILE_END );
	}

	retVal = NFWriteDataToFile(hFile, sizeof(unsigned long) , (unsigned char*)&indwBufferLen);

	retVal = NFWriteDataToFile(hFile,indwBufferLen, inlpbBuffer);

	CloseHandle( hFile );

	return retVal;

}
//-----------------------------------------------------------------------------

//!	Reads the elngth of the buffer and the buffer data from the file named inszFileName.
BOOL UtilReadBufferFromFile(
	/*__in*/ wchar_t* inszFileName,
	/*__out*/ unsigned char* &outlpbBuffer,
	/*__out*/ unsigned long &outdwBufferLen
	)
{
	BOOL retVal = TRUE;

	HANDLE hFile = NFOpenFileMax(inszFileName, OPEN_EXISTING, FALSE );

	if( !NFIsValidFileHandle( hFile ) )
	{
		return FALSE;
	}

	retVal = NFReadDataFromFile( hFile, 0, sizeof(unsigned long), (unsigned char*)&outdwBufferLen );//len

	retVal = NFReadDataFromFile( hFile, sizeof(unsigned long), outdwBufferLen, outlpbBuffer );//data
	CloseHandle( hFile );

	return retVal;
}
//-----------------------------------------------------------------------------


//! Computes the equivalent of miliseconds contained by inFileTime.
//	BZ 7599 - this function takes the entire period into consideration, as oposed to SystemTimeToMiliSeconds()
//			which only considered the hour,min,sec,milisec for computation.
//
//	NOTE:	The inFileTime can be either time elapsed since 1601-01-01 or a relative value.
//			It is up to the caller to make sure the desired value is passed to this function.
uint64_t getFileTimeToMS( FILETIME inFileTime )
{
	return ( (((uint64_t)inFileTime.dwHighDateTime) << 32)  + (uint64_t)inFileTime.dwLowDateTime ) / 10000;
}
//-------------------------------------------------------------------------------------------------

//! Computes the elapsed time between start and stop file times in MiliSeconds.
//	NOTE[1]	The input FILETIME parameters can be either time elapsed since 1601-01-01 or a relative value.
//			In order for this function to succede, the input values have to be both either absolute or relative.
//			It is up to the caller to make sure the desired value is passed to this function.
//	NOTE[2] The function assumes that the start parameter is smaller than the stop parameter and does no checks
//			to ensure the assumption is true. It is up to the caller to provider correctly ordered paramaters.
uint64_t getElapsedFileTimeMS( FILETIME inftStart, FILETIME inftStop )
{
	return getFileTimeToMS( inftStop ) - getFileTimeToMS( inftStart );
}
//-------------------------------------------------------------------------------------------------

//! Retreives the current time as elapsed time between 1601-01-01 between start and stop file times in MiliSeconds.
//	The return VALUE is UTC.
//	BZ 7599 - this function was implemented as part of the bug resolution.
BOOL getCurrentFileTime( FILETIME *outFiletime )
{
	if( outFiletime == NULL )
	{
		return FALSE;
	}

	//get Now time
	SYSTEMTIME stNow;
	GetSystemTime( &stNow );

	//transform the system time to file time
	SystemTimeToFileTime( &stNow, outFiletime );

	return TRUE;
}
//-------------------------------------------------------------------------------------------------


//! Computes the CPU time and App Run time in MiliSeconds for the current process by calling GetProcessTimes()
//	BZ 7599 - changed the support functions from SystemTime to FILETIME ones.
void getCPUAndRUNTimesInMS(
	/*__out*/ uint64_t &outullCPUTime,
	/*__out*/ uint64_t &outullRUNTime 
	)
{
	HANDLE hProcess = GetCurrentProcess();

	FILETIME ftCreationTime;
	FILETIME ftExitTime;
	FILETIME ftKernelTime;
	FILETIME ftUserTime;

	//retreive the current process times
	GetProcessTimes( hProcess, &ftCreationTime, &ftExitTime, &ftKernelTime, &ftUserTime	);

	//	Compute the elapsed time since process start
	//	BZ 7599 - chaged elapsed time computation method.
	FILETIME ftNow;
	getCurrentFileTime( &ftNow );

	//The elapsed time is computed as difference between NOW and creation time.
	outullRUNTime = getElapsedFileTimeMS( ftCreationTime, ftNow );

	outullCPUTime = getFileTimeToMS(ftKernelTime) + getFileTimeToMS( ftUserTime );
}
//-----------------------------------------------------------------------------

//! Gets the minimum size of a large page by calling SIZE_T WINAPI GetLargePageMinimum(void);
SIZE_T NFGetLargePageMinimum(void)
{
	//determine if the current OS is XP
	OSVERSIONINFO osvi;
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	GetVersionEx (&osvi);

	SIZE_T nrRetVal = SIZE_T(2 * MEGA);

	//If the Current OS is XP, the default value will be returned.
	if( ! ( (osvi.dwMajorVersion == 5) && (osvi.dwMinorVersion == 1) ) )
	{
		//	For Win Server 2003 and later, we dynamically load the dll 
		//	and call kernel32.dll/SIZE_T WINAPI GetLargePageMinimum(void);
		//	to determine the proper value.

		typedef SIZE_T (CALLBACK* LPFNGetLargePageMinimum)(void);
		
		HINSTANCE hDLL;  // Handle to DLL
		LPFNGetLargePageMinimum lpfnGetLargePageMinimum; // Function pointer

		hDLL = LoadLibrary(L"kernel32.dll");

		if( hDLL != NULL )
		{
			lpfnGetLargePageMinimum = (LPFNGetLargePageMinimum)GetProcAddress(hDLL, "GetLargePageMinimum" );

			if( lpfnGetLargePageMinimum != NULL )
			{
				nrRetVal = lpfnGetLargePageMinimum();
			}

			FreeLibrary(hDLL);
		}

	}
	return nrRetVal;
}
//-----------------------------------------------------------------------------

//!	Safely copies the source string to the destination address.
unsigned long UtilSafeCopyString( 
	/*__inout*/		wchar_t* inoutszDestination, 
	/*__in*/ const	wchar_t* inszSource,
	/*__in*/ unsigned long  inulCharactersToCopy	//! if zero, the computation of the input string is done by the method.
	)
{
	unsigned long ulLength = inulCharactersToCopy;

	if( ulLength == 0 )
	{
		//compute the length of the input string.
		if( inszSource != NULL )
		{
			ulLength = (unsigned long)wcslen( inszSource ) ;
		}
	}

	//Only copy if the input buffer is not NULL.
	//It is NULL, we will just set the destination buffer to the empty string.
	if( inszSource != NULL )
	{
		memcpy( inoutszDestination, inszSource, ulLength * sizeof(wchar_t) );
	}
	else
	{
		ulLength = 0;
	}

	//	NOTE: If the input string is null we set the output to emty string leaving it to the caller to deal with
	//	this situation
	inoutszDestination [ulLength] = L'\0';

	return ulLength;
}
//-------------------------------------------------------------------------------------------------

//! Allocates or releases a byte buffer according to input parameters.
bool AllocateOrReleaseByteBuffer(
	/*__out*/ ByteBuffer* &outbBuffer, 
	/*__in*/ unsigned long inulBufferLength 
	)
{
	bool retVal = true;

	if( 0 == inulBufferLength )
	{//Release any previously allocated buffers
		if( NULL != outbBuffer )
		{
			if( NULL != outbBuffer->buffer )
			{
				free( outbBuffer->buffer );
				outbBuffer->buffer = NULL;
			}

			outbBuffer->length = 0;
			outbBuffer->maximumLength = 0;

			delete outbBuffer;
		}
		outbBuffer = NULL;
	}
	else
	{//allocate
		AllocateOrReleaseByteBuffer( outbBuffer, 0 );//clean any previously allocated things

		outbBuffer = new ByteBuffer();

		outbBuffer->buffer = (unsigned char*)malloc(inulBufferLength);
		if( NULL != outbBuffer->buffer )
		{
			outbBuffer->length = inulBufferLength;
			outbBuffer->maximumLength = outbBuffer->length;
		}
		else
		{
			retVal = false;//we could not allocate the req length...return error
		}
	}

	return retVal;
}
//-----------------------------------------------------------------------------

//! Allocates or releases a string buffer according to input parameters.
bool AllocateOrReleaseStringBuffer(
	/*__out*/ StringBuffer* &outSBuffer,//!< Buffer to be allocated/deleted 
	/*__in*/ unsigned short inusBufferLength	//!< If set to 0 the object is deleted.
	)
{
	if( 0 == inusBufferLength )
	{//Release any previously allocated buffers
		if( NULL != outSBuffer )
		{
			if( NULL != outSBuffer->buffer )
			{
				free( outSBuffer->buffer );
				outSBuffer->buffer = NULL;
			}

			delete outSBuffer;
			outSBuffer = NULL;
		}
	}
	else
	{//allocate
		AllocateOrReleaseStringBuffer( outSBuffer, 0 );//clean any previously allocated things

		outSBuffer = new StringBuffer();

		outSBuffer->buffer = (wchar_t*)malloc( ((size_t)inusBufferLength + 1) * sizeof(wchar_t) );
		if( NULL != outSBuffer->buffer )
		{
			outSBuffer->length = inusBufferLength;
			outSBuffer->maxiumLength = inusBufferLength;
		}
		else
		{
			return false;
		}
	}

	return true;
}
//-----------------------------------------------------------------------------

//! Copy-constructor for StringBuffer
bool InitStringBuffer( 
	/*__out*/ StringBuffer* &outStringBuffer,
	/*__in*/ const wchar_t* inszSource,
	/*__in*/ size_t inusLength //!Number of WCHAR characters without the terminating string.
	)
{
	if( inszSource == NULL )
	{
		return false;
	}

	unsigned short usLength = (unsigned short)( (inusLength != 0) ? inusLength : wcslen(inszSource) );
	AllocateOrReleaseStringBuffer( outStringBuffer, usLength + 1 );

	//we failed to allocate the buffer. Delete the object and return false.
	if( NULL == outStringBuffer->buffer )
	{
		delete outStringBuffer;
		outStringBuffer = NULL;
		return false;
	}

	//Copy the bytes
	memcpy( outStringBuffer->buffer, inszSource, (usLength + 1) * sizeof(wchar_t) );

	return true;
}
//-----------------------------------------------------------------------------


//! Tokenizes a string into substrings separated by delimiter
int TokenizeString(const wstring str, const wstring delimiters, vector<wstring>& tokens)
{
    wstring::size_type lastPos = str.find_first_not_of(delimiters, 0);
    wstring::size_type pos     = str.find_first_of(delimiters, lastPos);

    while (string::npos != pos || string::npos != lastPos)
    {
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        lastPos = str.find_first_not_of(delimiters, pos);
        pos = str.find_first_of(delimiters, lastPos);
    }

	return 0;
}
//-----------------------------------------------------------------------------

//! Checks if a path is a file or directory exists.
/*!
*	The function first attempts to find the file/dir with sglwapi::PathFileExists() using the input
*	path. If unsuccesfull it appends backslashes to the path to check if there could be a directory named the same way as
*	the input parameter.
*
*	Return: True if the file or directory were found.
*/
BOOL NFPathFileExists( 
	/*__in*/ const wchar_t* inszPath 
	)
{
	if( inszPath == NULL )
	{
		return FALSE;
	}

	if( PathFileExists( inszPath ) )
	{
		return TRUE;
	}

	wstring strPath = inszPath;
	strPath = strPath + L"\\";

	if( PathFileExists( strPath.c_str() ) )
	{
		return TRUE;
	}

	return FALSE;
}
//-----------------------------------------------------------------------------

//! Checks if a path is a ROOT.
/*!
*	The function first attempts to see it the path is a root with shlwapi::PathIsRoot() using the input
*	path. If unsuccesfull it appends backslashes to the path to check if there could be a root named the same way as
*	the input parameter.
*
*	Return: 
*		TRUE for paths such as "\", "X:\" or "\\server\share", "\\?\c:". 
*		FALSE for paths such as "..\path2" or "\\server\".
*/
BOOL NFPathIsRoot( 
	/*__in*/ const wchar_t* inszPath 
	)
{
	if( inszPath == NULL )
	{
		return FALSE;
	}

	if( PathIsRoot( inszPath ) )
	{
		return TRUE;
	}

	wstring strPath = inszPath;
	strPath = strPath + L"\\";

	if( PathIsRoot( strPath.c_str() ) )
	{
		return TRUE;
	}

	return FALSE;
}
//-----------------------------------------------------------------------------

//! Creates the directory structure to the directory identified by the inszPath parameter
bool NFCreateDirectoryStructure( 
	/*__in*/ const wchar_t * inszPath, 
	/*__in*/ bool inbDeleteCreatedFolders //!< If set to true, the folders created by this function
												// will be deleted.

	)
{
	wstring path = inszPath;
	bool bRetValue = false;

	vector<wstring> dirs;	//holds all folder names
	wstring currentDir = L"";

	vector<wstring> vFoldersCreatedByThisFunction;

	if (path.substr(0, 4) == L"\\\\?\\"){
		TokenizeString(path.substr(4, path.size() - 4), L"\\", dirs);	
		currentDir = L"\\\\?\\";
	}
	else
	{
		TokenizeString(path, L"\\", dirs);
	}
	
	for(size_t i = 0;i<dirs.size();i++)
	{
		currentDir += dirs[i];

		//if (!isDrive(currentDir) && !pathExists(currentDir+L"\\")) 
		if (!NFPathIsRoot( currentDir.c_str() ) && !NFPathFileExists( currentDir.c_str() )) 
		{
			if (!::CreateDirectory(currentDir.c_str(), NULL))
			{
				LogError(0 , DBG_LOCATION, L"CreateDirectory('%s') failed.", currentDir.c_str());
				bRetValue = false;
				break; //exit the loop throu dirs
			}
			else
			{
				if( inbDeleteCreatedFolders )
				{//need to store the created folders here
					vFoldersCreatedByThisFunction.push_back( currentDir );
				}
			}
		}
		currentDir += L"\\";
	}
	bRetValue = true;

	if( inbDeleteCreatedFolders && (vFoldersCreatedByThisFunction.size() > 0) )
	{
		//delete all created folders
		for( size_t i = vFoldersCreatedByThisFunction.size() ; i > 0 ; i-- )
		{
			::RemoveDirectory( vFoldersCreatedByThisFunction[i-1].c_str() );
		}
	}

	return bRetValue;
}
//-----------------------------------------------------------------------------

//!Function NFGetFullPath() - returns the full path identified by inStrPath.
/*!
	uses GetfullPath
	prepends UNICODE prefix to the path if required.
	if the input path is a directory it ads \ to it

	The function allocates the buffer for the return path.

	The function does not validate the return path in any way.

**/
BOOL NFGetFullPath( 
	/*__in*/  const wchar_t* inszPath,
	/*__out*/ wchar_t* &outszFullPath,
	/*__in*/ bool inbAppendUnicodePrefix
	)
{
	wstring strFullPath = L"";

	if( inszPath == NULL )
	{
		return FALSE;
	}

	if( wcslen(inszPath) == 0 )
	{
		return FALSE;
	}

	wchar_t szTempPath[NF_MAX_PATH_UNICODE];
	unsigned long dwMaxCharacters = NF_MAX_PATH_UNICODE;

	unsigned long dwBytesNeeded = GetFullPathName( inszPath, dwMaxCharacters, szTempPath, NULL);

	if(  dwBytesNeeded == 0 )
	{
		return FALSE;
	}

	if( inbAppendUnicodePrefix )
	{
		strFullPath = L"\\\\?\\";
	}
	strFullPath = strFullPath + szTempPath;

	//allocate the reuturn buffer
	dwBytesNeeded = (unsigned long)( strFullPath.length() + 1 ) * sizeof(wchar_t);
	outszFullPath = (wchar_t*)malloc( dwBytesNeeded  );

	memcpy( outszFullPath, strFullPath.c_str(), dwBytesNeeded );

	return TRUE;
}
//-----------------------------------------------------------------------------

//!Function NFSartProcess() - starts the process identified by inszProcessPath.
BOOL NFSartProcess( 
	/*__in*/ const wchar_t* inszProcessPath,
	/*__in*/ const wchar_t* inszCommandLineParameters,
	/*__in*/ bool inbShowProcessWindow,
	/*__in*/ bool inbStartService
	)
{
	STARTUPINFO startupInfo;
    PROCESS_INFORMATION processInformation;
    ZeroMemory( &startupInfo, sizeof(startupInfo) );
    startupInfo.cb = sizeof(startupInfo);
    ZeroMemory( &processInformation, sizeof(processInformation) );

	wstring strStartCommand;
	StringBuffer* sbProcessDirectory = new StringBuffer();
	sbProcessDirectory->buffer = NULL;
	StringBuffer* sbCommandLine = NULL;

	if( !inbStartService )
	{
		//////	Start an application //////////////////////

		//Check input parameter for validity
		if( !NFPathFileExists( inszProcessPath ) )
		{
			//	NOTE: NFPathFileExists() also checks the input parameter for NULL.
			return FALSE;
		}

		//compose command line 
		strStartCommand = wstring (L"\"") + inszProcessPath + wstring(L"\"") + 
			(( inszCommandLineParameters != NULL ) ? ( wstring(L" ") + inszCommandLineParameters ) : wstring(L"") );

		//get the process directory
		if( !InitStringBuffer( sbProcessDirectory, inszProcessPath ) )
		{
			goto ERREXIT;
		}
		PathRemoveFileSpec( sbProcessDirectory->buffer );
	}
	else
	{
		//////	Start a service //////////////////////

		//compose the command line as L"net start " + inszProcessPath(service name)
		strStartCommand = wstring (L"net start \"") + inszProcessPath + wstring(L"\""); 
	}

	//transform it to StringBuffer so we do not have an const WCHAR*
	if( !InitStringBuffer( sbCommandLine, strStartCommand.c_str(), strStartCommand.length() ) )
	{
		goto ERREXIT;
	}

	//Show process window or not
	unsigned long dwCreationFlags = (inbShowProcessWindow ? 0 : CREATE_NO_WINDOW) ;

	if( !CreateProcess( 
			NULL,					// module name : maybe we need L"@start cmd /C ";
			sbCommandLine->buffer , // Command line
			NULL,					// Process handle not inheritable
			NULL,					// Thread handle not inheritable
			FALSE,					// Set handle inheritance to FALSE
			dwCreationFlags,		// No creation flags
			NULL,					// Use parent's environment block
			sbProcessDirectory->buffer,	// Use input path directory 
			&startupInfo,			// Pointer to STARTUPINFO structure
			&processInformation     // Pointer to PROCESS_INFORMATION structure
			)
    ) 
    {
		unsigned long dwError = GetLastError();
		goto ERREXIT;
    }

	//Success return
	AllocateOrReleaseStringBuffer( sbCommandLine, 0 );
	AllocateOrReleaseStringBuffer( sbProcessDirectory, 0 );
	return TRUE;

ERREXIT:
	AllocateOrReleaseStringBuffer( sbCommandLine, 0 );
	AllocateOrReleaseStringBuffer( sbProcessDirectory, 0 );
	return FALSE;

}
//-----------------------------------------------------------------------------

//!Function NFStopProcess() - stops the process identified by inszProcessPath.
BOOL NFStopProcess( 
	/*__in*/ wchar_t* inszProcessNameNoExtension
	)
{
	if( inszProcessNameNoExtension == NULL )
	{
		return FALSE;
	}


	wstring strKillCommand = L"@taskkill /F /IM \"";
	strKillCommand = strKillCommand + inszProcessNameNoExtension;
	strKillCommand = strKillCommand + L"\"";

	//Kill the task if it exists
	//TO DO: deal with return paramater
	int iRetCode = _wsystem( strKillCommand.c_str() );

	return TRUE;
}
//-----------------------------------------------------------------------------
