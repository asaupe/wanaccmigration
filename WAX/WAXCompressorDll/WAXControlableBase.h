//********************************************************/
//  ./WAXTControlableBase.h
//
//  Owner : AV / BH / CM
//  
//  Neverfail             ________________
//           PROJECT   :  WAN Acceleration
//           Component :  WAX Controlable Base
//
//  $Rev: 1694 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: cmaris $  
//  $Date: 2009-02-09 08:57:56 +0000 (Mon, 09 Feb 2009) $
//
//********************************************************/

//********************************************************/
//
//	WAX Controlable Base is the common ancestor for all
//	components that need to implement control methods.
//
//	This base class declares the common interfaces wich in
//	turn update the proper state flags for each component.
//
//	The control methods declared here are:
//		initialize(void)
//		exit(void)
//		allocate()
//		release(void)
//		start(bool inAsEncoder )
//		stop(void)
//
//	The state flags are:
//		m_isInitialized;	//!< Set to true between an initialize() and exit() call
//		m_isAllocated;		//!< Set to true between a allocate and a release call
//		m_isStarted;		//!< Set to true between a start and a stop call
//		m_isEncoder;		//!< Role of the current server - encoder / decoder
//
//********************************************************/

#pragma once

#include "WAXTypes.h"

//! Set/Get objects state
/*! The WAXControlableBase gets and sets the states of the instantiated
	objects derived from it.
*/
class WAXControlableBase
{
protected:
	WAXControlableBase() 
	{
		m_isAllocated = false;
		m_isEncoder   = false;
		m_isStarted   = false;
	}
public:
	//! Set the initialized flag to true
	/*!
		Set the initialized flag to true.
		Return: WAX_RESULT_OK
	*/
	virtual WAX_RESULT initialize(void)
	{
		m_isInitialized = true;
		return WAX_RESULT_OK;
	}

	//! Set the exit flag to true
	/*!
		Set the initialized flag to false.
		Return: WAX_RESULT_OK
	*/
	virtual WAX_RESULT exit(void){
		m_isInitialized = false;
		return WAX_RESULT_OK;
	}

	//! Set the allocate flag to true
	/*!
		Set the allocate flag to true.
		Return: WAX_RESULT_OK
	*/
	virtual WAX_RESULT allocate()
	{
		m_isAllocated = true;
		return WAX_RESULT_OK;
	}

	//! Set the release flag to true
	/*!
		Set the allocate flag to false.
		Return: WAX_RESULT_OK
	*/
	virtual WAX_RESULT release(void){
		m_isAllocated = false;
		return WAX_RESULT_OK;
	}

	//! Set the start flag to true
	/*!
		Set the start flag to true, the stopped flag to false and sets the
		"IsEncoder" flag to the given input value.

		Return: WAX_RESULT_OK
	*/
	virtual WAX_RESULT start(
		/*[IN]*/ bool inAsEncoder = false	//!< Specifies if the current object is used as Encoder or not
		){
		m_isStarted = true;
		m_isEncoder = inAsEncoder;
		return WAX_RESULT_OK;
	}

	//! Set the stop flag to true
	/*!
		Set the start flag to false.
		Return: WAX_RESULT_OK
	*/
	virtual WAX_RESULT stop(void){
		////	BZ 7530 - set the proper state flag for a STOPPED WAXControlable.
		m_isEncoder = false;	
		m_isStarted = false;
		return WAX_RESULT_OK;
	}

	//! Get the value of the initialized flag
	bool IsInitialized(void){return m_isInitialized;}

	//! Get the value of the allocate flag
	bool IsAllocated(void){return m_isAllocated;}

	//! Get the value of the start flag
	bool IsStarted(void){return m_isStarted;}

protected:
	
	//State flags - define the current state of the component
	bool m_isInitialized;	//!< Set to true between an initialize() and exit() call
	bool m_isAllocated;		//!< Set to true between a allocate and a release call
	bool m_isStarted;		//!< Set to true between a start and a stop call

	bool m_isEncoder;		//!< Role of the current server - encoder / decoder
};
