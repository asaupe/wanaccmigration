//********************************************************/
//  ./AllUtils.h
//
//  Owner : CM / AV / BH
// 
//	Company		: Neverfail ________________
//	PROJECT		: WAN Acceleration 
//  Component	: Utilities 
//	
//	Purpose: Headers for the utilites (File,etc.) used by the WAX compressor
//
//  $Rev: 2400 $ - Revision of last commit
//
//********************************************************/

//********************************************************/
//
// Last Change - Author and Date of last change:
// $Author: cmaris $  
// $Date: 2009-05-12 07:56:18 +0100 (Tue, 12 May 2009) $
//
//********************************************************/

//********************************************************/
//
//	AllUtils module will be used for implementing general purpose utilities throughout 
//	the WAX Compression Rpovider project.
//
//********************************************************/


#pragma once
#include <WaxTypes.h>
#include <windows.h>

#define ACCESS_FILE_READ  (GENERIC_READ | READ_CONTROL | ACCESS_SYSTEM_SECURITY)
#define ACCESS_FILE_WRITE (GENERIC_READ | GENERIC_WRITE | READ_CONTROL | WRITE_DAC | WRITE_OWNER | ACCESS_SYSTEM_SECURITY)

#define ACCESS_DIR_READ	  (READ_CONTROL | STANDARD_RIGHTS_READ | FILE_READ_ATTRIBUTES | FILE_READ_EA | ACCESS_SYSTEM_SECURITY)
#define ACCESS_DIR_WRITE (STANDARD_RIGHTS_WRITE | FILE_WRITE_ATTRIBUTES | FILE_WRITE_EA| READ_CONTROL | WRITE_DAC | WRITE_OWNER | ACCESS_SYSTEM_SECURITY)

#define SHARE_ALL		 (FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE)
#define SHARE_NONE		 0x00

#define FLAGS_ATTRIB_ALL (FILE_ATTRIBUTE_NORMAL | FILE_FLAG_BACKUP_SEMANTICS | FILE_FLAG_OPEN_REPARSE_POINT)

#define NF_MAX_PATH_UNICODE 32767 

//! Utility Wrapper over CreateFile(). 
/*!
*	Opens a file for reading or writing.
*	Return:	
*		NULL if the file can not be opened.
*		The file handle in case iof success.
*/
HANDLE NFOpenFileMax(
	/*__in*/ const wchar_t * inszFileName, 
	/*__in*/ unsigned long indwCreationDisposition, 
	/*__in*/ BOOL  inbOpen4Write,
	/*__in*/ unsigned long indwShareDisposition = SHARE_NONE, 
	/*__in*/ BOOL  inbDeleteOnClose = FALSE
	);

//! Checks the input file handle for NULL and INVALID_HANDLE_VALUE
BOOL NFIsValidFileHandle( 
	/*__in*/ HANDLE inhFileHandle 
	);

//! Sets the file pointer to the desired value.
/*!
*	Reason for implementing it, is not having a LARGE_INTEGER declaration where we need to resize
*	a file.
*/
BOOL NFSetFilePointer(
	/*__in*/ HANDLE		inhFile,			//!<
	/*__in*/ long long	inllDistanceToMove,	//!<
	/*__in*/ unsigned long		indwMoveMethod		//!<
	);

//! Writes a data buffer to the file identified by inhFile.
/*!
*	The function also check that all the bytes were written.
*	Returns FALSE if the write operation failed.
*/
BOOL NFWriteDataToFile(
	/*__in*/ HANDLE inhFile,
	/*__in*/ unsigned long	indwDataLen,
	/*__in*/ unsigned char* inlpbData 
	);

//! Writes a data buffer to the memory location identified by inDestination
/*!
*	Reason for implementing it, is the posibility of changing memcpy()
*	to another memory copy function that has better error checking.
*/
BOOL NFWriteDataToMem (
	/*__in*/ void* inDestination, 
	/*__in*/ size_t innDataLen, 
	/*__in*/ void* inSource 
	);

//! Reads a data buffer from the file identified by inhFile
/*!
*	Reads a number of bytes equal to indwDataLen from the offset inFileOffset of the file
*	identified by inhFile.
*	
*	Returns FALSE if it can not set the file pointer, or not all bytes requested could be read.
*	
*	NOTE[1]	The output buffer outbData must be allocated by the caller.
*	NOTE[2]	In order for the function to succeeded the input file handle must be valid.
*/
BOOL NFReadDataFromFile(
	/*__in*/ HANDLE inhFile,
	/*__in*/ long long inFileOffset,
	/*__in*/ unsigned long indwDataLen,
	/*__out*/ unsigned char* outbData 
	);

//! Initializes security for current process
BOOL NFInitSecurity(void);

//!Sets the size of an opened file. Resets its contents if requested.
/*!
*	The function sets the size of an opened file. Resets its contents if requested.
*	In order for the function to succeed the file handle must be valid. 
*	Return: 
*		FALSE if either SetFilePonter or SetEndOfFile(where the case) failed.
*		TRUE if everything succeeded.
*/
BOOL NFResizeFile(
	/*__in*/ HANDLE		inhFile,					//!<
	/*__in*/ long long	inllNewFileSize,			//!<
	/*__in*/ BOOL		inbResetContent  = FALSE,	//!<
	/*__in*/ BOOL		inbGoToFileBegin = FALSE
	);

///////////////////////////////////////////////////////////////////////////////////////////////////
////////		Statistics and Execution Times Utilities	///////////////////////////////////////

//! Convertes the CPU Frequency returned by QueryPerformanceFrequency() to ULONGLONG
uint64_t getCPUFrequency(void);

//!	Converts the return of QueryPerformanceCounter() to ULONGLONG
uint64_t getTimeNowByQueryPerformanceCounter(void);

//! Converts the hour, minute, second and millisecond portion of a SYSTEMTIME structure to Milliseconds.
uint64_t SystemTimeToMiliSeconds(
	/*__in*/ const SYSTEMTIME& systemTime
	);

//! Computes the CPU time and App Run time in MiliSeconds for the current process by calling GetProcessTimes()
void getCPUAndRUNTimesInMS(
	/*__out*/ uint64_t &outullCPUTime,
	/*__out*/ uint64_t &outullRUNTime 
	);

//! Retreives the current time as elapsed time between 1601-01-01 between start and stop file times in MiliSeconds.
BOOL getCurrentFileTime( FILETIME *outFiletime );

//! Computes the elapsed time between start and stop file times in MiliSeconds.
uint64_t getElapsedFileTimeMS( FILETIME inftStart, FILETIME inftStop );


////////		END Statistics and Execution Times Utilities	///////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

//!	Reads the elngth of the buffer and the buffer data from the file named inszFileName.
/*!
*	Reads a buffer represented by outlpbBuffer from the file named inszFileName.
*	It also reads the length of the buffer from the the first sizeof(DWORD) bytes in the file to the output parameter outdwBufferLen.
*	The output buffer has to be allocated by the caller.
*	
*	The function return TRUE if all operations succeded, and FALSE if any operation fails.
*/
BOOL UtilReadBufferFromFile(
	/*__in*/ wchar_t* inszFileName,
	/*__out*/ unsigned char* &outlpbBuffer,
	/*__out*/ unsigned long &outdwBufferLen
	);

//! Writes the buffer length and buffer data to the file named inszFileName.
/*!
*	Writes the buffer represented by inlpbBuffer to the file named inszFileName.
*	It writes the length of the buffer as the first sizeof(DWORD) bytes.
*	If inbAppend is false, the destination file is overwrittent. Otherwise content is appended to it.
*	
*	The function return TRUE if all operations succeded, and FALSE if any operation fails.
*/
BOOL UtilWriteBufferToFile(
	/*__in*/ const wchar_t* inszFileName,
	/*__in*/ unsigned char* inlpbBuffer, 
	/*__in*/ unsigned long indwBufferLen, 
	/*__in*/ bool inbAppend = false
	);

//! Gets the minimum size of a large page by calling SIZE_T WINAPI GetLargePageMinimum(void);
/*! The function gets the minimum size of a large page by calling SIZE_T WINAPI GetLargePageMinimum(void);
*	This re-write is necessary since GetLargePageMinimum() is not supported in windows XP.
*	Return:
*		On windows XP - 2 * MEGA
*		On later windows versions the size returned by GetLargePageMinimum()
*	
*/
SIZE_T NFGetLargePageMinimum(void);

//!	Safely copies the source string to the destination address.
/*!	
*	The function copies the source string to the destination address:
*		If the input string is NULL, it will set the destination string to L"\0"
*
*	Return: the number of copied characters and ZERO if nothing was copied. 
*
*	NOTE: The method asumes the destination buffer is allocated and has enough space to 
*		store all the characters in the source.
*/
unsigned long UtilSafeCopyString( 
	/*__inout*/		wchar_t* inoutszDestination, 
	/*__in*/ const	wchar_t* inszSource,
	/*__in*/ unsigned long  inulCharactersToCopy = 0	//! if zero, the length of the input string is determined by the method.
	);

//! Allocates or releases a byte buffer according to input parameters.
/*!
*	The function allocates or releases a byte buffer according to input parameters:
*		If the buffer is NULL, and the length is non zero it will be allocated.
*		If the buffer is NON null, it will be re-allocated to match the new length
*		If the buffer is allocated and the input length is zero, it will be deleted.
*	Return:
*		false - if the buffer can not be allocated.
*		true  - otherwise.
*/
bool AllocateOrReleaseByteBuffer(
	/*__inout*/ ByteBuffer* &inoutbBuffer, 
	/*__in*/ unsigned long inulBufferLength 
	);

//! Allocates or releases a string buffer according to input parameters.
/*!
*	The function allocates or releases a string buffer according to input parameters:
*		If the buffer is NULL, and the length is non zero it will be allocated.
*		If the buffer is NON null, it will be re-allocated to match the new length
*		If the buffer is allocated and the input length is zero, it will be deleted.
*	Return:
*		false - if the buffer can not be allocated.
*		true  - otherwise.
*/
bool AllocateOrReleaseStringBuffer(
	/*__out*/ StringBuffer* &outSBuffer,//!< Buffer to be allocated/deleted 
	/*__in*/ unsigned short inusBufferLength	//!< If set to 0 the object is deleted.
	);

//! Copy-constructor for StringBuffer
/*!
*	The function allocates space to hold the character string and copies it to the 
*	StringBuffer->buffer member.
**/
bool InitStringBuffer( 
	/*__out*/ StringBuffer* &outStringBuffer,
	/*__in*/ const wchar_t* inszSource,
	/*__in*/ size_t inusLength = 0 //!Number of WCHAR characters without the terminating string.
	);

//! Creates the directory structure to the directory identified by the inszPath parameter.
/*!
*	The function creates the on disk directory structure identified by the input parameter.
*
*	Return:
*		true  - if the on disk directory structure already exists or it was succesfuly created.
*		false - otherwise.
**/
bool NFCreateDirectoryStructure( 
	/*__in*/ const wchar_t * inszPath, 
	/*__in*/ bool inbDeleteCreatedFolders = false	//!< If set to true, the folders created by this function
												// will be deleted.
	);

//! Checks if a path is a file or directory exists.
/*!
*	The function first attempts to find the file/dir with sglwapi::PathFileExists() using the input
*	path. If unsuccesfull it appends backslashes to the path to check if there could be a directory named the same way as
*	the input parameter.
*
*	Return: True if the file or directory were found.
*/
BOOL NFPathFileExists( 
	/*__in*/ const wchar_t* inszPath 
	);

//! Checks if a path is a ROOT.
/*!
*	The function first attempts to see it the path is a root with shlwapi::PathIsRoot() using the input
*	path. If unsuccesfull it appends backslashes to the path to check if there could be a root named the same way as
*	the input parameter.
*
*	Return: 
*		TRUE for paths such as "\", "X:\" or "\\server\share", "\\?\c:". 
*		FALSE for paths such as "..\path2" or "\\server\".
*/
BOOL NFPathIsRoot( 
	/*__in*/ const wchar_t* inszPath 
	);

//!Function NFGetFullPath() - returns the full path identified by inStrPath.
BOOL NFGetFullPath( 
	/*__in*/  const wchar_t* inszPath,
	/*__out*/ wchar_t* &outszFullPath,
	/*__in*/ bool inbAppendUnicodePrefix = false
	);

//!Function NFSartProcess() - starts the process identified by inszProcessPath.
BOOL NFSartProcess( 
	/*__in*/ const wchar_t* inszProcessPath,
	/*__in*/ const wchar_t* inszCommandLineParameters = NULL,
	/*__in*/ bool inbShowProcessWindow = false,
	/*__in*/ bool inbStartService = false
	);

//!Function NFStopProcess() - stops the process identified by inszProcessPath.
BOOL NFStopProcess( 
	/*__in*/ wchar_t* inszProcessPath
	);
