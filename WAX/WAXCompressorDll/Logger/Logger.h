//********************************************************/
//  ./Logger/Logger.h
//
//  Owner : Ciprian Maris
// 
//	Company		: Neverfail 
//	PROJECT		: WAN Acceleration 
//  Component	: Logger
//
//  $Rev: 2489 $ - Revision of last commit
//
/********************************************************
//
// Last Change - Author and Date of last change:
// $Author: cmaris $  
// $Date: 2009-05-26 11:01:41 +0100 (Tue, 26 May 2009) $
//
//********************************************************/

//********************************************************/
//
//	This Unit contains:
//		- The Logger class - the logging mechanism for the compressor.
//		- The log level definitions
//		- Tracing macros - Macros that represent the exact location in the source files.
//
//	NOTE:	[1] This implementation is a spin-off from the Logger used in DRM.
//			Those sources were changed hers to adapt to the logging requirements of WAX.
//			There are also some perfromance optimisations.
//
//********************************************************/

#pragma once
#include "WAXTypes.h"
#include "../Common.h"

#include <time.h>
#include <sys/timeb.h>
#include <string>


using namespace std;

typedef int		log_level;

#define LOG_LEVEL_NOTHING	0x0000
#define	LOG_LEVEL_ERROR		0x0001
#define	LOG_LEVEL_INFO		0x0002
#define	LOG_LEVEL_DEBUG		0x0004
#define LOG_LEVEL_ALL		0xffff

////////////////////////////////////////////////////////////////////////////////////////////////////
//	Helper macros /////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

#define GEN_EVAL(X) X
#define GEN_STRINGIZE_ARG(X) #X
#define GEN_STRINGIZE(X) GEN_EVAL(GEN_STRINGIZE_ARG(X))
#define GEN_MERGE(A, B) A##B
#define GEN_MAKE_W(A) GEN_MERGE(L, A)

#define GEN_WSTRINGIZE(X) GEN_MAKE_W(GEN_STRINGIZE_ARG(X))

#define __WFILE__ GEN_MAKE_W(GEN_EVAL(__FILE__))
#define __WFUNCTION__ GEN_MAKE_W(GEN_EVAL(__FUNCTION__))
#define __WLINE__ GEN_MAKE_W(GEN_EVAL( __LINE__ ))
#define _LINE_WIDE_ GEN_WSTRINGIZE(__LINE__) 

#define __WFILE__ GEN_MAKE_W(GEN_EVAL(__FILE__))
#define __WFUNCTION__ GEN_MAKE_W(GEN_EVAL(__FUNCTION__))

//	Macros that represent the exact location in the source files (provided for exact tracing).
#define DBG_INFO __WFILE__ , __LINE__, __WFUNCTION__ //localisation info to be passed from failing.
#define DBG_LOCATION L"File: " __WFILE__ L" ,Function: " __WFUNCTION__ L" ,Line: " _LINE_WIDE_
#define DBG_MESSAGE  L"Runtime Error in " DBG_LOCATION


////////////////////////////////////////////////////////////////////////////////////////////////////
//	END Helper macros //////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////
/////////	Exported LOGGER Functions to be used for logging		
/*
	The following functions and their equivalent macros will ve used by WAX components for logging.
	All these functions provide support for formated logging, in order to accomodate the logging of
	generic text.
**/

//!	Logs an event passed as message string and from the component ID.
extern void LogEventMessage(
	log_level inLevel,
	WAX_ID inwiComponentID,
	wchar_t* inszMessage
	);

//!	Logs a generic event also providing the component ID.
extern void LogEventFormated(
	log_level inLevel,
	WAX_ID inwiComponentID,
	wchar_t* inszFormat,
	...
	);

//!	Logs an error providing the component ID and the location where the error occurred.
extern void LogError(
	WAX_ID inwiComponentID,
	wchar_t* inszLocation,
	wchar_t* inszFormat,
	...
	);

//!	Logs a debug message providing the component ID and the location.
extern void LogDebug(
	WAX_ID inwiComponentID,
	wchar_t* inszLocation,
	wchar_t* inszFormat,
	...
	);
/////////		
////////////////////////////////////////////////////////////////////////////////////////////////////

//!	Logger - The Logger class implements the logging mechanism for the WAX Compressor.
/*! \class Logger
*  \brief Implements the logging mechanism for the WAX Compressor.
*
*	The class has all its members and methods static. 
*
*  Is called by all WAX components (classes). It is not called by the WAXCompressor.
*
*   \author Alex Naparu, Ciprian Maris
*   \version 1.0.$Rev: 2489 $
*   \date    SEP-2008
*/
class Logger
{
	public:

		///////////////////////////////////////////////////////////////////////////////////////////
		///////		Control Methods			///////////////////////////////////////////////////////
		
		static bool ComposeNameAndInitialize(
			/*__in*/ const wchar_t* logFileName,	//!< The name of the log file passed to the logger.
			/*__in*/ const wchar_t* defaultLogFileName,
			/*__in*/ const wchar_t* logNameAddon,
			/*__in*/ bool  inbSetDebugMode,		//!< The initial debug mode of the logger.
			/*__out*/ wstring &strFailureReason	//!< Set by this method to inform the caller on the cause of the failure.
			);

		static bool Init(wstring fileName);
		static void Close(void);
		static void Flush(void);
		///////////////////////////////////////////////////////////////////////////////////////////

		
		///////////////////////////////////////////////////////////////////////////////////////////
		///////		Logging Methods			///////////////////////////////////////////////////////
		
		//! Method Log() - perfroms the actual logging (writing to file).
		static void Log(
			log_level inLogLevel,	//!< Log level of the current event.
			const wchar_t* component,	//!< Identifier of the component perfroming the logging as string. 
			const wchar_t* message	//!< message to be logged.
			);

		//! Method LogMessageFromComponentID() - logs an event from the component identified by WAX ID.
		static void LogMessageFromComponentID(
			log_level inLevel,		//!< Log level of the current event.
			WAX_ID inwiComponentID,	//!< Identifier of the component perfroming the logging as WAX component ID. 
			wchar_t* inszLocation,	//!< Event location. For exact event location the DBG_LOCATION macro can 
									//	be used to be passed as this parameter.
			wchar_t* inszMessage		//!< message to be logged.
			);
		///////////////////////////////////////////////////////////////////////////////////////////

		///////////////////////////////////////////////////////////////////////////////////////////
		///////		Getters - Setters		///////////////////////////////////////////////////////

		static bool IsInitialized(void)			{ return m_bIsInitialized; }

		static void SetLogLevel(log_level level){ m_logLevel = level; }

		static log_level GetLogLevel(void)		{ return m_logLevel; }

		static void SetNumberOfLogFiles(long inlNumberOfLogFiles) ;

		static long GetNumberOfLogFiles(void)	{ return m_maxNumberOfLogFiles; }

		//! Return true if the Logger is initialized and the requested log level is bigger than the Logger::logLevel
		static bool IsOkToLog( log_level inLogLevel )
		{
			return m_bIsInitialized && ( m_logLevel & inLogLevel );
		}

		//! Retreives the error message collection (trace) content.
		static const wchar_t* GetErrorInfo(void) 
		{ 
			return (const wchar_t*)m_wsLastErrorInformation.c_str();  
		}
		///////////////////////////////////////////////////////////////////////////////////////////

		//! Resets the error message collection (trace) to its default value.
		static void ResetErrorInfo(void);

	private:
		static bool m_bIsInitialized;		//!< Flag set to true if the Logger was initialized correctly.

		static wstring m_logFileName;		//!< File name currently used for logging in the format:
											//	FileNameBase + CurrentDate + logIndex + Extension

		static wstring m_logFileNameBase;	//!< Base file name from which the m_logFileName is composed.

		static FILE* m_filePointer;			//!< File pointer of the log file.
		//static int m_logFileHandle;			//!< File descriptor of the log file.

		static log_level m_logLevel;		//!< Maximum log level for which events will be logged.

		static int m_linesLogged;			//!< Number of lines written to the current logfile.
		static int m_autoCommitInterval;	//!< Commit after specified number of lines.

		static int m_maxNumberOfLogFiles;	//!< Maximum number of log files that will be left on disk.
		static size_t m_fileSize;			//!< File size of currently used log file.

		//! The error message collection (trace) stores the entire history of error messages from the 
		//	method that caused the failure to the initial caller.
		//
		//  E.g: if allocate() is called and OpenFile() fails, this variable will contain the following:
		//		- The starting error string is LOG_ERR_INFO_DEFAULT + 
		//		- the Error Message from AllUtils / NFOpenFileMax() +
		//		- the Error Message from StoreManager::allocate() +
		//		- the Error Message from WAXInterface::allocate(). 
		//
		//	This variable is initialized by every call to the WAX compressor. The lifetime of the 
		//	trace is from one WAX compressor call to the next.
		//
		//	If any of the WAX Compressor methods fail, the caller is expected to call getErrorInfo()
		//	to retrieve the detailed information on the failure contained in this variable.
		static wstring m_wsLastErrorInformation;//!< Error message collection (trace) for the last error.

		//!Method getLogFileName() - Determines the file name for loggging.
		static void getLogFileName(
			/*IN*/	wstring pstrFileNamePath,	//!< Base file name for the logging file with full or relative directory path.
			/*OUT*/ wstring& pstrLogFileName	//!< outout log file name in the format: 
												//	FileNameBase + CurrentDate + logIndex + Extension
			);

		//! Method openLogFile() - Opens the file identified by the class member m_logFileName for logging.
		static void openLogFile(void);


		///////////////////////////////////////////////////////////////////////////////////////////
		///////		Utility Methods  		///////////////////////////////////////////////////////
		
		//! Converts a numeric log level to its equivalent string value.
		static void log_level2wstring(
			log_level inLogLevel,	//!< Input numeric log level to be converted to string.
			wstring& outString		//!< Outout string representing the requested log level.
			);

		//! Converts a long value to its equivalent string value.
		static void long2wstring(
			long inlValue,			//!< Input long value to be converted to string.
			wstring& outString		//!< Outout string representing the requested long value.
			);
		//! Converts a _timeb date-time value to its equivalent string value.
		static void Logger::timeb2wstring(
			_timeb inDateTime,	//!< [in]	date-time value to be converted to string.
			wstring& outString	//!< [out]	string representing the requested date-time value.
			);

		//!	Method getBaseAndExtensionForFileName() - Extracts the path, base name and extension of an filename.
		static void getBaseAndExtensionForFileName(
			const wstring &pstrFileName,//!< [in] log file name (path+base+ext)
			wchar_t* pszBase,				//!< [out] base name - defined as the path + filename without extension
			wchar_t* pszExtension,		//!< [out] extension
			wchar_t* pszPath				//!< [out] path to the file (directory structure)
			);

		//!	Method listFiles() - Lists the files names matching a rule into a vector.
		static void Logger::listFiles( 
			const wstring& pstrSearchRule,	//!< [in]	Rule by which the files are be searched.
			vector <wstring>&pvFileList		//!< [out]	Vector containing the list of found files.
			);

		//!	Method deleteOldLogsFiles() - Search and delete old log files that match the file name base.
		static void deleteOldLogsFiles( 
			const wstring &pstrFileNameBase,//!< [in] Filename base (including path) of the log files to be deleted.
			long plNrLeft					//!< [in] number of log files to be left on disk.
			);

		//!	Method getLogIndex() - Extracts the log index out of the log filename.
		static int Logger::getLogIndex(
			const wstring &pstrLogFileName //! [in] log file name to be analysed
			);

		//!	Method compareFileNames() - Compares two file log names from the WAX::Logger perspective.
		static int Logger::compareFileNames( 
			const wchar_t* pName1,	//!< [in] First file name to be compared.
			const wchar_t* pName2		//!< [in] Second file name to be compared.
			);
		///////////////////////////////////////////////////////////////////////////////////////////

		Logger()	{ }
		~Logger()	{ }
};
