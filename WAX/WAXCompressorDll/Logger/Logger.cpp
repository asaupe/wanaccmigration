//********************************************************/
//  ./Logger/Logger.cpp
//
//  Owner : Ciprian Maris
//
//	Company		: Neverfail
//	PROJECT		: WAN Acceleration
//  Component	: Logger
//
//  $Rev: 2489 $ - Revision of last commit
//
/********************************************************
//
// Last Change - Author and Date of last change:
// $Author: cmaris $
// $Date: 2009-05-26 11:01:41 +0100 (Tue, 26 May 2009) $
//
//********************************************************/

#include "Logger.h"

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/timeb.h>
#include <io.h>
#include <stdio.h>
#include <stdarg.h>

#pragma warning(disable:4995)
#pragma warning(disable:4996)
#include "shlwapi.h"

#define AUTO_COMMIT_DEFAULT	1 //1024 //Changed to 0 for perfromance/internal testing.
#define AUTO_COMMIT_NEVER	0
#define MAX_FILE_SIZE		10485760 //6 //0

#define MAX_LOG_FILES_DEFAULT 10
#define LOG_FILES_MIN 5

//#define LOG_FILE_HANDLE_INVALID -1

#define LOG_LOCATION_UNKNOWN L"UNKNOWN"
#define LOG_LOCATION_EMPTY L" "

#define MAX_PATH_UNICODE 32767 // maximum path length for UNICODE paths
#define LOGGER_BUFF_LEN MAX_PATH_UNICODE
#define LOG_MESSAGE_MAXLEN	LOGGER_BUFF_LEN

#define NUM_BUF_LEN 1024

//!	ERR_LOG_INFO_DEFAULT is the starting string of the error trace
//	\sa Logger::m_wsLastErrorInformation for details
#define LOG_ERR_INFO_DEFAULT L"\n"

//! The default extension for the log file name if the caller of init did not provide one
#define LOG_DEFAULT_FILE_EXTENSION L".log"
size_t  LOG_DEFAULT_LEN_FILE_EXTENSION = wcslen(LOG_DEFAULT_FILE_EXTENSION);
///////////////////////////////////////////////////////////////////////////////////////////
///////		Initialization of the static components   /////////////////////////////////////
wstring Logger::m_logFileName;
FILE*	Logger::m_filePointer = nullptr;
wstring Logger::m_logFileNameBase = L"";
//int Logger::m_logFileHandle = LOG_FILE_HANDLE_INVALID;
log_level Logger::m_logLevel = LOG_LEVEL_INFO | LOG_LEVEL_ERROR;
int Logger::m_linesLogged = 0;
int Logger::m_autoCommitInterval = AUTO_COMMIT_DEFAULT;
size_t Logger::m_fileSize = 0;
int Logger::m_maxNumberOfLogFiles = MAX_LOG_FILES_DEFAULT;
bool Logger::m_bIsInitialized = false;
wstring Logger::m_wsLastErrorInformation = LOG_ERR_INFO_DEFAULT;//!< Error message collection (trace) for the last error
///////////////////////////////////////////////////////////////////////////////////////////

//	Predefined buffers used in this unit.
wchar_t szLogBuffer[LOGGER_BUFF_LEN];	//!< Log buffer for formated output.
wchar_t szNumBuffer[NUM_BUF_LEN];		//!< Buffer for numeric values conversions.


/*****************************************************************************/
/////////		Logger Class Implementation		///////////////////////////////
/*****************************************************************************/


//! Method Log() - perfroms the actual logging (writing to file).
/*!
*	The method is the only one perfroming the actual logging (writing to the log file).
*
*	All other methods in this unit will end up calling it.
*
*	It will also append ERROR_LEVEL messages to m_wsLastErrorInformation.
*
**/
void Logger::Log(
	log_level inLogLevel,	//!< Log level of the current event.
	const wchar_t* component,//!< Identifier of the component perfroming the logging as string.
	const wchar_t* message	//!< message to be logged.
	)
{
	if (!(inLogLevel & m_logLevel))
	{
		return;
	}

	_timeb logTimeNow;	//Date-Time NOW - the time of the logged event
	_ftime(&logTimeNow);

	wstring logMessage = L"";
	wstring dummy;

	timeb2wstring(logTimeNow, dummy);
	logMessage += dummy + L" ";

	log_level2wstring(inLogLevel, dummy);
	logMessage += dummy + L" ";

	long2wstring(clock(), dummy);
	logMessage += dummy;

	logMessage.append(L" [");
	logMessage.append(component);
	logMessage.append(L"] - ");
	logMessage.append(message);
	logMessage.append(L"\n");
	
	if ( m_filePointer == nullptr )
	{
		openLogFile();
	}

	if (m_filePointer != nullptr )
	{
		unsigned long msgLen = (unsigned long)logMessage.size();

		fwprintf( m_filePointer, logMessage.c_str() );

		//TO DO: consider removing this?
		if( inLogLevel & LOG_LEVEL_DEBUG )
		{//also print on console in DEBUG mode
			wprintf( logMessage.c_str() );
		}

		m_linesLogged++;

		if ( (m_autoCommitInterval > 0) && (m_linesLogged >= m_autoCommitInterval) )
		{
			fflush( m_filePointer );
			m_linesLogged = 0;
		}

		//increase the file size by the number of characters written
		m_fileSize+= msgLen;

		if( m_fileSize > MAX_FILE_SIZE )
		{//change log file since the current one is full
			Logger::Init( m_logFileNameBase );
		}
	}

	if( inLogLevel & LOG_LEVEL_ERROR )
	{//	Append the current error message to the trace.
		m_wsLastErrorInformation += L"\t" + logMessage ;
	}
}
//-----------------------------------------------------------------------------

//! Method LogMessageFromComponentID() - logs an event from the component identified by WAX ID.
/*!
*	The method
*		Logs an event from the component identified by WAX ID.
*		Has the location parameter for exact event location tracting. The recommended value for this
*		parameter is the DBG_LOCATION macro.
*
*		Calls Log() to perfrom the actual logging.
**/
void Logger::LogMessageFromComponentID(
	log_level inLevel,		//!< Log level of the current event.
	WAX_ID inwiComponentID,	//!< Identifier of the component perfroming the logging as WAX component ID.
	wchar_t* inszLocation,	//!< Event location. For exact event location the DBG_LOCATION macro can
							//	be used to be passed as this parameter.
	wchar_t* inszMessage		//!< message to be logged.
	)
{
	if (! (inLevel & m_logLevel ))//Only log the events that have the proper log level set
	{
		return;
	}

	wstring wsErrorLocation = L"Component ID: ";
	_itow( inwiComponentID, szNumBuffer, 10 );
	wsErrorLocation += szNumBuffer ;
	if( wcscmp(inszLocation, L" " ) )
	{
		wsErrorLocation += L" " ;
		wsErrorLocation += inszLocation ;
	}

	Log(inLevel, wsErrorLocation.c_str(), inszMessage);
}
//-----------------------------------------------------------------------------


///////////////////////////////////////////////////////////////////////////////////////////////////
///////		Control Methods			///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

/*!
*	This method was implemented as a resolution to BZ 8261.
*	The IPC component will call this method when running out of precess (x86).
*	The NfWaxHB unit will call this method when running in of precess (x64).
**/
bool Logger::ComposeNameAndInitialize(
	/*__in*/ const wchar_t* logFileName,		 //!< The name of the log file passed to the logger.
	/*__in*/ const wchar_t* defaultLogFileName,//!< This parameter can not be empty or NULL
	/*__in*/ const wchar_t* logNameAddon,		 //!< This parameter can be empty but not NULL
	/*__in*/ bool  inbSetDebugMode,			 //!< The initial debug mode of the logger.
	/*__out*/ wstring &strFailureReason		 //!< Set by this method to inform the caller on the cause of the failure.
										 // It is only set when the method returns false.
	)
{
	if( defaultLogFileName == NULL )
	{
		strFailureReason = L"defaultLogFileName is NULL";
		return false;
	}

	if( logNameAddon == NULL )
	{
		strFailureReason = L"logNameAddon is NULL";
		return false;
	}

	//IF the input log file name is NULL we still want to be able to log.
	if( logFileName == NULL )
	{
		logFileName = defaultLogFileName;
	}

	//Allocate buffer for log name.
	wchar_t* szLoggerPath = (wchar_t*)malloc( ( wcslen( logFileName ) + wcslen( logNameAddon ) + 1 ) * sizeof(wchar_t) ) ;
	if( szLoggerPath == NULL )
	{
		strFailureReason = L"Log File Name allocation failed";
		return false;//return false since we did fail to allocate the name buffer.
	}

	//copy log name to the IPC Client log name.
	wcscpy( szLoggerPath, logFileName );

	wstring strNewExtension = logNameAddon;
	strNewExtension.append( PathFindExtension( szLoggerPath ) );

	if (! PathRenameExtension( szLoggerPath, strNewExtension.c_str() ) )
	{
		strFailureReason = L"PathRenameExtension() failed";
		goto ERREXIT;
	}

	if (! Logger::Init( szLoggerPath ) )
	{
		strFailureReason = L"IPC Logger::Init() failed";
		goto ERREXIT;
	}

	//Set the debug mode to the desired value.
	if ( inbSetDebugMode )
	{
		Logger::SetLogLevel(Logger::GetLogLevel() | LOG_LEVEL_DEBUG);
	}
	else
	{
		Logger::SetLogLevel(Logger::GetLogLevel() & ~LOG_LEVEL_DEBUG);
	}

	//Cleanup and return true;
	if( szLoggerPath != NULL )
	{
		free (szLoggerPath);
		szLoggerPath = NULL;
	}

	return true;

ERREXIT://Cleanup and return false;
	if( szLoggerPath != NULL )
	{
		free (szLoggerPath);
		szLoggerPath = NULL;
	}

	return false;

}

bool Logger::Init(
	wstring fileName
	)
{
	//check if we have a non empty name for the log.
	if( fileName.size() == 0 )
	{
		return false;//no name was provided for the logger.
	}

	wstring strLogFileName;
	getLogFileName( fileName, strLogFileName );

	//check and delete old log files
	Flush(); //flush and close curent file
	Close();

	deleteOldLogsFiles( fileName, m_maxNumberOfLogFiles );//delete old log files

	//open the file
	m_logFileName = strLogFileName;
	openLogFile();
	if( m_filePointer != nullptr )
	{
		m_bIsInitialized = true;
	}
	else
	{
		m_bIsInitialized = false;
	}

	//set the base of the log filename
	if( m_logFileNameBase.length() == 0 )
	{
		m_logFileNameBase = fileName;
	}

	return m_bIsInitialized;
}
//-----------------------------------------------------------------------------

void Logger::Close(void)
{
	if (m_filePointer != nullptr)
	{
		fflush( m_filePointer );

		fclose( m_filePointer );
		m_filePointer = nullptr;
	}
}
//-----------------------------------------------------------------------------

void Logger::Flush(void)
{
	if( m_filePointer != nullptr )
	{
		m_linesLogged = 0;
		fflush( m_filePointer );
	}
}
//-----------------------------------------------------------------------------

//!Method getLogFileName() - Determines the file name for loggging.
/**
*	The medhod 
*		Determines the file name for logging in the format 
*			FileNameBase + CurrentDate + logIndex + Extension
*		Selects the last file created in this format (if it already exists), or composes a new file name.
*		Eg.
*			pstrFileNamePath = 'WAXLog.log'
*			current date = 2008.09.12
*			last opened file index for current date = 2
*			resulted filename is WAXLog20080912_2.log
*/
void Logger::getLogFileName(
	wstring pstrFileNamePath,	//!< Base file name for the logging file with full or relative directory path.
	wstring& pstrLogFileName	//!< out log file name in the format: FileNameBase + CurrentDate + logIndex + Extension
	)
{
	wstring strNameSearchRule,		//buffer for search rule
			strLastFileOpened;		//last log file opened
	vector <wstring> vFileNames;	//list of all files matching criteria

	wchar_t szDateBuffer [MAX_PATH];		//
	wchar_t szBaseBuffer [MAX_PATH_UNICODE];
	wchar_t szExtBuffer  [MAX_PATH];
	wchar_t szDirBuffer  [MAX_PATH_UNICODE];

	int iIndex = 1;//index of the log file to be used
	pstrLogFileName.clear();

	getBaseAndExtensionForFileName( pstrFileNamePath, szBaseBuffer, szExtBuffer, szDirBuffer);

	//compose file name to be used as search rule
	_timeb dtNow;//cet current date-time
	_ftime(&dtNow);

	wcsftime(szDateBuffer, MAX_PATH, L"%Y%m%d", localtime(&dtNow.time));

	strNameSearchRule = szBaseBuffer;
	strNameSearchRule.append(szDateBuffer);
	strNameSearchRule.append(L"_*");
	strNameSearchRule.append( szExtBuffer );

	listFiles(strNameSearchRule, vFileNames);

	if( vFileNames.size() )
	{
		strLastFileOpened = vFileNames[ vFileNames.size() - 1 ];

		if ( m_fileSize < MAX_FILE_SIZE )
		{//use the old file
			pstrLogFileName = szDirBuffer;
			pstrLogFileName.append( strLastFileOpened );
		}
		else
		{//use the next index
			//determine the current (last) index
			iIndex = getLogIndex( strLastFileOpened ) + 1;
		}
	}

	if( pstrLogFileName.empty() )
	{
		wstring strIndex;
		long2wstring( iIndex, strIndex );

		pstrLogFileName.append( szBaseBuffer );
		pstrLogFileName.append( szDateBuffer );
		pstrLogFileName.append(L"_");
		pstrLogFileName.append( strIndex );
		pstrLogFileName.append( szExtBuffer );
	}
}
//-----------------------------------------------------------------------------

//! Method openLogFile() - Opens the file identified by the class member m_logFileName for logging.
/*!
*	The method opens the file identified by the class member m_logFileName for logging.
*
*	If the file open procedure succeeds
*		- it sets the file handle m_logFileHandle value 
*		- updates the m_bIsInitialized flag
*		- sets the file pointer to the end of the file
*		- setts the m_fileSize value
*/
void Logger::openLogFile(void)
{
	m_filePointer = _wfopen(m_logFileName.c_str(), L"a+");
	if( !m_filePointer )
	{//If it comes to this (filePointer = NULL)...logging can not be performed.
		m_bIsInitialized = false;
	}

	m_fileSize = 0;//initialize file size

	if (m_filePointer != nullptr)
	{
		fseek(m_filePointer, 0L, SEEK_END);

		//Determine the current file size of the log file.
		long filePointerPosition = ftell( m_filePointer );//get the pointer position
		m_fileSize = (filePointerPosition != -1) ? filePointerPosition : 0;
	}
}
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////////////////
///////		Utility Methods  		///////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////

//!	Method getLogIndex() - Extracts the log index out of the log filename.
/*!
*	Extracts the log index out of the log filename.
*
*	Returns the extracted index
*		E.g. file name 'log20050922_11.log' => index 11
*
*/
int Logger::getLogIndex(
	const wstring &pstrLogFileName //! [in] log file name to be analysed
	)
{
	wchar_t szIndex	   [MAX_PATH]; //buffer for index
	wchar_t szBaseBuffer [MAX_PATH_UNICODE];
	wchar_t szExtBuffer  [MAX_PATH];

	//extract the index from the filename= string between '_' and '.'
	getBaseAndExtensionForFileName( pstrLogFileName, szBaseBuffer, szExtBuffer, NULL);

	int iUnderscorePosition = (int)(wcsrchr(szBaseBuffer, L'_') - szBaseBuffer);

	if( iUnderscorePosition > -1 )
	{
		int iCopyLen = (int)(wcslen(szBaseBuffer) - iUnderscorePosition + 1);
		wcsncpy( szIndex,
				 szBaseBuffer + iUnderscorePosition + 1,
				 (size_t)iCopyLen );
	}

	//return
	return _wtoi( szIndex );
}
//-----------------------------------------------------------------------------

//!	Method compareFileNames() - Compares two file log names from the WAX::Logger perspective.
/*!
*	Compares two file log names from the WAX::Logger perspective in which the file names are equal
*	if both base names and indexes are equal.
*
*	Returns ZERO if the file names are equal.
*
*/
int Logger::compareFileNames(
	const wchar_t* pName1,	//!< [in] First file name to be compared.
	const wchar_t* pName2		//!< [in] Second file name to be compared.
	)
{
	size_t iUnderscorePosition;
	int iRetVal = 0;

	//determine position of undesrscore
	iUnderscorePosition = wcsrchr( pName1, L'_') - pName1;

	iRetVal = wcsncmp( pName1, pName2, iUnderscorePosition + 1 );

	if( iRetVal == 0)
	{//we have equal dates, undecided => compare indexes
		iRetVal = getLogIndex( pName1 ) - getLogIndex( pName2 );
	}

	return iRetVal;
}
//-----------------------------------------------------------------------------

//!	Method listFiles() - Lists the files names matching a rule into a vector.
/**
*	Lists the on disk files for which the names match the input rule into a vector by using findFirst-findNext.
*	The files in the vector are ordered by creation date.
*/
void Logger::listFiles(
	const wstring& pstrSearchRule,	//!< [in]	Rule by which the files are be searched.
	vector <wstring>&pvFileList		//!< [out]	Vector containing the list of found files.
	)
{
	WIN32_FIND_DATAW lpFindFileData;
	HANDLE hFindFile;
	size_t i, iMax;

	pvFileList.clear();//clear old file list

	hFindFile = FindFirstFileW(pstrSearchRule.c_str(), &lpFindFileData);
	if ( hFindFile != INVALID_HANDLE_VALUE )
	{
		do
		{//Insert each new file name ordered ascending
			if( pvFileList.empty() )
			{//insert first file name
				pvFileList.push_back( lpFindFileData.cFileName );
			}
			else
			{
				i = 0;
				iMax = pvFileList.size();
				//while i < iMax and new_elem > current_list_elem
				while(
					  (i < iMax) &&
					  ( compareFileNames ( lpFindFileData.cFileName , (wchar_t*)pvFileList[i].c_str() ) > 0 ) )
				{
					i++;
				}

				pvFileList.insert( pvFileList.begin() + i, lpFindFileData.cFileName);
			}
		}
		while ( FindNextFileW( hFindFile, &lpFindFileData) );
		FindClose( hFindFile );
	}
}
//-----------------------------------------------------------------------------

//!	Method deleteOldLogsFiles() - Search and delete old log files that match the file name base.
/**
*	Search and delete old log files that match the file name base.
*/
void Logger::deleteOldLogsFiles(
	const wstring &pstrFileNameBase,//!< [in] Filename base (including path) of the log files to be deleted.
	long plNrLeft					//!< [in] number of log files to be left on disk.
	)
{
	//	Check file name base for empty: avoid wipping out the entire folder content if the
	//	caller made a mistake.
	if( pstrFileNameBase.size() == 0 )
	{
		return;
	}

	wchar_t szBaseBuffer[MAX_PATH_UNICODE];
	wchar_t szExtBuffer [MAX_PATH];
	wchar_t szPathBuffer[MAX_PATH_UNICODE];

	getBaseAndExtensionForFileName( pstrFileNameBase, szBaseBuffer, szExtBuffer, szPathBuffer);

	wstring strBase, strExt, strPath;
	strBase = szBaseBuffer;
	strExt	= szExtBuffer;
	strPath = szPathBuffer;

	//	Check if the search criteria is empty: avoid wipping out the entire folder content if the
	//	base file name and extension are empty.
	wstring strSearchRule = strBase + L"*" + strExt;
	if( strSearchRule.length() == 1)
	{
		//This means the search rule only contains the '*' character.
		return;
	}

	vector <wstring> vstrLogFiles;//vector to hold the file list to be deleted.
	listFiles( strSearchRule, vstrLogFiles);
	wstring strFileToDelete;
	if( vstrLogFiles.size() > (size_t)plNrLeft )
	{
		size_t iMax = vstrLogFiles.size() - (size_t)plNrLeft;
		for( size_t i = 0; i < iMax; i++)
		{
			strFileToDelete =  strPath + vstrLogFiles[i] + L"\0";

			if( ! ::DeleteFile( strFileToDelete.c_str() ) )
			{
				//If these files could not be deleted it means they are not supposed to be deleted.
				//So there is no action required.
			}
		}
	}
}
//-----------------------------------------------------------------------------


//!	Method getBaseAndExtensionForFileName() - Extracts the path, base name and extension of an filename.
/*!
*	Extracts the path, base name and extension of an filename
*	basename is defined as the path + filename without extension
*/
void Logger::getBaseAndExtensionForFileName(
	const wstring &pstrFileName,//!< [in] log file name (path+base+ext)
	wchar_t* pszBase,				//!< [out] base name - defined as the path + filename without extension
	wchar_t* pszExtension,		//!< [out] extension
	wchar_t* pszPath				//!< [out] path to the file (directory structure)
	)
{
	//Fix relative path bug: initialize the input strings to 0
	pszBase[0] = L'\0';
	pszExtension[0] = L'\0';

	size_t iDotPosition = pstrFileName.find_last_of(L'.');
	if( iDotPosition != -1)
	{
		wcsncpy( pszBase, pstrFileName.c_str(), iDotPosition);
		pszBase[iDotPosition] = L'\0';
		wcsncpy( pszExtension,
				pstrFileName.c_str() + iDotPosition,
				pstrFileName.size() - iDotPosition);
		pszExtension[pstrFileName.size() - iDotPosition] = L'\0';
	}
	else
	{
		//	There is no extenssion in file name. Set the base as the incomming buffer;
		wcsncpy( pszBase, pstrFileName.c_str(), pstrFileName.length() );
		pszBase[pstrFileName.length()] = L'\0';
	}

	if ( pszPath != NULL)
	{
		pszPath[0] = L'\0';//make sure we do not "invent" a path if none is needed for relative path names.

		size_t iSlashPosition = pstrFileName.find_last_of(L'\\');
		if( iSlashPosition != -1)
		{
			iSlashPosition++;
			wcsncpy( pszPath, pstrFileName.c_str(), iSlashPosition);
			pszPath[iSlashPosition] = L'\0';
		}
	}

	if( wcslen( pszExtension ) == 0 )
	{
		//Set the extension to the default value
		wcsncpy( pszExtension, LOG_DEFAULT_FILE_EXTENSION, LOG_DEFAULT_LEN_FILE_EXTENSION );
		pszExtension[LOG_DEFAULT_LEN_FILE_EXTENSION] = 0;
	}
}
//-----------------------------------------------------------------------------

//! Converts a _timeb date-time value to its equivalent string value.
void Logger::timeb2wstring(
	_timeb inDateTime,	//!< [in]	date-time value to be converted to string.
	wstring& outString	//!< [out]	string representing the requested date-time value.
	)
{
	wchar_t _Time[MAX_PATH];

	time_t time = inDateTime.time;
	wcsftime(_Time, MAX_PATH, L"[%Y-%m-%d] %H:%M:%S", localtime(&time));

	outString = _Time;
	outString += L",";
	_itow( inDateTime.millitm, _Time, 10 );
	outString += _Time;
}
//-----------------------------------------------------------------------------

//! Converts a long value to its equivalent string value.
void Logger::long2wstring(
	long inlValue,			//!< [in]	long value to be converted to string.
	wstring& outString		//!< [out]	string representing the requested long value.
	)
{
	wchar_t dummy[MAX_PATH];
	_ltow(inlValue, dummy, 10);
	outString = dummy;
}
//-----------------------------------------------------------------------------

//! Converts a numeric log level to its equivalent string value.
void Logger::log_level2wstring(
	log_level inLogLevel,	//!< [in]	numeric log level to be converted to string.
	wstring& outString		//!< [out]	string representing the requested log level.
	)
{
	switch( inLogLevel )
	{
		case	LOG_LEVEL_ERROR:
			outString = L"ERROR";
			break;

		case	LOG_LEVEL_INFO:
			outString = L"INFO";
			break;

		case	LOG_LEVEL_DEBUG:
			outString = L"DEBUG";
			break;

		default:
			outString = L"UNKNOWN";
			break;
	}
}
//-----------------------------------------------------------------------------

//! Resets the error message collection (trace) to its default value.
void Logger::ResetErrorInfo(void)
{
	m_wsLastErrorInformation.assign(LOG_ERR_INFO_DEFAULT);
}
//-----------------------------------------------------------------------------


///////////////////////////////////////////////////////////////////////////////////////////////////
///////		Getters - Setters		///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

//! Method SetNumberOfLogFiles() - sets the maximum number of log files for the current day.
/*
*	NOTE: [1]	The implementation of this setter is done in the .CPP file because the LOG_FILES_MIN
*				number is only visible in this unit.
**/
void Logger::SetNumberOfLogFiles( long inlNumberOfLogFiles )
{
	m_maxNumberOfLogFiles = (inlNumberOfLogFiles > LOG_FILES_MIN) ? inlNumberOfLogFiles : LOG_FILES_MIN;
}
//-----------------------------------------------------------------------------




///////////////////////////////////////////////////////////////////////////////////////////////////
/////////	Exported LOGGER Functions to be used for logging		///////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

//!	Logs an event passed as message string and from the component ID.
extern void LogEventMessage(
	log_level inLevel,
	WAX_ID inwiComponentID,
	wchar_t* inszMessage
	)
{
	if (! Logger::IsOkToLog( inLevel ) )	//Only log the events that have the proper log level set
	{
		return;
	}

	Logger::LogMessageFromComponentID( inLevel, inwiComponentID, LOG_LOCATION_UNKNOWN, inszMessage);
}


/*
void LogEvent(
	log_level inLevel,
	WCHAR* inszFormat,
	...
	)
{
	if (! Logger::IsOkToLog( inLevel ) )	//Only log the events that have the proper log level set
	{
		return;
	}

	va_list args;
	va_start( args, inszFormat );
	vswprintf_s( szLogBuffer, LOGGER_BUFF_LEN, inszFormat, args );

	Logger::LogMessageFromComponentID( inLevel, WAX_ID_UNKNOWN, LOG_LOCATION_UNKNOWN, szLogBuffer);
}
*/
//-----------------------------------------------------------------------------

void LogEventFormated(
	log_level inLevel,
	WAX_ID inwiComponentID,
	wchar_t* inszFormat,
	...
	)
{
	if (! Logger::IsOkToLog( inLevel ) )	//Only log the events that have the proper log level set
	{
		return;
	}

	va_list args;
	va_start( args, inszFormat );
	vswprintf_s( szLogBuffer, LOGGER_BUFF_LEN, inszFormat, args );

	Logger::LogMessageFromComponentID( inLevel, inwiComponentID, LOG_LOCATION_EMPTY, szLogBuffer);
}
//-----------------------------------------------------------------------------

void LogError(
	WAX_ID inwiComponentID,
	wchar_t* inszLocation,
	wchar_t* inszFormat,
	...
	)
{
	if (! Logger::IsOkToLog(LOG_LEVEL_ERROR ) )	//Only log the events that have the proper log level set
	{
		return;
	}

	va_list args;
	va_start( args, inszFormat );
	vswprintf_s( szLogBuffer, LOGGER_BUFF_LEN, inszFormat, args );

	Logger::LogMessageFromComponentID( LOG_LEVEL_ERROR,inwiComponentID, inszLocation, szLogBuffer);
}
//-----------------------------------------------------------------------------

void LogDebug(
	WAX_ID inwiComponentID,
	wchar_t* inszLocation,
	wchar_t* inszFormat,
	...
	)
{
	if (! Logger::IsOkToLog( LOG_LEVEL_DEBUG ) )	//Only log the events that have the proper log level set
	{
		return;
	}

	va_list args;
	va_start( args, inszFormat );
	vswprintf_s( szLogBuffer, LOGGER_BUFF_LEN, inszFormat, args );

	Logger::LogMessageFromComponentID( LOG_LEVEL_DEBUG,inwiComponentID, inszLocation, szLogBuffer);
}
//-----------------------------------------------------------------------------

#pragma warning(default:4996)
