//********************************************************/
//  ./WAXStatistics/WAXStatistics.h
//
//  Owner : Bogdan Hruban
// 
//	Company		: Neverfail 
//	PROJECT		: WAN Acceleration 
//  Component	: Statistics 
//
//  $Rev: 2480 $ - Revision of last commit
//
/********************************************************
//
// Last Change - Author and Date of last change:
// $Author: cmaris $  
// $Date: 2009-05-22 14:08:44 +0100 (Fri, 22 May 2009) $
//
//********************************************************/

#pragma once

#include "IStats.h"
#include "../waxsingletontemplate.h"
#include "../ConfigMan/ConfigMan.h"
#include "../StoreManager/StoreManager.h"
#include "../MemoryPool/MemoryPool.h"
#include "../uwm/UWM.h"
#include "../uwm/Tokenizer.h"
#include "../rpr/RPR.h"

//! Statistic module
class WAXStatistics: public WaxSingletonTemplate<WAXStatistics>, public IStats
{
	friend class WaxSingletonTemplate<WAXStatistics>;
private:
	WAXStatistics();

public:
	//! Get the statistics from this module
	WAX_RESULT GetStatistics(
	/*__inout*/ Statistics *inoutStats //!< output statistic structure
	);

	//! Calls statistics provider components to initialize stat values.
	void StartStatistics( void );

private:
	//! Initializes the statistics structure before the first use
	WAX_RESULT InitializeStatistics(
	/*__inout*/ Statistics *inoutStats	//!< statistic structure to be initialized
	);

private:
	uint64_t m_ullDataVolCurrent;			//!< current data volumes and current compression rates	

	static WAX_ID m_ComponentID;

	UWM* m_UWM;
	Tokenizer* m_Tokenizer;
	RPR* m_RPR;
	MemoryPool* m_MemoryPool;
	StoreManager* m_StoreManager;
	ConfigMan* m_ConfigMan;
	SignatureLog* m_SigLog;
	WAXEncoder* m_Encoder;

};