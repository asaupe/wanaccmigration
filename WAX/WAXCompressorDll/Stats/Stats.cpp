//********************************************************/
//  ./WAXStatistics/WAXStatistics.cpp
//
//  Owner : Bogdan Hruban
// 
//	Company		: Neverfail 
//	PROJECT		: WAN Acceleration 
//  Component	: Statistics 
//
//  $Rev: 2480 $ - Revision of last commit
//
/********************************************************
//
// Last Change - Author and Date of last change:
// $Author: cmaris $  
// $Date: 2009-05-22 14:08:44 +0100 (Fri, 22 May 2009) $
//
//********************************************************/

#include "Stats.h"
#include "../AllUtils.h"

WAX_ID WAXStatistics::m_ComponentID = (WAX_ID)WAX_ID_STATS;

WAXStatistics::WAXStatistics(void):m_ullDataVolCurrent(0)
{
	m_RPR			= RPR::getInstance();
	m_Tokenizer		= Tokenizer::getInstance();
	m_ConfigMan		= ConfigMan::getInstance();
	m_UWM			= UWM::getInstance();
	m_MemoryPool	= MemoryPool::getInstance();
	m_StoreManager	= StoreManager::getInstance();
	m_SigLog		= SignatureLog::getInstance();
	m_Encoder		= WAXEncoder::getInstance();

}

//! Get the statistics from all the modules
/*!
	The WAXStatistics::GetStatistics() function is responsible with the gathering
	of statistical data from all the components that contain such information.

	\sa MemoryPool::GetStatistics()
	\sa StoreManager::GetStatistics()
	\sa UWM::GetStatistics()
	\sa Tokenizer::GetStatistics()
	\sa RPR::GetStatistics()
*/
WAX_RESULT WAXStatistics::GetStatistics(
	/*__inout*/ Statistics *inoutStats //!< output statistic structure
	)
{
	if (NULL == inoutStats)
	{
		return WAX_RESULT_INBUFF_NULL;
	}

	WAX_RESULT res = InitializeStatistics(inoutStats);

	// get UpTime and CpuTime
	getCPUAndRUNTimesInMS(inoutStats->cpuTime, inoutStats->uptime);
	
	// get statistics from MemoryPool
	res = res & m_MemoryPool->GetStatistics(inoutStats);

	// get statistics from StoreManager
	res = res & m_StoreManager->GetStatistics(inoutStats);

	// get statistics from UWM
	res = res & m_UWM->GetStatistics(inoutStats);

	// get statistics from Tokenizer
	res = res & m_Tokenizer->GetStatistics(inoutStats);

	// get statistics from RPR
	res = res & m_RPR->GetStatistics(inoutStats);

	//get Signature log stats
	res = res & m_SigLog->GetStatistics( inoutStats );

	//get encoder statistics
	res = res & m_Encoder->GetStatistics( inoutStats );

	//compute the current data volume
	double nInvariant = m_ConfigMan->GetNInvariant();
	inoutStats->dataVolCurrent = (uint64_t)((1- nInvariant)*m_ullDataVolCurrent + nInvariant*inoutStats->dataVolLZero);
	m_ullDataVolCurrent = inoutStats->dataVolCurrent;

	return res;
}

//! Initializes the statistics structure before the first use
WAX_RESULT WAXStatistics::InitializeStatistics(
	/*__inout*/	Statistics *inoutStats //!< statistic structure to be initialized
	)
{
	if (NULL == inoutStats)
	{
		return WAX_RESULT_INBUFF_NULL;
	}

	inoutStats->bytesInReg = 0;
	inoutStats->compressionRate = 0;
	inoutStats->cpuTime = 0;
	inoutStats->daBytesNumber = 0;
	inoutStats->daStringsNumber = 0;
	inoutStats->dataVolCurrent = 0;
	inoutStats->dataVolL0 = 0;
	inoutStats->dataVolL1 = 0;
	inoutStats->dataVolL2 = 0;
	inoutStats->dataVolL3 = 0;
	inoutStats->dataVolSinceInit = 0;
	inoutStats->dataVolLZero = 0;
	inoutStats->dedupBytesNumber = 0;
	inoutStats->dedupOldBytesNumber = 0;
	inoutStats->dedupOldStringsNumber = 0;
	inoutStats->dedupRecentBytesNumber = 0;
	inoutStats->dedupRecentStringsNumber = 0;
	inoutStats->dedupStringsNumber = 0;
	inoutStats->diskUsed = 0;
	inoutStats->memoryAllocated = 0;
	inoutStats->memoryUsed = 0;
	inoutStats->msgNumber = 0;
	inoutStats->regNumber = 0;
	inoutStats->rprBytesNumber = 0;
	inoutStats->rprStringsNumber = 0;
	inoutStats->uptime = 0;
	inoutStats->genericBytesNumber = 0;
	inoutStats->genericStringsNumber = 0;

	return WAX_RESULT_OK;
}

//! Calls statistics provider components to initialize stat values.
/*!
*	The method only calls the statistics provider components whose values
*	are changed by the data flowing through the compressor. These are also 
*	the components that do not implement a start() method:
*
*	The following compionents already intialize stats in the start() method:
*		- signature log
*		- encoder
*
*	The other statistics provider components are not called:
*		- memory pool
*		- store manager
*
**/
void WAXStatistics::StartStatistics( void )
{
	// initialize statistics for UWM
	m_UWM->StartStatistics();

	// initialize statistics for Tokenizer
	m_Tokenizer->StartStatistics();

	// initialize statistics for RPR
	m_RPR->StartStatistics();

}
