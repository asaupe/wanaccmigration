//********************************************************/
//  ./Stats/IStats.h
//
//  Owner : Bogdan Hruban
// 
//	Company		: Neverfail 
//	PROJECT		: WAN Acceleration 
//  Component	: Statistics 
//
//  $Rev: 1483 $ - Revision of last commit
//
/********************************************************
//
// Last Change - Author and Date of last change:
// $Author: avajda $  
// $Date: 2009-01-14 10:20:20 +0000 (Wed, 14 Jan 2009) $
//
//********************************************************/

#pragma once

#include "WAXTypes.h"

//! Interface for gathering statistics
/*!
	This class will be inherited by all the modules that are responsible
	with returning statistical data.
*/
class IStats
{
public:
	//! Update statistics record to the current values
	virtual WAX_RESULT GetStatistics(Statistics *inoutStats) = 0;
};