//********************************************************/
//  ./WaxInterface.cpp
//
//  Owner : Ciprian Maris
//  
//	Company		: Neverfail 
//	PROJECT		: WAN Acceleration 
//	Component	: WAX Interface
//
//  $Rev: 2746 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: cmaris $  
//  $Date: 2009-07-01 14:35:26 +0100 (Wed, 01 Jul 2009) $
//
//********************************************************/

#include <windows.h>

#include "WAXInterface.h"

#include "WAXTypes.h"
#include "Common.h"
#include "Logger\Logger.h"

#include "AllUtils.h"

#include "ConfigMan\ConfigMan.h"
#include "Encoder\encoder.h"
#include "MemoryPool\memorypool.h"
#include "RPR\RPR.h"
#include "StoreManager\SignatureLog.h"
#include "StoreManager\StoreManager.h"
#include "UWM\Tokenizer.h"


WAX_ID WAXInterface::m_ComponentID = (WAX_ID)WAX_ID_WAXINTERFACE;

WAXInterface::WAXInterface()
{
	initialize();
}
//-----------------------------------------------------------------------------

WAXInterface::~WAXInterface()
{
	exit();
}
//-----------------------------------------------------------------------------

//! Method initialize() - Initialises the components of the compressor.
/*!
*	The method
*		Initialises all components by calling their getInstance method.
*
*	Return: The method always returns WAX_RESULT_OK for now.
*			Later development stages might change that. If not, the return value of it will be removed.
*
*	Caller: WAXCompressorDLL::initialize().
*/
WAX_RESULT WAXInterface::initialize(void)
{
	//initialise all components by calling their getInstance method

	// IMPORTANT: Memory pool must be first initialized 
	MemoryPool::getInstance();
	WAXEncoder::getInstance();
	RPR::getInstance();
	SignatureLog::getInstance();
	StoreManager::getInstance();
	Tokenizer::getInstance();
	
	//	Initialize the UWM local component - will be used for all subsequent encode/decode calls.
	//	NOTE:	The reason why only UWM is a local member is that all other components are invoked
	//			during allocate/start, stop/release, so we can use their getInstance() method without
	//			performance penalty.
	m_UWM = UWM::getInstance();

	// TODO: comment
	m_Stats= WAXStatistics::getInstance();

	//	NOTE:	Init and exit events are logged, to keep logging symetry.
	//			The logger has to be initialised by the WAXCompressor prior the call to this method
	LogEventMessage(LOG_LEVEL_INFO, m_ComponentID, L"WAX Compressor initialized succesfully");

	return WAX_RESULT_OK;
}
//-----------------------------------------------------------------------------

//! Method exit() - ends the lifespan of the compressor 
/*!
*	The method calls release() to /make sure we do not leak in case exit is called with no prior release() call.
*
*	Return: The method always returns WAX_RESULT_OK for now.
*			Later development stages might change that. If not, the return value of it will be removed.
*
*	Caller: WAXCompressorDLL::exit().
*/
WAX_RESULT WAXInterface::exit(void)
{
	release();//make sure we do not leak in case exit is called with no prior release() call.

	//	Log exit event
	LogEventMessage(LOG_LEVEL_INFO, m_ComponentID, L"WAX Compressor is exiting.");

	Logger::Close();//call Logger::close() to flush any remaining logs to disk and close the file handle.

	return WAX_RESULT_OK;
}
//-----------------------------------------------------------------------------

WAX_RESULT WAXInterface::queryStatistics(
		/*__inout*/ Statistics *outStatistics //!<	statistics structure to receive the entire statistics set
						   )
{
	return m_Stats->GetStatistics(outStatistics);
}
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
//////	Control and Management methods		///////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

//! Method allocate() - Allocates resources for the compressor.
/*!
*	The method
*		Attempts to allocate resources (memory and disk).
*		Fails if either of them can not be allocated.
*
*	Return: In case of success return WAX_RESULT_OK. In the case of failure it returns the 
*			failure reason.
*
*	Caller: WAXCompressorDLL::allocate().
*/
WAX_RESULT WAXInterface::allocate(
		/*__in*/ unsigned long minimumMemoryBufferSize,	//!< Minimum memory to allocate for the compressor in MB's
		/*__in*/ unsigned long maximumMemoryBufferSize,	//!< Maximum memory to allocate for the compressor in MB's
		/*__in*/ unsigned long transferLogSize,			//!< On disk transfer Log Size in MB's
        /*__in*/ const StringBuffer * transferLogPath	//!< On disk transfer Log File path (complete path)
               )
{
	WAX_RESULT retVal = WAX_RESULT_OK;

	//	Parameter validation

	//BZ 7543 - call configMan to check input parameters
	if( ( ConfigMan::getInstance()->SetMaxMemUsed( maximumMemoryBufferSize ) == false) ||
		( ConfigMan::getInstance()->SetMinMemUsed( minimumMemoryBufferSize ) == false) ||
		( ConfigMan::getInstance()->SetTLogSize( (uint64_t)transferLogSize * MEGA )	 == false) )
	{
		LogError(m_ComponentID, DBG_LOCATION, L"Allocation parameters error: Minimum/Maximum Memory sizes or Transfer Log size are incorrect.", retVal);
		return WAX_RESULT_INVALID_ALLOCATE_PARAMETERS;
	}

	if( minimumMemoryBufferSize > maximumMemoryBufferSize )
	{
		LogError(m_ComponentID, DBG_LOCATION, L"Allocation parameters error: Invalid Minimum and Maximum memory size.", retVal);
		return WAX_RESULT_INVALID_ALLOCATE_PARAMETERS;
	}

	if( maximumMemoryBufferSize > transferLogSize )
	{
		LogError(m_ComponentID, DBG_LOCATION, L"Allocation parameters error: Invalid Memory and Transfer Log size.", retVal);
		return WAX_RESULT_INVALID_ALLOCATE_PARAMETERS;
	}

	//	Initialise security - needed for opening the transfer log file with special permissions
	//	and with no sharing.
	if( !NFInitSecurity() )
	{
		LogError( m_ComponentID, DBG_LOCATION, L"NFInitSecurity() failed."  );
		return WAX_RESULT_INIT_SECURITY_FAILED;
	}

	//allocate disk
	retVal = StoreManager::getInstance()->allocate( transferLogSize, transferLogPath );
	if( retVal != WAX_RESULT_OK )
	{
		LogError(m_ComponentID, DBG_LOCATION, L"StoreManager could not allocate. Return code is: %u", retVal);

		//release allocated resources...if any
		release();

		return retVal;
	}

	//allocate memory
	//	NOTE: this will also set the ConfigMan::m_uipMemorySize settings variable with the actual allocated memory size
	retVal = MemoryPool::getInstance()->allocate( minimumMemoryBufferSize, maximumMemoryBufferSize );
	if( retVal != WAX_RESULT_OK )
	{
		LogError(m_ComponentID, DBG_LOCATION, L"MemoryPool could not allocate. Return code is: %u", retVal);

		//release allocated resources...if any
		release();

		return retVal;
	}

	LogEventFormated( LOG_LEVEL_INFO, m_ComponentID, 
		L"WAX Compressor allocateted succesfully. MemorySize(MB) = %d, TransferLogSize(MB) = %d, TransferLogPath = %s.",
		(unsigned long)(ConfigMan::getInstance()->GetMemorySize()/MEGA), transferLogSize, transferLogPath->buffer ); 


	return WAX_RESULT_OK;
}
//-----------------------------------------------------------------------------

//! Method release() - releases pre-allocated disk space and memory
/*!
*	The method releases pre-allocated disk space and memory.	
*
*	Return: The method always returns WAX_RESULT_OK for now.
*			Later development stages might change that. If not, the return value of it will be removed.
*
*	Caller: WAXCompressorDLL::release().
*/
WAX_RESULT WAXInterface::release(void)
{
	//avoid releasing twice, if we already did
	//if( !m_isAllocated )
	//{
	//	return WAX_RESULT_OK;
	//}

	//release disk
	StoreManager::getInstance()->release();

	//release memory
	MemoryPool::getInstance()->release();

	LogEventMessage( LOG_LEVEL_INFO, m_ComponentID, L"WAX Compressor released succesfully.");

	return WAX_RESULT_OK;
}
//-----------------------------------------------------------------------------

//! Method start() - Sets the start parameters and starts the components that need to aquire pre-allocated resources
/*!
*	The method
*		Sets the start parameters in configmanager.
*		Starts the components that need to aquire pre-allocated resources:
*			SignatureLog
*
*	Return: In case of success return WAX_RESULT_OK. In the case of failure it returns the 
*			failure reason.
*
*	Caller: WAXCompressorDLL::start().
*/
WAX_RESULT WAXInterface::start(
		/*__in*/ bool inAsEncoder,					//!< Server role (compressor/decompressor)
		/*__in*/ unsigned long inSignatureBase,				//!< signature base
		/*__in*/ unsigned long inSignatureDistance,			//!< signature distance
		/*__in*/ unsigned long inMinRPRPatternsLength,		//!< minimum len of ZERO/DA pattern replaced by a RPR token
		/*__in*/ unsigned long inMinDeDuplicateLength,		//!< min len of the buffer to be considered for DD
		/*__in*/ unsigned long inMinDDLengthReplacedByToken,	//!< minimum length of data replaced by a DD token
		/*__in*/ const bool inEncodeOutOfBoundaryEnabled, //!< advanced encode for out of boundary entries
		/*__in*/ double indNInvariant						  //!< Factor for recent data volume and compression computation.
				)
{
	WAX_RESULT retVal = WAX_RESULT_OK;

	ConfigMan* configMan = ConfigMan::getInstance();
	
	//1. Set the parameters in Config Man - BZ 7543 - configMan will perfrom a range check on them if necessary
	//	As part of the resolution to BZ 7543 the input parameters are displayed here so that there is a notion
	//	in the logs of which input value is not valid.
	LogEventFormated( LOG_LEVEL_INFO, m_ComponentID, L"Start received with the following parameters: \
		\n\t AsEncoder=%d \n\t SignatureBase=%d \n\t SignatureDistance=%d \
		\n\t MinRPRPatternsLength=%d \n\t MinDeDuplicateLength=%d \n\t MinDDLengthReplacedByToken=%d \
		\n\t EncodeOutOfBoundaryEnabled=%d \n\t NInvariant=%f",
		inAsEncoder, inSignatureBase, inSignatureDistance,			
		inMinRPRPatternsLength,	inMinDeDuplicateLength,	inMinDDLengthReplacedByToken,
		inEncodeOutOfBoundaryEnabled, indNInvariant	);

	configMan->SetIsEncoder( inAsEncoder );

	if (!configMan->SetSignatureBase( inSignatureBase ) )
	{
		LogError(m_ComponentID, DBG_LOCATION, L"Start parameters error: Invalid SB");
		return WAX_RESULT_INVALID_START_PARAMETERS;
	}

	if (! configMan->SetSignatureDistance( inSignatureDistance ) )
	{
		LogError(m_ComponentID, DBG_LOCATION, L"Start parameters error: Invalid SD");
		return WAX_RESULT_INVALID_START_PARAMETERS;
	}

	if (!configMan->SetMinRPRLength( inMinRPRPatternsLength ) )	
	{
		LogError(m_ComponentID, DBG_LOCATION, L"Start parameters error: Invalid MinRPRPatternsLength");
		return WAX_RESULT_INVALID_START_PARAMETERS;
	}

	if (!configMan->SetMinDeDuplicateLength( inMinDeDuplicateLength ) )
	{
		LogError(m_ComponentID, DBG_LOCATION, L"Start parameters error: Invalid MinDeDuplicateLength");
		return WAX_RESULT_INVALID_START_PARAMETERS;
	}

	if (!configMan->SetMinDeDuplicateLengthReplacedByToken( inMinDDLengthReplacedByToken ) )
	{
		LogError(m_ComponentID, DBG_LOCATION, L"Start parameters error: Invalid MinDDLengthReplacedByToken");
		return WAX_RESULT_INVALID_START_PARAMETERS;
	}

	if (!configMan->SetNInvariant( indNInvariant ) )
	{
		LogError(m_ComponentID, DBG_LOCATION, L"Start parameters error: Invalid NInvariant");
		return WAX_RESULT_INVALID_START_PARAMETERS;
	}

	//	Enhancement Bug 7885 -  Optimal usage of resources: make sure the SetIsAdvancedDeduplicationEnabled()
	//	is called before starting StoreManager.
	configMan->SetIsAdvancedDeduplicationEnabled ( inEncodeOutOfBoundaryEnabled );

	//Parameter validation: SD > SB
	if( inSignatureBase > inSignatureDistance )
	{
		LogError(m_ComponentID, DBG_LOCATION, L"Start parameters error: Invalid SB-SD settings.");
		return WAX_RESULT_INVALID_START_PARAMETERS;
	}

	// Enhancement Bug 7885 -  Optimal usage of resources.
	if( false == inEncodeOutOfBoundaryEnabled )
	{
		configMan->SetTLogSize( (uint64_t) configMan->GetMaxMemUsed()*MEGA );
	}

	//2. Call start functions for components that implement the start method

	//	2.1 Call start for Memory Pool. IMPORTANT: make sure it is the first start to be called.
	retVal = MemoryPool::getInstance()->start(inAsEncoder);
	if( retVal != WAX_RESULT_OK )
	{
		LogError(m_ComponentID, DBG_LOCATION, L"MemoryPool could not start. Return code is: %u", retVal);

		stop();

		return retVal;
	}

	//	2.2 Call start for StoreManager.
	retVal = StoreManager::getInstance()->start(inAsEncoder);
	if( retVal != WAX_RESULT_OK )
	{
		LogError(m_ComponentID, DBG_LOCATION, L"StoreManager could not start. Return code is: %u", retVal);

		stop();

		return retVal;
	}

	//TO DO: uncomment or remove when decision is taken
	// tmp until we decide that advanced crc64 dictionary based de-duplication will be configurable
	// for testing only 
	retVal = WAXEncoder::getInstance()->start( inAsEncoder, inEncodeOutOfBoundaryEnabled );
	if( retVal != WAX_RESULT_OK )
	{
		LogError(m_ComponentID, DBG_LOCATION, L"WAXEncoder could not start. Return code is: %u", retVal);

		stop();

		return retVal;
	}

	retVal = Tokenizer::getInstance()->start((StoreManagerBase*)StoreManager::getInstance());
	if( retVal != WAX_RESULT_OK )
	{
		LogError(m_ComponentID, DBG_LOCATION, L"Tokenizer could not start. Return code is: %u", retVal);

		stop();

		return retVal;
	}

	//	BZ 8257 - call start statistics
	m_Stats->StartStatistics();

	//	BZ 7543 - log the actual start parameters.
	LogEventFormated( LOG_LEVEL_INFO, m_ComponentID, L"The WAX Compressor started succesfully with the following parameters: \
		\n\t SignatureBase=%d \n\t SignatureDistance=%d \
		\n\t MinRPRPatternsLength=%d \n\t MinDeDuplicateLength=%d \n\t MinDDLengthReplacedByToken=%d \
		\n\t EncodeOutOfBoundaryEnabled=%d \n\t NInvariant=%f",
		configMan->GetSignatureBase(), configMan->GetSignatureDistance(),
		configMan->GetMinRPRLength(), configMan->GetMinDeDuplicateLength(), configMan->GetMinDeDuplicateLengthReplacedByToken(),
		configMan->GetIsAdvancedDeduplicationEnabled(), configMan->GetNInvariant() );

	return WAX_RESULT_OK;
}
//-----------------------------------------------------------------------------

//! Method stop()
/*!
*	The method stops the Compressor.
*
*	Return: The method always returns WAX_RESULT_OK for now.
*			Later development stages might change that. If not, the return value of it will be removed.
*
*	Caller: WAXCompressorDLL::stop().
*/
WAX_RESULT WAXInterface::stop(void)
{
	//Call stop functions for methods
	StoreManager::getInstance()->stop();
	WAXEncoder::getInstance()->stop();

	LogEventMessage(LOG_LEVEL_INFO, m_ComponentID, L"The WAX Compressor stopped succesfully.");

	return WAX_RESULT_OK;
}
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
//////	END Control and Management methods		///////////////////////////////
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//////	Operations methods		///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

//! Method encode() encodes the input buffer on source
/*!
*	The method calls UWM::ProcessBuffer() to encode the input buffer
*
*	Return: In case of success return WAX_RESULT_OK. In the case of failure it returns the 
*			failure reason.
*
*	Caller: WAXCompressorDLL::encode().
*/
WAX_RESULT WAXInterface::encode(
		/*__in*/ const ByteBuffer*  inputBuffer,	//!< source buffer to be encoded
        /*__out*/ ByteBuffer*  outputBuffer,		//!< destination buffer - pre-allocated by caller
        /*__in*/ bool removeRepeatedPatterns,	//!< FLAG - RPR requested by caller
        /*__in*/ bool deDuplicate				//!< FLAG - DD requested by caller
		)
{
	WAX_RESULT retVal = WAX_RESULT_OK;

	retVal = m_UWM->EncodeBuffer( inputBuffer->buffer, inputBuffer->length, 
								   outputBuffer->buffer, (outputBuffer->length), 
								   removeRepeatedPatterns, deDuplicate);

	if( retVal != WAX_RESULT_OK )
	{
		LogError( m_ComponentID, DBG_LOCATION, L"EncodeBuffer failed. Return code is: %u", retVal);
	}

	//	NOTE:	For perfromance reasons, there will be no INFO level logging in the encode path.
	LogDebug( m_ComponentID,DBG_LOCATION, L"Call to encode( RPR=%d, DD=%d ) returned %u.", removeRepeatedPatterns, deDuplicate, retVal);

	return retVal;
}
//-----------------------------------------------------------------------------

//! Method decode() decodes the input buffer on target
/*!
*	The method calls UWM::ProcessBuffer() to decode the input buffer
*
*	Return: In case of success return WAX_RESULT_OK. In the case of failure it returns the 
*			failure reason.
*
*	Caller: WAXCompressorDLL::decode().
*/
WAX_RESULT WAXInterface::decode(
		/*__in*/ const ByteBuffer*  inputBuffer,//!< source buffer to be decoded
		/*__out*/ ByteBuffer*  outputBuffer,	//!< destination buffer - pre-allocated by caller
		/*__in*/ bool bRPREncoded,
		/*__in*/ bool bDDEncoded
		)
{
	WAX_RESULT retVal = WAX_RESULT_OK;

	retVal = m_UWM->DecodeBuffer( 
						inputBuffer->buffer, inputBuffer->length, 
						outputBuffer->buffer, outputBuffer->length,
						bRPREncoded, bDDEncoded );

	if( retVal != WAX_RESULT_OK )
	{
		LogError( m_ComponentID, DBG_LOCATION, L"DecodeBuffer failed. Return code is: %u", retVal);
	}

	//	NOTE:	For perfromance reasons, there will be no INFO level logging in the decode path.
	LogDebug( m_ComponentID,DBG_LOCATION, L"Call to decode( ) returned %u.", retVal);

	return retVal;
}
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////
//////	END Operations methods		///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
