//********************************************************/
//  ./WaxSingletonTemplate.h
//
//  Owner : Attila Vajda
//  
//	Company		: Neverfail 
//	PROJECT		: WAN Acceleration 
//	Component	: COMMON: Wax Singleton Template
//
//  $Rev: 1483 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: avajda $  
//  $Date: 2009-01-14 10:20:20 +0000 (Wed, 14 Jan 2009) $
//
//********************************************************/

//********************************************************/
//
//	The Wax Singleton Template Provides the Singleton structure 
//	common to all WAX components.
//
//********************************************************/
#pragma once

//!  WAX Singleton Template
//********************************************************
//    WAX Singleton Template
//********************************************************
template <class _TClass> 
class WaxSingletonTemplate 
{
	friend _TClass;
	friend WaxSingletonTemplate<_TClass>;

protected:
	
	//!  WaxSingletonTemplate - 
	WaxSingletonTemplate(){};

	~WaxSingletonTemplate(){};
	//!  WaxSingletonTemplate - 
	WaxSingletonTemplate( const WaxSingletonTemplate& );
	//!  operator= - 
	const WaxSingletonTemplate& operator=(const WaxSingletonTemplate&	src);
	//!  operator= - 
	const _TClass& operator=(const _TClass&	src);

protected:
	//************************************
	// Initialized flag of WaxSingletonTemplate
	//************************************

public:
	//!  getInstance - 
	static _TClass* getInstance()
	{
		_TClass* pReturn = NULL;

		static _TClass tTemporary;
		pReturn = &tTemporary;

		return pReturn;
	}
};

