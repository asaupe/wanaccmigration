//********************************************************/
//  ./Encoder/adler.h
//
//  Owner     : Lyndon Clarke/Tom Stones - minor changes by WAX Team 
//
//  Company   : Neverfail 
//  PROJECT   : WAN Acceleration 
//  Component : Adler - WAXEncoder
//  Purpose   : Implementation of Adler32 checksum algorithm.
//
//  $Rev: 2744 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: cmaris $  
//  $Date: 2009-07-01 14:28:14 +0100 (Wed, 01 Jul 2009) $
//
//********************************************************/

#pragma once
#include <windows.h>

class Adler
{
public:
	Adler();
	~Adler();
	
	//! return value of Adler
	unsigned long  get( void );
	
	//! Setup the Adler - was /*void update( byte* buf, int len);
	void setup(
		/*__in*/ const byte* inBuffer,    //!< setup Adler on this buffer
		/*__in*/ const int   inDataLength //!< the size of the buffer
		);

	//! Slide by one byte. - was /*void update( byte bp, byte bi);*/
	void slide(
		/*__in*/ const byte inByteOut,    //!< byte to remove from Adler
		/*__in*/ const byte inByteIn      //!< new byte to add to Adler
		);

private:
	int a;
	int b;
	int l;
};
