//********************************************************/
//  ./Encoder/encoder.cpp
//
//  Owner     : Attila Vajda
//
//  Company   : Neverfail
//  PROJECT   : WAN Acceleration
//  Component : WAX Encoder
//  Purpose   : Implementation of encoding algorithm
//
//  $Rev: 2391 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: cmaris $
//  $Date: 2009-05-08 14:04:40 +0100 (Fri, 08 May 2009) $
//
//********************************************************/

#include "encoder.h"
#include "crc64.h"
#include "../configman/configman.h"
#include "../Logger/Logger.h"
#include "../AllUtils.h"
#include <sstream>

WAX_ID WAXEncoder::m_ComponentID = (WAX_ID)WAX_ID_ENCODER;

WAXEncoder::WAXEncoder()
{
	m_bWriteToStorman = true;
	m_StatisticsAdvancedGoodMatchesInMemory    = 0;
	m_StatisticsAdvancedMatchesInMemory        = 0;
	m_StatisticsAdvancedGoodMatchesoUToFmEMORY = 0;
	m_StatisticsAdvancedMatchesoUToFmEMORY     = 0;
	m_StatisticsAdvancedNOTONBOUNDARY          = 0;
	m_StatisticsAdvancedNOTonBoundaryHIT       = 0;
	m_StatisticsAdvancedSearched               = 0;
	m_StatisticsAdvancedSignaturesCreated      = 0;
	m_StatisticsAdvancedGROWRight              = 0;
	m_StatisticsAdvancedGROWLeft               = 0;
	m_StatisticsAdvancedProcessedBufferSize    = 0;
	m_StatisticsAdvancedTokensGenerated        = 0;
	m_StatisticsAdvancedNumberOfTails          = 0;
	m_StatisticsAdvancedTokensSIZE             = 0;
	m_StatisticsAdvancedAdlerReuseFromMap      = 0;
	m_StatisticsAdvancedSLIDES                 = 0;
	m_StatisticsAdvancedAdlerSetup             = 0;
	m_StatisticsAdvancedNumberOf511Token       = 0;
	m_bAdvancedDictionaryBasedDeDuplication = false;

	initialize();
}


WAXEncoder::~WAXEncoder()
{
	// if stop is not called ... 
	if( NULL != m_TailBuffer )
	{
		free( m_TailBuffer );
		m_TailBuffer = NULL;
	}
}

//! Generates ChunkCharacteristic for the input buffer.
/*!
*  Generates ChunkCharacteristicfor the input buffer.
*
*  \sa computeSignaturesOnBuffer()
*  \return - ChunkCharacteristic
*/
void WAXEncoder::
generateChunkCharacteristic(
	/*__in*/    const unsigned char* inData, //!< buffer where we will compute signatures
	/*__inout*/ Adler& inoutAdler,   //!< call setup for this and compute Adler
	/*__inout*/ ChunkCharacteristic& outChunkCharacteristic //!< fill a chunk characteristic 
	)
{
	inoutAdler.setup( inData, m_SignatureBase);
	outChunkCharacteristic.signature       = inoutAdler.get();
	// Enhancement Bug 7885 -  Optimal usage of resources.
	if( m_bAdvancedDictionaryBasedDeDuplication )
	{
		outChunkCharacteristic.strongSignature = ComputeCRC64( inData, m_SignatureDistance );
	}
	else 
	{	//BZ 8119 - set the strong signature to ZERO as an indicator that is is not used.
		outChunkCharacteristic.strongSignature = 0;
	}

	outChunkCharacteristic.sigLogEntry     = NULL;
}


//! Creates list of Signatures (Signature and Strong Signature ) for the current buffer.
/*!
*	Computes signatures - fills m_SignatureMAP with Signature(Adler32) and StrongSignature(CRC64).
*		m_SignatureMAP is a MAP of ChunkCharacteristic struct. It is passed to the StoreManager to be added to signature log.
*
*		m_characteristicsVector is populated with signatures. It contains the same data as m_SignatureMAP.
*
*		m_AdlerMAP is a MAP containing pre-calculated Adler values that are re-used for sliding.
*
*	Caller : processTailAndComputeSignatures() - this is the only caller
*
*  \sa processTailAndComputeSignatures()
*  \return - WAX_RESULT_INVALID_BUFFER_OR_SIZE - nrOfSignatures not invalid - cannot create signatures.
*          - WAX_RESULT_OK - signatures successfully computed
*/	
WAX_RESULT WAXEncoder::
computeSignaturesOnBuffer (
	/*__in*/ const unsigned char* inData,          //!< pointer -to the buffer that needs to be processed.
	/*__in*/ const unsigned long  inDataLength,    //!< size of the inData - this is for verify validity of the inData
	/*__in*/ const unsigned long  inOffsetAllign,  //!< because of the tail left over from the previous buffer processed we
	                                   //   will have to align the buffer
	/*__in*/ const UINT   inNrOfSignatures //!< nr of signatures that we will process - the offset that we start to
	                                   //   compute the signatures is in inOffsetAllign and this will compute
	                                   //   inNrOfSignatures number of signatures.
	)
{	
	//inNrOfSignatures signatures will be created starting from currentOffset
	unsigned long currentOffset = inOffsetAllign;

	if ( (inNrOfSignatures * m_SignatureDistance) > inDataLength )
	{
		return WAX_RESULT_INVALID_BUFFER_OR_SIZE;
	}

	m_StatisticsAdvancedSignaturesCreated+=inNrOfSignatures;
	Adler localAdler;
	ChunkCharacteristic chkChar;

	for ( UINT i=0; i < inNrOfSignatures; i++)
	{
		generateChunkCharacteristic( 
			&inData[currentOffset],
			localAdler,
			chkChar
			);

		m_SignatureMAP[currentOffset] = chkChar ;

		// this will be passed to write buffer 
		m_characteristicsVector.push_back( chkChar );

		//save a copy of this adler
		m_AdlerMAP[currentOffset] = localAdler;

		currentOffset+= m_SignatureDistance;
	}

	return WAX_RESULT_OK;
}

//! Processes the tail and calls computeSignaturesOnBuffer()
/*!
*  Prepares data for signature computation.
*        - Checks and calculates left over tail.
*        - Calls computeSignaturesOnBuffer() and checks its return values.
*
*  Caller : EncodeBuffer() - this is the only caller
*
* \sa EncodeBuffer()
*
* \return - WAX_RESULT_GENERATE_TOKENS - signals that should continue to generate tokens.
*         - WAX_RESULT_INVALID_BUFFER_OR_SIZE - if computeSignaturesOnBuffer fails propagate error
*         - WAX_RESULT_OK - tail processed and do not continue to generate tokens.
*/	
WAX_RESULT WAXEncoder::
processTailAndComputeSignatures(
	/*__in*/ const unsigned char* inData,      //!< pointer -to the buffer that needs to be processed.
	/*__in*/ const unsigned long  inDataLength //!< size of the inData - this is for verify validity of the inData
	)
{
	WAX_RESULT wResult = WAX_RESULT_OK;

	//initialize local vars to be used in this method
	UINT  nrOfSignatures     = 0;
	unsigned long ulAlignSgBoundary  = 0;
	bool  bBufferTooShort    = false; // FLAG - true if the input data length + old tail length is shorter than SD

	//check the tail
	if( (m_TailLength > 0) && (m_TailLength < m_SignatureDistance) )
	{
		m_StatisticsAdvancedNumberOfTails++;
		ulAlignSgBoundary = (m_SignatureDistance - m_TailLength);

		// keep in mind that the left over buffer ( the tail ) was already saved in m_TailBuffer
		// to be sure we gonna check for 0 (zero) at the beginning of the
		// filling area of m_TailBuffer
		if( ulAlignSgBoundary >= inDataLength )
		{
			// The input data length + old tail length is shorter than signature distance
			// we flag this cause we will not have to carry on ...
			// we update the tail (copy the new incoming bytes to it) and add to transfer log and that's it
			// update the tail
			memcpy( &m_TailBuffer[m_TailLength], inData, inDataLength );
			m_TailLength += inDataLength;
		
			bBufferTooShort = true;
		}
		else
		{
			memcpy( &m_TailBuffer[m_TailLength], inData , ulAlignSgBoundary );
		}

		if( (!bBufferTooShort) || (ulAlignSgBoundary == inDataLength) )
		{
			ChunkCharacteristic chkChar;
			Adler adlr;

			generateChunkCharacteristic( 
				m_TailBuffer,
				adlr,
				chkChar
				);

			// this is added first to this vector and it will be signaled to writeBuffer
			// that these are signatures for left over tail
			m_characteristicsVector.push_back( chkChar );
		}
	}
	else
	{
		ulAlignSgBoundary = 0;
	}

	// get the length of the new tail
	if( inDataLength >= ulAlignSgBoundary )
	{
		if( ! bBufferTooShort )
		{
			m_TailLength = ( (inDataLength - ulAlignSgBoundary) % m_SignatureDistance );
		}
	}
	else
	{
		//special case where
		// seams like there is not enough room for tail .. leave it like that
	}

	//if we have an invalid tail this should never happen
	if( !( (m_TailLength >= 0) && (m_TailLength < m_SignatureDistance) ) )
	{
		m_TailLength = 0;
	}
	else
	{    
		// this is the temporary offset - this is the offset in the current
		// processed buffer; at the end of processing ask StoreManager for offset
		// of the tail this will be implemented in  processBuffer() - make sure you
		// check there
		if( !bBufferTooShort && (m_TailLength!=0) )
		{
			m_TailOffset = inDataLength - m_TailLength;
		}
	}

	if( !bBufferTooShort )
	{
		nrOfSignatures = 0;

		if (inDataLength >= ( ulAlignSgBoundary + m_TailLength ) )
		{
			nrOfSignatures = ( (inDataLength - ulAlignSgBoundary - m_TailLength)/m_SignatureDistance);
		}

		// we process signatures from new offset calculated above 
		// in order to have fixed sized chunk SignatureLog of the TransferLOG
		// the entry for the SignatureLOG of m_TailLength + (m_SD-m_TailLength)
		// is processed above and it will be a separate add to SignatureLOG
		// nr. of signatures to create
		WAX_RESULT waxResult = computeSignaturesOnBuffer( 
			inData, 
			inDataLength, 
			ulAlignSgBoundary , 
			nrOfSignatures 
			);
	
		if ( WAX_RESULT_OK != waxResult )
		{
			///* review temp comment out
			LogError(
				m_ComponentID,
				DBG_LOCATION,
				L"computeSignaturesOnBuffer() Failed with result:%d",
				waxResult
				);

			return waxResult;
		}

		if ( (m_TailLength > 0) && (NULL != m_TailBuffer) )
		{
			if ( (m_TailOffset+m_TailLength) <=inDataLength )
			{
				memset( m_TailBuffer, 0, m_SignatureDistance );
				memcpy( m_TailBuffer, &inData[ m_TailOffset ], m_TailLength);
			}
			else
			{
				LogError(
					m_ComponentID,
					DBG_LOCATION, 
					L"Failed to process tail." 
					);
			}
		}

		wResult = WAX_RESULT_GENERATE_TOKENS;
	}
	//else - we know that we have no signatures so we will not call computeSignatures

	return wResult;
}


//! Grow Compare byte by byte compare 2 memory addresses - grow right.
/*!
* Grow Compare byte by byte compare 2 memory addresses 
*	\param inCount - count of bytes to compare - the max size
*		of the memory to be compared is computed
*		before calling method - inCount is guaranteed to be
*		available count in both buffers
*
*	NOTE:	A right length of 1 means just the current byte. A right length of 2 means current plus 1 more,
*			but a left length of 1 means one byte to left of current etc.
*
*	\sa checkInMemoryEntry()
*	\return - nr of equal bytes in inProcessedMemory and inInMemoryTRLOGMemory
*/	
unsigned long WAXEncoder::
growCompareRight(
	/*__in*/ const                   unsigned long inCount,              //!< count of bytes to compare
	/*__in*/ const __bcount(inCount) byte* inProcessedMemory,    //!< 1'st memory to me compare with 2'nd
	/*__in*/ const __bcount(inCount) byte* inInMemoryTRLOGMemory //!< 2'nd memory to me compare with 1'st
	)
{
	unsigned long i = 0;

	while ( ( i < inCount ) && ( inProcessedMemory[i] == inInMemoryTRLOGMemory[i] ) )
	{
		i++;
	}

	return i;
}

//! Grow Compare byte by byte compare 2 memory addresses - grow left.
/*!
* Grow Compare byte by byte compare 2 memory addresses - grow left.
*
*   The max size of the memory to be compared is computed
*   before calling method - inCount is guaranteed to be
*   available count in both buffers - backwards - left of buffer
*
*	NOTE:	A right length of 1 means just the current byte. A right length of 2 means current plus 1 more,
*			but a left length of 1 means one byte to left of current etc.
*
*	\sa checkInMemoryEntry()
*	\return  - nr of equal bytes to in left side (grow left) of inProcessedMemory and inInMemoryTRLOGMemory
*/
unsigned long WAXEncoder::
growCompareLeft( 
	/*__in*/ const unsigned long inCount,              //!< count of bytes to compare 
	/*__in*/ const byte* inProcessedMemory,    //!< 1'st memory to me compare with 2'nd
	/*__in*/ const byte* inInMemoryTRLOGMemory //!< 2'nd memory to me compare with 1'st
	)
{
	unsigned long i = 0;

	for( i = 0; i < inCount; i++)
	{
		inProcessedMemory--;     
		inInMemoryTRLOGMemory--;
		if( inProcessedMemory[0] != inInMemoryTRLOGMemory[0] )
		{ 
			break;
		}
	}

	return i;
}

//! Generates a token from the supplied parameters and adds it to the output token vector.
/*!
*	Generates a token from the supplied parameters and adds it to the output token vector.
*
*	\sa findTokensInBuffer()
*	\return  WAX_RESULT_OK 
*/
WAX_RESULT WAXEncoder::
generateTokenAndSave(
	/*__in*/    const unsigned long     inTokenOffsetCurrentBuffer,     //!< offset of token in current buffers
	/*__in*/    const uint64_t inTokenOffsetInTransferLOG,     //!< offset of token in transfer log
	/*__in*/    const unsigned long     inTokenSize,                    //!< size of the token
	/*__inout*/ CompressedTokenVector& outCompressedTokenVector //!< location to add the new token generated using 
	                                                        //   the supplied parameters
	)
{
	CompressedToken intempCompressedToken;
	if( WAX_SHAREPOINT_MAGIC_NUMBER == inTokenSize ) // 511 :)
	{
		m_StatisticsAdvancedNumberOf511Token++;
	}

	m_StatisticsAdvancedTokensGenerated++;
	m_StatisticsAdvancedTokensSIZE+=inTokenSize;
	// generate the token - add to inCompressedTokenVector
	intempCompressedToken.offset     = inTokenOffsetCurrentBuffer;
	intempCompressedToken.tLogOffset = inTokenOffsetInTransferLOG;
	intempCompressedToken.length     = inTokenSize;

	outCompressedTokenVector.push_back( intempCompressedToken );

	return WAX_RESULT_OK;
}


//! Checking out of memory entry - dictionary based de-duplication using StrongSignature
/*!
*	Two main functionalities :
*        - check for on boundary hit ( already computed StrongSignature )
*        - for not on boundary hit - compute new StrongSignature 
*
*   Whenever the inSigLogEntry's StrongSignature matches with in or out of
*   boundary the return value is true else is false.
*
*	\sa findTokensInBuffer()
*	\return  true/false - is strongSignature equal.
*/
bool WAXEncoder::
checkOutOfMemoryEntry(
	/*__in*/    SignatureLogEntry* inSigLogEntry, //!< SignatureLogEntry - that needs to be validated 
	/*__in*/    const unsigned char* inData,              //!< pointer to the buffer that is processed.
	/*__in*/    const  unsigned long inDataLength,        //!< size of input buffer.
	/*__inout*/ const  unsigned long inOffset             //!< The offset in the inData buffer
	)
{

	//BZ 8119 - check if dictionary based Dedupe is required for all cases.
	if( !m_bAdvancedDictionaryBasedDeDuplication )
	{
		return false;
	}

	bool bWeHaveAStrongSignatureMatch = false;
	
	// following calls - get the pre-calculated StrongSignature we search for
	// current offset in the signature map if we do not have entry for the
	// current offset that is because this offset is not on boundary.

	 // iterator for signatureMap used to search in map
	m_signatureMapIter = m_SignatureMAP.find( inOffset );

	if ( m_signatureMapIter != m_SignatureMAP.end() ) //Aligned CRC64 compare
	{
		ChunkCharacteristic* pChunkCharacteristicTemp = &m_signatureMapIter->second;

		// We will not check for signature cause this was found in dictionary
		// we will only check if strong signature is the same.
		// Also, we DO NOT have to check for NULL pChunkCharacteristicTemp because
		// in the map we cannot have a NULL chunkCharacteristics - see computeSignaturesOnBuffer
		// for more details.
		/* ( ( NULL != pChunkCharacteristicTemp ) &&*/
		if( pChunkCharacteristicTemp->strongSignature == inSigLogEntry->strongSignature )
		{
			m_StatisticsAdvancedGoodMatchesoUToFmEMORY++;
			// This entry is out of memory.
			// There is no need for another operation in order
			// to move it to memory cause by design this will automatically
			// be set to in memory entry in signature log
			// need to pass this further to StoreManager
			pChunkCharacteristicTemp->sigLogEntry = inSigLogEntry;

			// We have a dictionary match - signature and strong signature are
			// equal - we create a token for it

			bWeHaveAStrongSignatureMatch = true;
		} 
	}
	else 
	{
		if( ( inOffset + m_SignatureDistance) < inDataLength )//attempt to match out of mem only if we have space
		{
			//////////////////////////////////////////////////////////////////////////////////
			//////////////////////////////////////////////////////////////////////////////////
			////  NOTE: this slows down the whole processing cause will compute a crc64.
			//////////////////////////////////////////////////////////////////////////////////
			////  1st implementation we did not want to do recalculate CRC64 
			////  this was a desired and intended behavior
			////  unaligned references to strong signatures are not allowed in signature map
			////  because the current offset is not aligned( will not be aligned in transfer log )
			////  This changed in order to have more Dictionary based de-duplication 
			//////////////////////////////////////////////////////////////////////////////////
			//////////////////////////////////////////////////////////////////////////////////
			// for debug only - will be removed
			m_StatisticsAdvancedNOTONBOUNDARY++;
			// for debug only - will be removed

			// unaligned references to an out of memory signature
			// compute CRC64 for this
			StrongSignatureType aStrongSignature = ComputeCRC64(
				&inData[inOffset],
				( m_SignatureDistance ) 
				);

			if ( aStrongSignature == inSigLogEntry->strongSignature )
			{
				m_StatisticsAdvancedNOTonBoundaryHIT++;
				// we have a a dictionary based de-duplication
				// crc64 match and we will create a token for it

				bWeHaveAStrongSignatureMatch = true;
			}
		}
	}

	return bWeHaveAStrongSignatureMatch;
}

//! Checking in memory entry - grow compare
/*!
*   Uses params to compare left and right of buffer.
*   before this call inDataBufferForCompare was validated by findTokensInBuffer()
*   and it has a minimum size of maxRightGrow and on the left of the pointer has
*   minimum size of sizeLGrow
*
*	\sa findTokensInBuffer()
*	\return  true/false - is (sizeLGrow+sizeRGrow )!=0.
*/
bool WAXEncoder::
checkInMemoryEntry(
	/*__in*/ const unsigned char*  addressInMemoryForGrowCompare, //!< SignatureLogEntry - that needs to be validated
	/*__in*/ const unsigned char*  inDataBufferForCompare,        //!< pointer to the buffer that is processed
	/*__in*/ const  unsigned long  maxRightGrow,                  //!< size of input buffer
	/*__in*/ const  unsigned long  maxLeftGrow,                   //!< The offset in the inData buffer
	/*__inout*/     unsigned long& sizeRGrow,                     //!< The offset in the inData buffer
	/*__inout*/     unsigned long& sizeLGrow                      //!< The offset in the inData buffer
	)
{
	//grow compare right from ulOffset
	 sizeRGrow = growCompareRight(
		maxRightGrow,
		inDataBufferForCompare,
		addressInMemoryForGrowCompare
		);

	if ( sizeRGrow >= m_SignatureBase )//tmp debug stat
	{
		m_StatisticsAdvancedGoodMatchesInMemory++;
	}//tmp debug stat

	//grow compare left  from ulOffset
	sizeLGrow = growCompareLeft(
		maxLeftGrow,
		inDataBufferForCompare,
		addressInMemoryForGrowCompare
		);

	m_StatisticsAdvancedGROWLeft  +=sizeLGrow;
	m_StatisticsAdvancedGROWRight +=sizeRGrow;
	
	return ( (sizeRGrow+sizeLGrow)!=0 )?true:false;
}


//! Implementation of the WAX DD Algorithm.
/*!
*	findTokensInBuffer() is the implementation of the WAX De-Duplication Algorithm.
*
*	It uses index (dictionary) based search for locating possible string of matching data and grow compare algorithm
*	to check that the data is the same. This method is used for the portion of the traffic seen by the source server 
*	That still resides in memory.
*
*	For the portion of traffic that is only referenced by pairs of <signature, Strong_signature>
*	the algorithm uses dictionary based DD. The match is done here by comparing 64 bit CRC on data strings that had a 
*	signature match.
*/	
WAX_RESULT WAXEncoder::
findTokensInBuffer(
	/*__in*/  const unsigned char* inData,       //!< pointer to the buffer that is processed.
	/*__in*/  const unsigned long  inDataLength, //!< size of input buffer.
	/*__out*/ CompressedTokenVector& outCompressedTokenVector //!< Compressed Token Vector - output storage for tokens.
	)
{
	WAX_RESULT wResult = WAX_RESULT_OK;

	unsigned long ulLastMatchOffset = 0; // last match offset for calculation of max left grow
	unsigned long ulOffset          = 0; // currently processed offset
	unsigned long maxLeftGrow       = 0; // max left grow  - used by grow compare 
	unsigned long maxRightGrow      = 0; // max right grow - used by grow compare 

	long long llTransferLogOffset; // a offset in the transferLog

	SignatureLogEntry*   sigLogEntry     = NULL;   // temporary signature log entry
	unsigned char* addressInMemoryForGrowCompare = NULL;   // temporary address in memory for grow compare

	Adler_MAP::iterator adlerMapIter; // iterator for m_AdlerMAP used to search in map 

	Adler currentAdlerSlider; // current Adler32 slider - used to compute Signatures 
	bool  bSlide = false;     // first time we do not slide; will be used to flag sliding 
	                          // in case that we have a token we will not slide
	unsigned long sizeLGrow = 0;
	unsigned long sizeRGrow = 0;

	// For every offset in the buffer starting from the beginning of the buffer until (the end-m_SignatureBase) we
	// do slide and lookup in signature log for anchors.
	// currentAdlerSlider - this is the slider whenever an anchor is found this 
	// will be recalculated or if offset is on boundary use a pre-calculated Adler from m_AdlerMAP
	while( (ulOffset + m_SignatureBase)<=inDataLength  )
	{
		if ( bSlide )
		{
			m_StatisticsAdvancedSLIDES++;
			currentAdlerSlider.slide(
				inData[ ulOffset ],
				inData[ ulOffset + m_SignatureBase ] 
			);

			ulOffset++;//increase the ulOffset here only!!!
		}
		else
		{
			adlerMapIter = m_AdlerMAP.find( ulOffset );
			if( adlerMapIter != m_AdlerMAP.end() )
			{
				currentAdlerSlider = adlerMapIter->second;
			}
			else
			{
				m_StatisticsAdvancedAdlerSetup++;
				currentAdlerSlider.setup( 
					&inData[ulOffset], 
					m_SignatureBase 
					);
			}
		}
		
		bSlide = true;

		wResult = m_SignatureLog->searchEntryBySignature( 
			currentAdlerSlider.get(), 
			sigLogEntry 
			);

		m_StatisticsAdvancedSearched++;
		
		if ( WAX_RESULT_OK == wResult )
		{
			maxRightGrow = inDataLength - ulOffset;      
			maxLeftGrow  = ulOffset - ulLastMatchOffset;

			// maxRightGrow and maxLeftGrow will be adjusted by following call 
			// also will set the transferLog offset and the address in memory 
			// corresponding to signature log entry if the entry is in memory
			// If the entry is not in memory the returned value will be 
			// WAX_RESULT_OFFSET_NOT_IN_MEM
			// getBufferBySigLogEntry changes to maxRightGrow includes the current byte, 
			// and changes to maxLeftGrow does not include the current byte
			wResult = m_StoreManager->getBufferBySigLogEntry(
				sigLogEntry,
				maxLeftGrow,
				maxRightGrow,
				llTransferLogOffset,
				addressInMemoryForGrowCompare 
				);

			if ( WAX_RESULT_OFFSET_NOT_IN_MEM == wResult )
			{
				m_StatisticsAdvancedMatchesoUToFmEMORY++;

				if ( true == checkOutOfMemoryEntry( 
					sigLogEntry, 
					inData, 
					inDataLength, 
					ulOffset ) )
				{
					generateTokenAndSave(
						ulOffset,
						llTransferLogOffset,
						m_SignatureDistance,
						outCompressedTokenVector 
						);

					//STATS
					m_ullOldStringNumber++;
					m_ullOldBytesNumber += m_SignatureDistance;
					
					// Token created -> prepare flags for next round
					bSlide    = false;
					ulOffset += m_SignatureDistance;
					ulLastMatchOffset = ulOffset;
				}
			}// end of check out of memory 
			else if( WAX_RESULT_OK == wResult ) // !WAX_RESULT_OFFSET_NOT_IN_MEM - offset is in memory
			{
				m_StatisticsAdvancedMatchesInMemory++; //tmp debug stat

				if ( true == checkInMemoryEntry(
					addressInMemoryForGrowCompare,
					&inData[ulOffset],
					maxRightGrow,
					maxLeftGrow,
					sizeRGrow,
					sizeLGrow ) )
				{
					if ( (sizeLGrow + sizeRGrow) >= m_ulMinDeDuplicateLength )
					{ 
						//////////////////////////////////////////////////////////////
						//              sizeLgrow           sizeRgrow
						//           [-------------][=============================]
						//           ^              ^
						//      tokenOffset       ulOffset
						//
						// tokenOffsetCurrentBuffer = ( ulOffset - sizeLGrow)
						// tokenOffsetInTransferLOG = ( ullTransferLogOffset - sizeLGrow)
						// tokenSize                = ( sizeLgrow + sizeRGrow)
						// 
						//////////////////////////////////////////////////////////////
						generateTokenAndSave(
							( ulOffset - sizeLGrow ),
							( llTransferLogOffset - sizeLGrow ),
							( sizeLGrow + sizeRGrow ),
							outCompressedTokenVector 
							);

						//STATS
						m_ullRecentStringNumber++;
						m_ullRecentBytesNumber += ( sizeLGrow + sizeRGrow );


						//computed max ulLastMatchOffset maxLeftGrow will use this 
						ulLastMatchOffset = ulOffset + sizeRGrow;

						//wcout<<"\r\n.["<<sizeLGrow<<"<>"<<sizeRGrow<<"]("<<sizeLGrow+sizeRGrow<<")";
					}

					if( sizeRGrow > 0 )
					{
						ulOffset += sizeRGrow ;
						bSlide    = false;
					}
				}// end of check in memory 
			}
		}// end of entry found continue sliding
	}
	
	return WAX_RESULT_OK;
}



WAX_RESULT WAXEncoder::
GetStatistics(
	/*__inout*/ Statistics* inoutStats //!< output statistic structure
	)
{
	//Set DeDupe statistics
	inoutStats->dedupOldStringsNumber = m_ullOldStringNumber;
	inoutStats->dedupOldBytesNumber = m_ullOldBytesNumber;

	inoutStats->dedupRecentStringsNumber = m_ullRecentStringNumber;
	inoutStats->dedupRecentBytesNumber = m_ullRecentBytesNumber;

	if (inoutStats->bDumpAdditionalStats )
	{
		std::wstringstream logsss;

		uint64_t msRuntime;
		uint64_t msCPUTime;
		getCPUAndRUNTimesInMS( msCPUTime, msRuntime );

		logsss<<endl<<L"WAXENCODERSTATS"
			<<L","<<msCPUTime
			<<L","<<m_StatisticsAdvancedProcessedBufferSize
			<<L","<<m_StatisticsAdvancedGoodMatchesInMemory
			<<L","<<m_StatisticsAdvancedMatchesInMemory
			<<L","<<m_StatisticsAdvancedGoodMatchesoUToFmEMORY
			<<L","<<m_StatisticsAdvancedMatchesoUToFmEMORY
			<<L","<<m_StatisticsAdvancedNOTONBOUNDARY
			<<L","<<m_StatisticsAdvancedNOTonBoundaryHIT
			<<L","<<m_StatisticsAdvancedSearched
			<<L","<<m_StatisticsAdvancedSignaturesCreated
			<<L","<<m_StatisticsAdvancedGROWRight
			<<L","<<m_StatisticsAdvancedGROWLeft
			<<L","<<m_StatisticsAdvancedTokensGenerated
			<<L","<<m_StatisticsAdvancedNumberOfTails
			<<L","<<m_StatisticsAdvancedTokensSIZE
			<<L","<<m_StatisticsAdvancedAdlerReuseFromMap
			<<L","<<m_StatisticsAdvancedSLIDES
			<<L","<<m_StatisticsAdvancedAdlerSetup
			<<L","<<m_StatisticsAdvancedNumberOf511Token
			<<endl<<endl;

		LogEventMessage( LOG_LEVEL_INFO, m_ComponentID, (wchar_t*)logsss.str().c_str() );
	}

	return WAX_RESULT_OK;
}

//! Process buffer - entry point for WAX Encoder.
/*! Process buffer - entry point for WAX Encoder - contains logic of encoding the input buffer.
*
*  Calls : 
*		processTailAndComputeSignatures()
*		findTokensInBuffer()
*		m_StoreManager->writeBuffer()
*
*  Caller : UWM::EncodeBuffer()
*
*  \sa UWM::EncodeBuffer()
*  \return - WAX_RESULT_INVALID_BUFFER_OR_SIZE - if input pointer or size invalid 
*          - WAX_RESULT_OK - EncodeBuffer completed successfully 
*/
WAX_RESULT WAXEncoder::
EncodeBuffer(
	/*__in*/    const unsigned char* inData,       //!< pointer -to the buffer that is processed.
	/*__in*/    const unsigned long  inDataLength, //!< size of incoming buffer - inData
	/*__inout*/ CompressedTokenVector& outCompressedTokenVector //!< Compressed Token Vector - output storage for tokens.
	)
{
	m_StatisticsAdvancedProcessedBufferSize +=inDataLength;

	WAX_RESULT wResult = WAX_RESULT_OK;
	
	//clear data
	m_SignatureMAP.clear();
	m_AdlerMAP.clear();
	m_characteristicsVector.clear();

	if ( ( inDataLength == 0 ) || ( NULL == inData ) )
	{
		return WAX_RESULT_INVALID_BUFFER_OR_SIZE;
	}

	// First step is to create signatures for Signature Log
	// We will create fixed size chunks for every SD sized data ( chunk )
	// We will start the processing from the beginning of the input buffer inData
	// but we will add the last entry for signature log ( we have the tail ).
	wResult = processTailAndComputeSignatures(
		inData,
		inDataLength
		);

	if( wResult == WAX_RESULT_GENERATE_TOKENS )
	{
		// after processTailAndComputeSignatures() we have a vector of signatures and a vector of 
		// Adler pointers that we will use to slide.
		// in case that we make a grow until a close Adler we roll back from an 
		// existing Adler
		wResult = findTokensInBuffer(
			inData,
			inDataLength,
			outCompressedTokenVector
			);
	}
	else
	{
		if( WAX_RESULT_OK != wResult )
		{
			LogError(
				m_ComponentID,
				DBG_LOCATION,
				L"Failed to process signatures with result: %d",
				wResult 
				);
		}
	}

	// StoreManager::writeBuffer
	// call store manager in order to add processed buffer to transfer log and add 
	// new signature log entries 
	if( (WAX_RESULT_OK == wResult) && m_bWriteToStorman )
	{
		//set ByteBuffer for writeBuffer
		ByteBuffer writeBufferForeStore;

		writeBufferForeStore.buffer = (unsigned char*)inData;
		writeBufferForeStore.length = inDataLength;

		wResult = m_StoreManager->writeBuffer(
			&writeBufferForeStore, 
			m_characteristicsVector 
			);

		if( WAX_RESULT_OK != wResult )
		{
			LogError(
				m_ComponentID,
				DBG_LOCATION,
				L"Failed Write to store. %u",
				wResult);
		}

	}

	return wResult;
}


//! Implementation of start - for details see WaxControlerBase::start().
/*!
*  Start the WAXEncoder.
*/
WAX_RESULT WAXEncoder::
start(
	/*__in*/ const bool inAsEncoder,            //!< is start an encode process
	/*__in*/ const bool inAdvancedDeduplication //!< is advanced de-duplication enabled
	)
{
	//Initialize statistics
	m_ullRecentStringNumber = 0;
	m_ullRecentBytesNumber = 0;
	m_ullOldStringNumber = 0;
	m_ullOldBytesNumber = 0;

	if ( (!m_isStarted) && inAsEncoder )
	{
		ConfigMan* configManTemp = ConfigMan::getInstance();
		m_SignatureDistance      = configManTemp->GetSignatureDistance();
		m_SignatureBase          = configManTemp->GetSignatureBase();
		m_ulMinDeDuplicateLength = configManTemp->GetMinDeDuplicateLengthReplacedByToken();
		m_bAdvancedDictionaryBasedDeDuplication = inAdvancedDeduplication;

		m_StoreManager = StoreManager::getInstance();
		m_SignatureLog = SignatureLog::getInstance();
		
		if ((NULL == m_StoreManager) ||
			(NULL == m_SignatureLog) )
		{
			LogError(
				m_ComponentID,
				DBG_LOCATION,
				L"Invalid Components."
				);

			return WAX_RESULT_NOT_STARTED;
		}

		if ( NULL != m_TailBuffer )
		{
			// we need to delete if it is started with different signature distance 
			free(m_TailBuffer);
			m_TailBuffer = NULL;
		}

		m_TailBuffer = (unsigned char*)malloc( m_SignatureDistance );
		m_TailLength = 0;// no tail by default :)


		// TODO:check if buffers allocated are not null ... all of them are valid;
		// we might want to ZerroMemory() but that is not necessary  ... 
		if( (NULL != m_TailBuffer) )
		{
			WAXControlableBase::start( inAsEncoder );
		}
		else
		{
			LogError(
				m_ComponentID,
				DBG_LOCATION,
				L"Encoder failed to allocate tail."
				);

			return WAX_RESULT_NOT_STARTED;
		}
	}

	return WAX_RESULT_OK;
}


//! Implementation of stop - for details see WaxControlerBase::stop().
/*!
*	Stop the WAXEncoder.
*/
WAX_RESULT WAXEncoder::
stop()
{
	if (m_isStarted)
	{
		if( NULL != m_TailBuffer )
		{
			free( m_TailBuffer );
			m_TailBuffer = NULL;
		}

		WAXControlableBase::stop();
	}

	return WAX_RESULT_OK;
}


//! Implementation of initialize - for details see WaxControlerBase::initialize().
/*!
*	Initialize the WAXEncoder.
*/
WAX_RESULT WAXEncoder::
initialize()
{
	if (!m_isInitialized)
	{
		WAXControlableBase::initialize();
	}

	return WAX_RESULT_OK;
}

void WAXEncoder::setEnableWriteToStorman(
	/*__in*/ const bool bEnabled  //!< write to StoreMan if true
	)
{
	m_bWriteToStorman = bEnabled;
}
