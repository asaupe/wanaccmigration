#pragma once

#include <windows.h>
#include <cstdint>

//!< Compute CRC64 value
uint64_t ComputeCRC64( const void* inData, uint32_t inLength);
