//********************************************************/
//  ./Encoder/encoder.h
//
//  Owner     : Attila Vajda
//
//  Company   : Neverfail
//  PROJECT   : WAN Acceleration
//  Component : WAX Encoder
//  Purpose   : Implementation of encoding algorithm
//
//  $Rev: 1483 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: avajda $
//  $Date: 2009-01-14 10:20:20 +0000 (Wed, 14 Jan 2009) $
//
//********************************************************/

#pragma once
#include "WAXTypes.h"
#include "../WaxSingletonTemplate.h"
#include "adler.h"

#include "../WAXControlableBase.h"
#include "../StoreManager/StoreManager.h"
#include "../StoreManager/SignatureLog.h" 
#include <map>


//! WAXEncoder - encoding the buffer
/*! \class WAXEncoder
*   \brief Implements encoding algorithm for WAN Acceleration.
*
*   Is called by UNIT OF WORK Manager (UWM) with a buffer and the size of the buffer as parameters.
*
*   WAXEncoder returns a list of tokens to UWM leaving it to UWM to create an encoded buffer from the list of tokens.
*
*   The encoder uses dictionary based search for signatures and a grow compare algorithm to perform compression.
*
*   WAX Encoder has no knowledge about the input buffer structure and processes it from the beginning until the end.
*   For more details see EncodeBuffer method which is the entry point for WAXEncoder.
*
*    UWM - calls EncodeBuffer() which returns a list of tokens.
*    The input buffer is processed using signature based search for known patterns and grow compare algorithm.
*
*   \author Attila Vajda
*   \version 1.0.$Rev: 1483 $
*   \date    JUN-2008
*/
class WAXEncoder: public WaxSingletonTemplate<WAXEncoder>, WAXControlableBase, public IStats
{
	friend class WaxSingletonTemplate<WAXEncoder>;
	friend class EncoderTest;

public:
	//! Process buffer - entry point for WAX Encoder.
	WAX_RESULT EncodeBuffer(
		/*__in*/    const unsigned char* inData,       //!< pointer -to the buffer that is processed.
		/*__in*/    const  unsigned long inDataLength, //!< size of incoming buffer - inData
		/*__inout*/ CompressedTokenVector& outCompressedTokenVector //!< Compressed Token Vector - output storage for tokens.
	);

	// Methods declared in WaxControlerBase that are also implemented in the encoder.
	//! Implementation of initialize - for details see WaxControlerBase::initialize.
	WAX_RESULT initialize(
		);

	//! Implementation of start - for details see WaxControlerBase::start().
	WAX_RESULT start(
		/*__in*/ const bool inAsEncoder,            //!< is start an encode process
		/*__in*/ const bool inAdvancedDeduplication //!< is advanced de-duplication enabled
		);

	//! Implementation of stop - for details see WaxControlerBase::stop().
	WAX_RESULT stop(
		);

	//! Get the statistics from this module
	WAX_RESULT GetStatistics(
		/*__inout*/ Statistics* inoutStat //!< output statistic structure - in value is bDumpAdditionalStats
		);

private:
	WAXEncoder();
	~WAXEncoder();

	//! Generates ChunkCharacteristic for the input buffer.
	inline void WAXEncoder::generateChunkCharacteristic(
		/*__in*/    const unsigned char* inData, //!< buffer where we will compute signatures
		/*__inout*/ Adler& inoutAdler,   //!< call setup for this and compute Adler
		/*__inout*/ ChunkCharacteristic& outChunkCharacteristic //!< fill a chunk characteristic 
	);

	//! Creates list of Signatures (Signature and Strong Signature ) for the current buffer.
	WAX_RESULT computeSignaturesOnBuffer(
		/*__in*/ const  unsigned char* inData,          //!< pointer -to the buffer that needs to be processed.
		/*__in*/ const  unsigned long inDataLength,    //!< size of the inData - this is for verify validity of the inData
		/*__in*/ const  unsigned long inOffsetAllign,  //!< because of the tail left over from the previous buffer processed we
		                                   //   will have to align the buffer
		/*__in*/ const   UINT inNrOfSignatures //!< nr of signatures that we will process - the offset that we start to
		                                   //   compute the signatures is in inOffsetAllign and this will compute
		                                   //   inNrOfSignatures number of signatures.
		);

	//! Processes the tail and calls computeSignaturesOnBuffer() to compute signatures.
	WAX_RESULT processTailAndComputeSignatures(
		/*__in*/ const unsigned char* inData,      //!< pointer -to the buffer that needs to be processed.
		/*__in*/ const  unsigned long inDataLength //!< size of the inData - this is for verify validity of the inData
	);

	//! Implementation of the WAX DD Algorithm.
	WAX_RESULT findTokensInBuffer(
		/*__in*/    const unsigned char* inData,       //!< pointer to the buffer that is processed.
		/*__in*/    const  unsigned long inDataLength, //!< size of input buffer.
		/*__inout*/ CompressedTokenVector& outCompressedTokenVector //!< Compressed Token Vector - output storage for tokens.
		);

	//! Generates a token from the supplied parameters and adds it to the output token vector.
	WAX_RESULT generateTokenAndSave(
		/*__in*/    const     unsigned long inTokenOffsetCurrentBuffer,     //!< offset of token in current buffers
		/*__in*/    const uint64_t inTokenOffsetInTransferLOG,     //!< offset of token in transfer log
		/*__in*/    const     unsigned long inTokenSize,                    //!< size of the token
		/*__inout*/ CompressedTokenVector& outCompressedTokenVector //!< location to add the new token generated using 
		//   the supplied parameters
		);

	//! Checking out of memory entry - dictionary based de-duplication using StrongSignature 
	inline bool checkOutOfMemoryEntry(
		/*__in*/    SignatureLogEntry* inSigLogEntry, //!< SignatureLogEntry - that needs to be validated 
		/*__in*/    const unsigned char* inData,              //!< pointer to the buffer that is processed.
		/*__in*/    const  unsigned long inDataLength,        //!< size of input buffer.
		/*__inout*/ const  unsigned long inOffset             //!< The offset in the inData buffer
		);

	//! Checking in memory entry - grow compare
	inline bool checkInMemoryEntry(
		/*__in*/ const unsigned char*  addressInMemoryForGrowCompare, //!< SignatureLogEntry - that needs to be validated 
		/*__in*/ const unsigned char*  inDataBufferForCompare,        //!< pointer to the buffer that is processed.
		/*__in*/ const  unsigned long  maxRightGrow,                  //!< size of input buffer.
		/*__in*/ const  unsigned long  maxLeftGrow,                   //!< The offset in the inData buffer
		/*__inout*/     unsigned long& sizeRGrow,                     //!< The offset in the inData buffer
		/*__inout*/     unsigned long& sizeLGrow                      //!< The offset in the inData buffer
		);

	//! Grow Compare byte by byte compare 2 memory addresses - grow right.
	unsigned long growCompareRight(
		/*__in*/ const                   unsigned long inCount,              //!< count of bytes to compare
		/*__in*/ const __bcount(inCount) byte* inProcessedMemory,    //!< 1'st memory to me compare with 2'nd
		/*__in*/ const __bcount(inCount) byte* inInMemoryTRLOGMemory //!< 2'nd memory to me compare with 1'st
		);

	//! Grow Compare byte by byte compare 2 memory addresses - grow left. 
	unsigned long growCompareLeft( 
		/*__in*/ const unsigned long inCount,              //!< count of bytes to compare 
		/*__in*/ const byte* inProcessedMemory,    //!< 1'st memory to me compare with 2'nd
		/*__in*/ const byte* inInMemoryTRLOGMemory //!< 2'nd memory to me compare with 1'st
		);

	//////////////////////////////////////////////////////////////////////////
	// For test Only                                                        //
	//////////////////////////////////////////////////////////////////////////

	//! Enable/Disable Write to StoreManafer - used for testing only
	void setEnableWriteToStorman(
		/*__in*/ const bool bEnabled
		);

	//////////////////////////////////////////////////////////////////////////
	// Local Typedefs                                                       //
	//////////////////////////////////////////////////////////////////////////

	//! Characteristic vector passed to Store Manager
	typedef vector < ChunkCharacteristic > ResultVector;

	//! Signature map - Map of signatures at every fixed offset.
	//  used for dictionary based de-duplication
	typedef map< unsigned long, ChunkCharacteristic > Signatures_MAP;

	//! Adler map - will be used by findTokensInBuffer() to slide 
	//  this is a map of saved Adler;
	typedef map< unsigned long, Adler > Adler_MAP;

	//////////////////////////////////////////////////////////////////////////
	// Variables                                                            //
	//////////////////////////////////////////////////////////////////////////

	///////////////
	// STATIC    //
	///////////////
	static WAX_ID m_ComponentID;    //!< component ID of WAXEncoder

	///////////////
	// LOCAL     //
	///////////////
	StoreManager* m_StoreManager  ; //!< Store Manager object
	SignatureLog* m_SignatureLog  ; //!< Signature Log object

	// Local values initialized at start
	UINT  m_SignatureBase         ; //!< Signature Base 
	UINT  m_SignatureDistance     ; //!< Signature Distance
	unsigned long m_ulMinDeDuplicateLength; //!< Min de-duplicate length

	// Tail management members
	UINT   m_TailLength; //!< Length of the tail
	UINT   m_TailOffset; //!< Offset of tail in the current buffer
	unsigned char* m_TailBuffer; //!< buffer for tail - the size of it is m_SignatureDistance

	Adler_MAP 
		m_AdlerMAP;      //!< map of sliders - these are used by grow compare algorithm
		                 //   whenever the offset is on boundary

	Signatures_MAP 
		m_SignatureMAP;  //!< m_SignatureMAP - contains list of pre-calculated signatures
		                 //   from currently processed buffer used for dictionary based
		                 //   de-duplication

	Signatures_MAP::iterator 
		m_signatureMapIter; //!< m_SignatureMAP - contains list of pre-calculated signatures

	ResultVector 
		m_characteristicsVector; //!< passed StoreManager - a collection of characteristics - signatures
	                             //   for every fixed size chunk in processed buffer

	bool m_bAdvancedDictionaryBasedDeDuplication; //!< advanced dictionary based de-duplication enabler

	//for testing 
	bool m_bWriteToStorman; //!< for testing disable write to StoreMan

	//	STATS
	uint64_t 
		m_ullRecentStringNumber,
		m_ullRecentBytesNumber,
		m_ullOldStringNumber,
		m_ullOldBytesNumber;


	// Advanced statistics - might not get into statistics
	uint64_t 
		m_StatisticsAdvancedGoodMatchesInMemory,
		m_StatisticsAdvancedMatchesInMemory,
		m_StatisticsAdvancedGoodMatchesoUToFmEMORY,
		m_StatisticsAdvancedMatchesoUToFmEMORY,
		m_StatisticsAdvancedNOTONBOUNDARY,
		m_StatisticsAdvancedNOTonBoundaryHIT,
		m_StatisticsAdvancedSearched,
		m_StatisticsAdvancedSignaturesCreated,
		m_StatisticsAdvancedGROWRight,
		m_StatisticsAdvancedGROWLeft,
		m_StatisticsAdvancedProcessedBufferSize,
		m_StatisticsAdvancedTokensGenerated,
		m_StatisticsAdvancedNumberOfTails,
		m_StatisticsAdvancedTokensSIZE,
		m_StatisticsAdvancedAdlerReuseFromMap,
		m_StatisticsAdvancedSLIDES,
		m_StatisticsAdvancedAdlerSetup,
		m_StatisticsAdvanced_CurrentStat,
		m_StatisticsAdvancedNumberOf511Token;

	//////////////////////////////////////////////////////////////////////////
	// END OF WAXEncoder
	//////////////////////////////////////////////////////////////////////////
};
