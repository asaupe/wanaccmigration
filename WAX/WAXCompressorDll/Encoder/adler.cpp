//********************************************************/
//  ./Encoder/adler.cpp
//
//  Owner     : Lyndon Clarke/Tom Stones - minor changes by WAX Team 
//
//  Company   : Neverfail 
//  PROJECT   : WAN Acceleration 
//  Component : Adler - WAXEncoder
//  Purpose   : Implementation of Adler32 checksum algorithm.
//
//  $Rev: 2744 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: cmaris $  
//  $Date: 2009-07-01 14:28:14 +0100 (Wed, 01 Jul 2009) $
//
//********************************************************/

#include "adler.h"
int Fudgie = 31;
int Fudgie_X_4 = Fudgie * 4 ;
int Fudgie_X_6 = Fudgie * 6;

Adler::Adler()
{
	a = 0;
	b = 0;
	l = 0;
}

Adler::~Adler()
{
}

void Adler::setup(
		/*__in*/ const byte* inBuffer,    //!< setup Adler on this buffer
		/*__in*/ const int   inDataLength //!< the size of the buffer
		)
{
    //In this version we do 4-way loop unrolling and we add a const "fudgie" to each byte.
    a = 0;
    b = 0;
    l = inDataLength;
    byte *p1 = (byte*)inBuffer;
    byte *pend = (byte*)(inBuffer + inDataLength);
    byte *p2 = p1 + 1;
    byte *p3 = p1 + 2;
    byte *p4 = p1 + 3;
    while (p4 < pend) 
	{
        a += (*p1) + (*p2) + (*p3) + (*p4) + (Fudgie_X_4);
        b += (4*a) - (*p4*3) - (*p3*2) - (*p2) - (Fudgie_X_6);
        p1 += 4;
        p2 += 4;
        p3 += 4;
        p4 += 4;
    }

    while (p1 < pend) 
	{
        a += *p1 + (Fudgie);
        b += a;
        p1++;
    }
}

void Adler::slide(
	/*__in*/ const byte inByteOut,    //!< byte to remove from checksum
	/*__in*/ const byte inByteIn      //!< new byte to add to checksum
	) 
{
	a += ( inByteIn - inByteOut );   //fudgies match - one in and one out
	b += ( a - (l * (inByteOut+Fudgie)) ) ;
}

//! return value of Adler
unsigned long Adler::get( void )
{
	return (a & 0xffff) | (b << 16);
}
