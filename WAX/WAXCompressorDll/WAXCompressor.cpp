//********************************************************/
//  ./WAXCompressor.cpp
//
//  Owner : Ciprian Maris
//  
//	Company		: Neverfail 
//	PROJECT		: WAN Acceleration 
//	Component	: WAXCompressor 
//	Purpose		: Implementation of the functions exported by the WAX Compressor DLL 
//
//	NOTE:  This unit does not log any error. It only returns error codes to the caller.
//
//  $Rev: 1590 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: cmaris $  
//  $Date: 2009-01-26 10:02:17 +0000 (Mon, 26 Jan 2009) $
//
//********************************************************/

#include "WAXCompressor.h"
#include "WAXInterface.h"

#include "./Logger/Logger.h"
#include "./ConfigMan/ConfigMan.h"

/**
*	FLAGS that signal the current state of the DLL
*		Calls to methods in wax interface are made only if the proper flag values are set.
*		E.g. WAXInterface::start() is called only if bAllocated is true (which implies bInitialized is true)
*
*	NOTE:	The WaxInterface component will not be involved in WAXCompressor state management.
*			This means that no state flags are kept in WaxInterface.
*			WaxCompressor.cpp unit is responsible for maintaining state.
*
*/
bool bInitialized	= false;
bool bAllocated		= false;
bool bStarted		= false;

bool bIsDecoder		= false;
bool bIsEncoder		= false;


/*!
*	WAX interface component: the gate to the WAX compressor.
*	All calls to the compressor are made using it.
*/
WAXInterface* waxInterface = NULL;

BOOL APIENTRY DllMain( HMODULE hModule,
                       unsigned long  ul_reason_for_call,
                       void* lpReserved
					 )
{
    return TRUE;
}
//-----------------------------------------------------------------------------

//! Function getDLLRevisionString() - Returns the revision string of the Compressor DLL.
wchar_t* getDLLRevisionString()
{
	return DLL_REVISION;
}
//-----------------------------------------------------------------------------

//! Function initialize() - Initialises the components of the compressor.
WAX_RESULT initialize(
		/*__in*/ const wchar_t* logFileName,	//!< The name of the log file.
		/*__in*/ bool inbSetDebugMode		//!< The initial debug mode of the compressor.
					  )
{
	//	The call to Logger::ResetErrorInfo() must be at the start of each function of the compressor.
	Logger::ResetErrorInfo();

	if( bInitialized )
	{
		return WAX_RESULT_ALREADY_INITIALIZED;
	}

	//	Since the logger intialisation is the only one that can fail,
	//	it will be called first.
	if (!Logger::Init( logFileName ) )
	{
		return WAX_RESULT_LOG_INIT_FAILED;
	}

	//	If Logger::Init() succeded, it is safe now to initialize the 
	//	WAXInterface and set the bInitialized flag.
	waxInterface = WAXInterface::getInstance();//IMPORTANT: the logger has to be initialized prior to this call
	setDebugMode( inbSetDebugMode );

	bInitialized = true;

	return WAX_RESULT_OK;
}
//-----------------------------------------------------------------------------

//! Function setDebugMode() - enable/disable printing out of debug info.
WAX_RESULT setDebugMode(
		/*__in*/ bool inbOn		//!< Value of the DEBUG mode requested by the caller.
		)
{
	//	The call to Logger::ResetErrorInfo() must be at the start of each function of the compressor.
	Logger::ResetErrorInfo();

	if (inbOn)
	{
		Logger::SetLogLevel(Logger::GetLogLevel() | LOG_LEVEL_DEBUG);
	}
	else
	{
		Logger::SetLogLevel(Logger::GetLogLevel() & ~LOG_LEVEL_DEBUG);
	}

	//NOTE: 
	//	This method always return OK for now.
	//	The implementation of the logger might change to return an error case in which this method will return an error code
	//	If the implementation of the logger does not change, the return value of this method will be removed
	return WAX_RESULT_OK;
}
//-----------------------------------------------------------------------------

//! Function exit() - Ends the lifespan of the compressor. 
WAX_RESULT exit(void)
{
	//	The call to Logger::ResetErrorInfo() must be at the start of each function of the compressor.
	Logger::ResetErrorInfo();

	if( bInitialized )
	{
		//First relase and then exit to preserve component state. The release also perfroms a stop.
		release();

		bInitialized = false;
		return waxInterface->exit();
	}
	return WAX_RESULT_OK;
}
//-----------------------------------------------------------------------------


//! Function start() - Sets the start parameters and starts the components that need to aquire pre-allocated resources.
WAX_RESULT start(
		/*__in*/ bool inAsEncoder,						//!< Server role (compressor/decompressor)
		/*__in*/ unsigned long inSignatureBase,					//!< signature base
		/*__in*/ unsigned long inSignatureDistance,				//!< signature distance
		/*__in*/ unsigned long inMinRPRPatternsLength,			//!< minimum length of ZERO/DA pattern replaced by a RPR token
		/*__in*/ unsigned long inMinDeDuplicateLength,			//!< minimum length of the buffer to be considered for DD
		/*__in*/ unsigned long inMinDDLengthReplacedByToken,	//!< minimum length of data replaced by a DD token
		/*__in*/ const bool inEncodeOutOfBoundaryEnabled ,	//!< advanced encode for out of boundary entries
		/*__in*/ double indNInvariant						//!< Factor for recent data volume and compression computation.
		)
{
	//	The call to Logger::ResetErrorInfo() must be at the start of each function of the compressor.
	Logger::ResetErrorInfo();

	WAX_RESULT retVal;

	if( !bAllocated )
	{
		return WAX_RESULT_NOT_ALLOCATED;
	}

	if( bStarted )
	{
		return WAX_RESULT_ALREADY_STARTED;
	}

	retVal = waxInterface->start(
				inAsEncoder,						
				inSignatureBase,					
				inSignatureDistance,				
				inMinRPRPatternsLength,				
				inMinDeDuplicateLength,				
				inMinDDLengthReplacedByToken,		
				inEncodeOutOfBoundaryEnabled, 
				indNInvariant
				);

	if( retVal != WAX_RESULT_OK )
	{
		bStarted = false;
		return retVal;
	}

	//Set ther started flag
	bStarted = true;
	
	//Set the server role flag
	bIsEncoder = inAsEncoder;
	bIsDecoder = !bIsEncoder;

	return WAX_RESULT_OK;
}
//-----------------------------------------------------------------------------

//! Function stop() - stops the compressor.
WAX_RESULT stop(void)
{
	//	The call to Logger::ResetErrorInfo() must be at the start of each function of the compressor.
	Logger::ResetErrorInfo();

	if( bStarted )
	{
		bStarted = false;
		//Set the server role flag
		bIsEncoder = false;
		bIsDecoder = false;

		return waxInterface->stop();
	}

	return WAX_RESULT_OK;
}
//-----------------------------------------------------------------------------

//! Function allocate() - Allocates resources for the compressor.
WAX_RESULT allocate(
		/*__in*/ unsigned long minimumMemoryBufferSize,	//!< Minimum memory to allocate for the compressor in MB's
		/*__in*/ unsigned long maximumMemoryBufferSize,	//!< Maximum memory to allocate for the compressor in MB's
		/*__in*/ unsigned long transferLogSize,			//!< On disk transfer Log Size in MB's
        /*__in*/ const StringBuffer * transferLogPath	//!< On disk transfer Log File path (complete path)
               )
{
	//	The call to Logger::ResetErrorInfo() must be at the start of each function of the compressor.
	Logger::ResetErrorInfo();

	WAX_RESULT retVal;
	if( !bInitialized )
	{
		return WAX_RESULT_NOT_INITIALIZED;
	}

	if( bAllocated )
	{
		return WAX_RESULT_ALREADY_ALLOCATED;
	}

	retVal = waxInterface->allocate(
				minimumMemoryBufferSize,	//!< Minimum memory to allocate for the compressor in MB's
				maximumMemoryBufferSize,	//!< Maximum memory to allocate for the compressor in MB's
				transferLogSize,			//!< On disk transfer Log Size in MB's
				transferLogPath				//!< On disk transfer Log File path (complete path)
				);

	if( retVal != WAX_RESULT_OK )
	{
		bAllocated = false;
		return retVal;
	}

	bAllocated = true;
	return WAX_RESULT_OK;
}
//-----------------------------------------------------------------------------

//! Function release() - releases pre-allocated disk space and memory.
WAX_RESULT release(void)
{
	//	The call to Logger::ResetErrorInfo() must be at the start of each function of the compressor.
	Logger::ResetErrorInfo();

	if( bAllocated )
	{
		//First stop and then relase to prezerve component state
		stop();

		bAllocated = false;
		return waxInterface->release();
	}
	return WAX_RESULT_OK;
}
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////
//////	Operations methods		///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////


//! Function encode() encodes the input buffer on source.
WAX_RESULT encode(
        /*__in*/ const ByteBuffer* inputBuffer,	//!< source buffer to be encoded
        /*__out*/ ByteBuffer* outputBuffer,		//!< destination buffer - pre-allocated by caller
        /*__in*/ bool removeRepeatedPatterns,	//!< FLAG - RPR requested by caller
        /*__in*/ bool deDuplicate				//!< FLAG - DD requested by caller
		)
{
	//	The call to Logger::ResetErrorInfo() must be at the start of each function of the compressor.
	Logger::ResetErrorInfo();

	//	Check if the encoder was started
	if( bStarted && bIsEncoder)
	{

		return waxInterface->encode(
				inputBuffer,				
				outputBuffer,				
				removeRepeatedPatterns,		
				deDuplicate					
				);
	}
	else
	{
		return WAX_RESULT_WRONG_ENCODE_CALL;
	}

}
//-----------------------------------------------------------------------------

//! Function decode() decodes the input buffer on target.
WAX_RESULT decode(
        /*__in*/  const ByteBuffer* inputBuffer,	//!< source buffer to be decoded
        /*__out*/ ByteBuffer* outputBuffer,			//!< destination buffer - pre-allocated by caller
		/*__in*/ bool bRPREncoded,
		/*__in*/ bool bDDEncoded
		)
{
	//	The call to Logger::ResetErrorInfo() must be at the start of each function of the compressor.
	Logger::ResetErrorInfo();

	//The compressor needs to be started in order to perfrom decode operations.
	if( bStarted && bIsDecoder )
	{
		return waxInterface->decode( inputBuffer, outputBuffer, bRPREncoded, bDDEncoded);
	}
	else
	{
		return WAX_RESULT_WRONG_DECODE_CALL;
	}
}
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////
//////	END Operations methods		///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//////	ERROR / FAILURE trace functions    ////////////////////////////////
///////////////////////////////////////////////////////////////////////////

//! Retreives the error information of the last failed call from the compressor.
const wchar_t* getErrorInfo(void)
{
	return Logger::GetErrorInfo();
}
//-----------------------------------------------------------------------------


///////////////////////////////////////////////////////////////////////////
//////	ERROR / FAILURE trace functions    ////////////////////////////////
///////////////////////////////////////////////////////////////////////////


//! Function queryStatistics() - gathers stats from the WAX components.
WAX_RESULT queryStatistics(
		/*__inout*/ Statistics *inoutStatistics //!< Statistics structure to receive the entire statistics set
		)
{
	return waxInterface->queryStatistics( inoutStatistics );
}
//-----------------------------------------------------------------------------

//! This function is only implemented for the win32 out of process operation method.
//	It just returns WAX_RESULT_OK.
WAXDLLEXPORT WAX_RESULT setup(IN StringBuffer* inSBServicePath) 
{ 
	return WAX_RESULT_OK; 
}
//-----------------------------------------------------------------------------
 
//! This function is only implemented for the win32 out of process operation method.
//	It just returns WAX_RESULT_OK.
WAXDLLEXPORT WAX_RESULT teardown(void)
{ 
	return WAX_RESULT_OK; 
}
//-----------------------------------------------------------------------------
