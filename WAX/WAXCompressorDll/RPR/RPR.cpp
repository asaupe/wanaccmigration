//********************************************************/
//  ./RPR/RPR.cpp
//
//  Owner : Ciprian Maris
//  
//  Neverfail             ________________
//           PROJECT   :  WAN Acceleration
//           Component :  RPR
//
//  $Rev: 2480 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: cmaris $  
//  $Date: 2009-05-22 14:08:44 +0100 (Fri, 22 May 2009) $
//
//********************************************************/

//********************************************************/
//
//	NOTE:	The code of this unit was changed as a resolution for BZ 7534:
//			- the token structure was changed
//			- The following methods were changed to use the new tokens:
//			CreateTokenUnCompress(), CreateTokenGeneric(), CreateTokenDA(), DecodeBuffer()
//			- the EncodeBuffer() method was shortened and now uses a pre-generated DA 
//			pattern for byte-by-nyte comparison
//
//********************************************************/


#include "../Common.h"
#include "RPR.h"
#include "../ConfigMan/ConfigMan.h"
#include "../Logger/Logger.h"

#include "../SharedTypes.h" 


//TODO: check is define is moved to RPR or not
#define MAX_RPR_MSG_SIZE 256*1024	// 256 Kb

#define LENGTH_ONE 1
#define LENGTH_TWO 2

typedef enum RPRTokenSizes
{
	RPR_TOKEN_UNCOMPRESSED_SIZE = sizeof(RPR_Token),	
			//!< we need to add here the length of the uncompressed data
	RPR_TOKEN_GENERIC_SIZE = sizeof(RPR_Token),
			//!< we need to add here the length of the generic pattern
	RPR_TOKEN_DA_SIZE = sizeof(RPR_Token)
			//!< we do not need to add any more data here
};

//	BZ 7534 - changed to a define out of enum
#define	MIN_SIZE_TOKEN_HEADER sizeof(TokenUncompressedONE) //RPR_TOKEN_DA_SIZE

//	BZ 7534 - need to define the sizes of the tokens by type. The encode and decode methods
//	must have a common way of computing the offsets in the output and input buffer respectively.
unsigned long SIZE_OF_TOKEN_BY_TYPE[20];

///////////////////////////////////////////////////////////////////////////////////////////////////
//	Utility functions implemented as part of the resolution for 7534

//! Extract the length of the data replaced by the current token. 
unsigned long extractTokenDataLength( 
	/*__in*/ RPR_Token* inToken 
	);

//! Extract the data needed for decoding the current token. 
bool extractTokenDataForDecode(
	/*__in*/ RPR_Token* inRprToken, 
	/*__out*/ bool& bUncompressed, 
	/*__out*/ bool& bDA,
	/*__out*/ unsigned char& daByte, 
	/*__out*/ bool& bPattern, 
	/*__out*/ unsigned long& ulRepeatedPatternLength, 
	/*__out*/ unsigned char* lpbPattern
	);

//! Computes the first offset of a different byte in the pre-generated DA pattern.
unsigned long getDifferentByteOffsetInDAPattern(
	/*__in*/ unsigned char inbDifferentValue 
	);

void generateDAPattern( 
	/*__inout*/ unsigned char* inoutBuffer,
	/*__in*/ unsigned long inulPatterLength,
	/*__in*/ unsigned char  inbFirstDifferentByteValue,
	/*__in*/ unsigned long inulFirstDifferentByteOffset
	);
//	END Utility functions implemented as part of the resolution for 7534
///////////////////////////////////////////////////////////////////////////////////////////////////


#define MAKE_DA_BYTE(currByte)	((((currByte) & 0x0F) << 4) | 0x0A)
#define IS_DA_BYTE(currByte)	(((currByte) & 0x0F) == 0x0A)

#define GET_DA_FIRST_DIFF_BYTE_POS(inByte)		(((inByte) >> 4) & 0x0F)
#define GET_DA_FIRST_DIFF_BYTE_VALUE(inByte)	((inByte) & 0x0F)

#define SET_DA_FIRST_DIFF_BYTE_POS(inByte, outByte)		outByte = (outByte & 0x0F) | (((inByte) << 4) & 0xF0)
#define SET_DA_FIRST_DIFF_BYTE_VALUE(inByte, outByte)	outByte = (outByte & 0xF0) | ((inByte) & 0x0F)

WAX_ID RPR::m_ComponentID = (WAX_ID)WAX_ID_RPR;

#define MIN_DA_PAT_LEN 16

unsigned long GUL_DA_PATTERN_LENGTH = 512 * 1024 + 1 ;
unsigned char* GBP_DA_PATTERN = (unsigned char*)malloc( GUL_DA_PATTERN_LENGTH );


RPR::RPR():m_ullDABytesNumber(0),
m_ullDAStringsNumber(0),
m_ullGenericBytesNumber(0),
m_ullGenericStringsNumber(0)//,
{
	m_bRawBuffer = NULL;
	m_bRawBuffer = new unsigned char[MAX_RPR_MSG_SIZE * 2];
	m_ulRawBufferCounter = 0;

	//	BZ 7534 - compute and assign token sizes by type

	//	Tokens - Size of the type and data fields
	unsigned long SIZE_TYPE = sizeof(RPRTokenTypes);

	//	COMPRESSED Tokens - size of additional data for 2 bytes patterns
	unsigned long SIZE_ADDITIONAL_DATA =  sizeof(unsigned char);

	SIZE_OF_TOKEN_BY_TYPE[ RPR_TOKEN_CMP_ONE_SINGLE ]	=	SIZE_TYPE + sizeof(TokenCompressedONE);
	SIZE_OF_TOKEN_BY_TYPE[ RPR_TOKEN_CMP_ONE_DOUBLE ]	=	SIZE_TYPE + SIZE_ADDITIONAL_DATA + sizeof(TokenCompressedONE);	

	SIZE_OF_TOKEN_BY_TYPE[ RPR_TOKEN_CMP_TWO_SINGLE ]	=	SIZE_TYPE + sizeof(TokenCompressedTWO);
	SIZE_OF_TOKEN_BY_TYPE[ RPR_TOKEN_CMP_TWO_DOUBLE ]	=	SIZE_TYPE + SIZE_ADDITIONAL_DATA + sizeof(TokenCompressedTWO);

	SIZE_OF_TOKEN_BY_TYPE[ RPR_TOKEN_CMP_FOUR_SINGLE ]	=	SIZE_TYPE + sizeof(TokenCompressedFOUR);
	SIZE_OF_TOKEN_BY_TYPE[ RPR_TOKEN_CMP_FOUR_DOUBLE ]	=	SIZE_TYPE + SIZE_ADDITIONAL_DATA + sizeof(TokenCompressedFOUR);

	SIZE_OF_TOKEN_BY_TYPE[ RPR_TOKEN_DA_ONE ]	=	SIZE_TYPE + sizeof(TokenCompressedONE);
	SIZE_OF_TOKEN_BY_TYPE[ RPR_TOKEN_DA_TWO ]	=	SIZE_TYPE + sizeof(TokenCompressedTWO);		
	SIZE_OF_TOKEN_BY_TYPE[ RPR_TOKEN_DA_FOUR ]	=	SIZE_TYPE + sizeof(TokenCompressedFOUR);		

	SIZE_OF_TOKEN_BY_TYPE[ RPR_TOKEN_UCP_ONE ]	=	SIZE_TYPE + sizeof(TokenUncompressedONE);			
	SIZE_OF_TOKEN_BY_TYPE[ RPR_TOKEN_UCP_TWO ]	=	SIZE_TYPE + sizeof(TokenUncompressedTWO);			
	SIZE_OF_TOKEN_BY_TYPE[ RPR_TOKEN_UCP_FOUR ]	=	SIZE_TYPE + sizeof(TokenUncompressedFOUR);			

	//generate the predefined DA Pattern
	generateDAPattern( GBP_DA_PATTERN, GUL_DA_PATTERN_LENGTH, 0, 0 );
}


RPR::~RPR()
{
	if(m_bRawBuffer != NULL)
	{
		delete[] m_bRawBuffer;
	}
}

//!Pocesses an input buffer by RPR
/*!
	Removes all repeating patterns sequences over a specific length and DA patterns 
	with specific tokens. The uncompressed data is also stored in specific tokens.
	Output data is stored in an output buffer along with it's length.
	
	The structure of the output buffer is:
	[1:Offset of uncompressed data][2:Tokens][3:Uncompressed Data]
		- 1:Offset of uncompressed data - represents the total number of bytes taken by the tokens + length of this field 1:
		- 2:Tokens 
			- serialized aray of tokens as found in the current input buffer
			- can be either compressed and uncompressed tokens
		- 3:Uncompressed Data - the data from the current input buffer that contains no repeated patterns

	NOTE: This method was changed as a part of the resolution for BZ 7534. 

	NOTE: BZ 7916 - The crash was caused by the second while reading inlpRawBuff[inulRawBuffLen]
					which is out of bounds with error codes:
					STATUS_GUARD_PAGE_VIOLATION (0x80000001) OR
					STATUS_ACCESS_VIOLATION (0xC0000005).
					The code was changed to check bounds prior to reading the byte in the buffer.
					This fault could not be reproduced in native environments so there is no UnitTest for it.
					This fault was only reproduced while passing in a 256 Kb buffer from java for encoding under
					x64 bit machines.

*/
WAX_RESULT RPR::EncodeBuffer( 
	/*__in*/	const unsigned char* inlpRawBuff,		//!< input buffer
	/*__in*/	unsigned long inulRawBuffLen,			//!< input buffer size
	/*__inout*/	unsigned char* inoutlpRPRedBuff,		//!< output buffer
	/*__inout*/	unsigned long& inoutulRPRedBuffLen		//!< output buffer size
	)
{
	//stores the result of different operations through the encoding process
	WAX_RESULT result = WAX_RESULT_OK;

	//obtain minimum length of a RPR pattern to be replaced by a token (configurable parameter)
	unsigned long ulMinLength = ConfigMan::getInstance()->GetMinRPRLength();

	//stores the offset of a first separator DA byte in a DA sequence
	unsigned char firstDiffByteDAOffset = 0;

	//stores the value of the first DA separator byte
	unsigned char firstDiffByteDAValue = 0;

	//current position in the output buffer, which stores the encoded data
	unsigned long ulCurrPosOut = sizeof(unsigned long);

	//current position in the raw data buffer
	m_ulRawBufferCounter = 0;

	//used as an in/out parameter for the token creation methods
	//	in: stores how much free space is in the output buffer
	//	out: stores how much space in the output buffer was taken by the last created token
	unsigned long ulBuffFree = inoutulRPRedBuffLen;

	//last position of the last pattern that was detected and encoded in a generic/DA token
	LONG lLastPattern = 0;

	//init cursor's position - the third in the buffer
	unsigned long ulCursor = 2;

	//length of data replaced by token
	LONG lDataLen = 0;

	//Starting position of the pattern
	unsigned long ulPivot = 0;
	
	//Starting and ending position of the pattern
	unsigned long ulStart = 0;

	//while we have data to process do
	while (ulCursor < inulRawBuffLen)
	{
		//parse the uncompressable data first (doing nothing about it)
		//	BZ 7916 - check buffer limit before the comparision - only go up to (inulRawBuffLen - 1)
		while( ( (ulCursor + 1) < inulRawBuffLen) && (inlpRawBuff[ulCursor] != inlpRawBuff[ulCursor - 2]) )
		{
			ulCursor ++;
		}

		//initialize start position to cursor
		ulPivot = ulCursor;

		//parse buffer as long as we have equal values (as long as we are "inside" a pattern)
		//	BZ 7916 - check buffer limit before the comparision - only go up to (inulRawBuffLen - 1)
		while( ( (ulCursor + 1) < inulRawBuffLen) && (inlpRawBuff[ulCursor] == inlpRawBuff[ulCursor - 2]) )
		{
			ulCursor++;
		}

		//detect pattern type (1-length or 2-length patterns)
		LONG lSeqLen = inlpRawBuff[ulPivot] == inlpRawBuff[ulPivot - 1] ? 1 : 2;

		//determine pattern length
		LONG lPaternLength = ulCursor - ulPivot + 2;

		//make sure the pattern length is even if we have a 2-length pattern
		if ( (lSeqLen == 2) && (lPaternLength % 2 != 0) )
		{
			ulCursor--;
			lPaternLength--;
		}

		//set the starting and ending position of the pattern
		ulStart = ulPivot - 2;

		//	BZ 7534 - a sequence of 'DA' bytes is part of a 'DA pattern' only if its length is less
		//	than 15. If the DA sequence is longer only the last 15 bytes will be considered further for 
		//	inclusion in a DA pattern. All the bytes 'DA' bytes before the ones considered for the 'DA pattern'
		//	are replaced by a compressed token generic.
		// check if we are on a DA pattern
		if ((lSeqLen == 1) && 
			(inlpRawBuff[ulStart] == 0xDA) && 
			IS_DA_BYTE(inlpRawBuff[ulCursor]) && 
			( lPaternLength <= 15 ) )			// we are on a DA pattern
		{//	DA pattern candidate
			//We are on a DA separator 0x1A or 0x2A or ...
			firstDiffByteDAOffset = (unsigned char)(lPaternLength);//relative position of cursor
			firstDiffByteDAValue = GET_DA_FIRST_DIFF_BYTE_POS(inlpRawBuff[ulCursor]);

			//set the offset in the pregenerated pattern to start the search from
			unsigned long ulOffsetInGeneratedPattern = getDifferentByteOffsetInDAPattern( firstDiffByteDAValue );
				
			//Byte by byte compare the pattern in the buffer with the pregenerated one
			//	BZ 7916 - check buffer limit before the comparision 
			for( ; 
				( ulCursor < inulRawBuffLen ) && (inlpRawBuff[ulCursor] == GBP_DA_PATTERN[ulOffsetInGeneratedPattern]);
				ulCursor++, 
				ulOffsetInGeneratedPattern++
				)
			{//
			}	

			//	BZ 7534 - make sure we do not exceede the input buffer length
			if( ulCursor > inulRawBuffLen )
			{
				ulCursor = inulRawBuffLen;
			}

			LONG lDAPattternLength = ulCursor - ulPivot + 2;

			//Create DA Token and update positions in buffer if criteria is met
			if( lDAPattternLength > (LONG)ulMinLength )
			{
				//create uncompressed token for data between patterns, before the current pattern
				lDataLen = ulStart - lLastPattern;
				if(lDataLen > 0)
				{
					ulBuffFree = inoutulRPRedBuffLen - ulCurrPosOut;
					result = CreateTokenUnCompress(inlpRawBuff + lLastPattern, lDataLen, inoutlpRPRedBuff + ulCurrPosOut, ulBuffFree);
					ulCurrPosOut += ulBuffFree;
				}

				// create a DA token with:
				//	- length			: lPatLen
				//	- firstDiffByte_startByte		: firstDiffByteDA

				ulBuffFree = inoutulRPRedBuffLen - ulCurrPosOut;
				result = CreateTokenDA(firstDiffByteDAOffset,firstDiffByteDAValue, lDAPattternLength, inoutlpRPRedBuff + ulCurrPosOut,ulBuffFree);
				ulCurrPosOut += ulBuffFree;

				lLastPattern = ulCursor;
				//if we found a pattern we increase cursor position by 2
				ulCursor += 2;
			}
		}//	END - DA pattern candidate
		else
		{//	Candidate for Generic pattern
		
			//if we are on a generic pattern
			//if we have enough bytes counted to have a valid repeating pattern then
			if(lPaternLength >= (LONG)ulMinLength)//- lSeqLen - RPR_TOKEN_GENERIC_SIZE > 0)
			{
				//create uncompressed token for data between patterns, before the current pattern
				lDataLen = ulStart - lLastPattern;
				if(lDataLen > 0)
				{
					ulBuffFree = inoutulRPRedBuffLen - ulCurrPosOut;
					result = CreateTokenUnCompress(inlpRawBuff + lLastPattern, lDataLen, inoutlpRPRedBuff + ulCurrPosOut, ulBuffFree);
					ulCurrPosOut += ulBuffFree;
				}

				//create compressed token for current pattern
				ulBuffFree = inoutulRPRedBuffLen - ulCurrPosOut;

				result = CreateTokenGeneric(inlpRawBuff + ulStart, lSeqLen, lPaternLength, inoutlpRPRedBuff + ulCurrPosOut, ulBuffFree);
				ulCurrPosOut += ulBuffFree;

				lLastPattern = ulCursor;
				//if we found a pattern we increase cursor position by 2
				ulCursor += 2;
			}
			else
			{
				//if we haven't found a pattern long enough we increase cursor's position by 1
				ulCursor ++;
			}
		}//END - Candidate for Generic pattern
	}//END - while we have data to process do

	//process remaining data in buffer
	lDataLen = inulRawBuffLen - lLastPattern;
	if( lDataLen > 0)
	{
		ulBuffFree = inoutulRPRedBuffLen - ulCurrPosOut;
		result = CreateTokenUnCompress(inlpRawBuff + lLastPattern, lDataLen, inoutlpRPRedBuff + ulCurrPosOut, ulBuffFree);
		ulCurrPosOut += ulBuffFree;
	}

	//append data from the raw data temporary buffer to output buffer
	memcpy(inoutlpRPRedBuff + ulCurrPosOut, m_bRawBuffer, m_ulRawBufferCounter);
	*((unsigned long *)inoutlpRPRedBuff) = ulCurrPosOut;
	ulCurrPosOut += m_ulRawBufferCounter;

	if (result == WAX_RESULT_OK)
	{
		//output data length
		inoutulRPRedBuffLen = ulCurrPosOut;
	}
	else
	{
		inoutulRPRedBuffLen = 0;
	}
	return result;
}

//! Creates an uncompressed token from uncompressable data
/*! 
	Takes the specified uncompressed data and stores it in a structure which also contains 
	token type and data length that was stored.

	NOTE: This method was changed as a part of the resolution for BZ 7534. 
*/
WAX_RESULT RPR::CreateTokenUnCompress(
	/*__in*/	const unsigned char* inlpRawBuff,		//!< input buffer
	/*__in*/	unsigned long inulTokenizedDataLen,		//!< the length of the tokenized data
	/*__inout*/	unsigned char* inoutlpRPRedBuff,		//!< output buffer
	/*__inout*/	unsigned long& inoutulRPRedBuffLen		//!< output buffer size
									  )
{
	// check if we have enough space in the output buffer
	if (RPR_TOKEN_UNCOMPRESSED_SIZE + inulTokenizedDataLen > inoutulRPRedBuffLen)
	{
		//LogError( m_ComponentID, DBG_LOCATION, L"Insufficient memory in output buffer. (Error: %d)\n",WAX_RESULT_OUTBUFF_INSUFFICIENT_SIZE);
		inoutulRPRedBuffLen = 0;
		return WAX_RESULT_OUTBUFF_INSUFFICIENT_SIZE;
	}

	WAX_RESULT result = WAX_RESULT_OK;

	//set pointer to current position in output buffer
	PRPR_Token token = (PRPR_Token)inoutlpRPRedBuff;

	//set token type and data length
	if(inulTokenizedDataLen > 65535)
	{
		token->tokenType = RPR_TOKEN_UCP_FOUR;
		token->content.tUncompressedFour.ulLength = inulTokenizedDataLen;
	}
	else if(inulTokenizedDataLen <= 255)
	{
		token->tokenType = RPR_TOKEN_UCP_ONE;
		token->content.tUncompressedOne.bLength = (unsigned char)inulTokenizedDataLen;
	}
	else
	{
		token->tokenType = RPR_TOKEN_UCP_TWO;
		token->content.tUncompressedTwo.wLength = (WORD)inulTokenizedDataLen;
	}

	//copy uncompressed data from input buffer to raw data temporary buffer
	memcpy(m_bRawBuffer + m_ulRawBufferCounter, inlpRawBuff, inulTokenizedDataLen);

	//calculate how much space was taken by this raw data in the raw data buffer
	m_ulRawBufferCounter += inulTokenizedDataLen;

	//calculate how much space was taken by this token in the output buffer
	// BZ 7534
	inoutulRPRedBuffLen = SIZE_OF_TOKEN_BY_TYPE[ token->tokenType ];  //RPR_TOKEN_UNCOMPRESSED_SIZE;// + inulTokenizedDataLen;

	return result;
}

//! Creates a generic (compressed) token from a repeated pattern
/*!
	Creates a compressed token by storing token type, original data lenght, the 1 or 2-length repeating pattern and 
	the number of time it repeates in the specific sequence

	NOTE: This method was changed as a part of the resolution for BZ 7534. 
*/
WAX_RESULT RPR::CreateTokenGeneric( 
	/*__in*/	const unsigned char* inlpPattern,		//!< the generic pattern used for comparison
	/*__in*/	unsigned long inulPatternLen,			//!< the length of the generic pattern
	/*__in*/	unsigned long inulTokenizedDataLen,		//!< the length of the tokenized data
	/*__inout*/	unsigned char* inoutlpRPRedBuff,		//!< output buffer
	/*__inout*/	unsigned long& inoutulRPRedBuffLen		//!< output buffer size
   )
{
	// check if we have enough space in the output buffer
	if (RPR_TOKEN_GENERIC_SIZE + inulPatternLen > inoutulRPRedBuffLen)
	{
		//LogError( m_ComponentID, DBG_LOCATION, L"Insufficient memory in output buffer. (Error: %d)\n",WAX_RESULT_OUTBUFF_INSUFFICIENT_SIZE);
		inoutulRPRedBuffLen = 0;
		return WAX_RESULT_OUTBUFF_INSUFFICIENT_SIZE;
	}

	WAX_RESULT result = WAX_RESULT_OK;

	//set pointer to current position in output buffer
	PRPR_Token token = (PRPR_Token)inoutlpRPRedBuff;

	//set token type and data length
	if(inulTokenizedDataLen > 65535)
	{
		token->tokenType = (inulPatternLen == 1) ? RPR_TOKEN_CMP_FOUR_SINGLE : RPR_TOKEN_CMP_FOUR_DOUBLE;
		token->content.tCompressedFour.ulLength = inulTokenizedDataLen;
		// copy the generic pattern
		memcpy(token->content.tCompressedFour.tokenData, inlpPattern, inulPatternLen);
	}
	else if(inulTokenizedDataLen <= 255)
	{
		token->tokenType = (inulPatternLen == 1) ? RPR_TOKEN_CMP_ONE_SINGLE : RPR_TOKEN_CMP_ONE_DOUBLE;
		token->content.tCompressedOne.bLength = (unsigned char)inulTokenizedDataLen;
		// copy the generic pattern
		memcpy(token->content.tCompressedOne.tokenData, inlpPattern, inulPatternLen);
	}
	else
	{
		token->tokenType = (inulPatternLen == 1) ? RPR_TOKEN_CMP_TWO_SINGLE : RPR_TOKEN_CMP_TWO_DOUBLE;
		token->content.tCompressedTwo.wLength = (WORD)inulTokenizedDataLen;
		// copy the generic pattern
		memcpy(token->content.tCompressedTwo.tokenData, inlpPattern, inulPatternLen);
	}

	//calculate how much space was taken by this token in the output buffer
	//	BZ 7534
	inoutulRPRedBuffLen = SIZE_OF_TOKEN_BY_TYPE[token->tokenType]; //RPR_TOKEN_GENERIC_SIZE + inulPatternLen;

	//update statistics
	++m_ullGenericStringsNumber;
	m_ullGenericBytesNumber += inulTokenizedDataLen;//fix stats

	return result;
}

//! Replaces a DA pattern with a DA token
/*!
	Creates a DA token by storing the token type, original data length to be replaced and
	a single byte containing the first DA separator and it's offset in the DA pattern.

	NOTE: This method was changed as a part of the resolution for BZ 7534. 
*/
WAX_RESULT RPR::CreateTokenDA( 
	/*__in*/	unsigned char inFirstDiffByte,			//!< the position where the first different BYTE is located in the pattern
	/*__in*/	unsigned char inFirstDiffValue,			//!< the first different byte value found for the DA pattern
	/*__in*/	unsigned long inulTokenizedDataLen,		//!< the length of the tokenized data
	/*__inout*/	unsigned char* inoutlpRPRedBuff,		//!< output buffer
	/*__inout*/	unsigned long& inoutulRPRedBuffLen		//!< output buffer size
	)
{
	// check if we have enough space in the output buffer
	if (RPR_TOKEN_DA_SIZE > inoutulRPRedBuffLen)
	{
		//LogError( m_ComponentID, DBG_LOCATION, L"Insufficient memory in output buffer. (Error: %d)\n",WAX_RESULT_OUTBUFF_INSUFFICIENT_SIZE);
		inoutulRPRedBuffLen = 0;
		return WAX_RESULT_OUTBUFF_INSUFFICIENT_SIZE;
	}

	WAX_RESULT result = WAX_RESULT_OK;

	//set pointer to current position in output buffer
	PRPR_Token token = (PRPR_Token)inoutlpRPRedBuff;

	//set token type and data length
	if(inulTokenizedDataLen > 65535)
	{
		token->tokenType = RPR_TOKEN_DA_FOUR;
		token->content.tCompressedFour.ulLength = inulTokenizedDataLen;
		
		SET_DA_FIRST_DIFF_BYTE_POS(inFirstDiffByte, token->content.tCompressedFour.tokenData[0]);
		SET_DA_FIRST_DIFF_BYTE_VALUE(inFirstDiffValue, token->content.tCompressedFour.tokenData[0]);

	}
	else if(inulTokenizedDataLen <= 255)
	{
		token->tokenType = RPR_TOKEN_DA_ONE;
		token->content.tCompressedOne.bLength = (unsigned char)inulTokenizedDataLen;
		
		SET_DA_FIRST_DIFF_BYTE_POS(inFirstDiffByte, token->content.tCompressedOne.tokenData[0]);
		SET_DA_FIRST_DIFF_BYTE_VALUE(inFirstDiffValue, token->content.tCompressedOne.tokenData[0]);
	}
	else
	{
		token->tokenType = RPR_TOKEN_DA_TWO;
		token->content.tCompressedTwo.wLength = (WORD)inulTokenizedDataLen;
		
		SET_DA_FIRST_DIFF_BYTE_POS(inFirstDiffByte, token->content.tCompressedTwo.tokenData[0]);
		SET_DA_FIRST_DIFF_BYTE_VALUE(inFirstDiffValue, token->content.tCompressedTwo.tokenData[0]);
	}

	//calculate how much space was taken by this token in the output buffer
	// BZ 7534
	inoutulRPRedBuffLen = SIZE_OF_TOKEN_BY_TYPE[ token->tokenType ];  //RPR_TOKEN_DA_SIZE;

	//update statistics
	++m_ullDAStringsNumber;
	m_ullDABytesNumber += inulTokenizedDataLen;//fix stats

	return result;
}


//! Decodes a buffer containing RPR tokens
/*!
	The output buffer will contained the original data extracted from the tokens found in the input buffer.

	NOTE: This method was changed as a part of the resolution for BZ 7534. 
*/
WAX_RESULT RPR::DecodeBuffer(
	/*__in*/	const unsigned char* inlpRPRedBuff,		//!< input buffer
	/*__in*/	unsigned long inulRPRedBuffLen,			//!< input buffer size
	/*__inout*/	unsigned char* inoutlpRawBuff,			//!< output buffer
	/*__inout*/	unsigned long &inoutulRawBuffLen		//!< output buffer size
	)
{
	WAX_RESULT result = WAX_RESULT_OK;
	
	//current position in input buffer
	unsigned long currPosIn = sizeof(unsigned long);

	//current position in output bufffer
	unsigned long currPosOut = 0;

	//set the pointer to the first byte of uncompressed raw data
	m_ulRawBufferCounter = *((unsigned long *)inlpRPRedBuff);

	unsigned long ulStopLength = m_ulRawBufferCounter;

	// search for tokens in the input buffer
	while ((currPosIn < ulStopLength) && (result == WAX_RESULT_OK))
	{
		if ((currPosIn + MIN_SIZE_TOKEN_HEADER) > inulRPRedBuffLen)
		{
			result = WAX_RESULT_INBUFF_INCONSISTENT;
			//LogError( m_ComponentID, DBG_LOCATION, L"Inconsistent buffer formating. (Error: %d)\n",result);
			break;
		}

		// cast the current position in the buffer to a RPRToken
		PRPR_Token currToken = (PRPR_Token)(inlpRPRedBuff + currPosIn);

		//	Extract the token data length
		unsigned long ulTokenLength = extractTokenDataLength( currToken );
		if ( ulTokenLength == 0 )
		{
			result = WAX_RESULT_UNKNOWN_TOKEN_TYPE;
			break;
		}

		// Token type as Flags and needed data to decode
		bool bUncompressed = false;

		bool bDA = false;
		unsigned char daByte = 0;

		bool bPattern = false;
		unsigned long ulRepeatedPatternLength = 0;
		unsigned char  lpbPattern[2];
		
		//	Extract the info from the token : compress type and data.
		if( !extractTokenDataForDecode( currToken, bUncompressed, bDA, daByte, bPattern, ulRepeatedPatternLength, lpbPattern ) )
		{
			result = WAX_RESULT_UNKNOWN_TOKEN_TYPE;
			break;
		}

		//
		//	Decode according to token type + data

		//Uncompressed token
		if(bUncompressed)
		{
			// check if we have that amount of memory in the input buffer
			if ((m_ulRawBufferCounter + ulTokenLength) > inulRPRedBuffLen)
			{
				result = WAX_RESULT_INBUFF_INCONSISTENT;
				//LogError( m_ComponentID, DBG_LOCATION, L"Inconsistent buffer formating. (Error: %d)\n",result);
				break;
			}

			// copy the actual data
			memcpy( inoutlpRawBuff + currPosOut,
					inlpRPRedBuff + m_ulRawBufferCounter,
					ulTokenLength);

			currPosOut += ulTokenLength;
			m_ulRawBufferCounter += ulTokenLength;
		}

		//	Generic repetitive pattern 
		if( bPattern )
		{
			if ((ulTokenLength % ulRepeatedPatternLength) != 0)
			{
				result = WAX_RESULT_INBUFF_INCONSISTENT;
				//LogError( m_ComponentID, DBG_LOCATION, L"Inconsistent buffer formating. (Error: %d)\n",result);
				break;
			}

			//copy the pattern over and over until we reach the original data length
			unsigned long needData = ulTokenLength;
			while (needData > 0)
			{
				memcpy(inoutlpRawBuff + currPosOut, lpbPattern , ulRepeatedPatternLength);
				needData -= ulRepeatedPatternLength;
				currPosOut += ulRepeatedPatternLength;
			}
		}

		// DA pattern 
		if( bDA )
		{
			//obtain the value of the first DA separator
			unsigned char currByte = GET_DA_FIRST_DIFF_BYTE_VALUE( daByte );

			//obtain the offset of the first DA separator
			unsigned char firstDiffBytePos = GET_DA_FIRST_DIFF_BYTE_POS( daByte );

			//generate the corrsponding DA pattern
			generateDAPattern( inoutlpRawBuff + currPosOut, ulTokenLength, currByte, firstDiffBytePos );

			//increment buffers current position
			currPosOut += ulTokenLength;
		}

		//Increase position in input buffer - perfromed only once.
		currPosIn += SIZE_OF_TOKEN_BY_TYPE[currToken->tokenType]; 
	}

	if (result == WAX_RESULT_OK)
	{
		inoutulRawBuffLen = currPosOut;
	}
	else
	{
		inoutulRawBuffLen = 0;
	}

	return result;
}
//-------------------------------------------------------------------------------------------------

//! Process statistics
WAX_RESULT RPR::GetStatistics(
	/*__inout*/ Statistics *inoutStats	//!< output statistic structure
	)
{
	inoutStats->rprStringsNumber = m_ullGenericStringsNumber + m_ullDAStringsNumber;
	inoutStats->rprBytesNumber = m_ullGenericBytesNumber + m_ullDABytesNumber;
	inoutStats->daStringsNumber = m_ullDAStringsNumber;
	inoutStats->daBytesNumber = m_ullDABytesNumber;
	inoutStats->genericStringsNumber = m_ullGenericStringsNumber;
	inoutStats->genericBytesNumber = m_ullGenericBytesNumber;

	return WAX_RESULT_OK;
}
//-------------------------------------------------------------------------------------------------

//! Initializes stats values.
void RPR::StartStatistics( void )
{
	m_ullGenericStringsNumber = 0; 
	m_ullDAStringsNumber      = 0;

	m_ullGenericBytesNumber   = 0;
	m_ullDABytesNumber        = 0;
}


//! Computes the first offset of a different byte in the pre-generated DA pattern.
unsigned long getDifferentByteOffsetInDAPattern(
	/*__in*/ unsigned char inbDifferentValue 
	)
{
	unsigned long ulRetVal = 0;
	unsigned long ulByteVal = inbDifferentValue;
	//
	ulRetVal = ulByteVal * 16;

	return ulRetVal;
}
//-------------------------------------------------------------------------------------------------


//! Extract the length of the data replaced by the current token. 
/*!
	The method checks the type of the current token and returns the 
	length of the data replaced by it.

	In the case the current token type is unknown the method returns ZERO.
	The caller must act on the ZERO return value.

	NOTE: This method was implemented as a part of the resolution for BZ 7534. 
*/
unsigned long extractTokenDataLength( 
	/*__in*/ RPR_Token* inToken 
	)
{
	unsigned long ulTokenLength = 0;

	switch (inToken->tokenType)
	{
		case RPR_TOKEN_UCP_ONE: 
			{
				ulTokenLength = (unsigned long)inToken->content.tUncompressedOne.bLength ; 
				break;
			}
		case RPR_TOKEN_CMP_ONE_SINGLE:
		case RPR_TOKEN_CMP_ONE_DOUBLE:
		case RPR_TOKEN_DA_ONE:
			{
				ulTokenLength = (unsigned long)(unsigned long)inToken->content.tCompressedOne.bLength; 
				break;
			}
		case RPR_TOKEN_UCP_TWO: 
			{
				ulTokenLength = (unsigned long)inToken->content.tUncompressedTwo.wLength; 
				break;
			}
		case RPR_TOKEN_CMP_TWO_SINGLE:
		case RPR_TOKEN_CMP_TWO_DOUBLE:
		case RPR_TOKEN_DA_TWO:
			{
				ulTokenLength = (unsigned long)inToken->content.tCompressedTwo.wLength; 
				break;
			}
		case RPR_TOKEN_UCP_FOUR: 
			{
				ulTokenLength = (unsigned long)inToken->content.tUncompressedFour.ulLength;
				break;
			}
		case RPR_TOKEN_CMP_FOUR_SINGLE:
		case RPR_TOKEN_CMP_FOUR_DOUBLE:
		case RPR_TOKEN_DA_FOUR:
			{
				ulTokenLength = (unsigned long)inToken->content.tCompressedFour.ulLength;
				break;
			}
	}

	//	In case the ulTokenLength is not updated by the switch above, this method will return 0.
	return ulTokenLength;
}
//-------------------------------------------------------------------------------------------------


//! Extract the data needed for decoding the current token. 
/*!
	The method checks the type of the current token and returns the type as one of the categories:
		- uncompressed
		- DA
		- pattern (generic repetitive pattern)
	
	The decode process for DA and generic repetitive patterns also need some more info:
		- DA: the DA byte stored in the token
		- pattern: the pattern length and data

	NOTE: This method was implemented as a part of the resolution for BZ 7534. 
*/
bool extractTokenDataForDecode(
	/*__in*/ RPR_Token* inRprToken, 
	/*__out*/ bool& bUncompressed, 
	/*__out*/ bool& bDA,
	/*__out*/ unsigned char& daByte, 
	/*__out*/ bool& bPattern, 
	/*__out*/ unsigned long& ulRepeatedPatternLength, 
	/*__out*/ unsigned char* lpbPattern
	)
{
	//re-set all the flags and return values.
	bUncompressed = false; 
	bDA = false;
	daByte = 0; 
	bPattern  = false; 
	ulRepeatedPatternLength = 0;
	lpbPattern[0] = 0;
	lpbPattern[1] = 0;

	switch (inRprToken->tokenType)
	{
	case RPR_TOKEN_UCP_ONE:
	case RPR_TOKEN_UCP_TWO:
	case RPR_TOKEN_UCP_FOUR:
		{
			bUncompressed = true;
			break;
		}
	case RPR_TOKEN_CMP_ONE_DOUBLE:
		{
			bPattern = true;
			ulRepeatedPatternLength = LENGTH_TWO;
			memcpy( lpbPattern, inRprToken->content.tCompressedOne.tokenData, ulRepeatedPatternLength );
			break;
		}
	case RPR_TOKEN_CMP_TWO_DOUBLE:
		{
			bPattern = true;
			ulRepeatedPatternLength = LENGTH_TWO;
			memcpy( lpbPattern, inRprToken->content.tCompressedTwo.tokenData, ulRepeatedPatternLength );
			break;
		}
	case RPR_TOKEN_CMP_FOUR_DOUBLE:
		{
			bPattern = true;
			ulRepeatedPatternLength = LENGTH_TWO;
			memcpy( lpbPattern, inRprToken->content.tCompressedFour.tokenData, ulRepeatedPatternLength );
			break;
		}
	case RPR_TOKEN_CMP_ONE_SINGLE:
		{
			bPattern = true;
			ulRepeatedPatternLength = LENGTH_ONE;
			lpbPattern[0] = inRprToken->content.tCompressedOne.tokenData[0];
			break;
		}
	case RPR_TOKEN_CMP_TWO_SINGLE:
		{
			bPattern = true;
			ulRepeatedPatternLength = LENGTH_ONE;
			lpbPattern[0] = inRprToken->content.tCompressedTwo.tokenData[0];
			break;
		}
	case RPR_TOKEN_CMP_FOUR_SINGLE:
		{
			bPattern = true;
			ulRepeatedPatternLength = LENGTH_ONE;
			lpbPattern[0] = inRprToken->content.tCompressedFour.tokenData[0];
			break;
		}
	case RPR_TOKEN_DA_ONE:
		{
			bDA = true;
			daByte = inRprToken->content.tCompressedOne.tokenData[0];
			break;
		}
	case RPR_TOKEN_DA_TWO:
		{
			bDA = true;
			daByte = inRprToken->content.tCompressedTwo.tokenData[0];
			break;
		}
	case RPR_TOKEN_DA_FOUR:
		{
			bDA = true;
			daByte = inRprToken->content.tCompressedFour.tokenData[0];
			break;
		}
	default:
		{
			//we found an unknown token
			return false;
			break;
		}
	}//END get the operation type and data.
	
	return true;
}
//-------------------------------------------------------------------------------------------------

//! Generates a DA pattern
void generateDAPattern( 
	/*__inout*/ unsigned char* inoutBuffer,
	/*__in*/ unsigned long inulPatterLength,
	/*__in*/ unsigned char  inbFirstDifferentByteValue,
	/*__in*/ unsigned long inulFirstDifferentByteOffset
	)
{
	//generate the predefined DA Pattern
	memset(inoutBuffer, 0xDA, inulPatterLength);
	unsigned char currByte = inbFirstDifferentByteValue;

	for (unsigned long j = inulFirstDifferentByteOffset; j < inulPatterLength; j += 16)
	{
		inoutBuffer[j] = MAKE_DA_BYTE(currByte++);
	}
}
//-------------------------------------------------------------------------------------------------
