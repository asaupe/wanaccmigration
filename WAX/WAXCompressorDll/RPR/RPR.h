//********************************************************/
//  ./RPR/RPR.h
//
//  Owner : Ciprian Maris
//  
//  Neverfail             ________________
//           PROJECT   :  WAN Acceleration
//           Component :  RPR
//
//  $Rev: 2480 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: cmaris $  
//  $Date: 2009-05-22 14:08:44 +0100 (Fri, 22 May 2009) $
//
//********************************************************/

#pragma once

#include "WAXTypes.h"
#include "../waxsingletontemplate.h"
#include "../Stats/IStats.h"
#include "../Common.h"

typedef struct _RPR_Pattern
{
	unsigned long ulStart;
	unsigned long ulEnd;
}RPR_Pattern;

class RPR:public WaxSingletonTemplate<RPR>, public IStats
{
	friend class WaxSingletonTemplate<RPR> ;

public:
	~RPR();
	//! Decode a RPR data buffer
	/*!
		Decode a buffer created by the EncodeBuffer into the original form.

		The output buffer has to be pre-allocated by the caller and the size of the pre-allocated
	buffer is sent also. The output buffer size will be changed with the actual size required
	by the function.
	*/
	WAX_RESULT DecodeBuffer(
		/*__in*/	const unsigned char* inlpRPRedBuff,		//!< input buffer
		/*__in*/	unsigned long inulRPRedBuffLen,			//!< input buffer size
		/*__inout*/	unsigned char* inoutlpRawBuff,			//!< output buffer
		/*__inout*/	unsigned long &inoutulRawBuffLen		//!< output buffer size
		);
	
	//! Encode a buffer using the RPR data
	/*!
		The structure of a RPRRegion is the following:
			NumberOfRPRTokens_OriginalRegionLength_RPRToken1_RPRToken2_...RPRTokenN_UncompressedData

		The output buffer has to be pre-allocated by the caller and the size of the pre-allocated
	buffer is sent also. The output buffer size will be changed with the actual size required
	by the function.
	*/
	WAX_RESULT EncodeBuffer(
		/*__in*/	const unsigned char* inlpRawBuff,		//!< input buffer
		/*__in*/	unsigned long inulRawBuffLen,			//!< input buffer size
		/*__inout*/	unsigned char* inoutlpRPRedBuff,		//!< output buffer
		/*__inout*/	unsigned long& inoutulRPRedBuffLen		//!< output buffer size
		);

	WAX_RESULT GetStatistics(
		/*__inout*/	Statistics *inoutStats				//!< output statistic structure
		);

	//! Initializes stats values.
	void StartStatistics( void );

private:

	RPR();

	WAX_RESULT CreateTokenUnCompress(
		/*__in*/	const unsigned char* inlpRawBuff,		//!< input buffer
		/*__in*/	unsigned long inulTokenizedDataLen,		//!< the length of the tokenized data
		/*__inout*/	unsigned char* inoutlpRPRedBuff,		//!< output buffer
		/*__inout*/	unsigned long& inoutulRPRedBuffLen		//!< output buffer size
		);

	WAX_RESULT CreateTokenGeneric(
		/*__in*/	const unsigned char* inlpPattern,		//!< the generic pattern used for comparison
		/*__in*/	unsigned long inulPatternLen,			//!< the length of the generic pattern
		/*__in*/	unsigned long inulTokenizedDataLen,		//!< the length of the tokenized data
		/*__inout*/	unsigned char* inoutlpRPRedBuff,		//!< output buffer
		/*__inout*/	unsigned long& inoutulRPRedBuffLen		//!< output buffer size
		);

	WAX_RESULT CreateTokenDA(
		/*__in*/	unsigned char inFirstDiffByte,			//!< the position where the first different BYTE is located in the pattern
		/*__in*/	unsigned char inFirstDiffValue,			//!< the first different byte value found for the DA pattern
		/*__in*/	unsigned long inulTokenizedDataLen,		//!< the length of the tokenized data
		/*__inout*/	unsigned char* inoutlpRPRedBuff,		//!< output buffer
		/*__inout*/	unsigned long& inoutulRPRedBuffLen		//!< output buffer size
		);

private:
	static WAX_ID m_ComponentID;

	uint64_t m_ullGenericStringsNumber;					//!< number of generic-bytes strings
	uint64_t m_ullGenericBytesNumber;						//!< bytes in generic-bytes strings
	uint64_t m_ullDAStringsNumber;							//!< number of DA-bytes strings
	uint64_t m_ullDABytesNumber;							//!< bytes in DA-bytes strings

	unsigned char * m_bRawBuffer;
	unsigned long m_ulRawBufferCounter;

};
