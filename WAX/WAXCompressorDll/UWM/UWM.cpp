//********************************************************/
//  ./UWM/UWM.cpp
//
//  Owner : Bogdan Hruban
// 
//  Neverfail                 ________________
//                PROJECT   : WAN Acceleration 
//           Component : Unit of Work Manager 
//
//  $Rev: 2480 $ - Revision of last commit
//
/********************************************************
//
// Last Change - Author and Date of last change:
// $Author: cmaris $  
// $Date: 2009-05-22 14:08:44 +0100 (Fri, 22 May 2009) $
//
//********************************************************/

#include "../Common.h"
#include "UWM.h"
#include "../Encoder/crc64.h"
#include "../Logger/Logger.h"

#include "../SharedTypes.h"

#define UWM_MAX_DEDUP_RPR_BUFF_SIZE 256*KILO					// TODO: compute the MAX value
#define UWM_MAX_DEDUP_BUFF_SIZE UWM_MAX_DEDUP_RPR_BUFF_SIZE		// TODO: compute the MAX value

#define UWM_TEMP_BUFFER_SIZE 512*KILO


WAX_ID UWM::m_ComponentID = (WAX_ID)WAX_ID_CONFIGMAN;

//! Empty ChunkCharacteristic vector used for performing writes to the StoreManager after a decode
vector <ChunkCharacteristic> chunkCharactersisticsVector;



//! Type used for passing RPR data to thread routine
/*
*	In case we are processing RPR regions on threads, the information about
*	a region: the address where the region can be found and the length of 
*	the buffer. The result is placed in a specialized vector (a vector of 
*	RPR tokens)
*
*	\sa RprRegionVector
*/
typedef struct _RprDataParameter
{
	ULONG_PTR data;
	unsigned long dataLength;
	//RprRegionVector regVector;
} RprDataParam, *PRprDataParam;

//! Type used for identifying a dirty region
/*!
*	Type used for identifying a dirty region (offset and length) plus adding
*	the support of ordering the dirty regions by offset.
*/
typedef _Basic_Region DirtyDataRegion, *PDirtyDataRegion;


//
// Class methods
//

UWM::UWM():m_ullDataLovL0(0),
m_ullDataLovL1(0),
m_ullDataLovL2(0),
m_ulMsgNumber(0),
m_lpTempBuffer(NULL)
{ 
	initialize();
};


UWM::~UWM()
{
	if (m_lpTempBuffer != NULL)
	{
		free(m_lpTempBuffer);
	}
}


//! WAXControlableBase - initialize override
/*!
*	The function is meant to point to the StoreManager that will be used for getting the
*	data from. In case no parameter is given to the function, or a NULL one is given, it tries
*	to get an instance from StoreManager::getInstance().
*
*	Return: In case of success return WAX_RESULT_OK. In the case of failure it returns the 
*	failure reason.
*
*	Caller: WAXInterface::initialize().
*
*	\sa WAXControlableBase
*	\sa StoreManager::getInstance
*/
WAX_RESULT UWM::initialize(
		/* IN */ StoreManager* inStoreManager //!< pointer to instance of StoreManagerBase to use
		)
{
	WAX_RESULT result = WAX_RESULT_OK;

	// try to get the temporary buffer
	if (m_lpTempBuffer == NULL)
	{
		m_lpTempBuffer = (unsigned char*)malloc(UWM_TEMP_BUFFER_SIZE);
	}
	if (m_lpTempBuffer == NULL)
	{
		result = WAX_RESULT_NOT_INITIALIZED;
		LogError(m_ComponentID, DBG_LOCATION, L"Failed to get the temporary buffer. (Error %d)\n",result);
		return result;
	}
	
	memset(m_lpTempBuffer, 0 , UWM_TEMP_BUFFER_SIZE);

	// initialize the local variables
	m_Encoder = WAXEncoder::getInstance();
	m_RPR = RPR::getInstance();
	m_Tokenizer = Tokenizer::getInstance();
	
	if (inStoreManager == NULL)
	{
		m_StoreManager = StoreManager::getInstance();
	}
	else
	{
		m_StoreManager = inStoreManager;
	}

	if (m_StoreManager == NULL)
	{
		result = WAX_RESULT_NOT_INITIALIZED;
		LogError(m_ComponentID, DBG_LOCATION, L"Failed to get instance of StoreManager. (Error %d)\n",result);
	}

	if (result == WAX_RESULT_OK)
	{
		result = WAXControlableBase::initialize();
	}

	return result;
}

//! Encode an input buffer using deduplication algorithm and/or repetitive data removal.
/*!
*	Encode an input buffer using deduplication algorithm and/or repetitive data removal 
*	algorithm. The functions receives an input buffer and its size, an array of Annotations (or
*	NULL if we are running in OPAQUE mode) flags specifying the processing that will be done on the
*	current buffer (DeDup and/or RPR) and the output buffer and its size.
*
*	The output buffer has to be pre-allocated by the caller and the size of the pre-allocated
*	buffer is sent also. The output buffer size will be changed with the actual size required by 
*	the function.
*
*	Return: In case of success return WAX_RESULT_OK. In the case of failure it returns the 
*	failure reason.
*
*	Caller: WAXInterface::encode().
*
*	\sa WAXInterface::encode
*	\sa PAnnotations
*/
WAX_RESULT UWM::EncodeBuffer(
		/* IN */		/*const*/ unsigned char*	inlpRawData,				//!< input buffer
		/* IN */		unsigned long			inulRawDataLength,			//!< length of the input buffer
		/* IN/OUT */	unsigned char*			inoutlpEncodedData,			//!< output buffer
		/* IN/OUT */	unsigned long&			inoutulEncodedDataLength,	//!< length of the output buffer
		/* IN */		bool			inIsRprEnabled,				//!< is RPR processing enabled
		/* IN */		bool			inIsDeDupEnabled			//!< is WAX processing enabled
							 )
{
	WAX_RESULT result = WAX_RESULT_OK;

	if (NULL == inlpRawData)
	{
		return WAX_RESULT_INBUFF_NULL;
	}

	if (NULL == inoutlpEncodedData)
	{
		return WAX_RESULT_OUTBUFF_NULL;
	}

	// vector for tokens found by Encoder
	CompressedTokenVector arrayOfTokens;

	// number of bytes used to store RPR non-raw data
	unsigned long ulRPRRawDataOffset = 0;

	//
	// Build the UWM header
	//

	// set the UWM header as part of the output buffer	at the beginning of the buffer
	UWM_Header *header = (UWM_Header*)inoutlpEncodedData;
	header->BuffSize = inulRawDataLength;
	header->CRC = ComputeCRC64(inlpRawData,inulRawDataLength);

	// set the current output buffer size to the size of UWM_Header
	unsigned long outMsgSize = sizeof(UWM_Header);
	
	// Stats: increase the message number
	++m_ulMsgNumber;
	// Stats: increase the size of the uncompressed data
	m_ullDataLovL0 += inulRawDataLength;

	//
	// Send buffers to RPR
	//

	// Log the processing modes
	LogDebug( m_ComponentID, DBG_LOCATION, L"RPR Processing: %d\nDeDup Processing: %d\n",inIsRprEnabled, inIsDeDupEnabled);

	// current encoder buff size
	unsigned long rpredBuffSize = UWM_TEMP_BUFFER_SIZE/*MAX_RPR_MSG_SIZE*/;
	// the buffer that is sent to WAXEncoder for processing
	unsigned char* rpredBuff = NULL;

	//
	// Handle the case with RPR
	//
	if (inIsRprEnabled)
	{
		rpredBuff = m_lpTempBuffer;
		
		//send the whole input buffer to RPR
		result = m_RPR->EncodeBuffer(
			inlpRawData,
			inulRawDataLength,
			rpredBuff,
			rpredBuffSize);
		ulRPRRawDataOffset += *((unsigned long *) rpredBuff);

		// Stats: increase the size of the RPRed data
		m_ullDataLovL1 += rpredBuffSize;
		
		LogDebug( m_ComponentID, DBG_LOCATION, L"RPR finished with %d.\n", result);

		if (result != WAX_RESULT_OK)
		{
			LogError(m_ComponentID, DBG_LOCATION, L"RPR failed with error: %d",result);
		}
	}	

	//
	// Handle the case with DeDuplication enabled
	//
	if ((inIsDeDupEnabled) && (result == WAX_RESULT_OK))
	{
		//
		// Send the buffer to WAX Encoder
		//
		if (inIsRprEnabled)
		{
			// process the buffer created after RPR processing
			//WAXEncode(rpredBuff, rpredBuffSize, arrayOfTokens);
			
			//	BZ 7534 - only attempt DD encode if there still is unencoded data left
			//	This covers the case when RPR encodes the entire input buffer and there is no RAW data left for DD 
			if( (rpredBuffSize - ulRPRRawDataOffset) > 0 )
			{
				result = m_Encoder->EncodeBuffer( rpredBuff + ulRPRRawDataOffset, rpredBuffSize - ulRPRRawDataOffset, arrayOfTokens );
			}
			else
			{
				//	There is no data left for DD to encode so set the inIsDeDupEnabled to false so that the
				//	output message is properly composed.
				inIsDeDupEnabled = false;
			}
		}
		else
		{
			// process directly on the input buffer
			//WAXEncode(inlpRawData, inulRawDataLength, arrayOfTokens)
			result = m_Encoder->EncodeBuffer( inlpRawData, inulRawDataLength, arrayOfTokens );
		}

		if (result != WAX_RESULT_OK)
		{
			LogError(m_ComponentID, DBG_LOCATION, L"Deduplication failed with error: %d",result);
		}
	}

	//
	// Build the output message
	//
	// the output buffer (inoutlpEncodedData) is pre-allocated, and the MAX size is specified in 'inoutulEncodedDataLength"
	if (result == WAX_RESULT_OK)
	{
		// the UWM header is already placed at the beginning of the buffer

		// in case of WAX processing we have an array of tokens that need to be sent over
		// if no deduplication is done on the buffer, do not add any data
		if (inIsDeDupEnabled)
		{
			// tokenize the buffer
			unsigned long ulTokenizedBufferLength = inoutulEncodedDataLength - outMsgSize;
			
			if (inIsRprEnabled)
			{
				//copy the rpr token data into the output buffer
				memcpy(inoutlpEncodedData + outMsgSize, rpredBuff, ulRPRRawDataOffset);
				outMsgSize += ulRPRRawDataOffset;

				result = m_Tokenizer->TokenizeBuffer(
					rpredBuff + ulRPRRawDataOffset, 
					rpredBuffSize - ulRPRRawDataOffset, 
					arrayOfTokens, 
					inoutlpEncodedData + outMsgSize, 
					ulTokenizedBufferLength);
			}
			else
			{
				result = m_Tokenizer->TokenizeBuffer(
					inlpRawData, 
					inulRawDataLength, 
					arrayOfTokens, 
					inoutlpEncodedData + outMsgSize, 
					ulTokenizedBufferLength);
			}
			
			outMsgSize += ulTokenizedBufferLength;

			// Stats: set the size of the DeDup buffer
			m_ullDataLovL2 += ulTokenizedBufferLength;
		}
		else if (inIsRprEnabled)
		{
			//
			// we only had RPR processing
			//

			// put the RPR-ed buffer here
			memcpy(inoutlpEncodedData + outMsgSize, rpredBuff, rpredBuffSize);
			outMsgSize += rpredBuffSize;
		}
	}

	if (result != WAX_RESULT_OK)
	{
		inoutulEncodedDataLength = 0;
		LogError(m_ComponentID, DBG_LOCATION, L"Deduplication failed with error: %d",result);
	}
	else
	{
		inoutulEncodedDataLength = outMsgSize;
	}

	LogDebug(m_ComponentID, DBG_LOCATION, L"EncodeBuffer result: %d\n",result);

	return result;
}


//! Decode the input buffer, returning the original buffer that was sent to the encoder.
/*!
*	Decode the input buffer, returning the original buffer that was sent to the encoder. The
*	functions receives as parameters the encoded buffer and its size and the buffer where to place
*	the decoded buffer and the size of the pre-allocated decoded buffer.
*
*	The output buffer has to be pre-allocated by the caller and the size of the pre-allocated
*	buffer is sent also. The output buffer size will be changed with the actual size required by
*	the function.
*
*	Return: In case of success return WAX_RESULT_OK. In the case of failure it returns the 
*	failure reason.
*
*	Caller: WAXInterface::decode().
*
*	\sa WAXInterface::decode
*/
WAX_RESULT UWM::DecodeBuffer(
	/*__in*/ const unsigned char*	inlpEncodedData,			//!< input buffer
	/*__in*/ unsigned long			inulEncodedDataLength,		//!< input buffer length
	/*__in*/ unsigned char*			inoutlpRawData,				//!< output buffer
	/*__in*/ unsigned long&			inoutulRawDataLength,		//!< output buffer size
	/*__in*/ bool			isRPRProcessing,
	/*__in*/ bool			isWaxProcessing 
	)
{
	WAX_RESULT result = WAX_RESULT_OK;

	if (NULL == inlpEncodedData)
	{
		return WAX_RESULT_INBUFF_NULL;
	}

	if (NULL == inoutlpRawData)
	{
		return WAX_RESULT_OUTBUFF_NULL;
	}

	//
	// decode the in message
	//

	// current position in input buffer
	unsigned long currPosInInputBuffer = 0;

	//number of bytes used for tokens in rpr-ed data
	unsigned long ulRPRRawDataOffset = 0;

	// get the UWM header
	UWM_Header* header = ((UWM_Header*)inlpEncodedData);
	currPosInInputBuffer += sizeof(UWM_Header);

	// check if we have enough memory allocated for the final buffer
	if (header->BuffSize > inoutulRawDataLength)
	{
		inoutulRawDataLength = 0;
		LogError( m_ComponentID, DBG_LOCATION, L"Insufficient output buffer size %lu. (Error %d)",inoutulRawDataLength,WAX_RESULT_OUTBUFF_INSUFFICIENT_SIZE);
		return WAX_RESULT_OUTBUFF_INSUFFICIENT_SIZE;	// exit with an error
	}

	// Stats: increase the number of messages
	++m_ulMsgNumber;


	// Log the processing modes
	LogDebug( m_ComponentID, DBG_LOCATION, L"RPR Processing: %d\nDeDup Processing: %d\n",isRPRProcessing, isWaxProcessing);

	//	Initialize the length to ZERO - this will be checked later on prior to calling
	//	StoreManager::writeBuffer()
	m_bBufferForStoreManagerWrites.length = 0;

	// dirty data length
	int dirtyDataLen = 0;

	// dirty data
	unsigned char* dirtyData = NULL;

	//
	// read the tokens
	//

	// read the token one by one till the end of the inBuffer
	// the detokenized data is placed directly in the final buffer
	
	if(isRPRProcessing)
	{
		ulRPRRawDataOffset += *((unsigned long*)(inlpEncodedData + currPosInInputBuffer));
		currPosInInputBuffer += ulRPRRawDataOffset;
	}

	// size of the tokenized data
	unsigned long tokenDataSize = inulEncodedDataLength - currPosInInputBuffer;

	if ( result == WAX_RESULT_OK)
	{//	Previous operations suceeded. Decode can continue

		//	BZ 7534 - check that there is DD decode work to perfrom.
		//  If not it means that the current message was completely RPR encoded.
		if ( isWaxProcessing && ( tokenDataSize > 0 ) )
		{//we have deduplication processing and we have DD tokenized data - decode it

			if (isRPRProcessing)
			{// is RPR enabled

				// DeTokenizeBuffer - into a tmp buff and send it to RPR
				unsigned long deTokenBuffSize = UWM_TEMP_BUFFER_SIZE/*inoutulRawDataLength*/;
				unsigned char* deTokenBuff = m_lpTempBuffer;

				memcpy(deTokenBuff, inlpEncodedData + sizeof(UWM_Header), ulRPRRawDataOffset);

				result = m_Tokenizer->DeTokenizeBuffer(
							inlpEncodedData + currPosInInputBuffer,
							tokenDataSize,
							deTokenBuff + ulRPRRawDataOffset, //we leave first part empty for the rpr token data
							deTokenBuffSize);

				if( result == WAX_RESULT_OK )
				{
					//set the length and data to be written to the Store Manager
					//This data is saved to be written to the storeManager if RPR decode succedes.
					m_bBufferForStoreManagerWrites.buffer = deTokenBuff  + ulRPRRawDataOffset;
					m_bBufferForStoreManagerWrites.length = deTokenBuffSize;
				}

				
				// DeRPR tmp buffer in the final buffer
				if (result == WAX_RESULT_OK)
				{
					result = m_RPR->DecodeBuffer(deTokenBuff, deTokenBuffSize + ulRPRRawDataOffset, inoutlpRawData, inoutulRawDataLength);
				}

				// Stats: set the size of the data RPRed/DeDuped
				m_ullDataLovL1 += deTokenBuffSize + ulRPRRawDataOffset;
				m_ullDataLovL2 += tokenDataSize + ulRPRRawDataOffset;
			}
			else
			{// RPR is disabled
				result = m_Tokenizer->DeTokenizeBuffer(
					inlpEncodedData + currPosInInputBuffer,
					tokenDataSize, 
					inoutlpRawData, 
					inoutulRawDataLength);

				if( result == WAX_RESULT_OK )
				{
					//set the length and data to be written to the Store Manager
					//This data is saved to be written to the storeManager if RPR decode succedes.
					m_bBufferForStoreManagerWrites.buffer = inoutlpRawData;
					m_bBufferForStoreManagerWrites.length = inoutulRawDataLength;
				}

				// Stats: set the size of the data with DeDup
				m_ullDataLovL2 += tokenDataSize;
			}
		}//END - we have deduplication processing
		else if (isRPRProcessing)
		{// Decode RPR encoded Buffer
			//result = m_RPR->DecodeBuffer(inlpEncodedData + sizeof(UWM_Header), tokenDataSize - sizeof(UWM_Header), inoutlpRawData, inoutulRawDataLength);
			result = m_RPR->DecodeBuffer(
						inlpEncodedData + sizeof(UWM_Header), 
						inulEncodedDataLength - sizeof(UWM_Header), 
						inoutlpRawData, 
						inoutulRawDataLength);            
			// Stats: set the size of the data with RPR
			m_ullDataLovL1 += tokenDataSize;
		}
		else
		{
			result = WAX_RESULT_PROCESSING_FLAG_REQUIRED;
		}
	}//END we have tokenized data - decode it

	m_ullDataLovL0 += inoutulRawDataLength;

	// the buffer processing is complete
	// start checking for validity

	if ((result == WAX_RESULT_OK) && (inoutulRawDataLength != header->BuffSize))
	{
		result = WAX_RESULT_CHECK_OUTBUFF_SIZE_INCORECT;
	}
	else if ((result == WAX_RESULT_OK) && (ComputeCRC64(inoutlpRawData,inoutulRawDataLength) != header->CRC))
	{
		result = WAX_RESULT_CHECK_CRC_INCORECT;
	}

	if( ( result == WAX_RESULT_OK ) && ( m_bBufferForStoreManagerWrites.length > 0 ) )
	{
		//Write the buffer to the StoreManager only if decoding succeded.
		result = StoreManager::getInstance()->writeBuffer( &m_bBufferForStoreManagerWrites, chunkCharactersisticsVector);
	}

	LogDebug( m_ComponentID, DBG_LOCATION, L"DecodeBuffer result: %d\n",result);

	inoutulRawDataLength = (result == WAX_RESULT_OK) ? inoutulRawDataLength : 0;
	return result;
}

WAX_RESULT UWM::GetStatistics(
						 /* IN/OUT */	Statistics *inoutStats				//!< output statistic structure
						 )
{
	// set the size of uncompressed data
	inoutStats->dataVolL0 = m_ullDataLovL0;
	inoutStats->dataVolLZero = m_ullDataLovL0;
	inoutStats->dataVolSinceInit = m_ullDataLovL0;

	// set the size of data after RPR
	inoutStats->dataVolL1 = m_ullDataLovL1;

	// set the size of data after DeDup
	inoutStats->dataVolL2 = m_ullDataLovL2;

	// set the message number
	inoutStats->msgNumber = m_ulMsgNumber;

	// set the region number (Annotation number)
	inoutStats->regNumber = 0;

	// set the bytes in processed regions
	inoutStats->bytesInReg = 0;


	return WAX_RESULT_OK;
}

//! Initializes stats values.
void UWM::StartStatistics( void )
{
	// set the size of uncompressed data
	m_ullDataLovL0 = 0;

	// set the size of data after RPR
	m_ullDataLovL1 = 0;

	// set the size of data after DeDup
	m_ullDataLovL2 = 0;

	// set the message number
	m_ulMsgNumber = 0;
}
