//********************************************************/
//  ./UWM/Tokenizer.cpp
//
//  Owner : Bogdan Hruban
//
//  Neverfail                 ________________
//                PROJECT   : WAN Acceleration
//           Component : Tokenizer
//
//  $Rev: 2480 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: cmaris $
//  $Date: 2009-05-22 14:08:44 +0100 (Fri, 22 May 2009) $
//
//********************************************************/

#include "../Common.h"
#include "WAXTypes.h"
#include "Tokenizer.h"
#include "../StoreManager/StoreManager.h"
#include "../Logger/Logger.h"
#include "../SharedTypes.h"


//! Definition the minimum size that a token header can have
/*
*	NOTE: ( the +1 explanation )
*		Because a token can be of type compressed/uncompressed the minimum size of a Compressed 
*		token is sizeof(TokenCompressed) which is fixed and equal to sizeof(ULONGLONG), but the 
*		minimum size of an UnCompressed token is 1 (1 Byte).
*/
#define _MIN_SIZE_TOKEN_HEADER_ sizeof(WAX_TOKEN_TYPE) + sizeof(ULONG) + 1


WAX_ID Tokenizer::m_ComponentID = (WAX_ID)WAX_ID_TOKENIZER;

Tokenizer::Tokenizer():m_ullDeDupBytesNumber(0),
m_ullDeDupStringsNumber(0),
m_ullRecentBytesNumber(0),
m_ullRecentStringNumber(0)
{
}

//! Overwritten "start" function from WAXControlableBase.
/*!
*	The function is meant to point to the StoreManager that will be used for getting the
*	data from.
*	
*	Return: In case of success return WAX_RESULT_OK. In the case of failure it returns the 
*	failure reason. In case of failure the size of the decoded buffer is set to "0" (zero).
*
*	Caller: WAXInterface::start().
*
\sa StoreManagerBase
*/
WAX_RESULT Tokenizer::start(
		/* IN */	StoreManagerBase* inStoreManager //!< pointer to instance of StoreManagerBase to use
							)
{
	WAX_RESULT result = WAX_RESULT_OK;

	if (inStoreManager != NULL)
		m_storeManager = inStoreManager;
	else
	{
		// try to get the instance directly from StoreManager
		m_storeManager = (StoreManagerBase*)StoreManager::getInstance();
	}

	// check if Tokenizer has started correctly
	if (m_storeManager == NULL)
	{
		result = WAX_RESULT_NOT_STARTED;
		LogError( m_ComponentID, DBG_LOCATION, L"Unable to get instance to StoreManager. (Error: %d)\n",result);
	}
	result = WAXControlableBase::start(false);

	return result;
}

//! Tokenize buffer data
/*!
*	The function is called by UWM::EncodeBuffer() for encoding the data in the internal format
*	of WAX. This function handles the case of both compressed and uncompressed data with individual
*	calls inside to Tokenizer::TokenizeCompressed() and Tokenizer::TokenizeUncompressed().
*
*	The space for the tokenized buffer must be allocated before the function call is being done.
*	The parameter "inoutulTokenizedDataLength" must specify at the function call the size of the
*	allocated space available in the "inoutlpTokenizedData" and at the return of the function it will
*	contain the actual size required by the function to encode the data.
*
*	Return: In case of success return WAX_RESULT_OK. In the case of failure it returns the 
*	failure reason. In case of failure the size of the encoded buffer is set to "0" (zero).
*
*	Caller: UWM::EncodeBuffer().
*
*	\sa CompressedTokenVector
*	\sa UWM::EncodeBuffer()
*/
WAX_RESULT Tokenizer::TokenizeBuffer(
		/* IN */		const unsigned char*	inlpRawData,				//!< the in data buffer 
		/* IN */		unsigned long	inulRawDataLength,					//!< size of in data buffer 
		/* IN */		const CompressedTokenVector inTokenVector,	//!< vector if tokens found by Encoder
		/* IN/OUT */	unsigned char*	inoutlpTokenizedData,				//!< the resulting buffer
		/* IN/OUT */	unsigned long&	inoutulTokenizedDataLength			//!< size of the resulting buffer
		)
{
	WAX_RESULT result = WAX_RESULT_OK;

	//copy tokens 
	//create tokens from the result returned by WAX Encoder
	CompressedTokenVector::const_iterator tokenIt = inTokenVector.begin();
	CompressedTokenVector::const_iterator tokenItEnd = inTokenVector.end();

	// the offset of the previous uncompressed token
	// the tokens must be in ascending order
	// ordered by "offset"
	unsigned long prevOffset = 0;
	unsigned long outMsgSize = 0;
	unsigned long tmpTokenLen = inoutulTokenizedDataLength;

	while ((tokenIt != tokenItEnd) && (result == WAX_RESULT_OK))
	{
		// get the token
		const CompressedToken *tmpCompressedToken = &(*tokenIt);
		LogDebug( m_ComponentID, DBG_LOCATION, L"\n\tOffset : %x\tLength : %x\tTLogOffset : %X",tmpCompressedToken->offset,tmpCompressedToken->length,tmpCompressedToken->tLogOffset);

		// check if there is uncompressed data before the current token
		if (prevOffset < tmpCompressedToken->offset)
		{
			// TODO: check if two consecutive tokens that refer to
			// consecutive data both in memory and TLog should be
			// concatenated into one token

			// the data between prevOffset and tmpCompressedToken->offset represents
			// an uncompressed token

			// build the token directly into inoutlpEncodedData
			result = TokenizeUncompressed(
				inlpRawData+prevOffset,				// the start of the not WAXED buffer
				tmpCompressedToken->offset - prevOffset,	// the length of the buffer
				prevOffset,							// the offset where the data is found
				inoutlpTokenizedData + outMsgSize,	// the offset of the out buffer where to tokenize the data
				tmpTokenLen							// the length of the final tokenized message
				);
			
			outMsgSize += tmpTokenLen;
			tmpTokenLen = inoutulTokenizedDataLength - outMsgSize;
		}

		if (result == WAX_RESULT_OK)
		{
			// place the current CompressedToken into the final buffer
			result = TokenizeCompressed(
				tmpCompressedToken,					// pointer to WAXED token
				inoutlpTokenizedData + outMsgSize,	// the offset of the out buffer where to tokenize the data
				tmpTokenLen							// the length of the final tokenized message
				);

			outMsgSize += tmpTokenLen;

			// set the previous offset to match the last parsed token
			prevOffset = (tmpCompressedToken->offset + tmpCompressedToken->length);
			tmpTokenLen = inoutulTokenizedDataLength - outMsgSize;
		}

		tokenIt++;
	}
	// we have a token at the end of the data
	if ((prevOffset < inulRawDataLength) && (result == WAX_RESULT_OK))
	{
		result = TokenizeUncompressed(
			inlpRawData+prevOffset,				// the start of the not WAXED buffer
			inulRawDataLength - prevOffset,		// the length of the buffer
			prevOffset,							// the offset where the data is found
			inoutlpTokenizedData + outMsgSize,	// the offset of the out buffer where to tokenize the data
			tmpTokenLen							// the length of the final tokenized message
			);

		outMsgSize += tmpTokenLen;
	}

	if (result != WAX_RESULT_OK)
	{
		inoutulTokenizedDataLength = 0;
	}
	else
	{
		inoutulTokenizedDataLength = outMsgSize;
	}

	return result;
}

//! Detokenize buffer data
/*!
*	The function receives a buffer in Tokenized format and it decodes it resulting a buffer
*	in uncompressed format. 
*
*	The function checks for the consistency of the input buffer, checks for space in the output 
*	buffer.
*
*	The "inoutlpDetokenizedDataLength" must specify at the function call the size of the output 
*	buffer.
*
*	At the return of the function this parameter will contain the actual size of the decoded buffer.
*	The space for "inoutlpDetokenizedData" must be pre-allocated before the function call.
*
*	Return:In case of success return WAX_RESULT_OK. In the case of failure it returns the 
*	failure reason.
*
*	Caller: UWM::DecodeBuffer().
*/
WAX_RESULT Tokenizer::DeTokenizeBuffer(
		/* IN */		const unsigned char*	inlpTokenizedData,			//!< the in data buffer in tokenized format
		/* IN */		unsigned long	inulTokenizedDataLength,			//!< size of in data buffer
		/* IN/OUT */	unsigned char*	inoutlpDetokenizedData,				//!< the resulting buffer
		/* IN/OUT */	unsigned long&	inoutulDetokenizedDataLength		//!< size of the resulting buffer
		)
{
	WAX_RESULT result = WAX_RESULT_OK;

	unsigned long currPosIn = 0;
	unsigned long currPosOut = 0;
	
	// search for tokens in the input buffer
	while ((currPosIn < inulTokenizedDataLength) && (result == WAX_RESULT_OK))
	{
		if ((currPosIn + _MIN_SIZE_TOKEN_HEADER_) > inulTokenizedDataLength)
		{
			result = WAX_RESULT_INBUFF_INCONSISTENT;
			LogError( m_ComponentID, DBG_LOCATION, L"Inconsistent buffer formating. (Error: %d)\n",result);
			break;
		}

		// cast the current position in the buffer to a Token
		PToken currToken = (PToken)(inlpTokenizedData+currPosIn);

		// check if we have enough memory for the out buffer
		if (currPosOut + currToken->tokenDataLength > inoutulDetokenizedDataLength )
		{
			result = WAX_RESULT_OUTBUFF_INSUFFICIENT_SIZE;
			LogError( m_ComponentID, DBG_LOCATION, L"Insufficient memory in output buffer. (Error: %d)\n",result);
			break;
		}

		if (currToken->tokenType == TOKEN_TYPE_UNCOMPRESSED)
		{// we have a token of non-compressed type

			// check if we have that amount of memory in the input buffer
			if ((currPosIn + currToken->tokenDataLength) > inulTokenizedDataLength)
			{
				result = WAX_RESULT_INBUFF_INCONSISTENT;
				LogError( m_ComponentID, DBG_LOCATION, L"Inconsistent buffer formating. (Error: %d)\n",result);
				break;
			}

			// copy the actual data
			memcpy(
				(inoutlpDetokenizedData + currPosOut),
				currToken->tokenData.tUncompressed.uncompmressedData,
				currToken->tokenDataLength);
			
			currPosOut += currToken->tokenDataLength;
			currPosIn += currToken->tokenDataLength-sizeof(TokenCompressed);
		}
		else if (currToken->tokenType == TOKEN_TYPE_COMPRESSED)
		{// we have a compressed token

			// get the data from StoreManager
			result = m_storeManager->getBufferByTLogOffset(
				currToken->tokenData.tCompressed.compressedDataTLogOffset,	// TLog offset
				currToken->tokenDataLength,									// Data length
				inoutlpDetokenizedData+currPosOut							// Position in the output buffer where to copy the data	
				);

			currPosOut += currToken->tokenDataLength;

			++m_ullDeDupStringsNumber;
			m_ullDeDupBytesNumber += currToken->tokenDataLength;
		}
		else
		{
			result = WAX_RESULT_UNKNOWN_TOKEN_TYPE;
			LogError( m_ComponentID, DBG_LOCATION, L"Unknown token type. (Error: %d)\n",result);
		}

		// increase the current position in the input buffer to get to next Token
		currPosIn += sizeof(Token);
	}

	if (result == WAX_RESULT_OK)	// no errors occurred in buffer construction
	{
		inoutulDetokenizedDataLength = currPosOut;
	}
	else
	{
		inoutulDetokenizedDataLength = 0;
	}

	return result;
}

//! Tokenize data in compressed format
/*!
*	The function is called by Tokenizer::TokenizeBuffer() for replacing a data found by 
*	the Encoder with a Token. The data that is being replaced is already in TransferLog.
*
*	The "CompressedToken" parameter contains all the information required by the Tokenizer to
*	fully identify the data that is replaced in the TransferLog.
*
*	The space for the tokenized buffer must be allocated before the function call is being done.
*	The parameter "inoutulTokenizedDataLength" must specify at the function call the size of the
*	allocated space available in the "inoutlpTokenizedData" and at the return of the function it will
*	contain the actual size required by the function to encode the data.
*
*	Return: In case of success return WAX_RESULT_OK. In the case of failure it returns the 
*	failure reason. In case of failure the size of the encoded buffer is set to "0" (zero).
*
*	Caller: Tokenizer::TokenizeBuffer().
*
*	\sa CompressedToken
*	\sa Tokenizer::TokenizeBuffer()
*/
WAX_RESULT Tokenizer::TokenizeCompressed(
		/* IN */		const CompressedToken *inCompToken,		//!< compressed token structure
		/* IN/OUT */	unsigned char* inoutlpTokenizedData,			//!< the resulting buffer
		/* IN/OUT */	unsigned long& inoutulTokenizedDataLength		//!< size of the resulting buffer
		)
{
	WAX_RESULT result = WAX_RESULT_OK;
	
	// check is we have enough space for a Token
	if (inoutulTokenizedDataLength < sizeof(Token))
	{
		result = WAX_RESULT_OUTBUFF_INSUFFICIENT_SIZE;
		LogError( m_ComponentID, DBG_LOCATION, L"Insufficient memory in output buffer. (Error: %d)\n",result);
		inoutulTokenizedDataLength = 0;
		return result;
	}

	PToken compToken = (PToken)inoutlpTokenizedData;

	// set the token type
	compToken->tokenType = TOKEN_TYPE_COMPRESSED;

	// set the length of the token data
	compToken->tokenDataLength = inCompToken->length;

	// set the offset in TLog where the data is found
	compToken->tokenData.tCompressed.compressedDataTLogOffset = inCompToken->tLogOffset;

	inoutulTokenizedDataLength = sizeof(Token);

	++m_ullDeDupStringsNumber;
	m_ullDeDupBytesNumber += compToken->tokenDataLength;

	return result;
}

//! Tokenize data in uncompressed format
/*!
*	The function encodes a chunk of data that wasn't deduplicated by the Encoder. This function
*	takes as input the raw data that will be encoded, the length of the encoded data and the offset 
*	where the data required is found.
*
*	The encoded buffer is placed in "inoutlpTokenizedData". The space for this mus be pre-allocated
*	before the function call is made. The size of the allocated output buffer is required as input
*	("inoutulTokenizedDataLength"). At the exit of the function this parameter will contain the actual
*	size of the encoded buffer.
*
*	Return: In case of success return WAX_RESULT_OK. In the case of failure it returns the 
*	failure reason. In case of failure the size of the encoded buffer is set to "0" (zero).
*
*	Caller: Tokenizer::TokenizeBuffer().
*	
*	\sa Tokenizer::TokenizeBuffer()
*/
WAX_RESULT Tokenizer::TokenizeUncompressed(
		/* IN */		const unsigned char*	inlpRawData,			//!< the in data buffer - the buffer that has not been WAXED 
		/* IN */		unsigned long	inulRawDataLength,				//!< size of in data buffer 
		/* IN */		unsigned long	inulUncompDataOffset,			//!< the offset of the not WAXED data
		/* IN/OUT */	unsigned char*	inoutlpTokenizedData,			//!< the resulting buffer
		/* IN/OUT */	unsigned long&	inoutulTokenizedDataLength		//!< size of the resulting buffer
		)
{
	WAX_RESULT result = WAX_RESULT_OK;

	// check is we have enough space for a Token
	if (inoutulTokenizedDataLength < ( sizeof(Token) + inulRawDataLength - sizeof(TokenCompressed)))
	{
		result = WAX_RESULT_OUTBUFF_INSUFFICIENT_SIZE;
		LogError( m_ComponentID, DBG_LOCATION, L"Insufficient memory in output buffer. (Error: %d)\n",result);
		inoutulTokenizedDataLength = 0;
		return result;
	}

	PToken uncompToken = (PToken)inoutlpTokenizedData;

	// set the token type
	uncompToken->tokenType = TOKEN_TYPE_UNCOMPRESSED;

	// set the length of the token data
	uncompToken->tokenDataLength = inulRawDataLength;

	// copy the actual buffer into the out buff
	memcpy(uncompToken->tokenData.tUncompressed.uncompmressedData, inlpRawData, uncompToken->tokenDataLength);

	inoutulTokenizedDataLength = sizeof(Token) + uncompToken->tokenDataLength - sizeof(TokenCompressed);

	return result;
}

WAX_RESULT Tokenizer::GetStatistics(
						 /* IN/OUT */	Statistics *inoutStats						//!< output statistic structure
						 )
{
	inoutStats->dedupStringsNumber = m_ullDeDupStringsNumber;
	inoutStats->dedupBytesNumber = m_ullDeDupBytesNumber;

	return WAX_RESULT_OK;
}

//! Initializes stats values.
void Tokenizer::StartStatistics( void )
{
	m_ullDeDupStringsNumber = 0;
	m_ullDeDupBytesNumber   = 0;
}
