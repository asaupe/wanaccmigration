//********************************************************/
//  ./UWM/UWM.h
//
//  Owner : Bogdan Hruban
// 
//  Neverfail                   ________________
//					PROJECT   : WAN Acceleration 
//					Component : Unit of Work Manager (UWM)
//
//  $Rev: 2480 $ - Revision of last commit
//
/********************************************************
//
// Last Change - Author and Date of last change:
// $Author: cmaris $  
// $Date: 2009-05-22 14:08:44 +0100 (Fri, 22 May 2009) $
//
//********************************************************/

//******************************************************************/
//
//	The module handles the work with input buffers either for encoding or for decoding. 
//
//	It is used by WAXInterface::encode() in case an encoding procedure is	required and by 
//	WAXInterface::decode() for decoding.
//
//	UWM will manage all the processing done on an input buffer. 
//
//	Encode Buffer Flow 
//	- if we are not processing in OPAQUE mode, handle the dirty data
//	- if RPR processing is required send the buffer to RPR
//	- if DeDuplication processing is required send the buffer to WAXEncoder
//	- build the output buffer
//
//	Decode Buffer Flow
//	- determine the processing done on the input buffer
//	- if DeDuplication is enabled send the buffer to Tokenizer (DeTokenize)
//	- if RPR is enabled send the buffer to RPR (DecodeBuffer)
//	- check if the decoded buffer is correct
//******************************************************************/

#pragma once
#include "WAXTypes.h"
#include "../waxsingletontemplate.h"
#include "../WAXControlableBase.h"
#include "Tokenizer.h"
#include "../RPR/RPR.h"
#include "../Encoder/Encoder.h"
#include "../StoreManager/StoreManager.h"
#include "../Stats/IStats.h"


//! Unit of Work Manager
/*!
*	\class UWM
*	The UWM class will be responsible with the encoding and the decoding of the data. It will
*	encode the input buffers into an internal format making use also of the Tokenizer. 
*	
*	Message structure:
*	- UWM header
*		- original message size	[ULONG]
*		- CRC of the original message	[ULONGLONG]
*	- tokenized data [LPBYTE] (See Tokenizer for more information)
*
*	Caller: WAXInterface
*
*	\sa Tokenizer
*	\sa WAXInterface
*	\sa RPR
*	\sa WAXEncoder
*/
class UWM : public WaxSingletonTemplate<UWM>, 
			private WAXControlableBase,
			public IStats
{
	friend class WaxSingletonTemplate<UWM> ;
public:
	~UWM(void);

	//! WAXControlableBase - initialize override.
	WAX_RESULT initialize(
		/* IN */ StoreManager* inStoreManager = NULL //!< pointer to instance of StoreManagerBase to use
		);

	//! Encode an input buffer using deduplication algorithm and/or repetitive data removal.
	WAX_RESULT EncodeBuffer(
		/* IN */		/*const*/ unsigned char*	inlpRawData,				//!< input buffer
		/* IN */		unsigned long			inulRawDataLength,			//!< length of the input buffer
		/* IN/OUT */	unsigned char*			inoutlpEncodedData,			//!< output buffer
		/* IN/OUT */	unsigned long&			inoutulEncodedDataLength,	//!< length of the output buffer
		/* IN */		bool			inIsRprEnabled,				//!< is RPR processing enabled
		/* IN */		bool			inIsDeDupEnabled			//!< is WAX processing enabled
		);

	//! Decode the input buffer, returning the original buffer that was sent to the encoder.
	WAX_RESULT DecodeBuffer(
	/*__in*/ const unsigned char*	inlpEncodedData,			//!< input buffer
	/*__in*/ unsigned long			inulEncodedDataLength,		//!< input buffer length
	/*__in*/ unsigned char*			inoutlpRawData,				//!< output buffer
	/*__in*/ unsigned long&			inoutulRawDataLength,		//!< output buffer size
	/*__in*/ bool			isRPPProcessing,
	/*__in*/ bool			isWaxProcessing 
		);

	WAX_RESULT GetStatistics(
		/* IN/OUT */	Statistics *inoutStats				//!< output statistic structure
		);
	
	//! Initializes stats values.
	void StartStatistics( void );

private:
	UWM();

	static WAX_ID m_ComponentID;		//!< component ID

	WAXEncoder* m_Encoder;				//!< Encoder instance
	Tokenizer* m_Tokenizer;				//!< Tokenizer instance
	RPR* m_RPR;							//!< RPR instance
	StoreManager* m_StoreManager;		//!< StoreManager instance

	//! ByteBuffer local variable used for performing writes to the StoreManager after a decode
	ByteBuffer m_bBufferForStoreManagerWrites;

	unsigned char* m_lpTempBuffer;				//!< Temporary buffer used internally

	uint64_t m_ullDataLovL0;			//!< Volume of uncompressed data
	uint64_t m_ullDataLovL1;			//!< Volume of data after RPR
	uint64_t m_ullDataLovL2;			//!< Volume of data after DeDup

	unsigned long m_ulMsgNumber;				//!< Number of messages
};
