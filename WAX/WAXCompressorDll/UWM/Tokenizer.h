//********************************************************/
//  ./UWM/Tokenizer.h
//
//  Owner : Bogdan Hruban
//
//  Neverfail                 ________________
//                PROJECT   : WAN Acceleration
//           Component : Tokenizer
//
//  $Rev: 2480 $ - Revision of last commit
//
/********************************************************
//
//  Last Change - author and date of last change:
//  $Author: cmaris $
//  $Date: 2009-05-22 14:08:44 +0100 (Fri, 22 May 2009) $
//
//********************************************************/

//******************************************************************/
//	This module encodes and decodes the processed data using the following
//	encoding format:
//	
//	TokenFormat:
//		- token type [BYTE] (Compressed/UnCompressed)
//		- token data length [ULONG] (the length of the data represented by a token)
//		- token data
//			- offset in TransferLog [ULONGLONG] (if it is a Compressed token)
//			- data buffer [LPBYTE] (if it is an UnCompressed token)
//
//******************************************************************/

#pragma once

#include "WAXTypes.h"
#include "../waxsingletontemplate.h"
#include "../StoreManager/StoreManagerBase.h"
#include "../WAXControlableBase.h"
#include "../Stats/IStats.h"

//! Tokenize - tokenize/de-tokenize the data
/*! \class Tokenizer
*
*	The Tokenizer class is responsible with the encoding of tokens received from WAX Encoder. 
*	
*	It is called by UWM 
*		- with the function call TokenizeBuffer() - for compressing the data.
*		- with function call DeTokenizeBuffer() for decompression.
*
*/
class Tokenizer : public WaxSingletonTemplate<Tokenizer>,
	private WAXControlableBase,
	public IStats
{
	friend class WaxSingletonTemplate<Tokenizer>;
public:

	//! Tokenize buffer data
	WAX_RESULT TokenizeBuffer(
		/* IN */		const unsigned char*	inlpRawData,				//!< the in data buffer 
		/* IN */		unsigned long	inulRawDataLength,					//!< size of in data buffer 
		/* IN */		const CompressedTokenVector inTokenVector,	//!< vector if tokens found by Encoder
		/* IN/OUT */	unsigned char*	inoutlpTokenizedData,				//!< the resulting buffer
		/* IN/OUT */	unsigned long&	inoutulTokenizedDataLength			//!< size of the resulting buffer
		);

	//! Detokenize buffer data
	WAX_RESULT DeTokenizeBuffer(
		/* IN */		const unsigned char*	inlpTokenizedData,			//!< the in data buffer in tokenized format
		/* IN */		unsigned long	inulTokenizedDataLength,			//!< size of in data buffer
		/* IN/OUT */	unsigned char*	inoutlpDetokenizedData,				//!< the resulting buffer
		/* IN/OUT */	unsigned long&	inoutulDetokenizedDataLength		//!< size of the resulting buffer
		);

	//! Overwritten "start" function from WAXControlableBase
	WAX_RESULT start(
		/* IN */	StoreManagerBase* inStoreManager = NULL			//!< pointer to instance of StoreManagerBase to use
		);

	WAX_RESULT GetStatistics(
		/* IN/OUT */	Statistics *inoutStats						//!< output statistic structure
		);

	//! Initializes stats values.
	void StartStatistics( void );

private:
	Tokenizer();

	//! Tokenize data in compressed format
	WAX_RESULT TokenizeCompressed(
		/* IN */		const CompressedToken *inCompToken,			//!< compressed token structure
		/* IN/OUT */	unsigned char* inoutlpTokenizedData,				//!< the resulting buffer
		/* IN/OUT */	unsigned long& inoutulTokenizedDataLength			//!< size of the resulting buffer
		);

	//! Tokenize data in uncompressed format
	WAX_RESULT TokenizeUncompressed(
		/* IN */		const unsigned char*	inlpRawData,				//!< the in data buffer - the buffer that has not been WAXED 
		/* IN */		unsigned long	inulRawDataLength,					//!< size of in data buffer 
		/* IN */		unsigned long	inulUncompDataOffset,				//!< the offset of the not WAXED data
		/* IN/OUT */	unsigned char*	inoutlpTokenizedData,				//!< the resulting buffer
		/* IN/OUT */	unsigned long&	inoutulTokenizedDataLength			//!< size of the resulting buffer
		); 
private:
	static WAX_ID m_ComponentID;			//!< the component ID
	StoreManagerBase* m_storeManager;		//!< instance of Store Manager Base
	
	uint64_t m_ullDeDupStringsNumber;		//!< number of De-Dup strings
	uint64_t m_ullDeDupBytesNumber;		//!< bytes in De-Dup strings
	uint64_t m_ullRecentStringNumber;		//!< number of recent De-Dup strings
	uint64_t m_ullRecentBytesNumber;		//!< bytes in recent De-Dup strings
};
