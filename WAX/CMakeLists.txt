cmake_minimum_required (VERSION 2.8)
project (WANAcc)

if( MSVC )
	set( ZLIB_INCLUDE_DIR ${PROJECT_SOURCE_DIR}/ZLib/include )
	set( ZLIB_LIBRARIES ${PROJECT_SOURCE_DIR}/ZLib/lib/x64/zlibwapi.lib )
    include_directories( ${ZLIB_INCLUDE_DIRS} )
    #target_link_libraries( WaxTest ${ZLIB_LIBRARIES} )
else()
	find_package( ZLIB REQUIRED )
	if ( ZLIB_FOUND )
	    include_directories( ${ZLIB_INCLUDE_DIRS} )
	    #target_link_libraries( WaxTest ${ZLIB_LIBRARIES} )
	endif( ZLIB_FOUND )
endif( MSVC )

MESSAGE(STATUS "Cmake FindZLIB: using ZLIB includes at: ${ZLIB_INCLUDE_DIR}")
MESSAGE(STATUS "Cmake FindZLIB: using ZLIB libraries: ${ZLIB_LIBRARIES}")

SET(BUILD_SHARED_LIBS ON)

add_definitions( -DWIN64 -D_WIN64 -D_WINDOWS -D_USRDLL -DUNICODE )

include_directories ("${PROJECT_SOURCE_DIR}/WAXCompressorDll")
add_subdirectory (WAXCompressorDll)

include_directories ("${PROJECT_SOURCE_DIR}/NFWaxHB")
add_subdirectory (NFWaxHB)

include_directories ("${PROJECT_SOURCE_DIR}/WAXTestSVC")
add_subdirectory (WAXTestSVC)

